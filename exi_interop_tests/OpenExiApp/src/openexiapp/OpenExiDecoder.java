package openexiapp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;

import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;

import org.openexi.proc.common.AlignmentType;
import org.openexi.proc.common.EXIOptionsException;
import org.openexi.proc.common.GrammarOptions;
import org.openexi.proc.grammars.GrammarCache;
import org.openexi.sax.EXIReader;
import org.openexi.schema.EXISchema;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;



public class OpenExiDecoder {

	public static void decodeEXI(String sourceFile, String destinationFile) throws 
	FileNotFoundException, IOException, SAXException,
	EXIOptionsException, TransformerConfigurationException {

		FileInputStream in = null;
		FileWriter out = null;
		StringWriter stringWriter = new StringWriter();

		// The Grammar Cache stores schema and EXI options information. The settings nust match when encoding
		// and subsequently decoding a data set.
		GrammarCache grammarCache;

		// All EXI options can expressed in a single short integer. DEFAULT_OPTIONS=2;
		short options = GrammarOptions.DEFAULT_OPTIONS;

		try {
			System.out.println("Conversion process started");

			// Standard SAX methods parse content and lexical values.
			SAXTransformerFactory saxTransformerFactory = (SAXTransformerFactory)SAXTransformerFactory.newInstance();
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			saxParserFactory.setNamespaceAware(true);
			TransformerHandler transformerHandler = saxTransformerFactory.newTransformerHandler();

			// EXIReader infers and reconstructs the XML file structure.
			EXIReader reader = new EXIReader();

			reader.setAlignmentType(AlignmentType.bitPacked);

			// Set the number of elements processed as a block.
			reader.setBlockSize(1000000);

			File inputFile = new File(sourceFile);

			// Create a schema and set it to null. If useSchema == "None" it remains null.
			EXISchema schema = null;

			in = new FileInputStream(inputFile);
			out = new FileWriter(destinationFile);


			grammarCache = new GrammarCache(schema, options);

			// Use the Grammar Cache to set the schema and grammar options for EXIReader.
			reader.setEXISchema(grammarCache);

			// Let the reader know whether this is an XML fragment rather than a well-formed document. 
			//reader.setFragment(fragment);

			// Prepare to send the results from the transformer to a StringWriter object.
			transformerHandler.setResult(new StreamResult(stringWriter));

			// Assign the transformer handler to interpret XML content.
			reader.setContentHandler(transformerHandler);

			// Parse the file information.
			reader.parse(new InputSource(in));

			// Get the resulting string, write it to the output file, and flush the buffer contents.
			final String reconstitutedString;
			reconstitutedString = stringWriter.getBuffer().toString();
			out.write(reconstitutedString);
			out.flush();

			in.close();
			out.close();

		}
		// Verify that the input and output files are closed.
		finally {
			if (in != null)
				in.close();
			if (out != null)
				out.close();
		}

	}

	public static void main(String[] args) {

		String exiLocation = args[0];
		try {
			decodeEXI(exiLocation, exiLocation + ".xml");
			System.out.println("Successfully converted exi to xml");
		} catch (TransformerConfigurationException | IOException | SAXException
				| EXIOptionsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
