package eXificientApp;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;

import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import com.siemens.ct.exi.EXIFactory;
import com.siemens.ct.exi.GrammarFactory;
import com.siemens.ct.exi.api.sax.EXIResult;
import com.siemens.ct.exi.api.sax.EXISource;
import com.siemens.ct.exi.grammars.Grammars;
import com.siemens.ct.exi.helpers.DefaultEXIFactory;


public final class ExificientDemo {

	static final String OUTPUT_FOLDER = "../exificient_output/";
	static final String EXI_EXTENSION = "_exi";
	static final String EXI_SCHEMA_EXTENSION = "_exi_schema";
	static final String XML_EXTENSION = ".xml";

	// XML
	String exiLocation;
	String exiName;
	// XML Schema
	String xsdLocation;

	private void parseAndProofFileLocations(String[] args) throws Exception {
		if (args.length >= 1) {
			// xml
			exiLocation = args[0];
			File exiFile = new File(exiLocation);
			exiName = exiFile.toString();

			if (exiFile.exists()) {
				// output path
				File outputDir = new File(OUTPUT_FOLDER);
				outputDir.mkdirs();
				return;
			}
		}

		//throw new IllegalArgumentException("Input files not valid!");
	}

	private String getEXILocation(boolean schemaLess) {
		if (schemaLess) {
			return exiName;
		} else {
			return exiName + EXI_SCHEMA_EXTENSION;
		}
	}

	protected void decodeSchemaLess() throws Exception {
		String exiLocation = exiName;		

		// decode
		SAXSource exiSource = new EXISource();
		XMLReader exiReader = exiSource.getXMLReader();
		decode(exiReader, exiLocation);
	}


	protected void decode(XMLReader exiReader, String exiLocation)
			throws SAXException, IOException, TransformerException {

		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();

		InputStream exiIS = new FileInputStream(exiLocation);
		SAXSource exiSource = new SAXSource(new InputSource(exiIS));
		exiSource.setXMLReader(exiReader);

		OutputStream os = new FileOutputStream(exiLocation + XML_EXTENSION);
		transformer.transform(exiSource, new StreamResult(os));
		os.close();
	}

	public static void main(String[] args) throws Exception {
		/*
		 * Note: we are using default coding options and SAX
		 */
		if (args.length > 0) {

			ExificientDemo sample = new ExificientDemo();
			sample.parseAndProofFileLocations(args);

			// schema-less

			sample.decodeSchemaLess();
			System.out.print(". ");
			System.out.println();
			System.out.println("# SchemaLess ");
			System.out.println("\t" + " --> " + sample.getEXILocation(true));
			System.out.println("\t" + " <-- " + sample.getEXILocation(true)
					+ XML_EXTENSION);


		} else {
			System.out.println("# EXIficient Sample, no input files specified");
			System.out.println("Usage: " + ExificientDemo.class.getName()
					+ " exiFile");
		}
	}

}
