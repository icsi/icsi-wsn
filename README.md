#ICSI WSN
------------------------------------------
##Repository Structure

- **apps**: contains the ICSI applications both for demo and deployment. The main icsi application is inside **icsi-net** folder
- **contiki**: NoES fork of Contiki, containing T-Res and EXi/M2M libraries
    - refer to the README in apps/exi directory for instructions on how to set up the exi application
- **tools**
    - californium: a third-party library implementing a CoAP/HTTP proxy, extended to support exi and uri expansion
    - java-http-server: a java web server simply logging every received packet
    - libs: Java libs for EXI
    - gw-config: Ansible scripts to setup the ICSI M2M Gateway
    - pyot-icsi: WSN external configuration manager w/ REST APIs
    - vagrant-centos7: Vagrant VM to emulate the Gateway
- **tests**: each subdirectory contains a unit test. When possible the tests can be run automatically. Differently a README for each test should provide instructions for the execution of the test.


## Software requirements

The following configuration applies to Ubuntu (12.04/14.04). The corresponding configuration for Centos7 64b is defined in *tools/gw-config* and can be used to setup the vagrant VM in tools/vagrant-centos7

####Contiki & Cooja & Californium

- openjdk-7-jdk 
- gcc-msp430
- ant
- maven


In order to compile T-Res scripts python2.6 is required
```sh
sudo add-apt-repository -y ppa:fkrull/deadsnakes
sudo apt-get update
sudo apt-get install -y python2.6 python2.6-dev
```

####PyoT
Follow README Instructions in *tools/pyot-icsi* folder

####VM Provisioning
*Vagrant* is required to setup a Centos-based VM emulating the Gateway
```sh
sudo apt-get install vagrant
```

####Configuration Manager
Ansible is used to configure the gateway (real or emulated) using the scripts contained in tools/icsi\_requirements tools/wsn\_config
```sh
sudo pip install ansible
```

## **ICSI Middleware Demo application**, How-To

The following how-to describes how to setup a local testing environment on a Ubuntu workstation after cloning the repository.

To run Cooja-based simulations add this line to *~/.profile*
```sh
export JAVA_HOME=$(readlink -f /usr/bin/javac | sed "s:/bin/javac::")
```

After cloning, enter *icsi-wsn* directory and run
```sh
git submodule update --init
cd contiki-tres/tools/cooja
ant jar
cd contiki-tres/tools
make tunslip6
```

In order to run the **ICSI WSN Middleware** application it is necessary to:

1. Start the **OM2M GSCL**
2. Create the necessary application/container resources on the GSCL
3. Start the Proxy
4. Start the Cooja simulation
5. Start *Tunslip* application

####Start the GSCL
Download and extract [OM2M] 0.8.0

[OM2M]:http://rm.mirror.garr.it/mirrors/eclipse/om2m/releases/0.8.0/OM2M-0.8.0.zip

Copy */tools/gw-config/conf/m2mconfiggscl.ini*  to  *OM2M-0.8.0/GSCL/configuration/config.ini*

Start the GSCL by executing *start.sh* in the corresponding directory.

####Create the application/container resources on the GSCL

Run *m2mstartup.py* application in *tools/m2m-config*

```sh
python m2mstartup.py -a 127.0.0.1 -p 8084
```

####Start the proxy
Compile and install the proxy running mvn install in *tools/californium* folder
Run *./startProxy.sh* in *tools* directory. The program will loop until the IPv6 address is available, then it will start the Californium Proxy.

####Start the Cooja Simulation
```sh
cd apps
make TARGET=cooja icsi-net.csc
```

####Start tunslip
```sh
sudo ./looptunslip
```

####How it works
The simulation includes three ICSI sensor nodes and a border-router. The sensor nodes run the ICSI Middleware. Node #3 performs in-network aggregation of raw data coming from two sources (the first source is local, the second one is hosted on node #4). When the aggregation is complete the output is encoded in a M2M message, compressed using EXI and finally sent to the GSCL passing through the Proxy. You can see the flow of information on the output of the terminals. The proxy console will display the XML decompressed output.

## Automated testing
The ICSI WSN repository includes some automated testing tools:

- WSN Middleware: In *tests* directory run *make tests*. The tests validate the main functionalities of the middleware (CoAP Communication, Base64 encoding, T-Res for the resource processing engine, M2M message generation and parsing using EXI compression)
- WSN Manager (PyoT): in *tools/pyot-icsi* run *./c_make_tests.sh*
- Californium Proxy: The tests are automatically executed during the compilation phase

**Note**: tests may occasionally fail due to well-known bugs in Cooja simulator. In case of failure please restart the test. 

