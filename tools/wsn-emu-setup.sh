#! /bin/sh
ip tuntap add tun1 mode tun
ifconfig tun1 inet6 add aaaa::2
ifconfig tun1 up
ip -6 route add aaaa::/64 dev tun1
