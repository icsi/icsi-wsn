##Ansible Scripts for Gw Configuration

Host machine: the workstation from where the installation scripts are launched.
Target machine(s): the GHUs where to install the wsn software (CENTOS 7). 

NOTE: root access is required on each target machine. 

####Content
A set of Ansible scripts to automatically configure the ICSI Gateway(s). 
The *.sh* scripts are a shortcut to call the *.yml*. 

1. Wsn: Installs PyoT and its supervisor-managed processes
2. Proxy: Copies and builds the HTTP/CoAP Proxy

####Installation Procedure
In order to install the ICSI WSN software on the target machine:

1. Edit the *hosts* file adding the target machine IP address to the [gw] section, one line for each machine. 
2. Install Ansible on the host machine (sudo pip install ansible==1.9.1)
3. Copy the local ssh key to the target machines to obtain passwordless access. (e.g., ssh-copy-id root@{ip-of-the-gateway})
4. Run the setupWsn.sh script to start the installation process of WSN-related sw.
5. Run the setupProxy.sh script to start the installation process of the proxy.

