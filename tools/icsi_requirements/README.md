##Ansible Scripts for Gw Configuration

Host machine: the workstation from where the installation scripts are launched.
Target machine(s): the GHUs where to install the requirements (CENTOS 7). 

NOTE: root access is required on each target machine. 

####Content
A script to automatically configure the ICSI Gateway(s) with the necessary requirements  (user creation, package installation, supervisor configuration etc.). 
The *.sh* script is a shortcut to call the *.yml*. 
The *hosts* file defines the ip address of the machine where to install the ICSI requirements.

####Installation Procedure
In order to install the ICSI requirements on the target machine:

1. Edit the *hosts* file adding the target machine IP address to the [gw] section, one line for each machine. 
2. Install Ansible on the host machine (sudo pip install ansible==1.9.1)
3. Copy the local ssh key to the target machines to obtain passwordless access. (e.g., ssh-copy-id root@{ip-of-the-gateway})
4. Run the installRequirements.sh script to start the installation process.



