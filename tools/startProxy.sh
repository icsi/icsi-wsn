#!/bin/bash

ADDRESS=aaaa::2

function finish {
  jps | grep ExampleCrossProxy | cut -d ' ' -f 1 | xargs -rn1 kill
}

trap finish EXIT

# Tests an IP Address (IPv4) or hostname to see if it is connected. 
# Exit code 0 means online. 
# Exit code 2 means offline or host is invalid. 
function is_address_alive() {
  ping6 -c 1 $1 > /dev/null 2>&1 
}

cd californium/run/

while : 
do
  is_address_alive $ADDRESS
  if [ $? -eq 2 ]; 
  then   
    sleep 1 
    echo "Californium Proxy: $ADDRESS not available"
  else 
    sleep 3
    java -cp ../../libs/OpenEXI/xercesImpl.jar:../../libs/OpenEXI/nagasena.jar:../../libs/OpenEXI/xml-apis.jar:cf-proxy-1.0.0-SNAPSHOT.jar org.eclipse.californium.examples.ExampleCrossProxy
    exit
  fi
done
