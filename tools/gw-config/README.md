##Ansible Scripts for Gw Configuration

####Prerequisites: Install *Ansible*. 
	sudo pip install ansible

####Content
A set of Ansible scripts to automatically configure the ICSI Gateway(s). 
The *.sh* scripts are a shortcut to call the *.yml*. 

1. Requirements: takes care of global system requirements (user creation, package installation, supervisor configuration etc.)
2. Wsn: Installs PyoT and its supervisor-managed processes
3. Proxy: Copies and builds the HTTP/CoAP Proxy

#### Passwordless access is required to run Ansible scripts. 
	ssh-copy-id root@192.168.33.10
