##Centos 7 Vagrant VM: Installation and startup

####Prerequisites: Install *Vagrant*. 
	sudo apt-get install vagrant

####Download the box image
	vagrant box add centos7 https://github.com/holms/vagrant-centos7-box/releases/download/7.1.1503.001/CentOS-7.1.1503-x86_64-netboot.box

#### Init the vm
	vagrant init centos7

#### Start the vm
	vagrant up

#### VM Configuration
Edit Vagrantfile to setup private network, bridged network, port forwarding etc.

####copy the ssh key to allow passwordless access
	ssh-copy-id root@192.168.33.10
