#!/usr/local/bin/python2.7
# encoding: utf-8
'''
 -- shortdesc

 is a description

It defines classes_and_methods

@author:     Andrea Azzara'

@copyright:  2015 CNIT/SSSUP. All rights reserved.

@license:    license

@contact:    a.azzara@sssup.it
@deffield    updated: Updated
'''

import sys
import time
import logging
import requests
import signal
import random
import base64

from m2mstartup import AUTHORIZATION
from m2mstartup import SERVER_IP, SERVER_PORT, m2mConnection, check_m2m_server, M2M_CONFIG_FILE
from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
from xml.etree.ElementTree import ElementTree

__all__ = []
__version__ = 0.1
__date__ = '2015-05-27'
__updated__ = '2015-05-27'


ip_address_vector = ['aaaa::202:2:2:2',
                     'aaaa::203:3:3:3',
                     'aaaa::204:4:4:4',
                     'aaaa::205:5:5:5',
                     'aaaa::206:6:6:6',
                     'aaaa::207:7:7:7']

event_types = {'ABN': '1',
               'PARK': '2',
               'VEHICLE': '3'}

slot_ids = range(20)
arch_ids = range(5)
slot_status = [0, 1]
time_windows = [10, 20, 30]
container_list = ['p1', 'p2', 'p3']

CONTENT = """ <?xml version="1.0" encoding="UTF-8"?>
<p0:contentInstance xmlns:p0="http://uri.etsi.org/m2m" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<p0:content>%s</p0:content>
</p0:contentInstance> """


class TimeoutException(Exception):
    pass


def createContentInstance(conn, appname, cname):
    container_uri = conn.container_uri % appname + '/' + cname + '/contentInstances'
    print container_uri

    ev = random.choice(event_types.keys())
    print ev
    if ev == 'ABN':
        ci = (event_types['ABN']
              + ';' + random.choice(ip_address_vector)
              + ';' + "9"
              + ';' + "superjam")
    elif ev == 'PARK':
        ci = (event_types['PARK']
              + ';' + random.choice(ip_address_vector)
              + ';' + str(random.choice(slot_ids))
              + ';' + str(random.choice(slot_status)))
    elif ev == 'VEHICLE':
        ci = (event_types['VEHICLE'] +
              ';' + random.choice(ip_address_vector) +
              ';' + str(random.choice(arch_ids)) +
              ';' + str(random.randrange(0, 100)) +  # avg speed
              ';' + str(random.randrange(0, 100)) +  # num of vehicles
              ';' + str(random.choice(time_windows)))

    container_create_content = ci  # CONTENT % ci  # base64.b64encode(ci)
    r = requests.post(url=container_uri,
                      data=container_create_content,
                      headers=AUTHORIZATION)
    print ci
    print r.reason
    if r.reason == 'Created':
        return True
    else:
        return False


def timeoutHandler(signum, frame):
    raise TimeoutException("end of time")


def main(argv=None):  # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    # program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version,
                                                     program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by Andrea Azzara' on %s.
  Copyright 2015 CNIT/SSSUP. All rights reserved.

  Licensed under the Apache License 2.0
  http://www.apache.org/licenses/LICENSE-2.0

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license,
                                formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument("-r", "--rate",
                            dest="rate",
                            action="store",
                            help="rate of message sending (messages/s) [default: %(default)s]",
                            default=1, type=int)
        parser.add_argument("-t", "--timeout",
                            dest="timeout",
                            action="store",
                            help="timeout of message sending (s) [default: %(default)s]",
                            default=10, type=int)
        parser.add_argument("-a", "--address",
                            dest="address",
                            action="store",
                            help="ip address of the GSCL [default: %(default)s]",
                            default=SERVER_IP, type=str)
        parser.add_argument("-p", "--port",
                            dest="port",
                            action="store",
                            help="port of the GSCL [default: %(default)s]",
                            default=SERVER_PORT, type=str)
        parser.add_argument("-v", "--verbose",
                            dest="verbose",
                            action="count", help="set verbosity level")
        parser.add_argument('-V', '--version',
                            action='version', version=program_version_message)
        print "parsing"
        # Process arguments
        args = parser.parse_args()

        verbose = args.verbose
        rate = args.rate
        timeout = args.timeout
        address = args.address
        port = args.port

        if verbose > 0:
            print("Verbose mode on")
        print rate


        conn = m2mConnection(address=address, port=port)
        print conn
        while not check_m2m_server(conn):
            print 'Connection not available'

        period = 1.0 / float(rate)
        print period


        # Parse XML config file and create applications and containers
        tree = ElementTree()
        tree.parse(M2M_CONFIG_FILE)
        root = tree.getroot()
        apps = []
        for wsn in root.iter('wsn'):
            cont = []
            for container in wsn.iter('container'):
                cont.append(container.text)
            apps.append([wsn.attrib['name'], cont])
        print apps

        signal.signal(signal.SIGALRM, timeoutHandler)
        signal.alarm(timeout)
        count = 0
        while True:
            app = random.choice(apps)
            print app[0] , random.choice(app[1])
            res = createContentInstance(conn, app[0] , random.choice(app[1]))
            if res:
                count += 1
            time.sleep(period)

        return 0
    except KeyboardInterrupt:
        return 0
    except TimeoutException:
        print 'Done, count = ', count
        return 0
    except Exception, e:
        logging.exception(e)
        return 2


if __name__ == "__main__":
    sys.exit(main())
