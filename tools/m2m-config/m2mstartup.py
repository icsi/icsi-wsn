#!/usr/bin/env python
# encoding: utf-8
'''
m2mstartup -- shortdesc

m2mstartup is a description

It defines classes_and_methods

@author:     Andrea Azzara

@copyright:  2015 CNIT/SSSUP. All rights reserved.

@license:    license

@contact:    a.azzara@sssup.it
@deffield    updated: Updated
'''

import sys
import os
import requests
import urllib
import base64
import logging
from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
from xml.etree.ElementTree import ElementTree, dump, tostring
import time

__all__ = []
__version__ = 0.1
__date__ = '2015-05-18'
__updated__ = '2015-05-18'

SERVER_IP = '192.168.33.10'
SERVER_PORT = '8084'
M2M_CONFIG_FILE = '../wsn_config_m2m2.xml'


# SERVER_BASE_URI = "http://" + SERVER_IP + ":" + SERVER_PORT
# M2M_BASE_URI = SERVER_BASE_URI + "/m2m"
# APP_BASE_URI = M2M_BASE_URI + "/applications"
# CONTAINER_URI = APP_BASE_URI + "/%s/containers"


class m2mConnection(object):
    """
    M2M Connection parameters
    """
    def __init__(self, address=SERVER_IP, port=SERVER_PORT):
        self.address = address
        self.port = port
        self.server_base_uri = "http://" + address + ":" + port
        self.m2m_base_uri = self.server_base_uri + "/m2m"
        self.app_base_uri = self.m2m_base_uri + "/applications"
        self.container_uri = self.app_base_uri + "/%s/containers"

    def __str__(self):
        return str(self.app_base_uri)


APP_CREATE_CONTENT = """<om2m:application xmlns:om2m="http://uri.etsi.org/m2m" appId="%s">
   <om2m:searchStrings>
      <om2m:searchString>Type/parking</om2m:searchString>
   </om2m:searchStrings>
</om2m:application>"""

CONTAINER_CONTENT = """<om2m:container xmlns:om2m="http://uri.etsi.org/m2m" om2m:id="%s">
</om2m:container>"""

AUTHORIZATION = {'Authorization': 'Basic ' + base64.b64encode('admin:admin')}
# YWRtaW46YWRtaW4=


def check_m2m_server(conn):
    try:
        r = requests.get(url=conn.app_base_uri,
                         headers=AUTHORIZATION)
        if r.reason != 'OK':
            return False
        return True
    except Exception, e:
        logging.exception(e)
        return False

def create_app(conn, appname):
    print "Creating application %s on %s" % (appname, conn.app_base_uri)
    app_create_content = APP_CREATE_CONTENT % appname
    r = requests.post(url=conn.app_base_uri,
                      data=app_create_content,
                      headers=AUTHORIZATION)
    print r.reason
    if r.reason == 'Created':
        return True
    else:
        return False


def container_exists(conn, appname, cname):
    container_uri = conn.container_uri % appname + '/' + cname
    r = requests.get(url=container_uri, headers=AUTHORIZATION)
    if "STATUS_NOT_FOUND" in r.text:
        return False
    return True


def create_container(conn, appname, cname):
    container_uri = conn.container_uri % appname
    print "Creating container %s on app %s, URI %s" % (appname, cname,
                                                       container_uri)
    container_create_content = CONTAINER_CONTENT % cname
    r = requests.post(url=container_uri,
                      data=container_create_content,
                      headers=AUTHORIZATION)
    print r.reason
    if r.reason == 'Created':
        return True
    else:
        return False


def main(argv=None):
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version,
                                                     program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by Andrea Azzara' on %s.
  Copyright 2015 CNIT/SSSUP. All rights reserved.

  Licensed under the Apache License 2.0
  http://www.apache.org/licenses/LICENSE-2.0

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))
    try:
        # MAIN BODY #
        # Setup argument parser
        parser = ArgumentParser(description=program_license,
                                formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument("-a", "--address",
                            dest="address",
                            action="store",
                            help="ip address of the GSCL [default: %(default)s]",
                            default=SERVER_IP, type=str)
        parser.add_argument("-p", "--port",
                            dest="port",
                            action="store",
                            help="port of the GSCL [default: %(default)s]",
                            default=SERVER_PORT, type=str)
        parser.add_argument("-v", "--verbose",
                            dest="verbose",
                            action="count", help="set verbosity level")
        parser.add_argument('-V', '--version',
                            action='version', version=program_version_message)
        # Process arguments
        args = parser.parse_args()

        verbose = args.verbose
        address = args.address
        port = args.port

        conn = m2mConnection(address=address, port=port)
        print conn
        if verbose > 0:
            print("Verbose mode on")

        while not check_m2m_server(conn):
            print 'Connection not available'
            time.sleep(1)

        # Parse XML config file and create applications and containers
        tree = ElementTree()
        tree.parse(M2M_CONFIG_FILE)
        root = tree.getroot()
        for wsn in root.iter('wsn'):
             create_app(conn, wsn.attrib['name'])
             for container in wsn.iter('container'):
                 if not container_exists(conn, wsn.attrib['name'], container.text):
                     create_container(conn, wsn.attrib['name'], container.text)

        # Update the configuration file on the GSCL
        create_app(conn, "ICSIWSNConfig")
        if not container_exists(conn, "ICSIWSNConfig", "configuration"):
            create_container(conn, "ICSIWSNConfig", "configuration")

        container_uri = conn.app_base_uri + '/ICSIWSNConfig/containers/configuration/contentInstances'

        xml = open(M2M_CONFIG_FILE, 'r')
        xmltext = xml.read()
        r = requests.post(url=container_uri,
                          data=xmltext,
                          headers=AUTHORIZATION)

    except KeyboardInterrupt:
        return 0
    except Exception, e:
        logging.exception(e)
        return 2


if __name__ == "__main__":
    sys.exit(main())
