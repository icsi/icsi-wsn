#!/bin/sh
# a.azzara@sssup.it


#Config*************************************************************************
SLIPRADIOID="1"
CORES=4
TARGET_KEY="NODE_ID"
NODEID_FILE=common/nodeid.h
COMMON_CONF=common/common-conf.h

ELF_DIR=../../seed-eye-elfs
README=../seed-eye-elfs/README
FIRST_PARK_SENSOR_ID=10
LAST_PARK_SENSOR_ID=13

AREA_A_COUNT=2

LAST_A=$(( $FIRST_PARK_SENSOR_ID + $AREA_A_COUNT ))
FIRST_B=$(( $LAST_A + 1 )) 

FIRST_FLOW_SENSOR_ID=5
LAST_FLOW_SENSOR_ID=6

SEEDEYE_MAKE=../contiki-tres/platform/seedeye/Makefile.seedeye

IP_PREFIX=aaaa::2

compile(){
    echo "Firmware type $1"
    make clean && make TARGET=seedeye $1 $2 -j $CORES 2>&1 | grep -E --color=always 'error|$' #> /dev/null 2>&1 #
}

#RF CHANNEL SETUP***************************************************************
RF_CHANNEL="15"
echo "Updating Channel: $RF_CHANNEL"
sed -i "s/^#define RF_CHANNEL *[0-9]*\+/#define RF_CHANNEL $RF_CHANNEL/" $COMMON_CONF

echo "******   ICSI-WSN Config   ******\n" > $README
echo "RF CHANNEL: $RF_CHANNEL" >> $README

# Fill README with information on compiled firmwares
echo "Border-router IP:$i\t$IP_PREFIX$SLIPRADIOID:$SLIPRADIOID:$SLIPRADIOID:$SLIPRADIOID\n"  >> $README
echo "SENSORS IP:"  >> $README
for i in $(seq $FIRST_PARK_SENSOR_ID $LAST_PARK_SENSOR_ID); do
    printf "PARK ID:%x\t$IP_PREFIX%02x:%x:%x:%x  --  icsi-net-park%d.elf\n" $i $i $i $i $i $i>> $README
done
printf "\n"
for i in $(seq $FIRST_FLOW_SENSOR_ID $LAST_FLOW_SENSOR_ID); do
    printf "FLOW ID:%x\t$IP_PREFIX%02x:%x:%x:%x  --  icsi-net-flow%d.elf\n" $i $i $i $i $i $i>> $README
done


#NATIVE BR *********************************************************************
echo "Compiling Native Br"

cd native-border-router
make -j $CORES  2>&1 | grep -E --color=always 'error|$' #> /dev/null 2>&1 #
cd ..


#SLIP RADIO ********************************************************************
#MAKEFILE SETUP SENSOR_NODE
sed -i "s/^CONTIKI_PLAT_DEFS += -D __USE_UART_PORT1A_FOR_SLIP__/CONTIKI_PLAT_DEFS += -D __USE_UART_PORT1B_FOR_SLIP__/" $SEEDEYE_MAKE
sed -i "s/^CONTIKI_PLAT_DEFS += -D __USE_UART_PORT1B_FOR_DEBUG__/CONTIKI_PLAT_DEFS += -D __USE_UART_PORT1A_FOR_DEBUG__/" $SEEDEYE_MAKE
echo "Compiling slip-radio"
sed -i "s/[0-9]\+/$SLIPRADIOID/" $NODEID_FILE
cd slip-radio
compile  
mv slip-radio.seedeye $ELF_DIR/slip-radio.elf
cd ..


#ICSI NET APP ******************************************************************
#MAKEFILE SETUP SENSOR_NODE
sed -i "s/^CONTIKI_PLAT_DEFS += -D __USE_UART_PORT1B_FOR_SLIP__/CONTIKI_PLAT_DEFS += -D __USE_UART_PORT1A_FOR_SLIP__/" $SEEDEYE_MAKE
sed -i "s/^CONTIKI_PLAT_DEFS += -D __USE_UART_PORT1A_FOR_DEBUG__/CONTIKI_PLAT_DEFS += -D __USE_UART_PORT1B_FOR_DEBUG__/" $SEEDEYE_MAKE
cd icsi-net-app

for i in $(seq $FIRST_PARK_SENSOR_ID $LAST_A); do
    echo "Compiling ICSI Park app, node#$i, area 1"
    sed -i "s/[0-9]\+/$i/" ../$NODEID_FILE
    compile APP=park AREA=1
    mv icsi-net.elf $ELF_DIR/icsi-net-park$i.elf
done

for i in $(seq $FIRST_B $LAST_PARK_SENSOR_ID); do
    echo "Compiling ICSI Park app, node#$i, area 2"
    sed -i "s/[0-9]\+/$i/" ../$NODEID_FILE
    compile APP=park AREA=2
    mv icsi-net.elf $ELF_DIR/icsi-net-park$i.elf
done
for i in $(seq $FIRST_FLOW_SENSOR_ID $LAST_FLOW_SENSOR_ID); do
    echo "Compiling ICSI Flow app, node#$i"
    sed -i "s/[0-9]\+/$i/" ../$NODEID_FILE
    compile APP=flow
    mv icsi-net.elf $ELF_DIR/icsi-net-flow$i.elf
done
cd ..
#********************************************************************************
echo "Git Status" >> $README
git show | sed -n '1,3p' >> $README

