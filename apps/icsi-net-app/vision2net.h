#ifndef __VISION2NET_H__
#define __VISION2NET_H__

#define STATUS_SIZE 4

#define REQ_IMG                 'i'
#define REQ_ROI_GET             'r'
#define REQ_ROI_PUT             'p'
#define REQ_STATUS              's'
#define REQ_OUTPUT              'o'
#define REQ_BATTERY             'b'
#define REQ_VISIO_RESET         'c'
#define REQ_VISIO_START         'w'
#define REQ_VISIO_STOP          'k'

void vision2net_start(void);
int get_algo_check_period(void);

#endif /* __VISION2NET__ */
