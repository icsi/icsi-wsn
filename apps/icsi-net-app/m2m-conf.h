#ifndef M2M_CONF_H
#define	M2M_CONF_H

#define EXI_BUFFER_SIZE 1500//550
#define PRINT_INCOMING_OUTGOING_MESSAGE 1

#if PRINT_INCOMING_OUTGOING_MESSAGE
#define DECODE_IN_EXI 1
#define PRINT_IN_OUT_EXI 1
#else
#define DECODE_INCOMING_EXI 0
#define PRINT_IN_OUT_EXI 0
#endif //PRINT_INCOMING_OUTGOING_MESSAGE


/* The codes for GSCL notifications, specified in the ICSI data model */
#define ABNO    1
#define PARK    2
#define FLOW    3

#define JAM_RATE   4

#ifndef APP_TYPE
#define APP_TYPE    FLOW
#endif

#if APP_TYPE == FLOW
#define CONTAINER_NAME "WSN2"
#define TRES_APP_NAME  "flow"
#else
#define CONTAINER_NAME "WSN1"
#define TRES_APP_NAME  "park"
#endif /* APP_TYPE_FLOW */

#define APP_URI_MAX_LEN      50

/* The parking area.(String)
 *   Via Pietrasantina = "1"
 *   Bowling = "2"
*/

#ifndef PARKING_AREA
#define PARKING_AREA         "1"
#endif

/* Defines the structure of the URI of M2M resources on the gateway for the
 * different ICSI use cases
 * */
#if APP_TYPE == PARK
#define M2M_APPLICATION_URI       CONTAINER_NAME "/containers/p" PARKING_AREA "_"
#else
#define M2M_APPLICATION_URI       CONTAINER_NAME "/containers/f"
#endif


/* PROXY address/port/uri configuration */
#define PROXY_NODE(ipaddr)        uip_ip6addr(ipaddr, 0xaaaa, 0, 0, 0, 0, 0, 0, 2)
#define PROXY_PORT                UIP_HTONS(5758)
#define PROXY_URI                   "coap2http"

/*  The following macros define:
 *
 *   1 - the period of execution of the RPE (T-RES) processing function.
 *   consequently it defines the period of possible notifications to the GSCL.
 *
 *   2- the period with which the vision board is polled for new events.
 *   */
#if APP_TYPE == PARK
#define DEFAULT_ICSI_VISION_POLL_PERIOD    6 //s
#define DEFAULT_ICSI_NOTIFICATION_PERIOD   (DEFAULT_ICSI_VISION_POLL_PERIOD/2)
#else
#define DEFAULT_ICSI_VISION_POLL_PERIOD    10 //s
#define DEFAULT_ICSI_NOTIFICATION_PERIOD   DEFAULT_ICSI_VISION_POLL_PERIOD/2
#endif
/* Important: this macro turns on/off ETSI M2M compatibility. When =1 the full
 * M2M library will be used to generate messages to the GSCL (i.e., the content
 * instance will be encoded as base64, inserted in a XML message and then
 * compressed using EXi).
 *
 *  When using the OM2M implementation the overhead of the XML message and
 *  base64 encoding can be avoided and the content instance can be sent
 *  as it is to the GSCL (=0). The GSCL will then add all the missing
 *  information to the resource. In the ICSI use case this leads to a
 *  considerable advantage: notifications to the GSCL become small enough to
 *  fit into a single CoAP message. (no blockwise transfer is required).
 *
 *  If a different implementation of the GSCL is to be used please consider
 *  turning this macro to 1 for full compatibility. */
#define ENABLE_M2M_EXI            0

/* NOTE: this id cannot be used as slot/flow id */
#define INVALID_DATA            255

#endif	/* M2M_CONF_H */
