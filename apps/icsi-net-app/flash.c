#include "flash.h"
#ifdef __PIC32__
#include <peripheral/nvm.h>

static void write_page(const void *from, void *to, uint32_t len)
{
	const unsigned int *f = from;
	unsigned int *t = to;
	uint32_t lw = 0U;
	while (lw < len) {
		uint32_t w;
		w = len - lw;
		if (w >= BYTE_ROW_SIZE) {
			w = BYTE_ROW_SIZE;
			NVMWriteRow(t, (void *)f);
		} else {
			uint32_t nwrd = (w + 3) / 4;
			unsigned i;
			for (i = 0; i < nwrd; ++i)
				NVMWriteWord(&t[i], f[i]);
		}
		f += w / 4;
		t += w / 4;
		lw += w;
	}
}


void flash_write_blocks(const void *from, void *to, uint32_t len)
{
	const unsigned char *f = from;
	unsigned char *t = to;
	uint32_t lw = 0U;
	while (lw < len) {
		uint32_t w;
		NVMErasePage(t);
		w = len - lw;
		if (w > BYTE_PAGE_SIZE)
			w = BYTE_PAGE_SIZE;
		write_page(f, t, w);
		f += w;
		t += w;
		lw += w;
	}
}
#endif //__PIC32__
