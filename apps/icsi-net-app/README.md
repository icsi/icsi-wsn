#ICSI-Net-Application

##Compilation issues

Compiled using Microchip compiler *xc32 (v1.20)*

The application includes many different libraries (erbium, pymite, t-res, exi). Due to the large code size by these libraries the following error message may appear during the linking phase:

>small-data section exceeds 64KB; lower small-data size limit (see option -G)

To solve the issue the file *contiki-tres/cpu/pic32/Makefile.pic32 *
has been modified changing the -G option, bringing the parameter from 7 to 0.



##Configuration Macros

ICSI_STATIC_CONF: sets a static configuration for T-Res, IS, OD, PF, with hardcoded addresses.

