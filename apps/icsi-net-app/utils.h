#ifndef __UTILS_H__
#define __UTILS_H__

void utils_start(void);

/**
 * Scans the tres task table.
 *
 * \return 1 if a running tres task is found. 0 otherwise.
 */
int utils_tres_running(void);

#ifdef __PIC32__

/* Initialize the random seed for the PIC32 platform
 * A new seed is written to flash so that after rebooting the board the RNG
 * will be initialized with a different seed. */
void utils_pic32_init_random_seed(void);
#else
#define utils_pic32_get_random_seed()
#endif


#endif /* __UTILS__ */
