#!/usr/bin/python

import serial
import re
import time

  
def main():
  ser = serial.Serial('/dev/ttyUSB2', 115200)
  
  try:
    while True:
      line = ser.readline()
      if line[0:2] != 'EV':
        continue
      #print line
      ev = line.split()[1]
      t = time.time() * 1000
      print 'event #', ev, int(t)
      f = open('icsi-log-serial.txt', 'a')
      f.write(str(ev) + ' ' + str(int(t))  + '\n');
      f.close()
      
  except:
    ser.close()
    f.close()
    print 'done'
    

if __name__ == "__main__":
  main()



