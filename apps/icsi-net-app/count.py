#!/usr/bin/python

import re
import time
from pandas import Series
from scipy import stats

proxy_file = '/home/andrea/Desktop/icsi-exp/proxy.txt'
http_file = '/home/andrea/Desktop/icsi-exp/http.txt'
serial_file = 'icsi-log-serial.txt'
MAX_ALL = 1000

def main():
  proxyTime = []
  httpTime = []
  serialTime = []
  try:
    with open(serial_file, 'r') as serial:
      #print
      #print 'Processing ', filename
      allLines = serial.readlines()  
      for line in allLines:
        t = line.split()[1]
        #print t
        serialTime.append(int(t))
        
    with open(proxy_file, 'r') as proxy:
      #print
      #print 'Processing ', filename
      allLines = proxy.readlines()  
      for line in allLines:
        t = line.split()[1]
        #print t
        proxyTime.append(int(t))

    with open(http_file, 'r') as http:
      #print
      #print 'Processing ', filename
      allLines = http.readlines()  
      for line in allLines:
        t = line.split()[1]
        #print t
        httpTime.append(int(t))
    
    httpDelay = []
    proxyDelay = []    
    
    print len(httpTime), len(proxyTime), len(serialTime)
    
    for i in range(len(httpTime)):
      httpDiff = httpTime[i]-serialTime[i]
      proxyDiff = proxyTime[i]-serialTime[i]
      #print httpDiff, proxyDiff
      if proxyDiff < 0 or proxyDiff > MAX_ALL:
        continue
      if httpDiff < 0 or httpDiff > MAX_ALL:
        continue        
      print i, httpDiff, proxyDiff
      httpDelay.append(httpDiff)
      proxyDelay.append(proxyDiff)
      httpdelaySeries = Series(httpDelay)
      proxydelaySeries = Series(proxyDelay)     
      
    print httpdelaySeries.describe() 
    print stats.sem(httpdelaySeries)     
    print proxydelaySeries.describe()
    print stats.sem(proxydelaySeries)      
                
  except Exception as e: 
    print 'error', e    
    

if __name__ == "__main__":
  main()
