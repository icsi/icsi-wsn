#include <stdlib.h>

#include "project-conf.h"
#include "contiki.h"
#include "contiki-net.h"

#include "er-coap.h"
#include "er-coap-engine.h"

#include "dev/leds.h"
#include "tres.h"
#include "tres-interface.h"
#include "node-id.h"

#ifdef __PIC32__
#include "flash.h"
#include "peripheral/nvm.h"
#include "pic32_timer.h"
#endif


#define DEBUG 1

#undef PRINTF
#undef PRINT6ADDR
#undef PRINTLLADDR
#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#define PRINTFLN(format, ...) printf(format "\n", ##__VA_ARGS__)
#define PRINT6ADDR(addr) PRINTF("[%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x]", ((uint8_t *)addr)[0], ((uint8_t *)addr)[1], ((uint8_t *)addr)[2], ((uint8_t *)addr)[3], ((uint8_t *)addr)[4], ((uint8_t *)addr)[5], ((uint8_t *)addr)[6], ((uint8_t *)addr)[7], ((uint8_t *)addr)[8], ((uint8_t *)addr)[9], ((uint8_t *)addr)[10], ((uint8_t *)addr)[11], ((uint8_t *)addr)[12], ((uint8_t *)addr)[13], ((uint8_t *)addr)[14], ((uint8_t *)addr)[15])
#define PRINTLLADDR(lladdr) PRINTF("[%02x:%02x:%02x:%02x:%02x:%02x]",(lladdr)->addr[0], (lladdr)->addr[1], (lladdr)->addr[2], (lladdr)->addr[3],(lladdr)->addr[4], (lladdr)->addr[5])
#else
#define PRINTF(...)
#define PRINT6ADDR(addr)
#define PRINTLLADDR(addr)
#define PRINTFLN(...)
#endif


static void reset_put_handler(void* request, void* response, uint8_t *buffer,
                              uint16_t preferred_size, int32_t *offset);

RESOURCE(reset, "title=\"reset\";rt=\"Control\"", NULL, NULL, reset_put_handler, NULL);

PROCESS(reset_process, "Reset Process");

#if TRES_SUPERVISOR_PROCESS
PROCESS(supervisor_process, "Supervisor Process");

PROCESS_THREAD(supervisor_process, ev, data)
{
  PROCESS_BEGIN();
  static struct etimer et;
  etimer_set(&et, CLOCK_SECOND * (TRES_SUPERVISOR_PERIOD));

  static int phase = 0;
  static tres_res_t *task;

  while(1) {
    PROCESS_YIELD();
    if (etimer_expired(&et) && phase == 0) {
      phase = 1;
      struct memb* tasks = get_tasks_mem();
      task = &tasks->mem[0]; //Note: only a single task is supported.
      if (task->monitoring == 1){
        tres_stop_monitoring(task);
        PRINTFLN("Supervisor Stopped task");
      }else{
        task = NULL;
      }
      etimer_set(&et, CLOCK_SECOND * TRES_SUPERVISOR_RESTART_DELAY);
    }
    if (etimer_expired(&et) && phase == 1){
      phase = 0;
      etimer_set(&et, CLOCK_SECOND * (TRES_SUPERVISOR_PERIOD));
      if (task != NULL){
        PRINTFLN("Supervisor Re-Starting task");
        tres_start_monitoring(task);
      }
    }
  }
  PROCESS_END();
}
#endif


PROCESS_THREAD(reset_process, ev, data)
{
  PROCESS_BEGIN();
  static struct etimer et;
  etimer_set(&et, CLOCK_SECOND *3);
  while(1) {
    PROCESS_YIELD();
    if (etimer_expired(&et)) {
      break;
    }
  }
  leds_on(LEDS_GREEN);
  while(1) { /* WAIT for WDT */  }
  PROCESS_END();
}


static void reset_put_handler(void* request, void* response, uint8_t *buffer,
                              uint16_t preferred_size, int32_t *offset){
  process_start(&reset_process, NULL);
}


void utils_start(){
  rest_activate_resource(&reset, "cfg/reset");
#if TRES_SUPERVISOR_PROCESS
  process_start(&supervisor_process, NULL);
#endif
}


int utils_tres_running(){
  int i;
  tres_res_t *task;
  struct memb* tasks = get_tasks_mem();
  for(i = 0; i < tasks->num; i++) {
    task = &tasks->mem[i];
    if (task->monitoring == 1){
      return 1;
    }
  }
  return 0;
}


#ifdef __PIC32__

int flash_seed CONFIG_ATTR = 0xFFAA;

void utils_pic32_init_random_seed(){
	unsigned int new_seed;
	uint16_t timer;
	int res;
	if (flash_seed == 0xFFAA){
		srand(SEEDEYE_ID);
		new_seed = rand();
		PRINTFLN("First boot after flashing: new seed %u", new_seed);
		NVMErasePage(&flash_seed);
		res = NVMWriteWord(&flash_seed, new_seed);
		return;
  }
	timer = pic32_timer1_get_val();
  srand(flash_seed + timer);
  new_seed = rand();
  PRINTFLN("Found SEED=%u in flash, new seed=%u", flash_seed, new_seed);
	NVMErasePage(&flash_seed);
	res = NVMWriteWord(&flash_seed, new_seed);
}
#endif //__PIC32__
