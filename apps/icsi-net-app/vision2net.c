/**
 * \file
 *         Seedeye-VisionBoard Serial Interface
 * \author
 *         Andrea Azzara' <a.azzara@sssup.i>
 */

#include "project-conf.h"
#include "contiki.h"
#include "contiki-net.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "endianess_converter.h"
#include "er-coap.h"
#include "er-coap-engine.h"
#include "vision2net.h"
#include "m2m-conf.h"
#include "node-id.h"
#include "utils.h"

#if !EMULATE_VISION_BOARD
#include "pic32serial.h"
#endif


#define DEBUG 1
#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#define PRINT6ADDR(addr) printf("[%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x]", ((uint8_t *)addr)[0], ((uint8_t *)addr)[1], ((uint8_t *)addr)[2], ((uint8_t *)addr)[3], ((uint8_t *)addr)[4], ((uint8_t *)addr)[5], ((uint8_t *)addr)[6], ((uint8_t *)addr)[7], ((uint8_t *)addr)[8], ((uint8_t *)addr)[9], ((uint8_t *)addr)[10], ((uint8_t *)addr)[11], ((uint8_t *)addr)[12], ((uint8_t *)addr)[13], ((uint8_t *)addr)[14], ((uint8_t *)addr)[15])
#define PRINTLLADDR(lladdr) printf("[%02x:%02x:%02x:%02x:%02x:%02x]",(lladdr)->addr[0], (lladdr)->addr[1], (lladdr)->addr[2], (lladdr)->addr[3],(lladdr)->addr[4], (lladdr)->addr[5])
#else
#define PRINTF(...)
#define PRINT6ADDR(addr)
#define PRINTLLADDR(addr)
#endif


#define CHUNKS_TOTAL    100000
#define MORE_CONTINUE       0x00
#define MORE_STOP       0x01
#define MAX_SERIAL_UP_COMMAND REST_MAX_CHUNK_SIZE + 10

#define LOCAL_BUFFER_LEN    100

static struct etimer vt;
int algo_check_period = DEFAULT_ICSI_VISION_POLL_PERIOD;
static uint8_t serial_buffer [LOCAL_BUFFER_LEN];

int get_algo_check_period(){
	return algo_check_period;
}


/* Handlers Forward Declarations */
static void img_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
static void roi_get_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
static void roi_put_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
static void period_put_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
static void period_get_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
static void status_get_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
static void status_put_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
static void battery_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);

static void output_get_handler(void *request, void *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
static void output_event_handler();


#define EXIT_IF_TASK_RUNNING() do {\
  if (utils_tres_running()){\
    REST.set_response_status(response, REST.status.SERVICE_UNAVAILABLE);\
    const char *error_msg = "Please stop running tasks before running cfg";\
    REST.set_response_payload(response, error_msg, strlen(error_msg));\
    return;\
  }\
} while(0)


/* CoAP Resources */
RESOURCE(img, "title=\"Image\";rt=\"Data\"", img_handler, NULL, NULL, NULL);
RESOURCE(roi, "title=\"ImageRoi\";rt=\"Data\"", roi_get_handler, NULL, roi_put_handler, NULL);
RESOURCE(period, "title=\"algo rate\";rt=\"Control\"", period_get_handler, NULL, period_put_handler, NULL);
RESOURCE(status, "title=\"VB status\";rt=\"Info\"", status_get_handler, NULL, status_put_handler, NULL);
RESOURCE(battery, "title=\"battery\";rt=\"Info\"", battery_handler, NULL, NULL, NULL);
EVENT_RESOURCE(output,
               "title=\"Algo output\";obs",
               output_get_handler,
               NULL,
               NULL,
               NULL,
               output_event_handler);


/* to generate random park id in the demonstrator */
static unsigned int getRandUint(unsigned int mod){
  return (unsigned int)(rand() % mod);
}


static void
get_serial_data(uint8_t* l, uint8_t* serial_buffer, uint8_t type){

#if EMULATE_VISION_BOARD
	if (type == REQ_OUTPUT){
#if APP_TYPE == PARK
  //uint8_t len = snprintf((char *)serial_buffer, REST_MAX_CHUNK_SIZE, "%u 20 30", (node_id == 4)? 3: node_id); //slotid;result;weight
  uint8_t len;
  unsigned int result = getRandUint(255);
  if (node_id == 4){
  	len = snprintf((char *)serial_buffer, REST_MAX_CHUNK_SIZE, "%u %u 60", 3, result); //slotid;result;weight
  }else if (node_id == 3 || node_id == 2){
  	len = snprintf((char *)serial_buffer, REST_MAX_CHUNK_SIZE, "%u %u 40", node_id, result); //slotid;result;weight
  }else{
    unsigned int did = 	getRandUint(47); //for the ICSI demo we generate slot id from 0 to 47
    len = snprintf((char *)serial_buffer, REST_MAX_CHUNK_SIZE, "%u %u 50", did, result); //slotid;result;weight
  }
#else //FLOWS
  uint8_t len = snprintf((char *)serial_buffer, REST_MAX_CHUNK_SIZE, "%u 20 30", node_id); //archid;avgspeed;numberofvehciles
#endif
  *l = len;
  return;
	}
#else
  uint8_t len;
  char msg [20];
  uint8_t idx = 0;
  msg[idx] = type;
  idx += 1;

  PRINTF("writing req...%d\n", idx);
  vision_serial_write(msg, idx);
  vision_serial_read(&len, LOCAL_BUFFER_LEN, serial_buffer);

  PRINTF("finished reading serial %d len \n", len);
  serial_buffer[len] = '\0';
  *l = len;
#endif //EMULATE_VISION_BOARD
}

/*
 * Handlers implementations
 *
 * */


/* Image Resource */
static
void
img_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  PRINTF("img handler\n");
  EXIT_IF_TASK_RUNNING();
#if EMULATE_VISION_BOARD
  int len = snprintf((char *)buffer, REST_MAX_CHUNK_SIZE, "Img get emulated resource");
  REST.set_response_payload(response, buffer, len);
#else

  /* Check the offset for boundaries of the resource data. */
  if (*offset>=CHUNKS_TOTAL)
  {
    REST.set_response_status(response, REST.status.BAD_OPTION);
    /* A block error message should not exceed the minimum block size (16). */

    const char *error_msg = "BlockOutOfScope";
    REST.set_response_payload(response, error_msg, strlen(error_msg));
    return;
  }

  uint8_t serial_buffer [LOCAL_BUFFER_LEN];
  uint8_t len;

  char msg [20];
  uint8_t idx = 0;
  msg[idx] = REQ_IMG;
  idx += 1;
  uint32ton(*offset, (uint8_t *)&msg[idx]);
  idx += 4;

  PRINTF("writing req...%d\n", idx);
  vision_serial_write(msg, idx);
  vision_serial_read(&len, LOCAL_BUFFER_LEN, serial_buffer);

  PRINTF("finished reading serial %d len \n", len);
  serial_buffer[len+1] = '\0';

  uint8_t actual_msg_len = len-1;

  memcpy(buffer, &serial_buffer[1], len);
  REST.set_response_payload(response, buffer, actual_msg_len);

  /* IMPORTANT for chunk-wise resources: Signal chunk awareness to REST engine. */
  *offset += actual_msg_len;

  /* Signal end of resource representation. */
  if (serial_buffer[0] == MORE_STOP)
  {
    *offset = -1;
  }
#endif /* EMULATE_VISION_BOARD */
}


/* ROI Resource */
static
void
roi_get_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  PRINTF("roi get handler\n");
  EXIT_IF_TASK_RUNNING();
#if EMULATE_VISION_BOARD
  int len = snprintf((char *)buffer, REST_MAX_CHUNK_SIZE, "Roi get emulated resource");
  REST.set_response_payload(response, buffer, len);
#else

  uint8_t serial_buffer [LOCAL_BUFFER_LEN];
  uint8_t len;
  char msg [MAX_SERIAL_UP_COMMAND];
  uint8_t idx;

  /* Check the offset for boundaries of the resource data. */
  if (*offset>=CHUNKS_TOTAL)
  {
    REST.set_response_status(response, REST.status.BAD_OPTION);
    /* A block error message should not exceed the minimum block size (16). */

    const char *error_msg = "BlockOutOfScope";
    REST.set_response_payload(response, error_msg, strlen(error_msg));
    return;
  }

  idx = 0;
  msg[idx] = REQ_ROI_GET;
  idx += 1;
  uint32ton(*offset, (uint8_t *)&msg[idx]);
  idx += 4;

  PRINTF("writing req...%d\n", idx);
  vision_serial_write(msg, idx);
  //PRINTF("reading reply...\n");
  vision_serial_read(&len, LOCAL_BUFFER_LEN, serial_buffer);

  PRINTF("finished reading serial %d len \n", len);
  serial_buffer[len+1] = '\0';

  uint8_t actual_msg_len = len-1;
  //snprintf((char *)buffer, len, (const char *)&serial_buffer[1]);

  memcpy(buffer, &serial_buffer[1], len);
  REST.set_response_payload(response, buffer, actual_msg_len);

  /* IMPORTANT for chunk-wise resources: Signal chunk awareness to REST engine. */
  *offset += actual_msg_len;

  /* Signal end of resource representation. */
  if (serial_buffer[0] == MORE_STOP)
  {
    *offset = -1;
  }
  REST.set_header_content_type(response, REST.type.APPLICATION_JSON);
#endif //EMULATE_VISION_BOARD
}


static
void
roi_put_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  PRINTF("roi post handler\n");
  EXIT_IF_TASK_RUNNING();
#if EMULATE_VISION_BOARD
  REST.set_response_status(response, REST.status.CHANGED);
#else
  uint32_t num = 0;
  uint8_t more = 0;
  uint16_t size = 0;
  uint32_t b1_offset = 0;
  uint16_t plen;
  const uint8_t *payload;
  uint8_t serial_buffer [LOCAL_BUFFER_LEN];
  uint8_t len;
  char msg [MAX_SERIAL_UP_COMMAND];
  uint8_t idx;

  if(coap_get_header_block1(request, &num, &more, &size, &b1_offset)) {
    coap_set_header_block1(response, num, more, size);
  }

  PRINTF("Request on /pf: num = %"PRIu32", more = %"PRIu8", size = %"PRIu16
         ", offset = %"PRIu32"\n", num, more, size, b1_offset);
  // if it's the first packet, stop input resource monitoring
  if(b1_offset == 0) {
    PRINTF("starting new upload\n");
  }
  plen = coap_get_payload(request, &payload);
  PRINTF("Payload len: %d\n", plen);
#if COPPER_WORKAROUND
  // if no block-wise transfer and payload len equal to maximum size, then
  // we probably need block wise transfer. FIXME: that may be not true
  if(size == 0 && plen == REST_MAX_CHUNK_SIZE) {
    more = 1;
    size = REST_MAX_CHUNK_SIZE;
    coap_set_header_block1(response, num, more, size);
  }
#endif
  // if it's the first packet, allocate a memory slot
  if(b1_offset == 0) {
  PRINTF("first block\n");
  }

  idx = 0;
  msg[idx] = REQ_ROI_PUT;
  idx += 1;
  msg[idx] = plen;
  idx += 1;
  msg[idx] = more;
  idx += 1;
  uint32ton(b1_offset, (uint8_t *)&msg[idx]);
  idx += 4;

  memcpy((uint8_t *)&msg[idx], payload, plen);
  idx += plen;

  PRINTF("writing req...%d\n", idx);
  vision_serial_write(msg, idx);
  //PRINTF("reading reply...\n");
  vision_serial_read(&len, LOCAL_BUFFER_LEN, serial_buffer);

  PRINTF("finished reading serial %d len \n", len);

  // if it's the final block, finalize the memory slot
  if(more == 0) {
    REST.set_response_status(response, REST.status.CHANGED);
  }
#endif //EMULATE_VISION_BOARD
}


/* Period Resource */
static void period_get_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset){
  snprintf((char *)buffer, REST_MAX_CHUNK_SIZE, "%d", algo_check_period);
  REST.set_response_payload(response, buffer, strlen((char *)buffer));
}

static void period_put_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset){
  EXIT_IF_TASK_RUNNING();
  uint8_t rcv_buffer[LOCAL_BUFFER_LEN];
  int8_t len;
  uint8_t *incoming = NULL;

  len = REST.get_request_payload(request, (const uint8_t **) &incoming);
  memcpy(rcv_buffer, incoming, len);
  rcv_buffer[len] = '\0';
  PRINTF("received %s", rcv_buffer);
  if (len > 0){
    algo_check_period = atoi((const char *)rcv_buffer);
  }
}

/* Status Resource */
void
static status_get_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
#if EMULATE_VISION_BOARD
  int len = snprintf((char *)buffer, REST_MAX_CHUNK_SIZE, "Emulated PID 10101");
  REST.set_response_payload(response, buffer, len);
#else
  uint8_t serial_buffer [LOCAL_BUFFER_LEN];
  uint8_t len;
  get_serial_data(&len, serial_buffer, REQ_STATUS);
  memcpy(buffer, serial_buffer, len);
  REST.set_response_payload(response, buffer, len);
#endif
}

void
static status_put_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  //handle start/stop, rst
	int len;
  uint8_t *incoming = NULL;

#if !(EMULATE_VISION_BOARD)
  uint8_t serial_buffer [LOCAL_BUFFER_LEN];
#endif

  len = REST.get_request_payload(request, (const uint8_t **) &incoming);
  if (strncmp(incoming, "start", len) == 0){
    PRINTF("START ALGO\n");
#if EMULATE_VISION_BOARD
    int len = snprintf((char *)buffer, REST_MAX_CHUNK_SIZE, "Process Started");
    REST.set_response_payload(response, buffer, len);
#else
    get_serial_data(&len, serial_buffer, REQ_VISIO_START);
    memcpy(buffer, serial_buffer, len);
    REST.set_response_payload(response, buffer, len);
#endif
  } else if (strncmp(incoming, "stop", len) == 0){
    PRINTF("STOP ALGO\n");
#if EMULATE_VISION_BOARD
    int len = snprintf((char *)buffer, REST_MAX_CHUNK_SIZE, "Process Stopped");
    REST.set_response_payload(response, buffer, len);
#else
    get_serial_data(&len, serial_buffer, REQ_VISIO_STOP);
    memcpy(buffer, serial_buffer, len);
    REST.set_response_payload(response, buffer, len);
#endif
  } else if (strncmp(incoming, "reset", len) == 0){
    PRINTF("RESET V.BOARD\n");
#if EMULATE_VISION_BOARD
    int len = snprintf((char *)buffer, REST_MAX_CHUNK_SIZE, "Rebooting vision board");
    REST.set_response_payload(response, buffer, len);
#else
    get_serial_data(&len, serial_buffer, REQ_VISIO_RESET);
    memcpy(buffer, serial_buffer, len);
    REST.set_response_payload(response, buffer, len);
#endif
  } else {
    PRINTF("UNSPEC.\n");
    REST.set_response_status(response, REST.status.INTERNAL_SERVER_ERROR);
    const char *error_msg = "Bad payload. Use start|stop|reset";
    REST.set_response_payload(response, error_msg, strlen(error_msg));
    return;
  }
}


/* Battery Resource */
static void
battery_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
#if EMULATE_VISION_BOARD
  int len = snprintf((char *)buffer, REST_MAX_CHUNK_SIZE, "Battery emulated resource");
  REST.set_response_payload(response, buffer, len);
#else
  uint8_t serial_buffer [LOCAL_BUFFER_LEN];
  uint8_t len;

  char msg [20];
  uint8_t idx = 0;
  msg[idx] = REQ_BATTERY;
  idx += 1;

  PRINTF("writing req...%d\n", idx);
  vision_serial_write(msg, idx);
  vision_serial_read(&len, LOCAL_BUFFER_LEN, serial_buffer);

  PRINTF("finished reading serial %d len \n", len);
  serial_buffer[len] = '\0';

  memcpy(buffer, serial_buffer, len);
  REST.set_response_payload(response, buffer, len);
#endif
}


/* Output Resource */
static void
output_get_handler(void *request, void *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  //PRINTF("Output get handler\n");
  REST.set_response_payload(response, buffer, snprintf((char *)buffer, preferred_size, (const char *)serial_buffer)); //slot id, status, confidence
}


static void
output_event_handler()
{
  /* Notify the registered observers which will trigger the res_get_handler to create the response. */
  REST.notify_subscribers(&output);
}


/*---------------------------------------------------------------------------*/
PROCESS(periodic_updates, "Periodic V2N updates");
/*---------------------------------------------------------------------------*/

void vision2net_start(){

#if !EMULATE_VISION_BOARD
  vision_serial_init();
#endif
  printf("Init vision board ok\n");
  /* Initialize the REST engine. */
  rest_activate_resource(&period, "cfg/period");
  rest_activate_resource(&img,"cfg/img");
  rest_activate_resource(&status, "cfg/status");
  rest_activate_resource(&roi, "cfg/roi");
  rest_activate_resource(&battery, "cfg/battery");
  rest_activate_resource(&output, "output");

  /* periodic_updates process start */
  process_start(&periodic_updates, NULL);
}
/*---------------------------------------------------------------------------*/



/*---------------------------------------------------------------------------*/
PROCESS_THREAD(periodic_updates, ev, data)
{
  PROCESS_BEGIN();
  etimer_set(&vt, algo_check_period * CLOCK_SECOND);
  while(1) {
    PROCESS_YIELD();
    if (etimer_expired(&vt)) {
      etimer_set(&vt, algo_check_period * CLOCK_SECOND);
      static uint8_t len;
      PRINTF("Periodic updates on serial interface: timer expired\n");
      get_serial_data(&len, serial_buffer, REQ_OUTPUT);
      //TODO: better check the validity of the event.
      if (len > 0 && strlen(serial_buffer) > 0){
        output.trigger();
      }
      //PRINTF("Triggering event\n");
    }
  } /* while (1) */
  PROCESS_END();
}
