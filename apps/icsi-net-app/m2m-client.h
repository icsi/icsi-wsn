#ifndef __M2M_CLIENT_H__
#define __M2M_CLIENT_H__

/* t-res */

#include "tres.h"
#include "tres-interface.h"
void m2m_tres_send(char * output, tres_res_t *task);

void m2m_client_init(void);

#endif	/*__M2M_CLIENT_H__*/
