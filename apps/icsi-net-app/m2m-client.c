
/* contiki */
#include "contiki.h"
#include "contiki-net.h"
#include "dev/button-sensor.h"
/* m2m */
#include "exiDecode.h"
#include "m2mBuild.h"
#include "m2m-conf.h"
#include "m2m-client.h"
#include "base64.h"
#include "node-id.h"
#include "pyot.h"
#include "vision2net.h"

#define DEBUG 1

#undef PRINTF
#undef PRINT6ADDR
#undef PRINTLLADDR
#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#define PRINTFLN(format, ...) printf(format "\n", ##__VA_ARGS__)
#define PRINT6ADDR(addr) PRINTF("[%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x]", ((uint8_t *)addr)[0], ((uint8_t *)addr)[1], ((uint8_t *)addr)[2], ((uint8_t *)addr)[3], ((uint8_t *)addr)[4], ((uint8_t *)addr)[5], ((uint8_t *)addr)[6], ((uint8_t *)addr)[7], ((uint8_t *)addr)[8], ((uint8_t *)addr)[9], ((uint8_t *)addr)[10], ((uint8_t *)addr)[11], ((uint8_t *)addr)[12], ((uint8_t *)addr)[13], ((uint8_t *)addr)[14], ((uint8_t *)addr)[15])
#define PRINTLLADDR(lladdr) PRINTF("[%02x:%02x:%02x:%02x:%02x:%02x]",(lladdr)->addr[0], (lladdr)->addr[1], (lladdr)->addr[2], (lladdr)->addr[3],(lladdr)->addr[4], (lladdr)->addr[5])
#else
#define PRINTF(...)
#define PRINT6ADDR(addr)
#define PRINTLLADDR(addr)
#define PRINTFLN(...)
#endif

static uint32_t structure_dim;
static uip_ipaddr_t proxy_addr;
static uint32_t offset;
static char outbuffer[EXI_BUFFER_SIZE];
static char chunk_buffer[REST_MAX_CHUNK_SIZE];
static uint8_t request_more;
static int error = 0;

static process_event_t new_content_event;

PROCESS(content_send, "Content Send Process");
AUTOSTART_PROCESSES(&content_send);

#define CONTENT_BUFFER_SIZE    1000
#define B64_SIZE (4 * (CONTENT_BUFFER_SIZE / 3) + (CONTENT_BUFFER_SIZE % 3 != 0 ? 4 : 0))
unsigned char content_instance[CONTENT_BUFFER_SIZE];
unsigned char base64[B64_SIZE];

void
client_chunk_handler(void *response) {
  const uint8_t *chunk;
  coap_packet_t * pkt = (coap_packet_t *) response;
  int len = coap_get_payload(response, &chunk);
  static uint8_t more_;
  //static uint8_t code;
  uint32_t seq_num;

  if (len > 0) {
  	PRINTFLN("|%.*s", len, (char *) chunk);
  }
  if (pkt->type == COAP_TYPE_ACK && IS_OPTION(pkt, COAP_OPTION_BLOCK1)){
    coap_get_header_block1(response, &seq_num, &more_, NULL, NULL);
    PRINTFLN("ACK for block %u", seq_num);
    error = 0;
    return;
  }
  if (pkt->type == COAP_TYPE_ACK){
  	PRINTFLN("ACK for message\n");
    error = 0;
    return;
  }
  if ((pkt->code == REST.status.OK)){
  	PRINTFLN("Response OK");
    error = 0;
    return;
  }
  if (pkt->code == REST.status.CREATED){
  	PRINTFLN("Resource Created");
    error = 0;
    return;
  }
  PRINTFLN("Response code %d", pkt->code);
  error = 1;
}


PROCESS_THREAD(content_send, ev, data) {
  PROCESS_BEGIN();
  static coap_packet_t request[1];
  PROXY_NODE(&proxy_addr);
  static uint8_t *token_ptr;
  static uint8_t token_len;
  static unsigned int len;
  static uint16_t num;
  static unsigned int* data_id;
#if ENABLE_M2M_EXI
  static errorCode tmp_err_code = EXIP_OK;
#endif
  static char application_uri[APP_URI_MAX_LEN];
  
  while (1) {
    PROCESS_YIELD();
    if(ev == new_content_event){
      //PRINTF("Sending coap Notification\n");
      data_id = data;
      //PRINTFLN("Data ID = %u", *data_id);
      snprintf(application_uri, APP_URI_MAX_LEN, "%s%u", M2M_APPLICATION_URI, *data_id);
      PRINTFLN("Application uri %s", application_uri);
      offset = 0;
      structure_dim = 0;
      num = 0;
      PRINT6ADDR(&proxy_addr);
      PRINTFLN(" : %u", UIP_HTONS(PROXY_PORT));
      request_more = 0;

#if ENABLE_M2M_EXI
      contentInstanceBuild(outbuffer, tmp_err_code, (SingleString)base64);
      PRINTFLN("Content instance creation");
#endif
      structure_dim = strlen(outbuffer);
      PRINTFLN("Sending: %s %u bytes", outbuffer, (unsigned int)structure_dim);

      coap_init_message(request, COAP_TYPE_CON, COAP_POST, coap_get_mid());
      PRINTFLN("Sending POST to %s", PROXY_URI);
      coap_set_header_uri_path(request, PROXY_URI);

      token_len = coap_generate_token(&token_ptr);
      coap_set_token(request, token_ptr, token_len);

      if (structure_dim <= REST_MAX_CHUNK_SIZE){ //blockwise not required
        memcpy(chunk_buffer, outbuffer, structure_dim);
        coap_set_payload(request, chunk_buffer, structure_dim);
        //PRINTFLN("structure dim =%u Block1 is not set",  structure_dim);
        coap_set_header_proxy_uri(request, application_uri);
#if ENABLE_M2M_EXI
        coap_set_header_content_format(request, APPLICATION_EXI);
#endif
        COAP_BLOCKING_REQUEST(&proxy_addr, PROXY_PORT, request, client_chunk_handler);
      }
      else { //blockwise required
        while(offset < structure_dim){
          coap_init_message(request, COAP_TYPE_CON, COAP_POST, coap_get_mid());
          PRINTFLN("n=%u, offset =%u, l=%u", num, offset, len);
          coap_set_header_uri_path(request, PROXY_URI);
          offset = (num * REST_MAX_CHUNK_SIZE);
          len = ((structure_dim - offset) > REST_MAX_CHUNK_SIZE ) ? REST_MAX_CHUNK_SIZE : (structure_dim - offset);
          memcpy(chunk_buffer, outbuffer + offset, len);
          offset += len;
          if (offset < structure_dim){
            request_more = 1;
          }else{
            request_more = 0;
          }
          coap_set_payload(request, chunk_buffer, structure_dim);
          coap_set_header_proxy_uri(request, application_uri);
#if ENABLE_M2M_EXI
          coap_set_header_content_format(request, APPLICATION_EXI);
#endif
          coap_set_header_block1(request, num, request_more, REST_MAX_CHUNK_SIZE);
          COAP_BLOCKING_REQUEST(&proxy_addr, PROXY_PORT, request, client_chunk_handler);
          if (error == 1) {
          	PRINTFLN("Error while sending block %u", num);
            error = 0;
            break;
          }
          num ++;
        }
      }
    } //event
  } //while 1
  PROCESS_END();
}


void m2m_client_init(){
  process_start(&content_send, NULL);
}


static unsigned int counter = 0;


#define NOTIFPARK(addr,type,id,status) "%u;aaaa::2%02x:%x:%x:%x;p" PARKING_AREA "_%u;%u", \
	  type, node_id, node_id, node_id, node_id, id, status

#define NOTIFFLOW(addr,type,id,avg_speed,nvehicles,time) "%u;aaaa::2%02x:%x:%x:%x;%u;%u;%u;%u", \
	  type, node_id, node_id, node_id, node_id, id, avg_speed, nvehicles, time

#define NOTIFCOND(addr,type,condid,message) "%u;aaaa::2%02x:%x:%x:%x;%u;%s", \
	  type, node_id, node_id, node_id, node_id, condid, message


void m2m_tres_send(char * output, tres_res_t* task){

  uint8_t type = APP_TYPE;
  //printf("Custom Send: Received %s\n", output);

#if APP_TYPE == PARK
  // Parking slot application
  unsigned int data_id, algo_out;

  sscanf((const char *)output, "%u %u", &algo_out, &data_id);

  if (data_id == INVALID_DATA){
  	//printf("Invalid data\n");
  	return;
  }
  printf("Parsing Park data: slotid= %u, output=%u\n", data_id, algo_out);
  algo_out = (algo_out > 128) ? 1 : 0;
  size_t input_length = snprintf((char *)content_instance, CONTENT_BUFFER_SIZE,	NOTIFPARK(&self_ipaddr, type, data_id, algo_out));

  /* Override results during testing phase */
//  PRINTFLN("WARN: Overriding results during testing phase");
//  static uint8_t id = 0;
//  uint8_t status = (id++ % 2);
//  input_length = snprintf((char *)content_instance, CONTENT_BUFFER_SIZE,	NOTIFPARK(&self_ipaddr, type, data_id, status));

#else //flows archid;avgspeed;numberofvehciles
  unsigned int data_id, avg_speed, numberofvehicles;
  int period = get_algo_check_period();
	size_t input_length;
	sscanf((const char *)output, "%u %u %u", &data_id, &avg_speed, &numberofvehicles);
  if (data_id == INVALID_DATA){
  	//printf("Invalid data\n");
  	return;
  }
  printf("****  archid= %u, avg_speed=%u, n_vehicles=%u\n", data_id, avg_speed, numberofvehicles);

  static uint8_t alternate = 0;
/*  if (alternate++ % 2){*/
  //if (numberofvehicles/period < JAM_RATE){
  input_length = snprintf((char *)content_instance, CONTENT_BUFFER_SIZE,	NOTIFFLOW(&self_ipaddr, type, data_id, avg_speed, numberofvehicles, period));
/*
	}else{
		input_length = snprintf((char *)content_instance, CONTENT_BUFFER_SIZE,	NOTIFCOND(&self_ipaddr, ABNO, 1, "jam"));
	}
*/
  printf("INPUT LEN = %d\n", input_length);

#endif //APP_TYPE == x

#if ENABLE_M2M_EXI
  size_t output_length = B64_SIZE;
  int ret = base64_encode(base64, &output_length, (const unsigned char *)content_instance, input_length);
  PRINTFLN("ret val = %d", ret);
  if (output_length == 0 || ret != 0){
    PRINTF("Warning: base64 output len = 0\n");
  }

  printf("Base64 string: %u %s\n", strlen(base64), base64);
#else  //m2m and exi are disabled
  snprintf(outbuffer, EXI_BUFFER_SIZE, "%s", content_instance);
#endif //ENABLE_M2M_EXI
  PRINTFLN("EV %u", counter++);

  process_post_synch(&content_send, new_content_event, &data_id);
  return;
}

