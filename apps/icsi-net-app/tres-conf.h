#ifndef __TRES_TEST_CONF_H__
#define __TRES_TEST_CONF_H__

/******************************************************************************/
/*                       T-Res example specific conf                          */
/******************************************************************************/


// workaround for copper always using blockwise transfer if debug is enabled
#define TRES_CONF_COPPER_WORKAROUND 1
#define COPPER_WORKAROUND 1  //TODO: check if actually needed

// enable stack overflow detection
#define STACK_OVERFLOW_DETECTION 1

// enable loopback communication
#define UIP_CONF_LOOPBACK 1

// if enabled, T-Res always uses CON messages
#ifndef TRES_CONF_RELIABLE
#define TRES_CONF_RELIABLE 1
#endif
#if TRES_CONF_RELIABLE
#define COAP_OBSERVING_CONF_REFRESH_INTERVAL 0
#else
#define COAP_OBSERVING_CONF_REFRESH_INTERVAL 1000000ul
#endif

/* Increase rpl-border-router IP-buffer when using 128. */
#ifndef REST_MAX_CHUNK_SIZE
#define REST_MAX_CHUNK_SIZE    64
#endif

/* Multiplies with chunk size, be aware of memory constraints. */
#ifndef COAP_MAX_OPEN_TRANSACTIONS
#define COAP_MAX_OPEN_TRANSACTIONS   12
#endif

/* Must be <= open transaction number. */
#ifndef COAP_MAX_OBSERVERS
#define COAP_MAX_OBSERVERS      5//COAP_MAX_OPEN_TRANSACTIONS-2
#endif

#ifndef COAP_CONF_MAX_OBSERVEES
#define COAP_CONF_MAX_OBSERVEES      5//COAP_MAX_OPEN_TRANSACTIONS-2
#endif

#define TRES_CONF_IS_MAX_NUMBER 5
#define TRES_CONF_OD_MAX_NUMBER 1

//TODO:
//TRES_CONF_PM_HEAPSIZE

#endif /* __TRES_TEST_CONF_H__ */
