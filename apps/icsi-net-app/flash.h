#ifndef FLASH_H
#define FLASH_H

#include "contiki.h"

#ifdef __PIC32__
/** Copy a buffer to the Flash memory.
 * @param from Buffer containing the data to write
 * @param to   Flash address.  It must be aligned at the block boundary of the
 * Flash memory.
 * @param len  Number of bytes to write.  It should be a multiple of the word
 * size of the Flash memory; if it is not, it is rounded up. For PIC32, the
 * Flash word is 4 bytes.
 */
void flash_write_blocks(const void *from, void *to, uint32_t len);
#else
#define flash_write_blocks(a, b, c)
#endif //__PIC32__

#define CONFIG_ATTR __attribute__((section ("cfg"), aligned (4096), space(prog)))

#endif /* FLASH_H */
