#include "contiki.h"
#ifdef __PIC32__
void vision_serial_init(void);
void vision_serial_read(uint8_t *len, int8_t max_len, uint8_t buffer[]);
void vision_serial_write(char buffer[], int8_t len);

#define DISABLE_INT_SERIAL_COMM             0

#if DISABLE_INT_SERIAL_COMM
/*
 * Enable interrupts
 */
static inline void EE_pic32_enableIRQ(void)
{
  ASM_EN_INT;
}

/*
 * Disable interrupts
 */
static inline void EE_pic32_disableIRQ(void)
{
  ASM_DIS_INT;
}
#else
#define EE_pic32_enableIRQ()
#define EE_pic32_disableIRQ()
#endif //DISABLE_INT_SERIAL_COMM

#endif /* __PIC32__ */
