
#include "project-conf.h"
#ifdef __PIC32__
#include "dev/serial-line.h"
#include <pic32_uart.h>
#include "dev/slip.h"
#include "pic32serial.h"


static int8_t my_uart1A_read_byte(uint8_t *data)
{
  /* Polling mode */
  if (U1ASTAbits.OERR) {
    U1ASTAbits.OERR = 0;
    return -2;
  }
  if (U1ASTAbits.URXDA) {
    *data = U1ARXREG & 0x00FF;
    return 0;
  }
  return -1;
}


void vision_serial_init(void)
{
  pic32_uart1A_init(115200, 0);
  IEC0CLR = _IEC0_U1AEIE_MASK | _IEC0_U1ATXIE_MASK | _IEC0_U1ARXIE_MASK;
  IFS0CLR = _IFS0_U1AEIF_MASK | _IFS0_U1ATXIF_MASK | _IFS0_U1ARXIF_MASK;
}

void vision_serial_read(uint8_t *len, int8_t max_len, uint8_t buffer[])
{
  uint8_t c;
  uint8_t i;
  int8_t ret;
  EE_pic32_disableIRQ();
  for (;;){
    ret = my_uart1A_read_byte(&c);
    if (ret == 0) {
        break;
    }
  }
  //TODO: check max len
  for(i = 0; i < c; i++){

    for (;;){
      ret = my_uart1A_read_byte(&buffer[i]);
      if (ret == 0) {
          break;
      }
    }
  }
  EE_pic32_enableIRQ();
  *len = c;
}

void vision_serial_write(char buffer[], int8_t len)
{
  int i;
  for (i = 0; i < len ; i++){
    pic32_uart1A_write(buffer[i]);
  }
}
#endif /* __PIC32__ */
