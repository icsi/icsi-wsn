/*
 * Copyright (c) 2015, Andrea Azzara'
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.

 */

/**
 * \file
 *
 * \author
 *      Andrea Azzara'  <a.azzara@sssup.it>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* contiki */
#include "contiki.h"
#include "contiki-net.h"
#include "dev/button-sensor.h"
#include "er-coap.h"
#include "er-coap-engine.h"
#include "er-coap-transactions.h"
#include "node-id.h"
#include "dev/leds.h"

/* m2m */
#include "exiDecode.h"
#include "m2mBuild.h"
#include "m2m-conf.h"
#include "m2m-client.h"

/* t-res */
#include "tres.h"
#include "tres-interface.h"

/* Vision2Net comm. */
#include "vision2net.h"

/* Rpl info */
#include "rplinfo.h"

/* Utility module */
#include "utils.h"

/* PyoT */
#include "pyot.h"

#include "static-test-config.h"

#define DEBUG 1

#undef PRINTF
#undef PRINT6ADDR
#undef PRINTLLADDR
#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#define PRINTFLN(format, ...) printf(format "\n", ##__VA_ARGS__)
#define PRINT6ADDR(addr) PRINTF("[%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x]", ((uint8_t *)addr)[0], ((uint8_t *)addr)[1], ((uint8_t *)addr)[2], ((uint8_t *)addr)[3], ((uint8_t *)addr)[4], ((uint8_t *)addr)[5], ((uint8_t *)addr)[6], ((uint8_t *)addr)[7], ((uint8_t *)addr)[8], ((uint8_t *)addr)[9], ((uint8_t *)addr)[10], ((uint8_t *)addr)[11], ((uint8_t *)addr)[12], ((uint8_t *)addr)[13], ((uint8_t *)addr)[14], ((uint8_t *)addr)[15])
#define PRINTLLADDR(lladdr) PRINTF("[%02x:%02x:%02x:%02x:%02x:%02x]",(lladdr)->addr[0], (lladdr)->addr[1], (lladdr)->addr[2], (lladdr)->addr[3],(lladdr)->addr[4], (lladdr)->addr[5])
#else
#define PRINTF(...)
#define PRINT6ADDR(addr)
#define PRINTLLADDR(addr)
#define PRINTFLN(...)
#endif

uip_ipaddr_t server_ipaddr;

#define REMOTE_PORT     UIP_HTONS(COAP_DEFAULT_PORT)

PROCESS(icsi_net_main, "ICSI net App main");
PROCESS(pyot_keepalive, "PyoT Keepalive");
AUTOSTART_PROCESSES(&icsi_net_main);


static int getRandUint(unsigned int mod){
  return (unsigned int)(rand() % mod);
}


#if PYOT_KEEPALIVE && PYOT_KEEPALIVE_CONFIRMABLE
static void
client_chunk_handler(void *response)
{
  const uint8_t *chunk;

  int len = coap_get_payload(response, &chunk);
  if (len > 0)
    printf("|%.*s", len, (char *)chunk);
}
#endif //PYOT_KEEPALIVE


void print_current_config(){
  PRINTFLN("  Application type " TRES_APP_NAME);
#ifdef RF_CHANNEL
  PRINTF("  RF channel: %u\n", RF_CHANNEL);
#endif
#if ICSI_STATIC_CONF
  PRINTFLN("  ICSI Static conf.");
#elif ICSI_STATIC_CONF_RESOURCE
  PRINTFLN("  ICSI Static conf. w/ demo resource.");
#endif
#if EMULATE_VISION_BOARD
  PRINTFLN("  Emulating Vision Board");
#endif
  PRINTFLN("  Notification Period = %u. Serial update Period = %u", DEFAULT_ICSI_NOTIFICATION_PERIOD, DEFAULT_ICSI_VISION_POLL_PERIOD);
}


#if ICSI_STATIC_CONF || ICSI_STATIC_CONF_RESOURCE
#define URI_MAX_LEN 50
unsigned char subscription_uri[URI_MAX_LEN];

extern unsigned char usrlib_img[];
tres_res_t * t1 = NULL;
static void static_test_config(){
  if (node_id == 3){
    t1 = tres_add_task(TRES_APP_NAME, DEFAULT_ICSI_NOTIFICATION_PERIOD);
    t1->pf_img = usrlib_img;
    /* loopback(3) and child node(4) */
    if(task_is_add(t1, NODE_URI_3)!= ERR_NONE){
      PRINTFLN("Error adding IS " NODE_URI_2);
    }
    if(task_is_add(t1, NODE_URI_4)!= ERR_NONE){
      PRINTFLN("Error adding IS " NODE_URI_4);
    }
    PRINTFLN("Node 3 configured");
  }
  else if (node_id == 4){
    /* do nothing, this is just an input node */
  }
  else{
  	/* subscribe to the local output resource */
  	snprintf(subscription_uri, URI_MAX_LEN, "<coap://[aaaa::2%02x:%x:%x:%x]/output>", node_id, node_id, node_id, node_id);
#if APP_TYPE == PARK
    t1 = tres_add_task(TRES_APP_NAME, DEFAULT_ICSI_NOTIFICATION_PERIOD);
#else
    t1 = tres_add_task(TRES_APP_NAME, 0);
#endif
    t1->pf_img = usrlib_img;
    /* loopback(3) and child node(4) */
    task_is_add(t1, subscription_uri);
    //task_is_add(t1, NODE_URI_4);
    PRINTFLN("Node 0x%02X configured, %s", node_id, subscription_uri);
  }
  PRINTFLN("Static config done, ID= %u", node_id);
}

static void static_test_start(){
  if ( node_id != 4){ //check node 4 function in static-test-config.h
    PRINTFLN("Static tres start");
    tres_start_monitoring(t1);
    PRINTFLN("Node %02X started", node_id);
  }
}

static void static_test_stop(){
  tres_stop_monitoring(t1);
}
#endif //T_RES_STATIC_CONF


PROCESS_THREAD(pyot_keepalive, ev, data) {
  PROCESS_BEGIN();
  SERVER_NODE(&server_ipaddr);
#if PYOT_KEEPALIVE
  static struct etimer pt;
  static coap_packet_t request[1]; /* This way the packet can be treated as pointer as usual. */
  
  static long unsigned int time=0;
  static char content[12];

  int base_wait = 4;

  etimer_set(&pt, (getRandUint(10) + base_wait) * CLOCK_SECOND);

  while(1) {
    PROCESS_YIELD();
    if (etimer_expired(&pt)) break;
  }
  etimer_reset(&pt);
  etimer_set(&pt, KEEPALIVE_PERIOD * CLOCK_SECOND);
  while(1) {
    PROCESS_YIELD();
    if (etimer_expired(&pt)) {
      PRINTF("Sending msg to rd...");

#if  !PYOT_KEEPALIVE_CONFIRMABLE
      coap_init_message(request, COAP_TYPE_NON, COAP_POST, 0);
      coap_set_header_uri_path(request, "/rd");
      coap_set_payload(request, content, snprintf(content, sizeof(content), "%lu", time++));
      coap_transaction_t *transaction;

      request->mid = coap_get_mid();
      if ((transaction = coap_new_transaction(request->mid, &server_ipaddr, REMOTE_RD_PORT)))
      {
        transaction->packet_len = coap_serialize_message(request, transaction->packet);
        coap_send_transaction(transaction);
        //coap_clear_transaction(transaction);
      }
      PRINTF("\n");
#else
      coap_init_message(request, COAP_TYPE_CON, COAP_POST, 0);
      coap_set_header_uri_path(request, "/rd");
      coap_set_payload(request, content, snprintf(content, sizeof(content), "%lu", time++));
      PRINT6ADDR(&server_ipaddr);
      PRINTF("\n");
      COAP_BLOCKING_REQUEST(&server_ipaddr, REMOTE_RD_PORT, request,
                            client_chunk_handler);
      PRINTF("...Done\n");
#endif //!PYOT_KEEPALIVE_CONFIRMABLE
      etimer_reset(&pt);
     }
  } /* while (1) */
#endif //PYOT_KEEPALIVE
  PROCESS_END();
}


#if ICSI_STATIC_CONF_RESOURCE

typedef enum {PREINIT, STARTED, STOPPED} taskstate_type;
taskstate_type task_state = PREINIT;

static void demo_put_handler(void* request, void* response, uint8_t *buffer,
                              uint16_t preferred_size, int32_t *offset);

RESOURCE(demo, "title=\"demo start/stop\";rt=\"Control\"", NULL, NULL, demo_put_handler, NULL);

static void demo_put_handler(void* request, void* response, uint8_t *buffer,
                              uint16_t preferred_size, int32_t *offset){

  size_t len = 0;
  const char *mode = NULL;
  uint8_t command = 0;
  if ((len=REST.get_query_variable(request, "mode", &mode)))
  {
    printf("query ");
    if (strncmp(mode, "on", len)==0)
    {
      printf("switch  on\n");
      command = 1;
    } else if (strncmp(mode, "off", len)==0) {
      printf("switch off\n");
      command = 0;

    }
  }

  switch(task_state){
    case PREINIT:{
      static_test_config();
      static_test_start();
      task_state = STARTED;
      REST.set_response_payload(response, (uint8_t *)buffer, snprintf((char *)buffer, REST_MAX_CHUNK_SIZE, "Started task"));
      break;
    }
    case STARTED:{
    	if (command == 1){
    		REST.set_response_payload(response, (uint8_t *)buffer, snprintf((char *)buffer, REST_MAX_CHUNK_SIZE, "already started"));
    		return;
    	}
      static_test_stop();
      task_state = STOPPED;
      REST.set_response_payload(response, (uint8_t *)buffer, snprintf((char *)buffer, REST_MAX_CHUNK_SIZE, "Stopped task"));
      break;
    }
    case STOPPED:{
    	if (command == 0){
    		REST.set_response_payload(response, (uint8_t *)buffer, snprintf((char *)buffer, REST_MAX_CHUNK_SIZE, "already Stopped"));
    		return;
    	}
    	static_test_start();
      task_state = STARTED;
      REST.set_response_payload(response, (uint8_t *)buffer, snprintf((char *)buffer, REST_MAX_CHUNK_SIZE, "Started task"));
      break;
    }
  }
}
#endif //ICSI_STATIC_CONF_RESOURCE

PROCESS_THREAD(icsi_net_main, ev, data) {
  PROCESS_BEGIN();
  static struct etimer et;
#ifdef __PIC32__
  node_id = SEEDEYE_ID;
  utils_pic32_init_random_seed();
#else
  srand(node_id);
#endif

  print_current_config();
  rest_init_engine();
  rplinfo_activate_resources();
  tres_init();
  tres_set_custom_out(m2m_tres_send);
  vision2net_start();
  m2m_client_init();
  utils_start();

  process_start(&pyot_keepalive, NULL);
#if ICSI_STATIC_CONF_RESOURCE
  rest_activate_resource(&demo, "demo");
#elif ICSI_STATIC_CONF
  static_test_config();
  /* wait some seconds and then configure t-res tasks */
  etimer_set(&et, CLOCK_SECOND *10 + (CLOCK_SECOND * getRandUint(30)));
  while(1) {
    PROCESS_YIELD();
    if (etimer_expired(&et)) {
      break;
    }
  }
  static_test_start();
#endif //ICSI_STATIC_CONF_RESOURCE
  etimer_reset(&et);
  etimer_set(&et, CLOCK_SECOND);
  while(1) {
    PROCESS_YIELD();
    if (etimer_expired(&et)) {
      leds_toggle(LEDS_RED);
      etimer_reset(&et);
    }
  }
  PROCESS_END();
}
