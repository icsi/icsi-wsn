#ifndef __STATIC_TEST_CONFIG_H__
#define __STATIC_TEST_CONFIG_H__

/*
 *    Static configuration for Cooja tests
 *
 *            COAP2HTTP PROXY
 *                |
 *           BORDER-ROUTER
 *                |
 *     ----------------------
 *     |                    |
 *   NODE2                NODE3
 *                          |
 *                        NODE4
 *
 *
 * T-Res Nodes: Node2, Node3, Node 4
 *
 * Node2 sends the notifications directly to the proxy
 *
 * Node3 aggregates the measurements coming from Node3 (localhost) and Node4.
 *
 *
 *
 * */

#ifdef __PIC32__
#define EMULATE_VISION_BOARD      0
#else  //cooja
#define EMULATE_VISION_BOARD      1
#endif //__PIC32__


/* When active a "/demo" resource will be active, allowing to turn on/off the
 * monitoring */
#ifndef ICSI_STATIC_CONF_RESOURCE
#define ICSI_STATIC_CONF_RESOURCE 	  1
#endif


/* When active nodes will be configured using a statically defined configuration
 * That means that a T-Res task will be automatically configured using a
 * pre-defined processing function. For nodes 2,3,4 the configuration is
 * depicted above. All the other nodes will be configured to only monitor their
 * local "/output" resource. */
#define ICSI_STATIC_CONF              1


/* Possibly override the emulation configuration when the Seed-eye is not
 * connected to the vision board */
//#undef EMULATE_VISION_BOARD
//#define EMULATE_VISION_BOARD 					1

#define NODE_URI_2    "<coap://[aaaa::202:2:2:2]/output>"
#define NODE_URI_3    "<coap://[aaaa::203:3:3:3]/output>"
#define NODE_URI_4    "<coap://[aaaa::204:4:4:4]/output>"

#endif /* __STATIC_TEST_CONFIG_H__ */
