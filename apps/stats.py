#!/usr/bin/python

import re
import time
from pandas import Series
from scipy import stats
results = 'results-profile2.txt'

def main():
  proxyTime = []
  httpTime = []
  try:
    with open(results, 'r') as res:
      #print
      #print 'Processing ', filename
      allLines = res.readlines()  
      for line in allLines:
        t1 = line.split()[1]
        t2 = line.split()[2]        
        httpTime.append(int(t1))
        proxyTime.append(int(t2))        
     
    print len(httpTime), len(proxyTime)

    httpdelaySeries = Series(httpTime) 
    proxydelaySeries = Series(proxyTime)     
      
    print httpdelaySeries.describe() 
    print stats.sem(httpdelaySeries)    
    print proxydelaySeries.describe()
    print stats.sem(proxydelaySeries)
                
  except Exception as e: 
    print 'error', e    
    

if __name__ == "__main__":
  main()
