/*
 * endianess_converter.h
 *
 *  Created on: May 26, 2010
 *      Author: Daniele Alessandrelli
 */

#ifndef ENDIANESS_CONVERTER_H_
#define ENDIANESS_CONVERTER_H_

#define BIG_ENDIAN_NETWORK
/******************************************************************************/
/*				HTON and NTOH				      */
/******************************************************************************/
#if defined(BIG_ENDIAN_NETWORK)
#	define ntouint32(nuint32) betouint32( nuint32)
#	define ntoint32(nint32) betoint32( nint32)
#	define uint32ton(uint32_val, nuint32) uint32tobe(uint32_val, nuint32);
#	define int32ton(int32_val, nint32) int32tobe(int32_val, nint32);
#	define ntouint16(nuint16) betouint16( nuint16)
#	define uint16ton(uint16_val, nuint16) uint16tobe(uint16_val, nuint16);
#elif defined(LITTLE_ENDIAN_NETWORK)
#	define ntouint32(nuint32) letouint32( nuint32)
#	define uint32ton(uint32_val, nuint32) uint32tole(uint32_val, nuint32);

#	define ntoint32(nint32) letoint32( nint32)
#	define int32ton(int32_val, nint32) int32tole(int32_val, nint32);

#	define ntouint16(nuint16) letouint16( nuint16)
#	define uint16ton(uint16_val, nuint16) uint16tole(uint16_val, nuint16);
#else
#	error "You must specify network endianess"
#endif

/******************************************************************************/
/*				LITTLE ENDIAN				      */
/******************************************************************************/

static inline uint32_t letouint32(const uint8_t *leuint32)
{
	uint32_t uint32_val;
	uint32_val = leuint32[0];
	uint32_val += ((uint32_t) leuint32[1]) << 8;
	uint32_val += ((uint32_t) leuint32[2]) << 16;
	uint32_val += ((uint32_t) leuint32[3]) << 24;
	return uint32_val;
}

static inline void uint32tole(uint32_t uint32_val, uint8_t *leuint32)
{
	leuint32[0] = uint32_val & 0xFF;
	leuint32[1] = (uint32_val >> 8) & 0xFF;
	leuint32[2] = (uint32_val >> 16) & 0xFF;
	leuint32[3] = (uint32_val >> 24) & 0xFF;
}

static inline uint16_t letouint16(const uint8_t *leuint16)
{
	uint16_t uint16_val;
	uint16_val = leuint16[0];
	uint16_val += ((uint16_t) leuint16[1]) << 8;
	return uint16_val;
}

static inline void uint16tole(uint16_t uint16_val, uint8_t *leuint16)
{
	leuint16[0] = uint16_val & 0xFF;
	leuint16[1] = (uint16_val >> 8) & 0xFF;
}

/******************************************************************************/
/*				BIG ENDIAN				      */
/******************************************************************************/

static inline uint32_t betouint32(const uint8_t *beuint32)
{
	uint32_t uint32_val;
	uint32_val = beuint32[3];
	uint32_val += ((uint32_t) beuint32[2]) << 8;
	uint32_val += ((uint32_t) beuint32[1]) << 16;
	uint32_val += ((uint32_t) beuint32[0]) << 24;
	return uint32_val;
}

static inline void uint32tobe(uint32_t uint32_val, uint8_t *beuint32)
{
	beuint32[3] = uint32_val & 0xFF;
	beuint32[2] = (uint32_val >> 8) & 0xFF;
	beuint32[1] = (uint32_val >> 16) & 0xFF;
	beuint32[0] = (uint32_val >> 24) & 0xFF;
}

static inline uint16_t betouint16(const uint8_t *beuint16)
{
	uint16_t uint16_val;
	uint16_val = beuint16[1];
	uint16_val += ((uint16_t) beuint16[0]) << 8;
	return uint16_val;
}

static inline void uint16tobe(uint16_t uint16_val, uint8_t *beuint16)
{
	beuint16[1] = uint16_val & 0xFF;
	beuint16[0] = (uint16_val >> 8) & 0xFF;
}

#endif /* ENDIANESS_CONVERTER_H_ */
