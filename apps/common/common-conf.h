/*
 * Copyright (c) 2013, Real-Time Systems laboratory, Sucola Superiore Sant'Anna
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#ifndef __COMMON_CONF_H__
#define __COMMON_CONF_H__

/******************************************************************************/
/*                     ICSI configuration                                     */
/******************************************************************************/

#include "nodeid.h"

#ifdef RF_CHANNEL
#undef RF_CHANNEL
#endif /* RF_CHANNEL */
#define RF_CHANNEL 26

#ifdef CC2420_CONF_CHANNEL
#undef CC2420_CONF_CHANNEL
#endif
#define CC2420_CONF_CHANNEL 26

/* Use csma/ca */
#undef NETSTACK_CONF_MAC
#define NETSTACK_CONF_MAC     csma_driver
//#define NETSTACK_CONF_MAC     nullmac_driver

/* No RDC */
#undef NETSTACK_CONF_RDC
#define NETSTACK_CONF_RDC     nullrdc_driver
//#define NETSTACK_CONF_RDC_CHANNEL_CHECK_RATE 8

/* Disable ACK mechanism */
#undef NULLRDC_CONF_802154_AUTOACK
#define NULLRDC_CONF_802154_AUTOACK 0

#undef CC2520_CONF_AUTOACK
#define CC2520_CONF_AUTOACK 0

/* include support 6lowpan fragmentation */
#undef SICSLOWPAN_CONF_FRAG
#define SICSLOWPAN_CONF_FRAG	1

/* compress all ipv6 packets */
#undef SICSLOWPAN_CONF_COMPRESSION_THRESHOLD
#define SICSLOWPAN_CONF_COMPRESSION_THRESHOLD 0

#undef UIP_CONF_DS6_NBR_NBU
#define UIP_CONF_DS6_NBR_NBU     15
#undef UIP_CONF_MAX_ROUTES
#define UIP_CONF_MAX_ROUTES   15

/* Save some memory for the sky platform. */
#undef UIP_CONF_DS6_NBR_NBU
#define UIP_CONF_DS6_NBR_NBU     14
#undef UIP_CONF_DS6_ROUTE_NBU
#define UIP_CONF_DS6_ROUTE_NBU   14

#undef UIP_CONF_ND6_SEND_NA
#define UIP_CONF_ND6_SEND_NA		1

#undef UIP_CONF_TCP
#define UIP_CONF_TCP                   0

#define SKIP_WAIT_PREFIX_TUNSLIP       0

#undef TCPIP_CONF_ANNOTATE_TRANSMISSIONS
#define TCPIP_CONF_ANNOTATE_TRANSMISSIONS 0


/* The T-Res supervisor process renews the state of a T-Res task periodically.
 *
 * The local task subscribes to the input resources. If a node hosting a remote
 * resource crashes and resets it loses the state of the subscription and stops
 * sending events. The supervisor process periodically cancels and renews the
 * subscriptions to address this issue.
 *
 * NOTE: If the period is too long and a node crashes at the beginning of the
 * period it may happen that notifications remain silent for the whole period.
 * If the period is too short some notifications may be lost in the interval
 * between cancellation and renewal.
 *  */
#define TRES_SUPERVISOR_PROCESS         1
#define TRES_SUPERVISOR_PERIOD					3600
#define TRES_SUPERVISOR_RESTART_DELAY   5

#endif /* __COMMON_CONF_H__ */
