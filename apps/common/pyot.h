#define PPCAT_NX(A, B) A ## B

/*
 * Concatenate preprocessor tokens A and B after macro-expanding them.
 */
#define PPCAT(A, B) PPCAT_NX(A, B)

#include "prefix.h"
/* pyot server configuration address/port/uri */
#define HEXPREFIX PPCAT(0x, PREFIX)
#define SERVER_NODE(ipaddr)   uip_ip6addr(ipaddr, HEXPREFIX, 0, 0, 0, 0, 0, 0, 2) /* rd server */
#define REMOTE_RD_PORT        UIP_HTONS(5757)
#define RD_SERVER_URI      "/rd"


/* PyoT keepalive. keepalives are used to register and keep updated the list
 * of connected hosts. Non confirmable messages to be preferred (less overhead,
 * no transactions are used and a single loss of a KA message is not enough
 * to remove a node from the list of the active nodes). */
#define KEEPALIVE_PERIOD    60

#define BASE_WAITING        5
#define MAX_WAITING         5

#define PYOT_KEEPALIVE      1
#define PYOT_KEEPALIVE_CONFIRMABLE     0
