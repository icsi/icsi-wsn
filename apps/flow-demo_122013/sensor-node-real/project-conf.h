#ifndef __PROJECT_TRES_EXAMPLE_CONF_H__
#define __PROJECT_TRES_EXAMPLE_CONF_H__

#include "common-conf.h"

/******************************************************************************/
/*                       T-Res example 6LoWPAN settings                       */
/******************************************************************************/

#undef UIP_CONF_LOGGING
#define UIP_CONF_LOGGING 0

/* Save some memory for the sky platform. */
#undef UIP_CONF_DS6_NBR_NBU
#define UIP_CONF_DS6_NBR_NBU     5
#undef UIP_CONF_DS6_ROUTE_NBU
#define UIP_CONF_DS6_ROUTE_NBU   5

/* Increase rpl-border-router IP-buffer when using 128. */
#ifndef REST_MAX_CHUNK_SIZE
#define REST_MAX_CHUNK_SIZE    64
#endif

// the period of the fake sensor
#define TRES_EXAMPLE_SENSOR_PERIOD 6

#define TRES_EXAMPLE_CONF_RANDOM_SENSOR_VALUE 0

#endif /* __PROJECT_TRES_EXAMPLE_CONF_H__ */
