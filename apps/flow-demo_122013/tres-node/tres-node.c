/*
 * Copyright (c) 2013, Real-Time Systems laboratory, Sucola Superiore Sant'Anna
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

/**
 * \author
 *      Daniele Alessandrelli - <d.alessandrelli@sssup.it>
 */

#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#include "contiki.h"
#include "node-id.h"
#include "erbium.h"
#include "tres.h"
#include "pm.h"

/*----------------------------------------------------------------------------*/
#if !UIP_CONF_IPV6_RPL                       \
    && !defined (CONTIKI_TARGET_MINIMAL_NET) \
    && !defined (CONTIKI_TARGET_NATIVE)
#warning "Compiling with static routing!"
#include "static-routing.h"
#endif

/*----------------------------------------------------------------------------*/
/*                               Extern variables                             */
/*----------------------------------------------------------------------------*/
uint8_t tres_start_monitoring(tres_res_t *tres);
uint8_t tres_stop_monitoring(tres_res_t *tres);
void task_is_add(tres_res_t *task, char *str);
void task_od_set(tres_res_t *task, char *str);

/*----------------------------------------------------------------------------*/
PROCESS(tres_process, "T-Res Evaluation");

AUTOSTART_PROCESSES(&tres_process);

/*----------------------------------------------------------------------------*/
/*                          Fake Actuator Resoruce                            */
/*----------------------------------------------------------------------------*/
RESOURCE(actuator, METHOD_GET | METHOD_POST | METHOD_PUT, "actuator",
         "title=\"A fake generic actuator\";rt=\"Text\"");

/*----------------------------------------------------------------------------*/
void
actuator_handler(void *request, void *response, uint8_t *buffer,
                 uint16_t preferred_size, int32_t *offset)
{
  static char setpoint[10];
  static uint8_t last_token_len = 0;
  static uint8_t last_token[COAP_TOKEN_LEN];
  const uint8_t *token;
  uint16_t len;
  rest_resource_flags_t method;
  const uint8_t *ptr;
  int i;

  // Filter duplicated messages
  // FIXME: token option is not enough, we must check also the client ip address
  len = coap_get_header_token(request, &token);
  if(last_token_len == len) {
    for(i = 0; i < len && token[i] == last_token[i]; i++);
    if(i == len) {
      // duplicated message
      return;
    }
  }
  last_token_len = len;
  for(i = 0; i < len; i++) {
    last_token[i] = token[i];
  }

  method = REST.get_method_type(request);
  if(method == METHOD_GET) {
    len = strlen(setpoint);
    memcpy(buffer, setpoint, len);
    REST.set_header_content_type(response, REST.type.TEXT_PLAIN);
    REST.set_response_payload(response, buffer, len);
  } else {                      // it's a put/post request
    len = REST.get_request_payload(request, &ptr);
    printf("A: %s\n", (char *)ptr);
    if(len > 9) {
      len = 9;
    }
    memcpy(setpoint, ptr, len);
    setpoint[len] = '\0';
  }
}

/*----------------------------------------------------------------------------*/
PROCESS_THREAD(tres_process, ev, data)
{
  PROCESS_BEGIN();

  srand(node_id);
  rest_init_engine();
  tres_init();
  rest_activate_resource(&resource_actuator);
  PROCESS_END();
}
