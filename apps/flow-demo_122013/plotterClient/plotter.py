'''
Created on Nov 26, 2013

@author: andrea
'''
import subprocess, os, signal
import time
import numpy as np
import matplotlib.pyplot as plt

RX_TIMEOUT = 5
COAP_PATH = '../../../libcoap-4.0.1/examples/'
COAP_CLIENT = COAP_PATH + 'coap-client'
DEFAULT_OBS_TIMEOUT = 60
allowedMethods = ['get','post','put','delete']
TERM_CODE = '0.00'
DEFAULT_URI = "coap://[aaaa::200:0:0:2]:5683/tasks/flow/lo"


fig=plt.figure()
#plt.axis([0,1000,0,1])


x=list()
y=list()

plt.ion()
plt.show()
plt.xlim([0, DEFAULT_OBS_TIMEOUT])

def coapRequest(method, uri, payload=None, timeout=None, observe=False, duration=60):
    if method not in allowedMethods:
        raise Exception('Method not allowed')
    if observe and method is not 'get':
        raise Exception('Observe works only with -get- method')

    print method + ' ' + uri
    
    req = COAP_CLIENT + ' -m '+  method

    if timeout:
        req = req + '  -B ' + str(timeout)
    if observe:
        req = req + ' -s ' + str(duration)
    if payload:
        req = req + ' e ' + payload

    req = req + ' ' + uri

    p = subprocess.Popen([req], 
                         stdin=subprocess.PIPE, 
                         stdout=subprocess.PIPE, 
                         shell=True)    
    return p

def parseResponse(response):
    code = response[0:4]  
    if code != '2.05':
        return code, ''
    else:
        return code, response[5:]
    
def isTermCode(response):
    if response[0:4] == TERM_CODE:
        return True
    else:
        return False
    
def coapObserve(uri = DEFAULT_URI, payload = None, timeout= None, duration = DEFAULT_OBS_TIMEOUT):
    try:
        p = coapRequest('get', uri, payload, observe=True, duration=duration)
        i=0
        while True:
            response = p.stdout.readline()
            if isTermCode(response):
                break 

            code, m = parseResponse(response)
            m = m.rstrip() 
            if m.isdigit(): 
                print 'Observe update from ' + uri + ': ' + m
                x.append(i)
                y.append(int(m))
                e = plt.plot(x, y, color='b',marker='o')
                i+=1
                plt.draw()

        print 'subscription ended...'                
                             
    except Exception, exc:
        print 'Exception COAP SERVER %s' % exc


    

if __name__ == '__main__':
   
    coapObserve()
