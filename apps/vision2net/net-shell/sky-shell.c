/**
 * \file
 *         TODO
 * \author
 *         TODO
 */

#include "contiki.h"
#include "contiki-net.h"
#include "dev/serial-line.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pic32_uart.h>
#include "dev/slip.h"

#include "endianess_converter.h"
#include "er-coap.h"
#include "er-coap-engine.h"


#define DEBUG 1
#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#define PRINT6ADDR(addr) printf("[%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x]", ((uint8_t *)addr)[0], ((uint8_t *)addr)[1], ((uint8_t *)addr)[2], ((uint8_t *)addr)[3], ((uint8_t *)addr)[4], ((uint8_t *)addr)[5], ((uint8_t *)addr)[6], ((uint8_t *)addr)[7], ((uint8_t *)addr)[8], ((uint8_t *)addr)[9], ((uint8_t *)addr)[10], ((uint8_t *)addr)[11], ((uint8_t *)addr)[12], ((uint8_t *)addr)[13], ((uint8_t *)addr)[14], ((uint8_t *)addr)[15])
#define PRINTLLADDR(lladdr) printf("[%02x:%02x:%02x:%02x:%02x:%02x]",(lladdr)->addr[0], (lladdr)->addr[1], (lladdr)->addr[2], (lladdr)->addr[3],(lladdr)->addr[4], (lladdr)->addr[5])
#else
#define PRINTF(...)
#define PRINT6ADDR(addr)
#define PRINTLLADDR(addr)
#endif


#define QUERY_PERIOD                   5
//static struct etimer et;


#define STATUS_SIZE 4
static struct etimer et;

int8_t my_uart1A_read_byte(uint8_t *data)
{
	/* Polling mode */
	if (U1ASTAbits.OERR) {
		U1ASTAbits.OERR = 0;
		return -2;
	}
	if (U1ASTAbits.URXDA) {
		*data = U1ARXREG & 0x00FF;
		return 0;
	}
	return -1;
}

/*
 * Enable interrupts
 */
void EE_pic32_enableIRQ(void)
{
	ASM_EN_INT;
}

/*
 * Disable interrupts
 */
void EE_pic32_disableIRQ(void)
{
	ASM_DIS_INT;
}

void vision_serial_init(void)
{
  pic32_uart1A_init(115200, 0);
  IEC0CLR = _IEC0_U1AEIE_MASK | _IEC0_U1ATXIE_MASK | _IEC0_U1ARXIE_MASK;   
  IFS0CLR = _IFS0_U1AEIF_MASK | _IFS0_U1ATXIF_MASK | _IFS0_U1ARXIF_MASK;  
}

void vision_serial_read(uint8_t *len, int8_t max_len, uint8_t buffer[])
{
  uint8_t c;
  uint8_t i; 
  int8_t ret;
  EE_pic32_disableIRQ();
  for (;;){
    ret = my_uart1A_read_byte(&c);
    if (ret == 0) {
        break;
    }
  }
  //TODO: check max len
  for(i = 0; i < c; i++){
  
    for (;;){
      ret = my_uart1A_read_byte(&buffer[i]);
      if (ret == 0) {
          break;
      }
    }    
  }
  EE_pic32_enableIRQ();
  *len = c;
}

void vision_serial_write(char buffer[], int8_t len)
{
  int i;
  for (i = 0; i < len ; i++){
    pic32_uart1A_write(buffer[i]);
  }
}


#define REQ_IMG                 'i'
#define REQ_ROI_GET             'r'
#define REQ_ROI_PUT             'p'
#define REQ_STATUS              's'
#define REQ_OUTPUT              'o'
#define REQ_BATTERY             'b'

#define COPPER_WORKAROUND 1//TODO: check if actually needed

/*
 * For data larger than REST_MAX_CHUKs_SIZE (e.g., stored in flash) resources must be aware of the buffer limitation
 * and split their responses by themselves. To transfer the complete resource through a TCP stream or CoAP's blockwise transfer,
 * the byte offset where to continue is provided to the handler as int32_t pointer.
 * These chunk-wise resources must set the offset value to its new position or -1 of the end is reached.
 * (The offset for CoAP's blockwise transfer can go up to 2'147'481'600 = ~2047 M for block size 2048 (reduced to 1024 in observe-03.)
 */

void
img_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);

RESOURCE(img, "title=\"Image\";rt=\"Data\"", img_handler, NULL, NULL, NULL);

#define CHUNKS_TOTAL    100000

#define MORE_CONTINUE       0x00
#define MORE_STOP       0x01

void
img_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  PRINTF("img handler\n");
  /* Check the offset for boundaries of the resource data. */
  if (*offset>=CHUNKS_TOTAL)
  {
    REST.set_response_status(response, REST.status.BAD_OPTION);
    /* A block error message should not exceed the minimum block size (16). */

    const char *error_msg = "BlockOutOfScope";
    REST.set_response_payload(response, error_msg, strlen(error_msg));
    return;
  }

  uint8_t serial_buffer [100];
  uint8_t len;
  
  char msg [20];  
  uint8_t idx = 0;
  msg[idx] = REQ_IMG;
  idx += 1;
  uint32ton(*offset, (uint8_t *)&msg[idx]);
  idx += 4; 

  PRINTF("writing req...%d\n", idx);  
  vision_serial_write(msg, idx);
  vision_serial_read(&len, 100, serial_buffer);
  
  PRINTF("finished reading serial %d len \n", len);
  serial_buffer[len+1] = '\0';

  uint8_t actual_msg_len = len-1;
  
  memcpy(buffer, &serial_buffer[1], len);
  REST.set_response_payload(response, buffer, actual_msg_len);

  /* IMPORTANT for chunk-wise resources: Signal chunk awareness to REST engine. */
  *offset += actual_msg_len;

  /* Signal end of resource representation. */
  if (serial_buffer[0] == MORE_STOP)  
  {
    *offset = -1;
  }
}


#define MAX_SERIAL_UP_COMMAND REST_MAX_CHUNK_SIZE + 10

void
roi_get_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);

void
roi_post_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);

RESOURCE(roi, "title=\"ImageRoi\";rt=\"Data\"", roi_get_handler, roi_post_handler, NULL, NULL);


void
roi_get_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  PRINTF("roi get handler\n");

  uint8_t serial_buffer [100];
  uint8_t len;
  char msg [MAX_SERIAL_UP_COMMAND];
  uint8_t idx;

  /* Check the offset for boundaries of the resource data. */
  if (*offset>=CHUNKS_TOTAL)
  {
	REST.set_response_status(response, REST.status.BAD_OPTION);
	/* A block error message should not exceed the minimum block size (16). */

	const char *error_msg = "BlockOutOfScope";
	REST.set_response_payload(response, error_msg, strlen(error_msg));
	return;
  }

  idx = 0;
  msg[idx] = REQ_ROI_GET;
  idx += 1;
  uint32ton(*offset, (uint8_t *)&msg[idx]);
  idx += 4;

  PRINTF("writing req...%d\n", idx);
  vision_serial_write(msg, idx);
  //PRINTF("reading reply...\n");
  vision_serial_read(&len, 100, serial_buffer);

  PRINTF("finished reading serial %d len \n", len);
  serial_buffer[len+1] = '\0';

  uint8_t actual_msg_len = len-1;
  //snprintf((char *)buffer, len, (const char *)&serial_buffer[1]);

  memcpy(buffer, &serial_buffer[1], len);
  REST.set_response_payload(response, buffer, actual_msg_len);

  /* IMPORTANT for chunk-wise resources: Signal chunk awareness to REST engine. */
  *offset += actual_msg_len;

  /* Signal end of resource representation. */
  if (serial_buffer[0] == MORE_STOP)
  {
	*offset = -1;
  }
  REST.set_header_content_type(response, REST.type.APPLICATION_JSON);
}

void
roi_post_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  PRINTF("roi post handler\n");

  uint32_t num = 0;
  uint8_t more = 0;
  uint16_t size = 0;
  uint32_t b1_offset = 0;
  uint16_t plen;
  const uint8_t *payload;
  uint8_t serial_buffer [100];
  uint8_t len;
  char msg [MAX_SERIAL_UP_COMMAND];
  uint8_t idx;

  if(coap_get_header_block1(request, &num, &more, &size, &b1_offset)) {
	coap_set_header_block1(response, num, more, size);
  }

  PRINTF("Request on /pf: num = %"PRIu32", more = %"PRIu8", size = %"PRIu16
	  ", offset = %"PRIu32"\n", num, more, size, b1_offset);
  // if it's the first packet, stop input resource monitoring
  if(b1_offset == 0) {
	PRINTF("starting new upload\n");
  }
  plen = coap_get_payload(request, &payload);
  PRINTF("Payload len: %d\n", plen);
#if COPPER_WORKAROUND
  // if no block-wise transfer and payload len equal to maximum size, then
  // we probably need block wise transfer. FIXME: that may be not true
  if(size == 0 && plen == REST_MAX_CHUNK_SIZE) {
	more = 1;
	size = REST_MAX_CHUNK_SIZE;
	coap_set_header_block1(response, num, more, size);
  }
#endif
  // if it's the first packet, allocate a memory slot
  if(b1_offset == 0) {
	PRINTF("first block\n");
  }

  idx = 0;
  msg[idx] = REQ_ROI_PUT;
  idx += 1;
  msg[idx] = plen;
  idx += 1;
  msg[idx] = more;
  idx += 1;
  uint32ton(b1_offset, (uint8_t *)&msg[idx]);
  idx += 4;

  memcpy((uint8_t *)&msg[idx], payload, plen);
  idx += plen;

  PRINTF("writing req...%d\n", idx);
  vision_serial_write(msg, idx);
  //PRINTF("reading reply...\n");
  vision_serial_read(&len, 100, serial_buffer);

  PRINTF("finished reading serial %d len \n", len);

  // if it's the final block, finalize the memory slot
  if(more == 0) {
	REST.set_response_status(response, REST.status.CHANGED);
  }
}

int algo_check_period = 30;
void period_post_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
void period_get_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);

RESOURCE(period, "title=\"algo rate\";rt=\"Control\"", period_get_handler, period_post_handler, NULL, NULL);

void period_get_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset){
	snprintf((char *)buffer, REST_MAX_CHUNK_SIZE, "%d", algo_check_period);
	REST.set_response_payload(response, buffer, strlen((char *)buffer));
}

void period_post_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset){
	uint8_t rcv_buffer[100];
	int8_t len;
	uint8_t *incoming = NULL;

	len = REST.get_request_payload(request, (const uint8_t **) &incoming);
	memcpy(rcv_buffer, incoming, len);
	rcv_buffer[len] = '\0';
	PRINTF("received %s", rcv_buffer);
	if (len > 0){
	  algo_check_period = atoi((const char *)rcv_buffer);
	}
}


void status_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
RESOURCE(status, "title=\"VB status\";rt=\"Info\"", status_handler, NULL, NULL, NULL);

void
status_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  uint8_t serial_buffer [100];
  uint8_t len;
  
  char msg [20];  
  uint8_t idx = 0;
  msg[idx] = REQ_STATUS;
  idx += 1;

  PRINTF("writing req...%d\n", idx);  
  vision_serial_write(msg, idx);
  //PRINTF("reading reply...\n"); 
  vision_serial_read(&len, 100, serial_buffer);
  
  PRINTF("finished reading serial %d len \n", len);
  serial_buffer[len] = '\0';

  uint8_t actual_msg_len = len;
  //snprintf((char *)buffer, len, (const char *)&serial_buffer[1]);
  
  memcpy(buffer, serial_buffer, len);
  //PRINTF("%s\n", (const char *)&serial_buffer[1]);
  //PRINTF("%s\n", (char *)buffer);
  //PRINTF("actually %d len \n", actual_msg_len);
  REST.set_response_payload(response, buffer, actual_msg_len);
}


void battery_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);

RESOURCE(battery, "title=\"battery\";rt=\"Info\"", battery_handler, NULL, NULL, NULL);

void
battery_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  uint8_t serial_buffer [100];
  uint8_t len;
  
  char msg [20];  
  uint8_t idx = 0;
  msg[idx] = REQ_BATTERY;
  idx += 1;

  PRINTF("writing req...%d\n", idx);  
  vision_serial_write(msg, idx);
  //PRINTF("reading reply...\n"); 
  vision_serial_read(&len, 100, serial_buffer);
  
  PRINTF("finished reading serial %d len \n", len);
  serial_buffer[len] = '\0';

  uint8_t actual_msg_len = len;
  //snprintf((char *)buffer, len, (const char *)&serial_buffer[1]);
  
  memcpy(buffer, serial_buffer, len);
  //PRINTF("%s\n", (const char *)&serial_buffer[1]);
  //PRINTF("%s\n", (char *)buffer);
  //PRINTF("actually %d len \n", actual_msg_len);
  REST.set_response_payload(response, buffer, actual_msg_len);

}


/*---------------------------------------------------------------------------*/
PROCESS(init_process, "Init");
AUTOSTART_PROCESSES(&init_process);
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(init_process, ev, data)
{
  PROCESS_BEGIN();
  vision_serial_init();
  
  /* Initialize the REST engine. */
  rest_init_engine();

  rest_activate_resource(&period, "cfg/period");
  rest_activate_resource(&img,"cfg/img");
  rest_activate_resource(&status, "cfg/status");
  rest_activate_resource(&roi, "cfg/roi");
  rest_activate_resource(&battery, "cfg/battery");
  etimer_set(&et, algo_check_period * CLOCK_SECOND);

  while(1) {
    PROCESS_YIELD();
    if (etimer_expired(&et)) {
      PRINTF("--Sending msg to rd...\n");
      etimer_set(&et, algo_check_period * CLOCK_SECOND);
     }
  } /* while (1) */


  PROCESS_END();
}

