#!/usr/bin/env python
# encoding: utf-8
'''
 -- shortdesc

 This program implements the serial communication 
 protocol between the networking board (contiki-based)
 and the computer vision board (linux-based). 
 It is expected to start at boot time on the 
 cv board, waiting for requests from the net-board.


@author:     Andrea Azzara'

@copyright:  2014 SSSUP - CNIT. All rights reserved.

@license:    license

@contact:    a.azzara@sssup.it
@deffield    updated: Updated
'''

import sys
import os
import time
import serial
from optparse import OptionParser
import collections
from struct import *
import array
from algoInterface import *

__all__ = []
__version__ = 0.1
__date__ = '2014-03-11'
__updated__ = '2014-03-11'


imgFileName = '320x240.jpg'
roiFileName = 'roiFile.txt'
CHUNKSIZE = 64

# Message type
REQ_IMG = 'i'
REQ_ROI_GET = 'r'
REQ_ROI_PUT = 'p'
REQ_STATUS = 's'
REQ_OUTPUT = 'o'
REQ_BATTERY = 'b'
REQ_VISIO_RESET = 'v'
REQ_VISIO_START = 'u'
REQ_VISIO_STOP = 't'

# CoAP Blockwise transfer 'more' byte definition
MORE = pack('B', 0x00)
NOMORE = pack('B', 0x01)


def processStatus(ser):
    try:
        message = algoGetStatus()
        msglen = pack('B', len(message))
        ser.write(msglen + message)
    except Exception as e:
        print(e)


def processBattery(ser):
    try:
        message = algoGetBattery()
        msglen = pack('B', len(message))
        ser.write(msglen + message)
    except Exception as e:
        print(e)


def processOutput(ser):
    try:
        message = algoGetOutput()
        msglen = pack('B', len(message))
        ser.write(msglen + message)
    except Exception as e:
        print(e)


def processVisioReset(ser):
    try:
        message = algoVisioReset()
        msglen = pack('B', len(message))
        ser.write(msglen + message)
    except Exception as e:
        print(e)


def processVisioStart(ser):
    try:
        message = algoVisioStart()
        msglen = pack('B', len(message))
        ser.write(msglen + message)
    except Exception as e:
        print(e)


def processVisioStop(ser):
    try:
        message = algoVisioStop()
        msglen = pack('B', len(message))
        ser.write(msglen + message)
    except Exception as e:
        print(e)


def processFileDownload(ser, filename):
    try:
        f = open(filename, 'rb')
        data = f.read()  # read the entire content of the file
        bytes = len(data)

        line = ser.read(4)
        print('line = ', len(line[0:]))

        offset = unpack_from('>I', line, 0)[0]
        print('Offset=' , offset)
        f.seek(offset)

        if (bytes < offset + CHUNKSIZE):
            chunk = f.read(bytes - offset)
            m = NOMORE
        else:
            chunk = f.read(CHUNKSIZE)
            m = MORE

        print('sending ', len(chunk) + 1)
        msglen = pack('B', len(chunk) + 1)

        ser.write(msglen + m + chunk)
        f.close()

        return
    except Exception as e:
        print(e)
        # message = 'Exception'
        # length = len(message)+1
        # ser.write(MSG + NOMORE + message)


uploadedFile = {}


def processFileUpload(ser, filename):
    try:
        line = ser.read(6)
        print('line = ', len(line[0:]))

        length, more, offset = unpack_from('>BBI', line, 0)
        # print 'Offset=' , offset
        # print 'more = ', more
        # print 'len = ', length

        payload = ser.read(length)
        # print payload

        if offset == 0:
           uploadedFile.clear()
        uploadedFile[offset] = payload
        if more == 0:
            print(uploadedFile)
            od = collections.OrderedDict(sorted(uploadedFile.items()))
            with open("rec_roi.txt", 'w') as f:
                for block in od:
                    f.write(od[block])

        msglen = pack('B', 1)

        ser.write(msglen + 'p')
    except Exception as e:
        print(e)


def process(ser):
    while True:
        command = ser.read(1)
        if command == REQ_IMG:
            print('Req image')
            processFileDownload(ser, imgFileName)
        if command == REQ_ROI_GET :
            print('Req ROI GET')
            processFileDownload(ser, roiFileName)
        if command == REQ_ROI_PUT :
            print('Req ROI PUT')
            processFileUpload(ser, roiFileName)
        if command == REQ_STATUS:
            print('Req status')
            processStatus(ser)
        if command == REQ_BATTERY:
            print('Req battery')
            processBattery(ser)
        if command == REQ_OUTPUT:
            print('Req Output')
            processOutput(ser)
        if command == REQ_VISIO_RESET:
            print('Req visio reset')
            processVisioReset(ser)
        if command == REQ_VISIO_START:
            print('Req visio reset')
            processVisioStart(ser)
        if command == REQ_VISIO_STOP:
            print('Req visio reset')
            processVisioStop(ser)



def main(argv=None):
    '''Command line options.'''

    program_name = os.path.basename(sys.argv[0])
    program_version = "v0.1"
    program_build_date = "%s" % __updated__

    program_version_string = '%%prog %s (%s)' % (program_version, program_build_date)
    program_longdesc = ''''''  # optional - give further explanation about what the program does
    program_license = "Copyright 2014 Andrea Azzara' (SSSUP - CNIT)                                            \
                Licensed under the Apache License 2.0\nhttp://www.apache.org/licenses/LICENSE-2.0"

    if argv is None:
        argv = sys.argv[1:]
    try:
        # setup option parser
        parser = OptionParser(version=program_version_string, epilog=program_longdesc, description=program_license)
        parser.add_option("-s", "--serial", dest="serialPort", help="set input serial [default: %default]")
        parser.add_option("-v", "--verbose", dest="verbose", action="count", help="set verbosity level [default: %default]")

        # set defaults
        parser.set_defaults(serialPort="/dev/ttyUSB0")

        # process options
        (opts, args) = parser.parse_args(argv)

        if opts.verbose > 0:
            print("verbosity level = %d" % opts.verbose)
        if opts.serialPort:
            print("serial = %s" % opts.serialPort)

        ser = serial.Serial(opts.serialPort, 115200, timeout=None)  # open first serial port
        print('Connected with ', ser.portstr)  # check which port was really used

        process(ser)

        ser.close()

    except Exception as e:
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        sys.stderr.write(indent + "  for help use --help")
        # raise(e)
        return 2


if __name__ == "__main__":
    sys.exit(main())
