import socket
from struct import *
import array
import sys

HOSTNAME = '127.0.0.1'
PORTNO = 10101

class UdpComm():

  def __init__(self, ipAddress=HOSTNAME, port=PORTNO):
    self.s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    self.s.connect((ipAddress, port))

  def udp_send_command(self):
    COMMAND = 0x0A
    RESPONSE_SIZE = 10
    print 'sending a command on the socket (a single byte)'
    MSG = pack('B', 0x0A)
    self.s.send(MSG)

    (data, addr) = self.s.recvfrom(RESPONSE_SIZE)
    print 'received', data, 'from', addr

    # unpacking data received from server
    idx = 0
    first_char = unpack_from('c', data, idx)[0]
    idx = idx + 1
    first_int = unpack_from('>I', data, idx)[0]
    idx = idx + 4
    second_int = unpack_from('>I', data, idx)[0]
    idx = idx + 4
    last_byte = unpack_from('B', data, idx)[0]

    print first_char, first_int, second_int, last_byte

def main(argv=None):
    updSock = UdpComm()
    updSock.udp_send_command()

if __name__ == "__main__":
    sys.exit(main())
