import socket
from struct import *
import array
import sys

"""
please refer to https://docs.python.org/2/library/struct.html
"""


def main(argv=None):
    HOSTNAME = '127.0.0.1'
    PORTNO = 10101
    RESPONSE_SIZE = 10

    sock = socket.socket(socket.AF_INET,  # Internet
                         socket.SOCK_DGRAM)  # UDP
    sock.bind((HOSTNAME, PORTNO))

    while True:
        data, addr = sock.recvfrom(1)
        recv_byte = unpack_from('B', data[0])[0]
        print "received message:", recv_byte

        responseMessage = array.array('c', '\0' * RESPONSE_SIZE)

        first_char = 'y'
        first_int = 41
        second_int = 42
        last_byte = 0x41

        idx = 0

         # packing 1 char + 2 unsigned integers + 1 byte (> = big endian)
         # in the array
        pack_into('>c2IB', responseMessage, idx, first_char, first_int, second_int,
                  last_byte)
        print 'sending response, len =', len(responseMessage)
        sock.sendto(responseMessage.tostring(), addr)

if __name__ == "__main__":
    sys.exit(main())
