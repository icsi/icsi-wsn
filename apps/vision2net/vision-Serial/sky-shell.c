/**
 * \file
 *         TODO
 * \author
 *         TODO
 */

#include "contiki.h"
#include "contiki-net.h"
#include "dev/serial-line.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pic32_uart.h>
#include "dev/slip.h"

#include "erbium.h"

#if WITH_COAP == 3
#include "er-coap-03.h"
#elif WITH_COAP == 7
#include "er-coap-07.h"
#elif WITH_COAP == 12
#include "er-coap-12.h"
#elif WITH_COAP == 13
#include "er-coap-13.h"
#else
#warning "Erbium example without CoAP-specifc functionality"
#endif

#if WITH_COAP == 3
#include "er-coap-03-engine.h"
#elif WITH_COAP == 6
#include "er-coap-06-engine.h"
#elif WITH_COAP == 7
#include "er-coap-07-engine.h"
#elif WITH_COAP == 12
#include "er-coap-12-engine.h"
#elif WITH_COAP == 13
#include "er-coap-13-engine.h"
#else
#error "CoAP version defined by WITH_COAP not implemented"
#endif

PROCESS(shell_process, "Vis2Net");

#define QUERY_PERIOD                   5
static struct etimer et;


static uint8_t toggle_status = 0;
#define STATUS_SIZE 4




int8_t my_uart1A_read_byte(uint8_t *data)
{
	/* Polling mode */
	if (U1ASTAbits.OERR) {
		U1ASTAbits.OERR = 0;
		return -2;
	}
	if (U1ASTAbits.URXDA) {
		*data = U1ARXREG & 0x00FF;
		return 0;
	}
	return -1;
}

/*
 * Enable interrupts
 */
void EE_pic32_enableIRQ(void)
{
	ASM_EN_INT;
}

/*
 * Disable interrupts
 */
void EE_pic32_disableIRQ(void)
{
	ASM_DIS_INT;
}

void vision_serial_init(void)
{
  pic32_uart1A_init(115200, 0);
  IEC0CLR = _IEC0_U1AEIE_MASK | _IEC0_U1ATXIE_MASK | _IEC0_U1ARXIE_MASK;   
  IFS0CLR = _IFS0_U1AEIF_MASK | _IFS0_U1ATXIF_MASK | _IFS0_U1ARXIF_MASK;  
}

void vision_serial_read(int8_t *len, int8_t max_len, char buffer[])
{
  uint8_t c;
  uint8_t i; 
  int8_t ret;

  for (;;){
    ret = my_uart1A_read_byte(&c);
    if (ret == 0) {
        printf("%d\n", c);
        break;
    }
  }
  //TODO: check max len
  for(i = 0; i < c; i++){
  
  
    for (;;){
      ret = my_uart1A_read_byte(&buffer[i]);
      if (ret == 0) {
          break;
      }
      printf("%d", ret);
    }    
  }
  *len = c;
}

void vision_serial_write(int8_t len, char buffer[])
{
  int i;
  for (i = 0; i < len ; i++){
    pic32_uart1A_write(buffer[i]);
  }
}



/*
 * For data larger than REST_MAX_CHUKs_SIZE (e.g., stored in flash) resources must be aware of the buffer limitation
 * and split their responses by themselves. To transfer the complete resource through a TCP stream or CoAP's blockwise transfer,
 * the byte offset where to continue is provided to the handler as int32_t pointer.
 * These chunk-wise resources must set the offset value to its new position or -1 of the end is reached.
 * (The offset for CoAP's blockwise transfer can go up to 2'147'481'600 = ~2047 M for block size 2048 (reduced to 1024 in observe-03.)
 */
RESOURCE(chunks, METHOD_GET, "test/chunks", "title=\"Blockwise demo\";rt=\"Data\"");

#define CHUNKS_TOTAL    100000

#define MORE_CONTINUE       0x00
#define MORE_STOP       0x01

void
chunks_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  int32_t strpos = 0;

  /* Check the offset for boundaries of the resource data. */
  if (*offset>=CHUNKS_TOTAL)
  {
    REST.set_response_status(response, REST.status.BAD_OPTION);
    /* A block error message should not exceed the minimum block size (16). */

    const char *error_msg = "BlockOutOfScope";
    REST.set_response_payload(response, error_msg, strlen(error_msg));
    return;
  }

  char serial_buffer [100];
  int8_t len;
  
  char * msg = "ciao\r\n";  

  vision_serial_write(strlen(msg), msg);

  vision_serial_read(&len, 100, serial_buffer);
  
  printf("finished reading serial\n");
  serial_buffer[len] = '\0';

  uint8_t actual_msg_len = len-1;
  snprintf((char *)buffer, actual_msg_len, &serial_buffer[1]);

  REST.set_response_payload(response, buffer, actual_msg_len);

  /* IMPORTANT for chunk-wise resources: Signal chunk awareness to REST engine. */
  *offset += preferred_size;

  /* Signal end of resource representation. */
  if (serial_buffer[0] == MORE_STOP)  
  {
    *offset = -1;
  }
}







/* A simple actuator example. Toggles the red led */
RESOURCE(toggle, METHOD_GET, "actuators/toggle", "title=\"Red LED\";rt=\"Control\"");
void
toggle_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{

  rest_resource_flags_t method = REST.get_method_type(request);
  
  char serial_buffer [100];
  int8_t len;
  
  char * msg = "ciao\r\n";  

  switch(method){
  case METHOD_GET:{
    vision_serial_write(strlen(msg), msg);
    
    vision_serial_read(&len, 100, serial_buffer);
    
    serial_buffer[len] = '\0';
    //printf(buffer);
  
    snprintf((char *)buffer, REST_MAX_CHUNK_SIZE, "%s", serial_buffer);
	  REST.set_response_payload(response, buffer, strlen((char *)buffer));
	  break;
  
  }
  default:
	  REST.set_response_status(response, REST.status.BAD_REQUEST);
	  break;
  }

}

/*---------------------------------------------------------------------------*/
PROCESS(init_process, "Init");
AUTOSTART_PROCESSES(&init_process);
/*---------------------------------------------------------------------------*/





/*---------------------------------------------------------------------------*/
PROCESS_THREAD(init_process, ev, data)
{
  PROCESS_BEGIN();
  vision_serial_init();
  
  /* Initialize the REST engine. */
  rest_init_engine();

  rest_activate_resource(&resource_toggle);
  rest_activate_resource(&resource_chunks);  
  PROCESS_END();
}

/*---------------------------------------------------------------------------*/

PROCESS_THREAD(shell_process, ev, data)
{
  PROCESS_BEGIN();

  printf("Send me a message!\n");
  
  void* response = data;
  uint8_t buffer[10];
 
  PROCESS_YIELD();
  if(ev == serial_line_event_message) {
    printf("received line: %s\n", (char *)data);
    snprintf((char *)buffer, REST_MAX_CHUNK_SIZE, "%s", (char *)data);
	  REST.set_response_payload(response, buffer, strlen((char *)buffer));
  }
  
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
