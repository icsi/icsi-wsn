/*
 * Copyright (c) 2015, Francesca Pacini, Andrea Azzara'
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.

 */

/**
 * \file
 *
 * \author
 *      Francesca Pacini <fkpacini@gmail.com>
 *      Andrea Azzara'  <a.azzara@sssup.it>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "client-m2m.h"

#include "exiDecode.h"
#include "m2mBuild.h"

#include "contiki.h"
#include "contiki-net.h"
#include "dev/button-sensor.h"
#include "er-coap.h"
#include "er-coap-engine.h"
#include "node-id.h"
//#include "uart1.h"
//#include "dev/serial-line.h"

#define DEBUG 1

#undef PRINTF
#undef PRINT6ADDR
#undef PRINTLLADDR
#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#define PRINT6ADDR(addr) PRINTF("[%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x]", ((uint8_t *)addr)[0], ((uint8_t *)addr)[1], ((uint8_t *)addr)[2], ((uint8_t *)addr)[3], ((uint8_t *)addr)[4], ((uint8_t *)addr)[5], ((uint8_t *)addr)[6], ((uint8_t *)addr)[7], ((uint8_t *)addr)[8], ((uint8_t *)addr)[9], ((uint8_t *)addr)[10], ((uint8_t *)addr)[11], ((uint8_t *)addr)[12], ((uint8_t *)addr)[13], ((uint8_t *)addr)[14], ((uint8_t *)addr)[15])
#define PRINTLLADDR(lladdr) PRINTF("[%02x:%02x:%02x:%02x:%02x:%02x]",(lladdr)->addr[0], (lladdr)->addr[1], (lladdr)->addr[2], (lladdr)->addr[3],(lladdr)->addr[4], (lladdr)->addr[5])
#else
#define PRINTF(...)
#define PRINT6ADDR(addr)
#define PRINTLLADDR(addr)
#endif

#define SERVER_NODE(ipaddr)   uip_ip6addr(ipaddr, 0xaaaa, 0, 0, 0, 0, 0, 0, 1)

#define LOCAL_PORT      UIP_HTONS(COAP_DEFAULT_PORT+1)
#define REMOTE_PORT     UIP_HTONS(COAP_DEFAULT_PORT)

#define TOGGLE_INTERVAL 10

PROCESS(coap_client_example, "COAP Client Example");
AUTOSTART_PROCESSES(&coap_client_example);

uint32_t structure_dim;
char* service_url = {"coap2http"};
uip_ipaddr_t server_ipaddr;
static struct etimer et;
static uint32_t offset;
char outbuffer[BUFFER_SIZE];
char chunk_buffer[REST_MAX_CHUNK_SIZE];
static uint8_t request_more;
static int error = 0;

void
client_chunk_handler(void *response) {
  const uint8_t *chunk;
  coap_packet_t * pkt = (coap_packet_t *) response;
  int len = coap_get_payload(response, &chunk);
  static uint8_t more_;
  static uint8_t code;
  uint32_t seq_num;

  if (len > 0) {
    PRINTF("|%.*s\n", len, (char *) chunk);
  }
  if (pkt->type == COAP_TYPE_ACK && IS_OPTION(pkt, COAP_OPTION_BLOCK1)){
    coap_get_header_block1(response, &seq_num, &more_, NULL, NULL);
    PRINTF("ACK for block %u\n", seq_num);
    error = 0;
    return;
  }
  if (pkt->type == COAP_TYPE_ACK){
    PRINTF("ACK for message\n");
    error = 0;
    return;
  }  
  if ((pkt->code == REST.status.OK)){
    PRINTF("Response OK\n");
    error = 0;
    return; 
  }
  if (pkt->code == REST.status.CREATED){
    PRINTF("Resource Created\n");
    error = 0;
    return;
  }
  PRINTF("Response code %d\n", pkt->code);
  error = 1;
}

#define APPLICATION_URI       "/myApplication/containers/flow/contentInstances"


PROCESS_THREAD(coap_client_example, ev, data) {
  PROCESS_BEGIN();
  srand(node_id);
#if PROFILE_1 && !PROFILE_2 && !PROFILE_3 && !PROFILE_4
  printf("##C PROFILE 1\n");
#elif !PROFILE_1 && PROFILE_2 && !PROFILE_3 && !PROFILE_4
  printf("##C PROFILE 2\n");
#elif !PROFILE_1 && !PROFILE_2 && PROFILE_3 && !PROFILE_4
  printf("##C PROFILE 3\n");
#elif !PROFILE_1 && !PROFILE_2 && !PROFILE_3 && PROFILE_4
  printf("##C PROFILE 4\n");
#else
  printf("##C PROFILE 0\n");
#endif

  static coap_packet_t request[1];
  SERVER_NODE(&server_ipaddr);
  static uint8_t *token_ptr;
  static uint8_t token_len;
  static unsigned int len;
  static uint16_t num;
  rest_init_engine();

  etimer_set(&et, 15 * CLOCK_SECOND);
  //uart1_set_input(serial_line_input_byte);
  //serial_line_init();

  printf("~~EXI schema less\n");

#if POST || PUT
  while (1) {
    PROCESS_YIELD();
    if(ev == sensors_event && data == &button_sensor){
    //if(etimer_expired(&et)) {
      offset = 0;
      structure_dim = 0;
      num = 0;
      PRINT6ADDR(&server_ipaddr);
      PRINTF(" : %u\n", UIP_HTONS(REMOTE_PORT));
      request_more = 0;
      errorCode tmp_err_code = EXIP_OK;
      //applicationBuild(outbuffer, tmp_err_code);
      SingleString content = "content";
      contentInstanceBuild(outbuffer, tmp_err_code, content);
      structure_dim = strlen(outbuffer);
      printf("Sending: %d bytes\n", structure_dim);
      printf("Sending: %s\n", outbuffer);
      printf("Sending: %u bytes\n", (unsigned int)structure_dim);

      coap_init_message(request, COAP_TYPE_CON, COAP_POST, coap_get_mid());
      PRINTF("Sending PUT/POST to %s\n", service_url);
      coap_set_header_uri_path(request, service_url);

      token_len = coap_generate_token(&token_ptr);
      coap_set_token(request, token_ptr, token_len);

      if (structure_dim <= REST_MAX_CHUNK_SIZE){ //blockwise not required
        memcpy(chunk_buffer, outbuffer, structure_dim);
        coap_set_payload(request, chunk_buffer, structure_dim);
        PRINTF("structure dim =%u Block1 is not set\n",  structure_dim);
        coap_set_header_proxy_uri(request, APPLICATION_URI);
        coap_set_header_content_format(request, APPLICATION_EXI);
        COAP_BLOCKING_REQUEST(&server_ipaddr, REMOTE_PORT, request, client_chunk_handler);
      }
      else { //blockwise required
        while(offset <= structure_dim){
          coap_init_message(request, COAP_TYPE_CON, COAP_POST, coap_get_mid());
          PRINTF("n=%u, offset =%u, l=%u\n", num, offset, len);
          coap_set_header_uri_path(request, service_url);
          offset = (num * REST_MAX_CHUNK_SIZE);
          len = ((structure_dim - offset) > REST_MAX_CHUNK_SIZE ) ? REST_MAX_CHUNK_SIZE : (structure_dim - offset);
          memcpy(chunk_buffer, outbuffer + offset, len);
          if (offset < structure_dim){
            request_more = 1;
          }else{
            request_more = 0;
          }
          coap_set_payload(request, chunk_buffer, structure_dim);
          coap_set_header_proxy_uri(request, APPLICATION_URI);
          coap_set_header_content_format(request, APPLICATION_EXI);
          coap_set_header_block1(request, num, request_more, REST_MAX_CHUNK_SIZE);
          COAP_BLOCKING_REQUEST(&server_ipaddr, REMOTE_PORT, request, client_chunk_handler);
          if (error == 1) {
            PRINTF("Error while sending block %u", num);
            error = 0;
            break;
          }
          num ++;
          offset += len;
        }
      }
    } //event
  } //while 1
#endif  //POST || PUT
  PROCESS_END();
}

