/* 
 * File:   test_configuration.h
 * Author: francesca
 *
 * Created on June 6, 2014, 8:22 PM
 */

#ifndef TEST_CONFIGURATION_H
#define	TEST_CONFIGURATION_H

#define EXI 1

/*_APPLICATION (create)*/
#define TD_M2M_COAP_01 1
/*_APPLICATION (retrieve)*/
#define TD_M2M_COAP_02 0
/*_APPLICATION (update)*/
#define TD_M2M_COAP_03 0
/*_SUBSCRIPTION (create)*/
#define TD_M2M_COAP_04 0
/*
 * NOTIFY (subscription notify)
 * #define TD_M2M_COAP_05 1
 * #define TD_M2M_COAP_06 subscription delete
 * #define TD_M2M_COAP_07 application delete
 */

/*CONTENT_INSTANCE (retrieve)*/
#define TD_M2M_COAP_08 0

/*
 * TD_M2M_COAP_09 gives the same output as TD_M2M_COAP_08
 * TD_M2M_COAP_10 partial addressing
 * TD_M2M_COAP_11 announcement ?
 * TD_M2M_COAP_12 proxy retrieval
 * TD_M2M_COAP_13 multi-hop retrieval
 */


#endif	/* TEST_CONFIGURATION_H */

