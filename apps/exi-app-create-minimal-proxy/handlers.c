#include "handlers.h"
#include "server-m2m.h"


void json_application_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
    const uint8_t *chunk;
    static uint8_t transaction_enabled;
    static uint8_t retransmission;
    coap_packet_t * pkt = (coap_packet_t *) request;

    if (IS_OPTION(pkt, COAP_OPTION_BLOCK1)) {

        coap_get_header_block1(request, &num, &more, &SZX_server, NULL);
        transaction_enabled = 0;
        retransmission=1;
        if (num == 0) {
            snprintf(current_token, pkt->token_len + 1, pkt->token);
            cpos_server = 0;
            local_num = 0;
            transaction_enabled=1;
            
        } else {
            if ((num == local_num + 1) && (!strcmp(pkt->token, current_token)))
            {
                local_num++;
                transaction_enabled=1;
            }
            /*Means the ack was lost so server can safely retransmit the previous ack*/
            if ((num == local_num) && (!strcmp(pkt->token, current_token))){
                retransmission=1;
            }

        }
        PRINTF("num %d\n", num);
        PRINTF("local num %d\n", local_num);
        PRINTF("current conversation token %s\n", current_token);
        PRINTF("token %s\n", pkt->token);


        if ((transaction_enabled)||(retransmission)) {

            int len = REST.get_request_payload(request, &chunk);
            coap_set_header_block1(response, num, 0, SZX_server);
            PRINTF("Received: %s\n", pkt->payload);
            if (cpos_server > INPUT_BUFFER)
                return;

            if (transaction_enabled){
            
            strncpy((char *) (in_message + cpos_server), (char*) chunk, len);

            cpos_server += len;

            snprintf((char *) in_message + cpos_server, len, "%c", '\0');
            }
            
            if (more) {
                REST.set_response_status(response, REST.status.OK);
                //printf("cpos_server %d\n", cpos_server);
                //printf("Overall message %s\n", in_message);
                cpos_server = 0;
            } else {
                REST.set_response_status(response, REST.status.NOT_MODIFIED);
                //printf("cpos_server %d \n", cpos_server);
                //printf("len %d \n", len);
                PRINTF("more messages expected %s more %d\n", in_message, more);
            }
        } else {

            printf("FAIL\n");
            REST.set_response_status(response, REST.status.BAD_OPTION);
            
        }
    } else {

        int len = REST.get_request_payload(request, &chunk);
        strncpy((char *) (in_message + cpos_server), (char*) chunk, len);
        PRINTF("Received: %s\n", pkt->payload);
        snprintf(current_token, pkt->token_len + 1, pkt->token);
        REST.set_response_status(response, REST.status.OK);
    }

}

void json_application_get_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  int strpos = 0;
    
  if (*offset == 0) {
        end_server_transmission = energest_type_time(ENERGEST_TYPE_TRANSMIT);
        time_difference_server_transmission = end_server_transmission / RATIO - start_server_transmission / RATIO;
        client_energy_consumption=time_difference_server_transmission*LISTEN_CURRENT;
#if HD
        //printf("Server_Trx_Time_tens_microseconds %lu\n", (uint32_t) time_difference_server_transmission);
        PRINTF_ENERGY("Client_listen_time_10^-8_Joule %lu\n", (uint32_t) client_energy_consumption);
#else
#if HUNDREDS
        //printf("Server_Trx_Time_hundreds_microseconds %lu\n", (uint32_t) time_difference_server_transmission);
        PRINTF_ENERGY("Client_listen_time_10^-7_Joule %lu\n", (uint32_t) client_energy_consumption);
#else
        //printf("Server_Trx_Time_ms %lu\n", (uint32_t) time_difference_server_transmission);
        PRINTF_ENERGY("Client_listen_time_10^-6_Joule %lu\n", (uint32_t) client_energy_consumption);
#endif
#endif
        start_server_transmission = end_server_transmission;
        
        pkt_count = 1;
        send_json();
        PRINTF("Total dimension %d\n", CHUNKS_TOTAL);
        PRINTF("Number of blocks %d\n", CHUNKS_TOTAL / preferred_size + 1);
        PRINTF("%s\n", outbuffer);
    } else pkt_count++;

    //PRINTF("Handler: chunks-request received offset %ld pkt_count %d bpos_out=%d offset=%d\n",*offset,pkt_count,bpos_out,*offset);

    if (*offset >= CHUNKS_TOTAL) {
        REST.set_response_status(response, REST.status.BAD_OPTION);
        /* A block error message should not exceed the minimum block size (16). */

        const char *error_msg = "BlockOutOfScope";
        REST.set_response_payload(response, error_msg, strlen(error_msg));
        ++rcounter;
        return;
    }
    /* Generate data until reaching CHUNKS_TOTAL. */
    while (strpos < preferred_size && (CHUNKS_TOTAL - *offset) >= strpos) {
        strpos += snprintf((char *) buffer + strpos, preferred_size - strpos + 1, "%c", *(outbuffer + strpos + *offset));
    }

    //overall_transmission_time_end = en_end_encode;


    if (strpos > preferred_size) {
        strpos = preferred_size;
    }

    if (*offset + (int32_t) strpos > CHUNKS_TOTAL) {
        strpos = CHUNKS_TOTAL - *offset;
    }

    REST.set_response_payload(response, buffer, strpos);

    *offset += strpos;


    if (*offset >= CHUNKS_TOTAL) {
        ++rcounter;
        *offset = -1;

#if PROFILE_1
        PRINT_S("Profile 1 JSON Encoding\n");
#elif PROFILE_2
        PRINT_S("Profile 2 JSON Encoding\n");
#elif PROFILE_3
        PRINT_S("Profile 3 JSON Encoding:\n");
#elif PROFILE_4
        PRINT_S("Profile 4 JSON Encoding:\n");
#endif

        //PRINT_S("Serialization time: %d number of chunks: %d\n", (uint32_t) (serialization_time / 32.768), pkt_count);
    }

}