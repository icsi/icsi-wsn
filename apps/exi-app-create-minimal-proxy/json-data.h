#ifndef _JSON_DATA_
#define _JSON_DATA_
 
#include "jsontree.h"

#define NUM_STRING 10
#define RAND_MOD 23
#define STR_LEN 8
#define ARRAY_SIZE 5


#define OFF 0
#define ON 1

#if !defined PROFILE_1
#define PROFILE_1 ON
#endif
#if !defined PROFILE_2
#define PROFILE_2 ON
#endif
#if !defined PROFILE_3
#define PROFILE_3 OFF
#endif
#if !defined PROFILE_4
#define PROFILE_B OFF
#endif

extern uint8_t rcounter; 
//char gettype();
void getrand_number(int16_t *num);
void getrand_string(char *str, int8_t len);
char getrand_boolean();
int16_t next_val();
#endif