/*
 * Copyright (c) 2014, Francesca Pacini
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 
 */

/**
 * \file
 *
 * \author
 *      Francesca Pacini <fkpacini@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "contiki.h"
#include "contiki-net.h"
#include "server-m2m.h"

#include "exiDecode.h"
#include "m2mBuild.h"


/* Define which resources to include to meet memory constraints. */

#include "er-coap.h"
#include "er-coap-engine.h"

#define DEBUG_SERVER 1

#if DEBUG_SERVER
#define PRINTF(...) printf(__VA_ARGS__)
#define PRINT6ADDR(addr) PRINTF("[%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x]", ((uint8_t *)addr)[0], ((uint8_t *)addr)[1], ((uint8_t *)addr)[2], ((uint8_t *)addr)[3], ((uint8_t *)addr)[4], ((uint8_t *)addr)[5], ((uint8_t *)addr)[6], ((uint8_t *)addr)[7], ((uint8_t *)addr)[8], ((uint8_t *)addr)[9], ((uint8_t *)addr)[10], ((uint8_t *)addr)[11], ((uint8_t *)addr)[12], ((uint8_t *)addr)[13], ((uint8_t *)addr)[14], ((uint8_t *)addr)[15])
#define PRINTLLADDR(lladdr) PRINTF("[%02x:%02x:%02x:%02x:%02x:%02x]",(lladdr)->addr[0], (lladdr)->addr[1], (lladdr)->addr[2], (lladdr)->addr[3],(lladdr)->addr[4], (lladdr)->addr[5])
#else
#define PRINTF(...)
#define PRINT6ADDR(addr)
#define PRINTLLADDR(addr)
#endif

/******************************************************************************/

char outbuffer[OUTPUT_BUFFER];


void post_put_handler(uint8_t put, uint32_t content_type, void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset, uint32_t code);
void get_handler(uint8_t exi, void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);

int pkt_count;

int bpos_out = 0;

uint8_t rcounter;

#define CHUNKS_TOTAL bpos_out

uint32_t start_server_transmission;
uint32_t end_server_transmission;
uint32_t time_difference_server_transmission;
uint32_t client_energy_consumption;

uint32_t serialization_start;
uint32_t serialization_end;
uint32_t serialization_time_difference;

char in_message[INPUT_BUFFER];
uint32_t cpos_server;
uint32_t num;
uint32_t size;
uint16_t SZX_server;
uint8_t more;
uint32_t local_num;
char current_token[16];

#if !_APPLICATION && !_SUBSCRIPTION && _CONTENT_INSTANCE
//CharType contentInstanceApplication[JSON_CONTENT_SIZE];

#if PROFILE_1 && !PROFILE_2 && !PROFILE_3 && !PROFILE_4
CharType * contentInstanceApplication = "12002345234541240365423427";
#elif !PROFILE_1 && PROFILE_2 && !PROFILE_3 && !PROFILE_4
CharType * contentInstanceApplication = "120023452345412403654234271330187635432134510984375113552871656321405173646721";
#elif !PROFILE_1 && !PROFILE_2 && PROFILE_3 && !PROFILE_4
CharType * contentInstanceApplication = "120023452345412403654234271330187635432134510984375113552871656321405173646721151043848438118303838383811950325216232201038437437121358437843752140484389434";
#elif !PROFILE_1 && !PROFILE_2 && !PROFILE_3 && PROFILE_4
CharType * contentInstanceApplication = "12002345234541240365423427133018763543213451098437511355287165632140517364672115104384843811830383838381195032521623220103843743712135843784375214048438943400104747437430020437843784013043884384601404848484340230948444748031529484843205127474747470530474374364";
#endif

#endif //!_APPLICATION && !_SUBSCRIPTION && _CONTENT_INSTANCE

uint8_t put_more;
uint8_t put_seq_num;


#if _APPLICATION && !SUBSCRIPTION && !_CONTENT_INSTANCE

void applications_POST_exi_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset) {

    coap_packet_t * pkt = (coap_packet_t *) request;
    //coap_content_type_t content_type = coap_get_header_content_type(pkt);
    //printf("Unsupported media type @ applications %lu\n", content_type);
    //printf("Unsupported media type @ applications %lu\n", pkt->content_type);
    PRINTF("calling POST handler\n");
    post_put_handler(0, APPLICATION_EXI, request, response, buffer, preferred_size, offset, REST.status.CREATED);
}

void app_PUT_exi_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset) {
	PRINTF("app PUT exi handler\n");
    coap_packet_t * pkt = (coap_packet_t *) request;

    if (!IS_OPTION(pkt, COAP_OPTION_BLOCK2)) {
        post_put_handler(1, APPLICATION_EXI, request, response, buffer, preferred_size, offset, REST.status.CHANGED);
    } else {
        bpos_out=strlen(in_message);
        uint32_t num_block;
        uint8_t more_block;
        uint32_t offset_block;
        coap_get_header_block2(pkt, &num_block, &more_block, more_block, NULL);
        offset_block = num_block*preferred_size;
        get_handler(0, request, response, buffer, preferred_size, offset);
    }
}

void
app_GET_exi_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset) {
	PRINTF("app GET exi handler\n");
    get_handler(1, request, response, buffer, preferred_size, offset);
}

RESOURCE(applications_POST_exi, "title=\"JSON SERVER\";rt=\"Data\"",
		NULL,
		applications_POST_exi_handler,
		NULL,
		NULL);

RESOURCE(app_GET_exi, "title=\"EXI SERVER\";rt=\"Data\"",
		app_GET_exi_handler,
		NULL,
		NULL,
		NULL);

RESOURCE(app_PUT_exi, "title=\"EXI SERVER\";rt=\"Data\"",
		NULL,
		NULL,
		app_PUT_exi_handler,
		NULL);



#endif //_APPLICATION && !SUBSCRIPTION && !_CONTENT_INSTANCE

#if !_APPLICATION && !_SUBSCRIPTION && _CONTENT_INSTANCE

void content_instance_GET_exi_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
RESOURCE(content_instance_GET_exi, "title=\"EXI SERVER\";rt=\"Data\"",
		 content_instance_GET_exi_handler,
		 NULL,
		 NULL,
		 NULL);

/*RETRIEVE CONTENT INSTANCE*/
void content_instance_GET_exi_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset) {
	PRINTF("content GET exi handler\n");
    get_handler(1, request, response, buffer, preferred_size, offset);
}

#endif  //!_APPLICATION && !_SUBSCRIPTION && _CONTENT_INSTANCE


PROCESS(rest_server_m2m, "Erbium M2M Server");
AUTOSTART_PROCESSES(&rest_server_m2m);

PROCESS_THREAD(rest_server_m2m, ev, data) {
    PROCESS_BEGIN();

    PRINTF("Starting Erbium M2M Server\n");

#ifdef RF_CHANNEL
    PRINTF("RF channel: %u\n", RF_CHANNEL);
#endif
#ifdef IEEE802154_PANID
    PRINTF("PAN ID: 0x%04X\n", IEEE802154_PANID);
#endif

    PRINTF("uIP buffer: %u\n", UIP_BUFSIZE);
    PRINTF("LL header: %u\n", UIP_LLH_LEN);
    PRINTF("IP+UDP header: %u\n", UIP_IPUDPH_LEN);
    PRINTF("REST max chunk: %u\n", REST_MAX_CHUNK_SIZE);

    /* Initialize the REST engine. */
    rest_init_engine();
    PRINTF("Activating Rest resources\n");

#if _APPLICATION && !_SUBSCRIPTION && !_CONTENT_INSTANCE
    rest_activate_resource(&applications_POST_exi, "gw0exi/scls/applications");
    rest_activate_resource(&app_PUT_exi, "gw0exi/scls/applications/app_put");
    rest_activate_resource(&app_GET_exi, "gw0exi/scls/applications/app_get");
#endif
#if !_APPLICATION && !_SUBSCRIPTION && _CONTENT_INSTANCE
    rest_activate_resource(&content_instance_GET_exi, "gw0exi/scls/app/app/c1/cI/t");
#endif

    //rest_activate_resource(&resource_app_get_json, "gwjson/scls/applications/app_get");
    PRINTF("Rest resources activated\n");
    cpos_server = 0;

    while (1) {
        PROCESS_YIELD();
    }
    PROCESS_END();
}

int putchar_out(int c) {
    if (bpos_out < OUTPUT_BUFFER) {
        outbuffer[bpos_out++] = c;
        return c;
    } else printf("DONE\n");
    return 0;
}

char * get_outbuffer;

void put_get(uint8_t *buffer, void* response, uint16_t preferred_size, int32_t *offset) {

    put_more = 0;
    put_seq_num = 0;
    int strpos = 0;
    int bpos_out = 0;
    coap_packet_t * pkt_resp = (coap_packet_t *) response;
    get_outbuffer = in_message;
    bpos_out = strlen(get_outbuffer);
    if (bpos_out > preferred_size)
        coap_set_header_block2(pkt_resp, put_seq_num, put_more, preferred_size);
    if (*offset >= CHUNKS_TOTAL) {
        REST.set_response_status(response, REST.status.BAD_OPTION);
        /* A block error message should not exceed the minimum block size (16). */

        const char *error_msg = "BlockOutOfScope";
        REST.set_response_payload(response, error_msg, strlen(error_msg));
        ++rcounter;
        return;
    }

    while (strpos < preferred_size && (CHUNKS_TOTAL - *offset) >= strpos) {
        strpos += snprintf((char *) buffer + strpos, preferred_size - strpos + 1, "%c", *(get_outbuffer + strpos + *offset));
    }


    /* snprintf() does not adjust return value if truncated by size. */
    if (strpos > preferred_size) {
        strpos = preferred_size;
    }

    /* Truncate if above CHUNKS_TOTAL bytes. */
    if (*offset + (int32_t) strpos > CHUNKS_TOTAL) {
        strpos = CHUNKS_TOTAL - *offset;
    }

    REST.set_response_payload(response, buffer, strpos);

}

void post_put_handler(uint8_t put, uint32_t content_type, void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset, uint32_t code) {

    coap_packet_t * pkt = (coap_packet_t *) request;
    int32_t strpos = 0;
    if ((content_type == APPLICATION_EXI) || (content_type == APPLICATION_JSON)) {

        const uint8_t *chunk;
        static uint8_t transaction_enabled;
        static uint8_t retransmission;

        if (IS_OPTION(pkt, COAP_OPTION_BLOCK1)) {

            coap_get_header_block1(request, &num, &more, &SZX_server, NULL);
            transaction_enabled = 0;
            retransmission = 1;

            if (num == 0) {
                snprintf(current_token, pkt->token_len + 1, (char *) pkt->token);
                cpos_server = 0;
                local_num = 0;
                transaction_enabled = 1;

            } else {
                if ((num == local_num + 1) && (!strcmp((char *) pkt->token, current_token))) {
                    local_num++;
                    transaction_enabled = 1;
                }
                /*Means the ack was lost so server can safely retransmit the previous ack*/
                if ((num == local_num) && (!strcmp((char *) pkt->token, current_token))) {
                    retransmission = 1;
                }

            }
            PRINTF("num %d\n", num);
            PRINTF("local num %d\n", local_num);
            PRINTF("current conversation token %s\n", current_token);
            PRINTF("token %s\n", pkt->token);


            if ((transaction_enabled) || (retransmission)) {

                int len = REST.get_request_payload(request, &chunk);
                coap_set_header_block1(response, num, 0, SZX_server);
                PRINTF("Received: %s\n", pkt->payload);
                if (cpos_server <= INPUT_BUFFER) {

                    if (transaction_enabled) {

                        strncpy((char *) (in_message + cpos_server), (char*) chunk, len);

                        cpos_server += len;

                        snprintf((char *) in_message + cpos_server, len, "%c", '\0');
                    }

                    if (more) {
                        REST.set_response_status(response, code);
                        cpos_server = 0;

                        if (content_type == APPLICATION_EXI) {

#if PRINT_INCOMING_OR_OUTGOING_MESSAGE
                            decodeMessage(in_message);
                            printf("Size %d\n", strlen(in_message));
#endif
                        }
#if _APPLICATION && !_SUBSCRIPTION && !_CONTENT_INSTANCE
                        if (put) {

                            put_get(buffer,response,preferred_size,offset);
                        }
#endif
                    } else {
                        REST.set_response_status(response, code);
                        PRINTF("cpos_server %d \n", cpos_server);
                        PRINTF("len %d \n", len);
                        PRINTF("more messages expected %s more %d\n", in_message, more);
                    }
                } else {
                    printf("FAIL\n");
                    REST.set_response_status(response, REST.status.BAD_OPTION);
                }
            } else {
                printf("FAIL\n");
                REST.set_response_status(response, REST.status.BAD_OPTION);
            }
        } else {
            put_get(buffer, response, preferred_size, offset);
            int len = REST.get_request_payload(request, &chunk);
            strncpy((char *) (in_message + cpos_server), (char*) chunk, len);
            PRINTF("Received: %s\n", pkt->payload);
            snprintf(current_token, (int) pkt->token_len + 1, (char *) pkt->token);
            REST.set_response_status(response, REST.status.OK);
            if (content_type == APPLICATION_EXI) {
#if PRINT_INCOMING_OR_OUTGOING_MESSAGE
                decodeMessage(in_message);
#endif
            }
        }
    } else {

        REST.set_response_status(response, REST.status.UNSUPPORTED_MEDIA_TYPE);
        printf("put_post_handler Unsupported media type\n");
    }
}



void get_handler(uint8_t exi, void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset) {
    //printf("Get handler\n");
    errorCode tmp_err_code = EXIP_OK;
    int32_t strpos = 0;
    if (*offset == 0) {
        //end_server_transmission = energest_type_time(ENERGEST_TYPE_TRANSMIT);
        //time_difference_server_transmission = end_server_transmission / RATIO - start_server_transmission / RATIO;
        //client_energy_consumption = time_difference_server_transmission*LISTEN_CURRENT;
        //start_server_transmission = end_server_transmission;

        pkt_count = 1;
        //printf("Generating EXI\n");

        serialization_start = energest_type_time(ENERGEST_TYPE_CPU);

        if (exi) {
#if _APPLICATION && !_SUBSCRIPTION && !_CONTENT_INSTANCE && ON_THE_FLY
            applicationBuild(outbuffer, tmp_err_code);
#endif
#if !_APPLICATION && _SUBSCRIPTION && !_CONTENT_INSTANCE && ON_THE_FLY
            subscriptionBuild(outbuffer, tmp_err_code);
#endif
#if !_APPLICATION && !_SUBSCRIPTION && _CONTENT_INSTANCE && ON_THE_FLY
            contentInstanceBuild(outbuffer, tmp_err_code, contentInstanceApplication);
#endif
            get_outbuffer = outbuffer;
#if PRINT_INCOMING_OR_OUTGOING_MESSAGE
            printf("Generated EXI %s\n", get_outbuffer);
            printf("Size: %d\n", strlen(get_outbuffer));
#endif
        }
        //serialization_end = energest_type_time(ENERGEST_TYPE_CPU);
        //serialization_time_difference = serialization_end / RATIO - serialization_start / RATIO;
        bpos_out = strlen(get_outbuffer);
        //printf("Size of data: %d\n",CHUNKS_TOTAL);
        //printf("Number of chunks: %d\n", CHUNKS_TOTAL / preferred_size + 1);
    } else pkt_count++;

    PRINTF("Handler: chunks-request received offset %ld pkt_count %d bpos_out=%d\n", *offset, pkt_count, bpos_out);
    if (*offset >= CHUNKS_TOTAL) {
        REST.set_response_status(response, REST.status.BAD_OPTION);
        /* A block error message should not exceed the minimum block size (16). */

        const char *error_msg = "BlockOutOfScope";
        REST.set_response_payload(response, error_msg, strlen(error_msg));
        ++rcounter;
        return;
    }

    while (strpos < preferred_size && (CHUNKS_TOTAL - *offset) >= strpos) {
        strpos += snprintf((char *) buffer + strpos, preferred_size - strpos + 1, "%c", *(get_outbuffer + strpos + *offset));
    }


    /* snprintf() does not adjust return value if truncated by size. */
    if (strpos > preferred_size) {
        strpos = preferred_size;
    }

    /* Truncate if above CHUNKS_TOTAL bytes. */
    if (*offset + (int32_t) strpos > CHUNKS_TOTAL) {
        strpos = CHUNKS_TOTAL - *offset;
    }

    //printf("sending %s\n",buffer);
    REST.set_response_payload(response, buffer, strpos);
    //printf("Sent %d bytes\n",strpos);
    /* IMPORTANT for chunk-wise resources: Signal chunk awareness to REST engine. */
    *offset += strpos;


    /* Signal end of resource representation. */

    if (*offset >= CHUNKS_TOTAL) {
        ++rcounter;
        *offset = -1;
    }

}
