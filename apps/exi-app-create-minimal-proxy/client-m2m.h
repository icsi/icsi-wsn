/* 
 * File:   client-m2m.h
 * Author: francesca
 *
 * Created on May 13, 2014, 1:49 PM
 */

#ifndef CLIENT_M2M_H
#define	CLIENT_M2M_H
#include "test_configuration.h"

//#define OFFSET 0

#define BUFFER_SIZE 550

#define PRINT_INCOMING_OUTGOING_MESSAGE 1
#define PRINT_DESERIALIZATION_TIME 0

#if TD_M2M_COAP_01
#define APPLICATION_CREATE 1
#define APPLICATION_RETRIEVE 0
#define APPLICATION_UPDATE 0
#define SUBSCRIPTION_CREATE 0
#define SUBSCRIPTION_NOTIFY 0
#define CONTENT_INSTANCE_RETRIEVE 0
#endif
#if TD_M2M_COAP_02
#define APPLICATION_CREATE 0
#define APPLICATION_RETRIEVE 1
#define APPLICATION_UPDATE 0
#define SUBSCRIPTION_CREATE 0
#define SUBSCRIPTION_NOTIFY 0
#define CONTENT_INSTANCE_RETRIEVE 0
#endif
#if TD_M2M_COAP_03
#define APPLICATION_CREATE 0
#define APPLICATION_RETRIEVE 0
#define APPLICATION_UPDATE 1
#define SUBSCRIPTION_CREATE 0
#define SUBSCRIPTION_NOTIFY 0
#define CONTENT_INSTANCE_RETRIEVE 0
#endif
#if TD_M2M_COAP_04
#define APPLICATION_CREATE 0
#define APPLICATION_RETRIEVE 0
#define APPLICATION_UPDATE 0
#define SUBSCRIPTION_CREATE 1
#define SUBSCRIPTION_NOTIFY 0
#define CONTENT_INSTANCE_RETRIEVE 0
#endif
#if TD_M2M_COAP_08
#define APPLICATION_CREATE 0
#define APPLICATION_RETRIEVE 0
#define APPLICATION_UPDATE 0
#define SUBSCRIPTION_CREATE 0
#define SUBSCRIPTION_NOTIFY 0
#define CONTENT_INSTANCE_RETRIEVE 1
#endif

#if APPLICATION_CREATE || SUBSCRIPTION_CREATE
#define GET 0
#define POST 1
#define PUT 0
#define DELETE 0
#endif

#if APPLICATION_RETRIEVE
#define GET 1
#define POST 0
#define PUT 0
#define DELETE 0
#endif

#if APPLICATION_UPDATE
#define GET 0
#define POST 0
#define PUT 1
#define DELETE 0
#endif

#if CONTENT_INSTANCE_RETRIEVE
#define GET 1
#define POST 0
#define PUT 0
#define DELETE 0
#endif

#if PRINT_INCOMING_OUTGOING_MESSAGE
#define DECODE_IN_EXI 1
#define PRINT_IN_OUT_EXI 1
#else
#define DECODE_INCOMING_EXI 0
#define PRINT_IN_OUT_EXI 0
#endif //PRINT_INCOMING_OUTGOING_MESSAGE

#define MAXL 50
#define ARRAY_SIZE 5
#define SEARCH_STRINGS_ARRAY_SIZE 5
#define CHAR_BUFFER_LEN 50

#define ACK_BUFFER_SIZE 30

#endif	/* CLIENT_M2M_H */
