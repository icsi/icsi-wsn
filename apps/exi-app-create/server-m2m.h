/* 
 * File:   server-m2m.h
 * Author: francesca
 *
 * Created on May 13, 2014, 2:34 PM
 */

#include "test_configuration.h"

#ifndef SERVER_M2M_H
#define	SERVER_M2M_H

#define PRINT_INCOMING_OR_OUTGOING_MESSAGE 1
#define PRINT_SERIALIZATION_TIME 0

#define DESERIALIZE_AT_SERVER OFF

#define ON_THE_FLY ON
#define OUTPUT_BUFFER 550
#define INPUT_BUFFER 550
#define REPORT 0
#define DEBUG_SERVER 1


#define EXI_SERVER EXI
/*
#if DEBUG_SERVER
#define PRINTF(...) printf(__VA_ARGS__)
#define PRINT6ADDR(addr) PRINTF("[%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x]", ((uint8_t *)addr)[0], ((uint8_t *)addr)[1], ((uint8_t *)addr)[2], ((uint8_t *)addr)[3], ((uint8_t *)addr)[4], ((uint8_t *)addr)[5], ((uint8_t *)addr)[6], ((uint8_t *)addr)[7], ((uint8_t *)addr)[8], ((uint8_t *)addr)[9], ((uint8_t *)addr)[10], ((uint8_t *)addr)[11], ((uint8_t *)addr)[12], ((uint8_t *)addr)[13], ((uint8_t *)addr)[14], ((uint8_t *)addr)[15])
#define PRINTLLADDR(lladdr) PRINTF("[%02x:%02x:%02x:%02x:%02x:%02x]",(lladdr)->addr[0], (lladdr)->addr[1], (lladdr)->addr[2], (lladdr)->addr[3],(lladdr)->addr[4], (lladdr)->addr[5])
#else
#define PRINTF(...)
#define PRINT6ADDR(addr)
#define PRINTLLADDR(addr)
#endif
*/
#if REPORT
#define PRINT_S(...) printf(__VA_ARGS__)
#else
#define PRINT_S 
#endif

#if ENERGY_REPORT
#define PRINTF_ENERGY(...) printf(__VA_ARGS__)
#else
#define PRINTF_ENERGY(...)
#endif

#define CHAR_BUFFER_LEN 50
#define MAXL 50

#endif	/* SERVER_M2M_H */

