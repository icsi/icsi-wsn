/*
 * Copyright (c) 2014, Francesca Pacini
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.

 */

/**
 * \file
 *
 * \author
 *      Francesca Pacini <fkpacini@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "client-m2m.h"

#include "exiDecode.h"
#include "m2mBuild.h"

#include "contiki.h"
#include "contiki-net.h"
#include "dev/button-sensor.h"
#include "er-coap.h"
#include "er-coap-engine.h"


#define DEBUG 1

#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#define PRINT6ADDR(addr) PRINTF("[%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x]", ((uint8_t *)addr)[0], ((uint8_t *)addr)[1], ((uint8_t *)addr)[2], ((uint8_t *)addr)[3], ((uint8_t *)addr)[4], ((uint8_t *)addr)[5], ((uint8_t *)addr)[6], ((uint8_t *)addr)[7], ((uint8_t *)addr)[8], ((uint8_t *)addr)[9], ((uint8_t *)addr)[10], ((uint8_t *)addr)[11], ((uint8_t *)addr)[12], ((uint8_t *)addr)[13], ((uint8_t *)addr)[14], ((uint8_t *)addr)[15])
#define PRINTLLADDR(lladdr) PRINTF("[%02x:%02x:%02x:%02x:%02x:%02x]",(lladdr)->addr[0], (lladdr)->addr[1], (lladdr)->addr[2], (lladdr)->addr[3],(lladdr)->addr[4], (lladdr)->addr[5])
#else
#define PRINTF(...)
#define PRINT6ADDR(addr)
#define PRINTLLADDR(addr)
#endif


#define SERVER_NODE(ipaddr)   uip_ip6addr(ipaddr, 0xaaaa, 0, 0, 0, 0x202, 0x2, 0x2, 0x2)
/* TODO: This server address is hard-coded for Cooja. */

#define LOCAL_PORT      UIP_HTONS(COAP_DEFAULT_PORT+1)
#define REMOTE_PORT     UIP_HTONS(COAP_DEFAULT_PORT)

#define TOGGLE_INTERVAL 10

#define COAP_BLOCKING_REQUEST_FOR_PUT_BLOCKWISE_RESPONSE(server_addr, server_port, request, chunk_handler) \
{ \
  static struct request_state_t request_state; \
  PT_SPAWN(process_pt, &request_state.pt, \
           coap_blocking_request_for_put_blockwise_response(&request_state, ev, \
                                 server_addr, server_port, \
                                 request, chunk_handler) \
  ); \
}

void coap_blocking_request_callback_blockwise_put(void *callback_data, void *response) {
    struct request_state_t *state = (struct request_state_t *) callback_data;
    state->response = (coap_packet_t*) response;
    process_poll(state->process);
}

PT_THREAD(coap_blocking_request_for_put_blockwise_response(struct request_state_t *state, process_event_t ev,
        uip_ipaddr_t *remote_ipaddr, uint16_t remote_port,
        coap_packet_t *request,
        blocking_response_handler request_callback)) {
    PT_BEGIN(&state->pt);
    //printf("\nclient in blocking put\n\n\n");
    
    static uint8_t more;
    static uint32_t res_block;
    static uint8_t block_error;

    state->block_num = 1;
    state->response = NULL;
    state->process = PROCESS_CURRENT();

    more = 0;
    res_block = 0;
    block_error = 0;

    do {
        request->mid = coap_get_mid();
        if ((state->transaction = coap_new_transaction(request->mid, remote_ipaddr, remote_port))) {
            state->transaction->callback = coap_blocking_request_callback_blockwise_put;
            state->transaction->callback_data = state;

            if (state->block_num > 0) {
                coap_set_header_block2(request, state->block_num, 0, REST_MAX_CHUNK_SIZE);
            }

            state->transaction->packet_len = coap_serialize_message(request, state->transaction->packet);


            coap_send_transaction(state->transaction);


            PRINTF("Requested #%lu (MID %u)\n", state->block_num, request->mid);

            PT_YIELD_UNTIL(&state->pt, ev == PROCESS_EVENT_POLL);
            
            if (!state->response) {
                PRINTF("Server not responding\n");
                PT_EXIT(&state->pt);
            }

            coap_get_header_block2(state->response, &res_block, &more, NULL, NULL);

            PRINTF("B. Received #%lu%s (%u bytes)\n", res_block, more ? "+" : "", state->response->payload_len);

            if (res_block == state->block_num) {
                request_callback(state->response);
                ++(state->block_num);
            } else {
                printf("WRONG BLOCK %lu/%lu\n", res_block, state->block_num);
                ++block_error;
            }
        } else {
            PRINTF("Could not allocate transaction buffer");
            PT_EXIT(&state->pt);
        }
    } while (more && block_error < COAP_MAX_ATTEMPTS);

    PT_END(&state->pt);
}


PROCESS(coap_client_example, "COAP Client Example");
AUTOSTART_PROCESSES(&coap_client_example);

uip_ipaddr_t server_ipaddr;
static struct etimer et;

#define NUMBER_OF_URLS 2

/* leading and ending slashes only for demo purposes, get cropped automatically when setting the Uri-Path */

#if APPLICATION_CREATE
char* service_url = {"gw0exi/scls/applications"};
#endif //APPLICATION_CREATE

#if APPLICATION_RETRIEVE
char* service_url = {"gw0exi/scls/applications/app_get"};
#endif //APPLICATION_RETRIEVE

#if APPLICATION_UPDATE
char* service_url = {"gw0exi/scls/applications/app_put"};
#endif //APPLICATION_UPDATE

#if SUBSCRIPTION_CREATE
char* service_url = {"gw0exi/scls/applications/app/subscriptions"};
#endif //SUBSCRIPTION_CREATE

#if CONTENT_INSTANCE_RETRIEVE && TD_M2M_COAP_08
char* service_url = {"gw0exi/scls/applications/app/c1/contentInstances/test"};
#endif //CONTENT_INSTANCE_RETRIEVE && TD_M2M_COAP_08

uint8_t ack_buffer[ACK_BUFFER_SIZE];
char incoming_message[BUFFER_SIZE];
uint8_t response_more;

uint8_t success;
uint16_t num;

int32_t structure_dim;
#define CHUNKS_TOTAL structure_dim
/* This function is will be passed to COAP_BLOCKING_REQUEST() to handle responses. */
#if APPLICATION_UPDATE
uint8_t blockwise_put_response;
#endif
uint32_t seq_num;
uint32_t seq_num_put;

#if POST || PUT

void
client_chunk_handler(void *response) {

    const uint8_t *chunk;
    coap_packet_t * pkt = (coap_packet_t *) response;
    int len = coap_get_payload(response, &chunk);

    //printf("********************\nReceived from server:\n");

    if (len > 0) {
        PRINTF("|%.*s\n", len, (char *) chunk);

    }

    PRINTF("Response code %d\n", pkt->code); /*
        if ((pkt->code!=REST.status.OK)&&((pkt->code!=REST.status.NOT_MODIFIED))) 
        {
            success=0;
            printf("fail!\n");
        }
     */
    static uint8_t more_;
    static uint8_t code;
#if APPLICATION_UPDATE
    blockwise_put_response = 0;
#endif
    if (CHUNKS_TOTAL > PREFERRED_CHUNK_SIZE) {

        if (IS_OPTION(pkt, COAP_OPTION_BLOCK1)) {
            coap_get_header_block1(response, &seq_num, &more_, NULL, NULL);
            code = pkt->code;
#if APPLICATION_CREATE || SUBSCRIPTION_CREATE
            if (code == REST.status.CREATED) {
                if (seq_num < ACK_BUFFER_SIZE) {
                    ack_buffer[seq_num] = 1;
                    PRINTF("ACK for chunk %d\n", seq_num);
                } else PRINTF("Cannot receive ack\n");
            } else PRINTF("Not a successful ack\n");
#endif
#if APPLICATION_RETRIEVE
            if ((!more_) && (code == REST.status.OK) || ((more_) && (code == REST.status.OK))) {
                if (seq_num < ACK_BUFFER_SIZE) {
                    ack_buffer[seq_num] = 1;
                    PRINTF("ACK for chunk %d\n", seq_num);
                } else PRINTF("Cannot receive ack\n");
            } else PRINTF("Not a successful ack\n");
#endif
#if APPLICATION_UPDATE
            if (code == REST.status.CHANGED) {
                if (seq_num < ACK_BUFFER_SIZE) {
                    ack_buffer[seq_num] = 1;
                    PRINTF("ACK for chunk %d\n", seq_num);
                } else printf("Cannot receive ack \n");
            } else printf("Not a successful ack code %d\n", code);
#endif



        } else PRINTF("No Block1 option\n");
#if APPLICATION_UPDATE
        if (IS_OPTION(pkt, COAP_OPTION_BLOCK2)) {
            blockwise_put_response = 1;
            coap_get_header_block2(response, &seq_num, &more_, NULL, NULL);
            if (more_)
                blockwise_put_response = 0;
        }
#endif    
    } else {
        if ((pkt->code == REST.status.OK))
            ack_buffer[0] = 1;
    }
}

#endif
#if GET

uint32_t cpos;
uint8_t more;

void
client_chunk_handler(void *response) {

    static uint8_t *chunk;
    int len = coap_get_payload(response, &chunk);
    if (cpos > BUFFER_SIZE)
        return; //buffer full discard rest of the message

    memcpy((char *) (incoming_message + cpos), (char*) chunk, len);
    cpos += len;

    //snprintf((char *) (char *) (incoming_message + cpos), "%c", "\0");
    //printf("len=%d\n",len);
    more = coap_get_header_block2(response, NULL, &more, NULL, NULL);

}

#endif //POST || PUT

int successful_iteration_count;
int failure_iteration_count;
int32_t offset;
char outbuffer[BUFFER_SIZE];
uint32_t currentpos;
char chunk_buffer[PREFERRED_CHUNK_SIZE];

uint8_t request_more;

#if APPLICATION_UPDATE
RESOURCE(application_update_callback, METHOD_POST, "callback", "title=\"CLIENT\";rt=\"Data\"");

void application_update_callback_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset) {

    printf("\n\nHello I'm the callback\n\n");
    REST.set_response_status(response, REST.status.DELETED);
}
#endif

PROCESS_THREAD(coap_client_example, ev, data) {
    PROCESS_BEGIN();

#if _APPLICATION && !_SUBSCRIPTION
    printf("##C Application\n");
#elif !_APPLICATION && _SUBSCRIPTION
    printf("##C Subscription\n");
#endif


#if PROFILE_1 && !PROFILE_2 && !PROFILE_3 && !PROFILE_4
    printf("##C PROFILE 1\n");
#elif !PROFILE_1 && PROFILE_2 && !PROFILE_3 && !PROFILE_4
    printf("##C PROFILE 2\n");
#elif !PROFILE_1 && !PROFILE_2 && PROFILE_3 && !PROFILE_4
    printf("##C PROFILE 3\n");
#elif !PROFILE_1 && !PROFILE_2 && !PROFILE_3 && PROFILE_4
    printf("##C PROFILE 4\n");
#else
    printf("##C PROFILE 0\n");
#endif

    failure_iteration_count = 0;
    successful_iteration_count = 0;

    static coap_packet_t request[1]; /* This way the packet can be treated as pointer as usual. */
    SERVER_NODE(&server_ipaddr);
    static uint8_t *token_ptr;
    static uint8_t token_len;
    rest_init_engine();
#if APPLICATION_UPDATE
    //rest_init_engine();
    rest_activate_resource(&resource_application_update_callback);
#endif
    //static uint32_t numeric_token;

    /* receives all CoAP messages */
    etimer_set(&et, TOGGLE_INTERVAL * CLOCK_SECOND);

#if POST || PUT
    //char * _token[TOKEN_SIZE];
    static int i;
#if POST
    printf("**POST request\n");
#endif
#if PUT
    printf("**PUT request\n");
#endif
#endif
#if GET
    printf("**GET request\n");
#endif
    printf("~~EXI schema less\n");

#if POST || PUT
    while (1) {
        PROCESS_YIELD();
        if (etimer_expired(&et)) {
            offset = 0;
            structure_dim = 0;
            num = 0;
            PRINT6ADDR(&server_ipaddr);
            PRINTF(" : %u\n", UIP_HTONS(REMOTE_PORT));

            request_more = 0;
            //printf("Start request\n");

            errorCode tmp_err_code = EXIP_OK;
            coap_set_header_content_format(request, APPLICATION_EXI);
#if SUBSCRIPTION_CREATE
            subscriptionBuild(outbuffer, tmp_err_code);
#endif
#if APPLICATION_CREATE || APPLICATION_UPDATE || SUBSCRIPTION_NOTIFY
            applicationBuild(outbuffer, tmp_err_code);
            PRINTF("*** APP BUILD %d\n", strlen(outbuffer));
#endif
            structure_dim = strlen(outbuffer);
            //printf("Sending: %d bytes\n", structure_dim);


#if PRINT_IN_OUT_EXI
            printf("Sending: %s\n", outbuffer);
            //printf("Sending: %d bytes\n", structure_dim);
#endif
            if (structure_dim / PREFERRED_CHUNK_SIZE <= ACK_BUFFER_SIZE) {
                for (i = 0; i < (structure_dim / PREFERRED_CHUNK_SIZE + 1); i++)
                    ack_buffer[i] = 0;

                while (offset < CHUNKS_TOTAL) {
                    currentpos = 0;
#if POST
                    coap_init_message(request, COAP_TYPE_CON, COAP_POST, 0);
#endif
#if PUT
                    coap_init_message(request, COAP_TYPE_CON, COAP_PUT, 0);
#endif
                    PRINTF("Sending PUT /POST to %s\n", service_url);
                    coap_set_header_uri_path(request, service_url);

                    while ((currentpos < (PREFERRED_CHUNK_SIZE - OFFSET)) && (CHUNKS_TOTAL - offset) >= currentpos) {
                        currentpos += snprintf((char *) chunk_buffer + currentpos, PREFERRED_CHUNK_SIZE - OFFSET - currentpos + 1, "%c", *(outbuffer + currentpos + offset));
                    }

                    PRINTF("OUT: strpos %d\n", currentpos);
                    PRINTF("OUT buffer: %s\n", chunk_buffer);
                    if (currentpos > PREFERRED_CHUNK_SIZE - OFFSET) {
                        currentpos = PREFERRED_CHUNK_SIZE - OFFSET;
                    }

                    if (offset + (int32_t) currentpos > CHUNKS_TOTAL) {
                        currentpos = CHUNKS_TOTAL - offset;
                    }
                    offset += currentpos;
                    PRINTF("offset %d\n", offset);
                    PRINTF("chunks_total %d\n", CHUNKS_TOTAL);
                    if (offset >= CHUNKS_TOTAL) {
                        {
                            PRINTF("More set\n");
                            request_more = 1;
                        }
                    }
                    if (CHUNKS_TOTAL > PREFERRED_CHUNK_SIZE - OFFSET) {
                        PRINTF("Set block1\n");
                        coap_set_header_block1(request, num, request_more, PREFERRED_CHUNK_SIZE - OFFSET);
                    }
                    static uint8_t _more;
                    coap_get_header_block1(request, NULL, &_more, NULL, NULL);
                    PRINTF("more %d\n", _more);

                    if IS_OPTION(request, COAP_OPTION_BLOCK1)
                        PRINTF("Block1 is set\n");
                    else PRINTF("Block1 is not set\n");
                    coap_set_payload(request, chunk_buffer, currentpos);
                    token_len = coap_generate_token(&token_ptr);
                    coap_set_token(request, token_ptr, token_len);
                    //PRINTF("token %s\n", _token);
                    //PRINTF("numeric_token %d\n", numeric_token);
                    PRINTF("Sending chunk %d\n", num);

                    COAP_BLOCKING_REQUEST(&server_ipaddr, REMOTE_PORT, request, client_chunk_handler);
                    num++;
                }
#if APPLICATION_UPDATE
                if (blockwise_put_response) {
                    coap_init_message(request, COAP_TYPE_CON, COAP_PUT, 0);
                    coap_set_header_uri_path(request, service_url);
                    COAP_BLOCKING_REQUEST_FOR_PUT_BLOCKWISE_RESPONSE(&server_ipaddr, REMOTE_PORT, request, client_chunk_handler)
                }
#endif    
                //numeric_token++;
            } else printf("F for FAIL\n");
            PRINTF("Checking ack for %d\n", num - 1);
            if (ack_buffer[num - 1]) {
                //printf("C number_chunks %d\n",num);
                successful_iteration_count++;
            } else {
                failure_iteration_count++;
            }
            etimer_reset(&et);
        }
    }

#endif  //POST || PUT
#if GET
    while (1) {
        PROCESS_YIELD();

        if (etimer_expired(&et)) {
            cpos = 0;

            coap_init_message(request, COAP_TYPE_CON, COAP_GET, 0);
            coap_set_header_uri_path(request, service_url);
            coap_set_header_content_format(request, APPLICATION_EXI);

            COAP_BLOCKING_REQUEST(&server_ipaddr, REMOTE_PORT, request, client_chunk_handler);
            if ((cpos > 0) && (more == 1)) {



#if DECODE_IN_EXI
                printf("Message decoding %s\n", incoming_message);
                decodeMessage(incoming_message);
                printf("~~~Size: %d\n",strlen(incoming_message));
#endif


                successful_iteration_count++;

            } else {
                failure_iteration_count++;
            }
            etimer_reset(&et);
        }
    }
#endif //GET

    PROCESS_END();
}
