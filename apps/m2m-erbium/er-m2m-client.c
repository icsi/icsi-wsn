/*
 * Copyright (c) 2014, Francesca Pacini
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.

 */

/**
 * \file
 *
 * \author
 *      Francesca Pacini <fkpacini@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "client-m2m.h"

#include "exiDecode.h"
#include "m2mBuild.h"

#if !UIP_CONF_IPV6_RPL && !defined (CONTIKI_TARGET_MINIMAL_NET) && !defined (CONTIKI_TARGET_NATIVE)
#warning "Compiling with static routing!"
#include "static-routing.h"
#endif

#include "contiki.h"
#include "contiki-net.h"
#include "jsonparse.h"
#include "dev/button-sensor.h"
#include "er-coap.h"
#include "er-coap-engine.h"


#define DEBUG 1

#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#define PRINT6ADDR(addr) PRINTF("[%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x]", ((uint8_t *)addr)[0], ((uint8_t *)addr)[1], ((uint8_t *)addr)[2], ((uint8_t *)addr)[3], ((uint8_t *)addr)[4], ((uint8_t *)addr)[5], ((uint8_t *)addr)[6], ((uint8_t *)addr)[7], ((uint8_t *)addr)[8], ((uint8_t *)addr)[9], ((uint8_t *)addr)[10], ((uint8_t *)addr)[11], ((uint8_t *)addr)[12], ((uint8_t *)addr)[13], ((uint8_t *)addr)[14], ((uint8_t *)addr)[15])
#define PRINTLLADDR(lladdr) PRINTF("[%02x:%02x:%02x:%02x:%02x:%02x]",(lladdr)->addr[0], (lladdr)->addr[1], (lladdr)->addr[2], (lladdr)->addr[3],(lladdr)->addr[4], (lladdr)->addr[5])
#else
#define PRINTF(...)
#define PRINT6ADDR(addr)
#define PRINTLLADDR(addr)
#endif

#define SERVER_NODE(ipaddr)   uip_ip6addr(ipaddr, 0xfe80, 0, 0, 0, 0x0200, 0,  \
                                          0, 0x02)
/* TODO: This server address is hard-coded for Cooja. */
//#define SERVER_NODE(ipaddr)   uip_ip6addr(ipaddr, 0xaaaa, 0, 0, 0, 0x0212, 0x7402, 0x0002, 0x0202) /* cooja2 */
//#define SERVER_NODE(ipaddr)   uip_ip6addr(ipaddr, 0xaaaa, 0, 0, 0, 0x0200, 0, 0, 0x0002) /* cooja2 */

#define LOCAL_PORT      UIP_HTONS(COAP_DEFAULT_PORT+1)
#define REMOTE_PORT     UIP_HTONS(COAP_DEFAULT_PORT)

#define TOGGLE_INTERVAL 10

#define COAP_BLOCKING_REQUEST_FOR_PUT_BLOCKWISE_RESPONSE(server_addr, server_port, request, chunk_handler) \
{ \
  static struct request_state_t request_state; \
  PT_SPAWN(process_pt, &request_state.pt, \
           coap_blocking_request_for_put_blockwise_response(&request_state, ev, \
                                 server_addr, server_port, \
                                 request, chunk_handler) \
  ); \
}

void coap_blocking_request_callback_blockwise_put(void *callback_data, void *response) {
    struct request_state_t *state = (struct request_state_t *) callback_data;
    state->response = (coap_packet_t*) response;
    process_poll(state->process);
}

PT_THREAD(coap_blocking_request_for_put_blockwise_response(struct request_state_t *state, process_event_t ev,
        uip_ipaddr_t *remote_ipaddr, uint16_t remote_port,
        coap_packet_t *request,
        blocking_response_handler request_callback)) {
    PT_BEGIN(&state->pt);
    //printf("\nclient in blocking put\n\n\n");
    
    static uint8_t more;
    static uint32_t res_block;
    static uint8_t block_error;

    state->block_num = 1;
    state->response = NULL;
    state->process = PROCESS_CURRENT();

    more = 0;
    res_block = 0;
    block_error = 0;

    do {

        request->mid = coap_get_mid();
        if ((state->transaction = coap_new_transaction(request->mid, remote_ipaddr, remote_port))) {
            state->transaction->callback = coap_blocking_request_callback_blockwise_put;
            state->transaction->callback_data = state;

            if (state->block_num > 0) {
                coap_set_header_block2(request, state->block_num, 0, REST_MAX_CHUNK_SIZE);
            }

            state->transaction->packet_len = coap_serialize_message(request, state->transaction->packet);


            coap_send_transaction(state->transaction);


            PRINTF("Requested #%lu (MID %u)\n", state->block_num, request->mid);

            PT_YIELD_UNTIL(&state->pt, ev == PROCESS_EVENT_POLL);
            
            if (!state->response) {
                PRINTF("Server not responding\n");
                PT_EXIT(&state->pt);
            }

            coap_get_header_block2(state->response, &res_block, &more, NULL, NULL);

            PRINTF("B. Received #%lu%s (%u bytes)\n", res_block, more ? "+" : "", state->response->payload_len);

            if (res_block == state->block_num) {
                request_callback(state->response);
                ++(state->block_num);
            } else {
                printf("WRONG BLOCK %lu/%lu\n", res_block, state->block_num);
                ++block_error;
            }
        } else {
            PRINTF("Could not allocate transaction buffer");
            PT_EXIT(&state->pt);
        }
    } while (more && block_error < COAP_MAX_ATTEMPTS);

    PT_END(&state->pt);
}


PROCESS(coap_client_example, "COAP Client Example");
AUTOSTART_PROCESSES(&coap_client_example);

struct jsonparse_state parse;
uip_ipaddr_t server_ipaddr;
static struct etimer et;

#define NUMBER_OF_URLS 2

/* leading and ending slashes only for demo purposes, get cropped automatically when setting the Uri-Path */

#if APPLICATION_CREATE
#if JSON && !EXI
char* service_url = {"gwjson/scls/applications"};
#elif !JSON && EXI
char* service_url = {"gw0exi/scls/applications"};
#else
char* service_url = {".well-known/core"};
#endif
#endif


#if APPLICATION_RETRIEVE
#if JSON && !EXI
char* service_url = {"gwjson/scls/applications/app_get"};
#elif !JSON && EXI
char* service_url = {"gw0exi/scls/applications/app_get"};
#else
char* service_url = {".well-known/core"};
#endif
#endif

#if APPLICATION_UPDATE
#if JSON && !EXI
char* service_url = {"gwjson/scls/applications/app_put"};
#elif !JSON && EXI
char* service_url = {"gw0exi/scls/applications/app_put"};
#else
char* service_url = {".well-known/core"};
#endif
#endif

#if SUBSCRIPTION_CREATE
#if JSON && !EXI
char* service_url = {"gwjson/scls/applications/app/subscriptions"};
#elif !JSON && EXI
char* service_url = {"gw0exi/scls/applications/app/subscriptions"};
#else
char* service_url = {".well-known/core"};
#endif
#endif

#if CONTENT_INSTANCE_RETRIEVE && TD_M2M_COAP_08
#if JSON && !EXI
char* service_url = {"gwjson/scls/applications/app/c1/contentInstances/test"};
#elif !JSON && EXI
char* service_url = {"gw0exi/scls/applications/app/c1/contentInstances/test"};
#else
char* service_url = {".well-known/core"};
#endif
#endif

uint8_t ack_buffer[ACK_BUFFER_SIZE];
char incoming_message[BUFFER_SIZE];
uint8_t response_more;

uint8_t success;
uint16_t num;

int32_t structure_dim;
#define CHUNKS_TOTAL structure_dim
/* This function is will be passed to COAP_BLOCKING_REQUEST() to handle responses. */
#if APPLICATION_UPDATE
uint8_t blockwise_put_response;
#endif
uint32_t seq_num;
uint32_t seq_num_put;

#if POST || PUT

void
client_chunk_handler(void *response) {

    const uint8_t *chunk;
    coap_packet_t * pkt = (coap_packet_t *) response;
    int len = coap_get_payload(response, &chunk);

    //printf("********************\nReceived from server:\n");

    if (len > 0) {
        PRINTF("|%.*s\n", len, (char *) chunk);

    }

    PRINTF("Response code %d\n", pkt->code); /*
        if ((pkt->code!=REST.status.OK)&&((pkt->code!=REST.status.NOT_MODIFIED))) 
        {
            success=0;
            printf("fail!\n");
        }
     */
    static uint8_t more_;
    static uint8_t code;
#if APPLICATION_UPDATE
    blockwise_put_response = 0;
#endif
    if (CHUNKS_TOTAL > PREFERRED_CHUNK_SIZE) {

        if (IS_OPTION(pkt, COAP_OPTION_BLOCK1)) {
            coap_get_header_block1(response, &seq_num, &more_, NULL, NULL);
            code = pkt->code;
#if APPLICATION_CREATE || SUBSCRIPTION_CREATE
            if (code == REST.status.CREATED) {
                if (seq_num < ACK_BUFFER_SIZE) {
                    ack_buffer[seq_num] = 1;
                    PRINTF("ACK for chunk %d\n", seq_num);
                } else PRINTF("Cannot receive ack\n");
            } else PRINTF("Not a successful ack\n");
#endif
#if APPLICATION_RETRIEVE
            if ((!more_) && (code == REST.status.OK) || ((more_) && (code == REST.status.OK))) {
                if (seq_num < ACK_BUFFER_SIZE) {
                    ack_buffer[seq_num] = 1;
                    PRINTF("ACK for chunk %d\n", seq_num);
                } else PRINTF("Cannot receive ack\n");
            } else PRINTF("Not a successful ack\n");
#endif
#if APPLICATION_UPDATE
            if (code == REST.status.CHANGED) {
                if (seq_num < ACK_BUFFER_SIZE) {
                    ack_buffer[seq_num] = 1;
                    PRINTF("ACK for chunk %d\n", seq_num);
                } else printf("Cannot receive ack \n");
            } else printf("Not a successful ack code %d\n", code);
#endif



        } else PRINTF("No Block1 option\n");
#if APPLICATION_UPDATE
        if (IS_OPTION(pkt, COAP_OPTION_BLOCK2)) {
            blockwise_put_response = 1;
            coap_get_header_block2(response, &seq_num, &more_, NULL, NULL);
            if (more_)
                blockwise_put_response = 0;
        }
#endif    
    } else {
        if ((pkt->code == REST.status.OK))
            ack_buffer[0] = 1;
    }
}

#endif
#if GET

uint32_t cpos;
uint8_t more;

void
client_chunk_handler(void *response) {

    static uint8_t *chunk;
    int len = coap_get_payload(response, &chunk);
    if (cpos > BUFFER_SIZE)
        return; //buffer full discard rest of the message

    memcpy((char *) (incoming_message + cpos), (char*) chunk, len);
    cpos += len;

    //snprintf((char *) (char *) (incoming_message + cpos), "%c", "\0");
    //printf("len=%d\n",len);
    more = coap_get_header_block2(response, NULL, &more, NULL, NULL);

}

#endif

int successful_iteration_count;
int failure_iteration_count;
int32_t offset;
char outbuffer[BUFFER_SIZE];
uint32_t currentpos;
char chunk_buffer[PREFERRED_CHUNK_SIZE];

#if JSON

struct jsonparse_state parse;
int pos_out;
struct jsontree_context json;
#include "jsontree.h"
#include "jsonparse.h"
#include "json-data.h"
const struct jsontree_string namespace = JSONTREE_STRING("http://uri.etsi.org/m2m");
#if _APPLICATION && !SUBSCRIPTION

#if PROFILE_1 || PROFILE_2 || PROFILE_3 || PROFILE_4
const struct jsontree_string accessRightID = JSONTREE_STRING("accessRightID");
#endif

#if PROFILE_2 || PROFILE_3 || PROFILE_4
const struct jsontree_string searchString_0 = JSONTREE_STRING("searchString");
JSONTREE_OBJECT(search_string_0,
        JSONTREE_PAIR("searchString", &searchString_0));

struct jsontree_value *jsontree_value_search_strings[1] = {&search_string_0};
struct jsontree_array search_strings_static = {JSON_TYPE_ARRAY, 1, jsontree_value_search_strings};
#endif

#if PROFILE_3 || PROFILE_4
const struct jsontree_string apoc = JSONTREE_STRING("apoc_");
const struct jsontree_string accessrightsreference = JSONTREE_STRING("/gsclBase/applications/appaccessRights");
const struct jsontree_string groups_reference = JSONTREE_STRING("/gsclBase/applications/app/groups");
#endif

#if PROFILE_4
const struct jsontree_string subscriptionsreference = JSONTREE_STRING("/gsclBase/applications/app/subscriptions");
const struct jsontree_string notificationchannelsreference = JSONTREE_STRING("/gsclBase/applications/app/notifications");
#endif

#if PROFILE_1
JSONTREE_OBJECT(application_obj,
        JSONTREE_PAIR("-xmlns", &namespace)
        );
#elif PROFILE_2
JSONTREE_OBJECT(application_obj,
        JSONTREE_PAIR("-xmlns", &namespace),
        JSONTREE_PAIR("accessRightID", &accessRightID),
        JSONTREE_PAIR("searchStrings", &search_strings_static)
        );
#elif PROFILE_3
JSONTREE_OBJECT(application_obj,
        JSONTREE_PAIR("-xmlns", &namespace),
        JSONTREE_PAIR("accessRightID", &accessRightID),
        JSONTREE_PAIR("searchStrings", &search_strings_static),
        JSONTREE_PAIR("aPoC", &apoc),
        JSONTREE_PAIR("groupsReference", &groups_reference),
        JSONTREE_PAIR("accessRightsReference", &accessrightsreference)
        );
#elif PROFILE_4
JSONTREE_OBJECT(application_obj,
        JSONTREE_PAIR("-xmlns", &namespace),
        JSONTREE_PAIR("accessRightID", &accessRightID),
        JSONTREE_PAIR("searchStrings", &search_strings_static),
        JSONTREE_PAIR("aPoC", &apoc),
        JSONTREE_PAIR("groupsReference", &groups_reference),
        JSONTREE_PAIR("accessRightsReference", &accessrightsreference),
        JSONTREE_PAIR("subscriptionsReference", &subscriptionsreference),
        JSONTREE_PAIR("notificationChannelsReference", &notificationchannelsreference)
        );
#endif

JSONTREE_OBJECT(tree,
        JSONTREE_PAIR("application", &application_obj)
        );

#endif
#if !_APPLICATION && _SUBSCRIPTION
static const struct jsontree_string expirationTime = JSONTREE_STRING("2012-07-31T13:33:55.000839");
static const struct jsontree_string delayTolerance = JSONTREE_STRING("2012-07-31T13:33:55.000839");
static const struct jsontree_string contact = JSONTREE_STRING("coap://DA_IP_Addr:Port/da_notif");

#if PROFILE_1
JSONTREE_OBJECT(subscription_obj,
        JSONTREE_PAIR("-xmlns", &namespace)
        );
#elif PROFILE_2
JSONTREE_OBJECT(subscription_obj,
        JSONTREE_PAIR("-xmlns", &namespace),
        JSONTREE_PAIR("expirationTime", &expirationTime)
        );
#elif PROFILE_3
JSONTREE_OBJECT(subscription_obj,
        JSONTREE_PAIR("-xmlns", &namespace),
        JSONTREE_PAIR("expirationTime", &expirationTime),
        JSONTREE_PAIR("delayTolerance", &delayTolerance)
        );
#elif PROFILE_4
JSONTREE_OBJECT(subscription_obj,
        JSONTREE_PAIR("-xmlns", &namespace),
        JSONTREE_PAIR("expirationTime", &expirationTime),
        JSONTREE_PAIR("delayTolerance", &delayTolerance),
        JSONTREE_PAIR("contact", &contact)
        );
#endif

JSONTREE_OBJECT(tree,
        JSONTREE_PAIR("subscription", &subscription_obj)
        );

#endif
#if !_APPLICATION && !_SUBSCRIPTION
JSONTREE_OBJECT(tree,
        );

#endif

#if TAKE_BLOCK_TIMING
uint32_t start_time_json;
uint32_t end_time_json;
uint32_t diff;
#endif

int putchar_out(int c) {
    if (pos_out < BUFFER_SIZE) {
#if TAKE_BLOCK_TIMING
        if (pos_out==0)
            start_time_json = energest_type_time(ENERGEST_TYPE_CPU);
        if (pos_out==63)
        {
            end_time_json= energest_type_time(ENERGEST_TYPE_CPU);
            diff= (uint32_t) ((end_time_json - start_time_json) / 0.32768);
            printf("JSON encoding of first block: %lu\n",diff);
        }
#endif
        outbuffer[pos_out++] = c;
        return c;
    } else printf("DONE\n");
    return 0;
}

void init_json() {
    printf("server-jsontree_context init...");
    json.putchar = putchar_out;
    json.values[0] = (struct jsontree_value *) &tree;
    pos_out = 0;
    //jsontree_array_init(&rnd_array,&array_callback);

}

void send_json_client() {
    pos_out = 0;

    jsontree_reset(&json);
    while (jsontree_print_next(&json) && json.path <= json.depth);

}

#endif

uint8_t request_more;

#if APPLICATION_UPDATE
RESOURCE(application_update_callback, METHOD_POST, "callback", "title=\"CLIENT\";rt=\"Data\"");

void application_update_callback_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset) {

    printf("\n\nHello I'm the callback\n\n");
    REST.set_response_status(response, REST.status.DELETED);
}
#endif

PROCESS_THREAD(coap_client_example, ev, data) {
    PROCESS_BEGIN();

#if _APPLICATION && !_SUBSCRIPTION
    printf("##C Application\n");
#elif !_APPLICATION && _SUBSCRIPTION
    printf("##C Subscription\n");
#endif


#if PROFILE_1 && !PROFILE_2 && !PROFILE_3 && !PROFILE_4
    printf("##C PROFILE 1\n");
#elif !PROFILE_1 && PROFILE_2 && !PROFILE_3 && !PROFILE_4
    printf("##C PROFILE 2\n");
#elif !PROFILE_1 && !PROFILE_2 && PROFILE_3 && !PROFILE_4
    printf("##C PROFILE 3\n");
#elif !PROFILE_1 && !PROFILE_2 && !PROFILE_3 && PROFILE_4
    printf("##C PROFILE 4\n");
#else
    printf("##C PROFILE 0\n");
#endif

    failure_iteration_count = 0;
    successful_iteration_count = 0;

    static coap_packet_t request[1]; /* This way the packet can be treated as pointer as usual. */
    SERVER_NODE(&server_ipaddr);

#if PRINT_DESERIALIZATION_TIME
    static uint32_t start_deserialization;
    static uint32_t end_deserialization;
    static uint32_t time_difference_deserialization;
#endif

    static uint32_t start_request_cpu;
    static uint32_t end_request_cpu;
    static uint32_t time_difference_cpu;
    static uint32_t time_difference_cpu_en;

    static uint32_t start_request_transmission;
    static uint32_t end_request_transmission;
    static uint32_t time_difference_transmission;
    static uint32_t time_difference_transmission_en;

    static uint32_t start_request_lpm;
    static uint32_t end_request_lpm;
    static uint32_t time_difference_lpm;
    
    static uint32_t start_block_request_lpm;
    static uint32_t end_block_request_lpm;
    static uint32_t time_block_difference_lpm;

    static uint32_t start_block_request_cpu;
    static uint32_t end_block_request_cpu;
    static uint32_t time_block_difference_cpu;

    static uint32_t energy_consumption_client;
    
    static uint32_t total_time_difference;
    rest_init_engine();
#if APPLICATION_UPDATE
    //rest_init_engine();
    rest_activate_resource(&resource_application_update_callback);
#endif
    static uint32_t numeric_token;

    /* receives all CoAP messages */
    //coap_receiver_init(); //TODO: check if something else is needed
    etimer_set(&et, TOGGLE_INTERVAL * CLOCK_SECOND);
    numeric_token = 10;
    energest_flush();
#if POST || PUT
    char * _token[TOKEN_SIZE];
    static int i;
#if POST
    printf("**POST request\n");
#endif
#if PUT
    printf("**PUT request\n");
#endif
#endif
#if GET
    printf("**GET request\n");
#endif


#if EXI && !JSON
#if WITH_SCHEMA
    printf("~~EXI schema enabled\n");
#else
    printf("~~EXI schema less\n");
#endif
#endif

#if !EXI && JSON
    printf("~~JSON\n");
#endif

#if PIPELINE
    printf("PIPELINE\n");
#else
    printf("NOT PIPELINE\n");
#endif
    
#if JSON && (POST||PUT)
    init_json();
#endif

#if POST || PUT
    while (1) {
        PROCESS_YIELD();

        if (etimer_expired(&et)) {

            offset = 0;
            structure_dim = 0;
            num = 0;
            PRINT6ADDR(&server_ipaddr);
            PRINTF(" : %u\n", UIP_HTONS(REMOTE_PORT));

            request_more = 0;
            //printf("Start request\n");

            start_request_cpu = energest_type_time(ENERGEST_TYPE_CPU);
            start_request_lpm = energest_type_time(ENERGEST_TYPE_LPM);
            start_request_transmission = energest_type_time(ENERGEST_TYPE_TRANSMIT);
            
#if EXI && !JSON
            errorCode tmp_err_code = EXIP_OK;
            coap_set_header_content_format(request, APPLICATION_EXI);
#if SUBSCRIPTION_CREATE
            subscriptionBuild(outbuffer, tmp_err_code);
#endif
#if APPLICATION_CREATE || APPLICATION_UPDATE || SUBSCRIPTION_NOTIFY
            applicationBuild(outbuffer, tmp_err_code);
#endif
            structure_dim = strlen(outbuffer);
            //printf("Sending: %d bytes\n", structure_dim);
#endif
#if !EXI && JSON
            coap_set_header_content_format(request, APPLICATION_JSON);
            //uint32_t content_type = coap_get_header_content_type(request);
            //printf("Content %lu\n", content_type);
            send_json_client();
            structure_dim = strlen(outbuffer);
#endif
#if PIPELINE
            start_request_cpu = energest_type_time(ENERGEST_TYPE_CPU);
#endif
#if PRINT_IN_OUT_JSON || PRINT_IN_OUT_EXI
            printf("Sending: %s\n", outbuffer);
            //printf("Sending: %d bytes\n", structure_dim);
#endif
            if (structure_dim / PREFERRED_CHUNK_SIZE <= ACK_BUFFER_SIZE) {
                for (i = 0; i < (structure_dim / PREFERRED_CHUNK_SIZE + 1); i++)
                    ack_buffer[i] = 0;

                while (offset < CHUNKS_TOTAL) {
                    currentpos = 0;
#if POST
                    coap_init_message(request, COAP_TYPE_CON, COAP_POST, 0);
#endif
#if PUT
                    coap_init_message(request, COAP_TYPE_CON, COAP_PUT, 0);
#endif
                    PRINTF("Sending PUT /POST to %s\n", service_url);
                    coap_set_header_uri_path(request, service_url);

                    while ((currentpos < (PREFERRED_CHUNK_SIZE - OFFSET)) && (CHUNKS_TOTAL - offset) >= currentpos) {
                        currentpos += snprintf((char *) chunk_buffer + currentpos, PREFERRED_CHUNK_SIZE - OFFSET - currentpos + 1, "%c", *(outbuffer + currentpos + offset));
                    }

                    PRINTF("OUT: strpos %d\n", currentpos);
                    PRINTF("OUT buffer: %s\n", chunk_buffer);
                    if (currentpos > PREFERRED_CHUNK_SIZE - OFFSET) {
                        currentpos = PREFERRED_CHUNK_SIZE - OFFSET;
                    }

                    if (offset + (int32_t) currentpos > CHUNKS_TOTAL) {
                        currentpos = CHUNKS_TOTAL - offset;
                    }
                    offset += currentpos;
                    PRINTF("offset %d\n", offset);
                    PRINTF("chunks_total %d\n", CHUNKS_TOTAL);
                    if (offset >= CHUNKS_TOTAL) {
                        {
                            PRINTF("More set\n");
                            request_more = 1;
                        }
                    }
                    if (CHUNKS_TOTAL > PREFERRED_CHUNK_SIZE - OFFSET) {
                        PRINTF("Set block1\n");
                        coap_set_header_block1(request, num, request_more, PREFERRED_CHUNK_SIZE - OFFSET);
                    }
                    static uint8_t _more;
                    coap_get_header_block1(request, NULL, &_more, NULL, NULL);
                    PRINTF("more %d\n", _more);

                    if IS_OPTION(request, COAP_OPTION_BLOCK1)
                        PRINTF("Block1 is set\n");
                    else PRINTF("Block1 is not set\n");
                    coap_set_payload(request, chunk_buffer, currentpos);
                    sprintf((char *) _token, "%lu", numeric_token);
                    coap_set_token(request, (uint8_t *) _token, TOKEN_SIZE);
                    PRINTF("token %s\n", _token);
                    PRINTF("numeric_token %d\n", numeric_token);
                    PRINTF("Sending chunk %d\n", num);
#if TAKE_BLOCK_TIMING 
                    start_block_request_lpm = energest_type_time(ENERGEST_TYPE_LPM);
                    start_block_request_cpu = energest_type_time(ENERGEST_TYPE_CPU);
#endif
                    COAP_BLOCKING_REQUEST(&server_ipaddr, REMOTE_PORT, request, client_chunk_handler);
#if TAKE_BLOCK_TIMING 
                    end_block_request_lpm = energest_type_time(ENERGEST_TYPE_LPM);
                    end_block_request_cpu = energest_type_time(ENERGEST_TYPE_CPU);
                    time_block_difference_lpm = (end_block_request_lpm - start_block_request_lpm) / RATIO;
                    time_block_difference_cpu = (end_block_request_cpu - start_block_request_cpu) / RATIO;
                    printf("Block %d: LPM time %lu\n", num, time_block_difference_lpm);
                    printf("Block %d: CPU time %lu\n", num, time_block_difference_cpu);
#endif
                    num++;
                }
#if APPLICATION_UPDATE
                if (blockwise_put_response) {
                    coap_init_message(request, COAP_TYPE_CON, COAP_PUT, 0);
                    coap_set_header_uri_path(request, service_url);
                    COAP_BLOCKING_REQUEST_FOR_PUT_BLOCKWISE_RESPONSE(&server_ipaddr, REMOTE_PORT, request, client_chunk_handler)
                }
#endif    
                numeric_token++;

            } else printf("F for FAIL\n");

            end_request_cpu = energest_type_time(ENERGEST_TYPE_CPU);
            end_request_lpm = energest_type_time(ENERGEST_TYPE_LPM);
            end_request_transmission = energest_type_time(ENERGEST_TYPE_TRANSMIT);
            //printf("End request\n");

            time_difference_cpu = (end_request_cpu - start_request_cpu) / RATIO;
            time_difference_lpm = (end_request_lpm - start_request_lpm) / RATIO;
            total_time_difference = time_difference_cpu + time_difference_lpm;

            time_difference_transmission = (end_request_transmission - start_request_transmission) / RATIO;
            
            time_difference_cpu_en = (end_request_cpu - start_request_cpu) / RATIO_EN;            
            time_difference_transmission_en = (end_request_transmission - start_request_transmission) / RATIO_EN;
            energy_consumption_client=VOLTAGE*(time_difference_cpu_en*CPU_CURRENT+time_difference_transmission_en*TX_CURRENT);


#if CLIENT_ENERGY_PRINT
#if HD_EN
            printf("CPU_contribution Joule*10^-9 %u\n", VOLTAGE*time_difference_cpu_en * CPU_CURRENT);
            printf("TX_contribution Joule*10^-9 %u\n", VOLTAGE*time_difference_transmission*TX_CURRENT);
            printf("ENERGY_CLIENT Joule*10^-9 %u\n",energy_consumption_client);
#else
#if HUNDREDS_EN
            printf("CPU_contribution Joule*10^-8 %u\n", VOLTAGE*time_difference_cpu_en * CPU_CURRENT);
            printf("TX_contribution Joule*10^-8 %u\n", VOLTAGE*time_difference_transmission*TX_CURRENT);
            printf("ENERGY_CLIENT Joule*10^-8 %u\n",energy_consumption_client);   
#else
            printf("CPU_contribution Joule*10^-7 %u\n", VOLTAGE*time_difference_cpu_en* CPU_CURRENT);
            printf("TX_contribution Joule*10^-7 %u\n", VOLTAGE*time_difference_transmission_en*TX_CURRENT);
            printf("ENERGY_CLIENT Joule*10^-7 %u\n",energy_consumption_client);
#endif          
#endif
#endif         

            PRINTF("Checking ack for %d\n", num - 1);
            if (ack_buffer[num - 1]) {
#if HD
                //printf("LPM_time tens_microseconds %u\n", time_difference_lpm);
                //printf("CPU_time tens_microseconds %u\n", time_difference_cpu);
                //printf("TX_time tens_microseconds %u\n", time_difference_transmission);
                printf("S_CPU %d T_tens_of_microseconds %lu\n", successful_iteration_count, (uint32_t) (time_difference_cpu));
                printf("S_LPM %d T_tens_of_microseconds %lu\n", successful_iteration_count, (uint32_t) (time_difference_lpm));
                printf("S %d T_tens_of_microseconds %lu\n", successful_iteration_count, (uint32_t) (total_time_difference));

#else
#if HUNDREDS
                //printf("LPM_time hundreds_microseconds %u\n", time_difference_lpm);
                //printf("CPU_time hundreds_microseconds %u\n", time_difference_cpu);
                //printf("TX_time hundreds_microseconds %u\n", time_difference_transmission);
                printf("S %d T_hundreds_of_microseconds %lu\n", successful_iteration_count, (uint32_t) (total_time_difference));
#else
                //printf("LPM_time ms %u\n", time_difference_lpm);
                //printf("CPU_time ms %u\n", time_difference_cpu);
                //printf("TX_time ms %u\n", time_difference_transmission);
                printf("S %d T_ms %lu\n", successful_iteration_count, (uint32_t) (total_time_difference));

#endif
#endif
                //printf("C number_chunks %d\n",num);
                successful_iteration_count++;
            } else {
#if HD
                printf("F %d T_tens_of_microseconds %lu\n", failure_iteration_count, (uint32_t) (total_time_difference));
#else
#if HUNDREDS
                printf("F %d T_hundreds_of_microseconds %lu\n", failure_iteration_count, (uint32_t) (total_time_difference));
#else
#endif
                printf("F %d T_ms %lu\n", failure_iteration_count, (uint32_t) (total_time_difference));
#endif
                failure_iteration_count++;
            }
            //etimer_reset(&et);
        }
    }

#endif
#if GET
    while (1) {
        PROCESS_YIELD();

        if (etimer_expired(&et)) {
            cpos = 0;

            coap_init_message(request, COAP_TYPE_CON, COAP_GET, 0);
            coap_set_header_uri_path(request, service_url);

#if EXI && !JSON
            coap_set_header_content_format(request, APPLICATION_EXI);
#elif !EXI && JSON
            coap_set_header_content_format(request, APPLICATION_JSON);
#endif

            start_request_cpu = energest_type_time(ENERGEST_TYPE_CPU);
            start_request_lpm = energest_type_time(ENERGEST_TYPE_LPM);
            start_request_transmission = energest_type_time(ENERGEST_TYPE_TRANSMIT);
            
            COAP_BLOCKING_REQUEST(&server_ipaddr, REMOTE_PORT, request, client_chunk_handler);

            end_request_cpu = energest_type_time(ENERGEST_TYPE_CPU);
            end_request_lpm = energest_type_time(ENERGEST_TYPE_LPM);
            end_request_transmission = energest_type_time(ENERGEST_TYPE_TRANSMIT);

            time_difference_cpu = (end_request_cpu - start_request_cpu) / RATIO;
            time_difference_lpm = (end_request_lpm - start_request_lpm) / RATIO;

            time_difference_transmission = (end_request_transmission - start_request_transmission) / RATIO;
            
            time_difference_cpu_en = (end_request_cpu - start_request_cpu) / RATIO_EN;            
            time_difference_transmission_en = (end_request_transmission - start_request_transmission) / RATIO_EN;
            energy_consumption_client=VOLTAGE*(time_difference_cpu_en*CPU_CURRENT+time_difference_transmission_en*TX_CURRENT);


#if CLIENT_ENERGY_PRINT
#if HD_EN
            printf("CPU_contribution Joule*10^-9 %u\n", VOLTAGE*time_difference_cpu_en * CPU_CURRENT);
            printf("TX_contribution Joule*10^-9 %u\n", VOLTAGE*time_difference_transmission*TX_CURRENT);
            printf("ENERGY_CLIENT Joule*10^-9 %u\n",energy_consumption_client);
#else
#if HUNDREDS_EN
            printf("CPU_contribution Joule*10^-8 %u\n", VOLTAGE*time_difference_cpu_en * CPU_CURRENT);
            printf("TX_contribution Joule*10^-8 %u\n", VOLTAGE*time_difference_transmission*TX_CURRENT);
            printf("ENERGY_CLIENT Joule*10^-8 %u\n",energy_consumption_client);   
#else
            printf("CPU_contribution Joule*10^-7 %u\n", VOLTAGE*time_difference_cpu_en* CPU_CURRENT);
            printf("TX_contribution Joule*10^-7 %u\n", VOLTAGE*time_difference_transmission_en*TX_CURRENT);
            printf("ENERGY_CLIENT Joule*10^-7 %u\n",energy_consumption_client);
#endif          
#endif
#endif 

            total_time_difference = time_difference_cpu + time_difference_lpm;
            if ((cpos > 0) && (more == 1)) {
#if HD
                //printf("LPM_time tens_microseconds %u\n", time_difference_lpm);
                //printf("CPU_time tens_microseconds %u\n", time_difference_cpu);
                //printf("TX_time tens_microseconds %u\n", time_difference_transmission);
                printf("S %d T_tens_of_microseconds %lu\n", successful_iteration_count, (uint32_t) (total_time_difference));
                printf("~~~Size: %d\n", strlen(incoming_message));
#else
#if HUNDREDS
                //printf("LPM_time hundreds_microseconds %u\n", time_difference_lpm);
                //printf("CPU_time hundreds_microseconds %u\n", time_difference_cpu);
                //printf("TX_time hundreds_microseconds %u\n", time_difference_transmission);
                printf("S %d T_hundreds_of_microseconds %lu\n", successful_iteration_count, (uint32_t) (total_time_difference));
#else
                //printf("LPM_time ms %u\n", time_difference_lpm);
                //printf("CPU_time ms %u\n", time_difference_cpu);
                //printf("TX_time ms %u\n", time_difference_transmission);
                printf("S %d T_ms %lu\n", successful_iteration_count, (uint32_t) (total_time_difference));
#endif
#endif

#if EXI
#if DECODE_IN_EXI
                printf("Message decoding %s\n", incoming_message);
                decodeMessage(incoming_message);
                printf("~~~Size: %d\n",strlen(incoming_message));
#endif
#if PRINT_DESERIALIZATION_TIME
                start_deserialization = energest_type_time(ENERGEST_TYPE_CPU);
                decodeMessage(incoming_message);
                end_deserialization = energest_type_time(ENERGEST_TYPE_CPU);
                time_difference_deserialization = (end_deserialization - start_deserialization) / RATIO;
#if HD
                printf("Deserialization tens_of_microseconds %lu\n", (uint32_t) (time_difference_deserialization));
#else
#if HUNDREDS
                printf("Deserialization hundreds_of_microseconds %lu\n", (uint32_t) (time_difference_deserialization));
#else
                printf("Deserialization ms %lu\n", (uint32_t) (time_difference_deserialization));
#endif
#endif
#endif
#endif         
#if PRINT_IN_OUT_JSON
                printf("Incoming message: %s\n", incoming_message);
                printf("~~~Size: %d\n", strlen(incoming_message));
#endif
                //printf("Size %d\n",strlen(incoming_message));
#if PRINT_DESERIALIZATION_TIME && _APPLICATION && JSON
                start_deserialization = energest_type_time(ENERGEST_TYPE_CPU);
                init_json_client(incoming_message, strlen(incoming_message));
                process_json(&parse);
                end_deserialization = energest_type_time(ENERGEST_TYPE_CPU);
                time_difference_deserialization = (end_deserialization - start_deserialization) / RATIO;
#if HD
                printf("Deserialization tens_of_microseconds %lu\n", (uint32_t) (time_difference_deserialization));
#else
#if HUNDREDS
                printf("Deserialization hundreds_of_microseconds %lu\n", (uint32_t) (time_difference_deserialization));
#else
                printf("Deserialization ms %lu\n", (uint32_t) (time_difference_deserialization));
#endif
#endif
#endif

                successful_iteration_count++;

            } else {
                failure_iteration_count++;
#if HD
                printf("F %d T_tens_of_microseconds %lu\n", failure_iteration_count, (uint32_t) (total_time_difference));
#else
#if HUNDREDS
                printf("F %d T_hundreds %lu\n", failure_iteration_count, (uint32_t) (total_time_difference));
#else
                printf("F %d T_ms %lu\n", failure_iteration_count, (uint32_t) (total_time_difference));
#endif
#endif
            }
            //etimer_reset(&et);
        }
    }
#endif

    PROCESS_END();
}

#if _APPLICATION && JSON
#include "../../apps/exip/M2Minclude/IcsiTypes.h"

void
init_json_client(const char *buf, int len) {
    jsonparse_setup(&parse, buf, len);
}

void
process_json(struct jsonparse_state *parser) {
    int type;
    int len;
    char currentobj[MAXL];
    while ((type = jsonparse_next(parser)) != 0) {
        if (type == JSON_TYPE_PAIR_NAME) {
            len = jsonparse_get_len(parser);
            jsonparse_copy_value(parser, currentobj, len + 1);
#if PRINT_BINARY_JSON
            printf("%s: \n", currentobj);
#endif
            object_handler(parser, currentobj);

        }
    }
}




_m2m_object m2mobj;


_application app;
CharType accessRightId[CHAR_BUFFER_LEN];
CharType searchString[CHAR_BUFFER_LEN];
SearchStrings_ searchStrings;
String * stringsVector[1];
CharType groupsReference[CHAR_BUFFER_LEN];
CharType subscriptionsReference[CHAR_BUFFER_LEN];
CharType apoc_[CHAR_BUFFER_LEN];
CharType notificationChannelsReference[CHAR_BUFFER_LEN];
CharType accessRightsReference[CHAR_BUFFER_LEN];
CharType namespace_[CHAR_BUFFER_LEN];

void
object_handler(struct jsonparse_state *parser, char *name) {
    int len;
#if _APPLICATION 
    if (strcmp(name, "application") == 0) {

        app.expirationTime = NULL;
        app.accessRightID = NULL;
        app.searchStrings = NULL;
        app.creationTime = NULL;
        app.lastModifiedTime = NULL;
        app.announceTo = NULL;
        app.aPoC = NULL;
        app.aPoCPaths = NULL;
        app.containersReference = NULL;
        app.accessRightsReference = NULL;
        app.subscriptionsReference = NULL;
        app.notificationChannelsReference = NULL;
        m2mobj.application = &(application);
    }

    if (strcmp(name, "accessRightID") == 0) {
        jsonparse_next(parser);
        jsonparse_next(parser);
        len = jsonparse_get_len(parser);
        jsonparse_copy_value(parser, accessRightId, len + 1);
        app.accessRightID = &accessRightId;
#if PRINT_BINARY_JSON
        printf("     %s \n", app.accessRightID);
#endif

    } else if (strcmp(name, "-xmlns") == 0) {
        jsonparse_next(parser);
        jsonparse_next(parser);
        len = jsonparse_get_len(parser);
        jsonparse_copy_value(parser, namespace_, len + 1);
        app.namespace = &namespace_;
#if PRINT_BINARY_JSON
        printf("     %s \n", app.namespace);
#endif
    } else if (strcmp(name, "searchString") == 0) {
        jsonparse_next(parser);
        jsonparse_next(parser);
        len = jsonparse_get_len(parser);
        jsonparse_copy_value(parser, searchString, len + 1);
        app.searchStrings = &searchStrings;
        app.searchStrings->strings = stringsVector;
        app.searchStrings->strings[0] = searchString;
        app.searchStrings->length = 1;
#if PRINT_BINARY_JSON
        printf("     %s \n", app.searchStrings->strings[0]);
#endif
    } else if (strcmp(name, "groupsReference") == 0) {
        jsonparse_next(parser);
        jsonparse_next(parser);
        len = jsonparse_get_len(parser);
        jsonparse_copy_value(parser, groupsReference, len + 1);
        app.groupsReference = groupsReference;
#if PRINT_BINARY_JSON
        printf("     %s \n", app.groupsReference);
#endif
    } else if (strcmp(name, "aPoC") == 0) {
        jsonparse_next(parser);
        jsonparse_next(parser);
        len = jsonparse_get_len(parser);
        jsonparse_copy_value(parser, apoc_, len + 1);
        app.aPoC = apoc_;
#if PRINT_BINARY_JSON
        printf("     %s \n", app.aPoC);
#endif
    } else if (strcmp(name, "subscriptionsReference") == 0) {
        jsonparse_next(parser);
        jsonparse_next(parser);
        len = jsonparse_get_len(parser);
        jsonparse_copy_value(parser, subscriptionsReference, len + 1);
        app.subscriptionsReference = subscriptionsReference;
#if PRINT_BINARY_JSON
        printf("     %s \n", app.subscriptionsReference);
#endif
    } else if (strcmp(name, "notificationChannelsReference") == 0) {
        jsonparse_next(parser);
        jsonparse_next(parser);
        len = jsonparse_get_len(parser);
        jsonparse_copy_value(parser, notificationChannelsReference, len + 1);
        app.notificationChannelsReference = notificationChannelsReference;
#if PRINT_BINARY_JSON
        printf("     %s \n", app.notificationChannelsReference);
#endif
    } else if (strcmp(name, "accessRightsReference") == 0) {
        jsonparse_next(parser);
        jsonparse_next(parser);
        len = jsonparse_get_len(parser);
        jsonparse_copy_value(parser, accessRightsReference, len + 1);
        app.accessRightsReference = accessRightsReference;
#if PRINT_BINARY_JSON
        printf("     %s \n", app.accessRightsReference);
#endif
    }
#endif
}

#endif

