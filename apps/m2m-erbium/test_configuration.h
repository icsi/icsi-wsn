/* 
 * File:   test_configuration.h
 * Author: francesca
 *
 * Created on June 6, 2014, 8:22 PM
 */

#ifndef TEST_CONFIGURATION_H
#define	TEST_CONFIGURATION_H
#define SERVER_ENERGY_PRINT 0
#define CLIENT_ENERGY_PRINT 1
#define SERVER_ENERGY_PRINT_EXTERNAL 1

#define TEST_WITH_LOSS 0
#define TAKE_BLOCK_TIMING 0
#define PIPELINE 0

#define EXI 1
#define JSON 0

#define CLIENT_RX 185
#define CPU_CURRENT 22
#define TX_CURRENT 258
#define VOLTAGE 1

/*_APPLICATION (create)*/
#define TD_M2M_COAP_01 0
/*_APPLICATION (retrieve)*/
#define TD_M2M_COAP_02 0
/*_APPLICATION (update)*/
#define TD_M2M_COAP_03 0
/*_SUBSCRIPTION (create)*/
#define TD_M2M_COAP_04 1
/*
 * NOTIFY (subscription notify)
 * #define TD_M2M_COAP_05 1
 * #define TD_M2M_COAP_06 subscription delete
 * #define TD_M2M_COAP_07 application delete
 */

/*CONTENT_INSTANCE (retrieve)*/
#define TD_M2M_COAP_08 0

/*
 * TD_M2M_COAP_09 gives the same output as TD_M2M_COAP_08
 * TD_M2M_COAP_10 partial addressing
 * TD_M2M_COAP_11 announcement ?
 * TD_M2M_COAP_12 proxy retrieval
 * TD_M2M_COAP_13 multi-hop retrieval
 */
#if TEST_WITH_LOSS
#define HD_EN 0
#define HUNDREDS_EN 0

#define HD 0
#define HUNDREDS 0
#else
#define HD_EN 0
#define HUNDREDS_EN 0

#define HD 1
#define HUNDREDS 0
#endif



#if HD_EN
#define RATIO 0.32768
#else
#if HUNDREDS_EN
#define RATIO 3.2768
#else
#define RATIO_EN 32.768
#endif
#endif

#if HD
#define RATIO 0.32768
#else
#if HUNDREDS
#define RATIO 3.2768
#else
#define RATIO 32.768
#endif
#endif

#endif	/* TEST_CONFIGURATION_H */

