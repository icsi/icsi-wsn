/*
 * Copyright (c) 2014, Francesca Pacini
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.

 */

/**
 * \file
 *
 * \author
 *      Francesca Pacini <fkpacini@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "contiki.h"
#include "contiki-net.h"
#include "server-m2m.h"


#include "exiDecode.h"
#include "m2mBuild.h"


/* Define which resources to include to meet memory constraints. */

#include "er-coap.h"
#include "er-coap-engine.h"

#define DEBUG_SERVER 1

#if DEBUG_SERVER
#define PRINTF(...) printf(__VA_ARGS__)
#define PRINT6ADDR(addr) PRINTF("[%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x]", ((uint8_t *)addr)[0], ((uint8_t *)addr)[1], ((uint8_t *)addr)[2], ((uint8_t *)addr)[3], ((uint8_t *)addr)[4], ((uint8_t *)addr)[5], ((uint8_t *)addr)[6], ((uint8_t *)addr)[7], ((uint8_t *)addr)[8], ((uint8_t *)addr)[9], ((uint8_t *)addr)[10], ((uint8_t *)addr)[11], ((uint8_t *)addr)[12], ((uint8_t *)addr)[13], ((uint8_t *)addr)[14], ((uint8_t *)addr)[15])
#define PRINTLLADDR(lladdr) PRINTF("[%02x:%02x:%02x:%02x:%02x:%02x]",(lladdr)->addr[0], (lladdr)->addr[1], (lladdr)->addr[2], (lladdr)->addr[3],(lladdr)->addr[4], (lladdr)->addr[5])
#else
#define PRINTF(...)
#define PRINT6ADDR(addr)
#define PRINTLLADDR(addr)
#endif

#if ENERGY_REPORT
#define PRINTF_ENERGY(...) printf(__VA_ARGS__)
#else
#define PRINTF_ENERGY(...)
#endif

/******************************************************************************/

char outbuffer[OUTPUT_BUFFER];


void post_put_handler(uint8_t put, uint32_t content_type, void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset, uint32_t code);
void get_handler(uint8_t exi, void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);

int pkt_count;

int bpos_out = 0;

uint8_t rcounter;

#define CHUNKS_TOTAL bpos_out

uint32_t start_server_transmission;
uint32_t end_server_transmission;
uint32_t time_difference_server_transmission;
uint32_t client_energy_consumption;

uint32_t serialization_start;
uint32_t serialization_end;
uint32_t serialization_time_difference;

char in_message[INPUT_BUFFER];
uint32_t cpos_server;
uint32_t num;
uint32_t size;
uint16_t SZX_server;
uint8_t more;
uint32_t local_num;
char current_token[16];

#if !_APPLICATION && !_SUBSCRIPTION && _CONTENT_INSTANCE
//CharType contentInstanceApplication[JSON_CONTENT_SIZE];

#if PROFILE_1 && !PROFILE_2 && !PROFILE_3 && !PROFILE_4
CharType * contentInstanceApplication = "12002345234541240365423427";
#elif !PROFILE_1 && PROFILE_2 && !PROFILE_3 && !PROFILE_4
CharType * contentInstanceApplication = "120023452345412403654234271330187635432134510984375113552871656321405173646721";
#elif !PROFILE_1 && !PROFILE_2 && PROFILE_3 && !PROFILE_4
CharType * contentInstanceApplication = "120023452345412403654234271330187635432134510984375113552871656321405173646721151043848438118303838383811950325216232201038437437121358437843752140484389434";
#elif !PROFILE_1 && !PROFILE_2 && !PROFILE_3 && PROFILE_4
CharType * contentInstanceApplication = "12002345234541240365423427133018763543213451098437511355287165632140517364672115104384843811830383838381195032521623220103843743712135843784375214048438943400104747437430020437843784013043884384601404848484340230948444748031529484843205127474747470530474374364";
#endif



#endif

uint8_t put_more;
uint8_t put_seq_num;

/*JSON PART*/
#if JSON_SERVER
#include "jsontree.h"
#include "jsonparse.h"
#include "json-data.h"
struct jsonparse_state parse;
void send_json();
void init_json();
int putchar_out(int c);
int putchar_out_content(int c);

static const struct jsontree_string namespace = JSONTREE_STRING("http://uri.etsi.org/m2m");

#if _APPLICATION && !_SUBSCRIPTION && !_CONTENT_INSTANCE
static const struct jsontree_string accessRightID = JSONTREE_STRING("accessRight");
static const struct jsontree_string searchString_0 = JSONTREE_STRING("searchString");
static const struct jsontree_string groups_reference = JSONTREE_STRING("/gsclBase/applications/app/groups");
static const struct jsontree_string apoc = JSONTREE_STRING("apoc_");
static const struct jsontree_string accessrightsreference = JSONTREE_STRING("/gsclBase/applications/appaccessRights");
static const struct jsontree_string subscriptionsreference = JSONTREE_STRING("/gsclBase/applications/app/subscriptions");
static const struct jsontree_string notificationchannelsreference = JSONTREE_STRING("/gsclBase/applications/app/notifications");

JSONTREE_OBJECT(search_string_0,
        JSONTREE_PAIR("searchString", &searchString_0));

static struct jsontree_value *jsontree_value_search_strings[1] = {&search_string_0};
static struct jsontree_array search_strings_static = {JSON_TYPE_ARRAY, 1, jsontree_value_search_strings};


#if PROFILE_1
JSONTREE_OBJECT(application_obj,
        JSONTREE_PAIR("-xmlns", &namespace)
        );
#elif PROFILE_2
JSONTREE_OBJECT(application_obj,
        JSONTREE_PAIR("-xmlns", &namespace),
        JSONTREE_PAIR("accessRightID", &accessRightID),
        JSONTREE_PAIR("searchStrings", &search_strings_static)
        );
#elif PROFILE_3
JSONTREE_OBJECT(application_obj,
        JSONTREE_PAIR("-xmlns", &namespace),
        JSONTREE_PAIR("accessRightID", &accessRightID),
        JSONTREE_PAIR("searchStrings", &search_strings_static),
        JSONTREE_PAIR("aPoC", &apoc),
        JSONTREE_PAIR("groupsReference", &groups_reference),
        JSONTREE_PAIR("accessRightsReference", &accessrightsreference)
        );
#elif PROFILE_4
JSONTREE_OBJECT(application_obj,
        JSONTREE_PAIR("-xmlns", &namespace),
        JSONTREE_PAIR("accessRightID", &accessRightID),
        JSONTREE_PAIR("searchStrings", &search_strings_static),
        JSONTREE_PAIR("aPoC", &apoc),
        JSONTREE_PAIR("groupsReference", &groups_reference),
        JSONTREE_PAIR("accessRightsReference", &accessrightsreference),
        JSONTREE_PAIR("subscriptionsReference", &subscriptionsreference),
        JSONTREE_PAIR("notificationChannelsReference", &notificationchannelsreference)
        );
#endif


#endif

#if !_APPLICATION && !_SUBSCRIPTION && _CONTENT_INSTANCE

static const struct jsontree_string accessRightID = JSONTREE_STRING("accessRight");
static const struct jsontree_string groups_reference = JSONTREE_STRING("/gsclBase/applications/app/groups");
static const struct jsontree_string apoc = JSONTREE_STRING("apoc_");
static const struct jsontree_string accessrightsreference = JSONTREE_STRING("/gsclBase/applications/appaccessRights");
static const struct jsontree_string subscriptionsreference = JSONTREE_STRING("/gsclBase/applications/app/subscriptions");
static const struct jsontree_string notificationchannelsreference = JSONTREE_STRING("/gsclBase/applications/app/notifications");

#if PROFILE_1
JSONTREE_OBJECT(application_obj,
        JSONTREE_PAIR("-xmlns", &namespace)
        );
#elif PROFILE_2
JSONTREE_OBJECT(application_obj,
        JSONTREE_PAIR("-xmlns", &namespace),
        JSONTREE_PAIR("accessRightID", &accessRightID)
        );
#elif PROFILE_3
JSONTREE_OBJECT(application_obj,
        JSONTREE_PAIR("-xmlns", &namespace),
        JSONTREE_PAIR("accessRightID", &accessRightID),
        JSONTREE_PAIR("aPoC", &apoc),
        JSONTREE_PAIR("accessRightsReference", &accessrightsreference),
        JSONTREE_PAIR("groupsReference", &groups_reference)
        );
#elif PROFILE_4
JSONTREE_OBJECT(application_obj,
        JSONTREE_PAIR("-xmlns", &namespace),
        JSONTREE_PAIR("accessRightID", &accessRightID),
        JSONTREE_PAIR("aPoC", &apoc),
        JSONTREE_PAIR("accessRightsReference", &accessrightsreference),
        JSONTREE_PAIR("groupsReference", &groups_reference),
        JSONTREE_PAIR("subscriptionsReference", &subscriptionsreference),
        JSONTREE_PAIR("notificationChannelsReference", &notificationchannelsreference)
        );
#endif
JSONTREE_OBJECT(tree_app,
        JSONTREE_PAIR("application", &application_obj)
        );
#endif

#if _APPLICATION && !_SUBSCRIPTION && !_CONTENT_INSTANCE

JSONTREE_OBJECT(tree,
        JSONTREE_PAIR("application", &application_obj)
        );


/*CREATE APPLICATION*/
void applications_POST_json_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset) {

    //coap_content_type_t content_type = coap_get_header_content_type(pkt);
    //printf("Unsupported media type @ applications %lu\n", content_type);
    //printf("Unsupported media type @ applications %lu\n", pkt->content_type);
    post_put_handler(0, APPLICATION_JSON, request, response, buffer, preferred_size, offset, REST.status.CREATED);

}

/*RETRIEVE APPLICATION*/
void app_GET_json_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset) {

    get_handler(0, request, response, buffer, preferred_size, offset);
}

/*PUT APPLICATION*/
void app_PUT_json_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset) {
    printf("json bpos_out %d\n",bpos_out);
    coap_packet_t * pkt = (coap_packet_t *) request;

    if (!IS_OPTION(pkt, COAP_OPTION_BLOCK2)) {
        post_put_handler(1, APPLICATION_JSON, request, response, buffer, preferred_size, offset, REST.status.CHANGED);
    }
    else{
        uint32_t num_block;
        uint8_t more_block;
        uint32_t offset_block;
        coap_get_header_block2(pkt, &num_block, &more_block, more_block, NULL);
        offset_block=num_block*preferred_size;
        get_handler(0, request, response, buffer, preferred_size, offset);
    }
}

RESOURCE(applications_POST_json, "title=\"JSON SERVER\";rt=\"Data\"",
		NULL,
		applications_POST_json_handler,
		NULL,
		NULL);

RESOURCE(app_GET_json,
		"title=\"JSON SERVER\";rt=\"Data\"",
		app_GET_json_handler,
		NULL,
		NULL,
		NULL);

RESOURCE(app_PUT_json, "title=\"JSON SERVER\";rt=\"Data\"",
		NULL,
		NULL,
		app_PUT_json_handler,
		NULL);

#elif !_APPLICATION && _SUBSCRIPTION && !_CONTENT_INSTANCE


/*CREATE SUBSCRIPTION*/
void subscriptions_POST_json_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset) {

    post_put_handler(0, APPLICATION_JSON, request, response, buffer, preferred_size, offset, REST.status.CREATED);
}

/*RETRIEVE SUBSCRIPTION*/
void subscription_DELETE_json_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset) {

    REST.set_response_status(response, REST.status.DELETED);
}

/*PUT SUBSCRIPTION*/
void subscription_PUT_json_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset) {

    post_put_handler(0, APPLICATION_JSON, request, response, buffer, preferred_size, offset, REST.status.CHANGED);
}

RESOURCE(subscriptions_POST_json, "title=\"JSON SERVER\";rt=\"Data\"",
		NULL,
		subscriptions_POST_json_handler,

