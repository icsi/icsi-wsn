/* 
 * File:   server-m2m.h
 * Author: francesca
 *
 * Created on May 13, 2014, 2:34 PM
 */

#include "test_configuration.h"

#ifndef SERVER_M2M_H
#define	SERVER_M2M_H

#define PRINT_INCOMING_OR_OUTGOING_MESSAGE 1
#define PRINT_SERIALIZATION_TIME 0

#define DESERIALIZE_AT_SERVER OFF

#define ON_THE_FLY ON
#define OUTPUT_BUFFER 550
#define INPUT_BUFFER 550
#define REPORT 0
#define DEBUG_SERVER 1
#define HD 0
#define HUNDREDS 0
#define ENERGY_REPORT 0

#define JSON_CONTENT_SIZE 420

#define LISTEN_CURRENT 1.6

#if HD
#define RATIO 0.32768
#else
#if HUNDREDS
#define RATIO 3.2768
#else
#define RATIO 32.768
#endif
#endif

#define EXI_SERVER EXI
#define JSON_SERVER 1

#if REPORT
#define PRINT_S(...) printf(__VA_ARGS__)
#else
#define PRINT_S 
#endif

#if ENERGY_REPORT
#define PRINTF_ENERGY(...) printf(__VA_ARGS__)
#else
#define PRINTF_ENERGY(...)
#endif

#define CHAR_BUFFER_LEN 50
#define MAXL 50

#endif	/* SERVER_M2M_H */

