/*
 * Copyright (c) 2012, Matthias Kovatsch and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 */

/**
 * \file
 *      based on Erbium (Er) REST Engine example (with CoAP-specific code)
 * \author
 *      Matthias Kovatsch <kovatsch@inf.ethz.ch>
 *      modified by Andrea Azzara' <a.azzara@sssup.it>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "contiki.h"
#include "contiki-net.h"
#include "node-id.h"

/* Define which resources to include to meet memory constraints. */

#include "er-coap.h"
#include "er-coap-engine.h"
#include "er-coap-transactions.h"

#define DEBUG 1

#undef PRINTF
#undef PRINT6ADDR
#undef PRINTLLADDR
#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#define PRINTFLN(format, ...) printf(format "\n", ##__VA_ARGS__)
#define PRINT6ADDR(addr) PRINTF("[%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x]", ((uint8_t *)addr)[0], ((uint8_t *)addr)[1], ((uint8_t *)addr)[2], ((uint8_t *)addr)[3], ((uint8_t *)addr)[4], ((uint8_t *)addr)[5], ((uint8_t *)addr)[6], ((uint8_t *)addr)[7], ((uint8_t *)addr)[8], ((uint8_t *)addr)[9], ((uint8_t *)addr)[10], ((uint8_t *)addr)[11], ((uint8_t *)addr)[12], ((uint8_t *)addr)[13], ((uint8_t *)addr)[14], ((uint8_t *)addr)[15])
#define PRINTLLADDR(lladdr) PRINTF("[%02x:%02x:%02x:%02x:%02x:%02x]",(lladdr)->addr[0], (lladdr)->addr[1], (lladdr)->addr[2], (lladdr)->addr[3],(lladdr)->addr[4], (lladdr)->addr[5])
#else
#define PRINTF(...)
#define PRINT6ADDR(addr)
#define PRINTLLADDR(addr)
#define PRINTFLN(...)
#endif

uip_ipaddr_t server_ipaddr;
#define SERVER_NODE(ipaddr)   uip_ip6addr(ipaddr, 0xaaaa, 0, 0, 0, 0, 0, 0, 1) /* rd server */

#define REMOTE_PORT     UIP_HTONS(COAP_DEFAULT_PORT)


static struct etimer et;

/* Example URIs that can be queried. */
#define NUMBER_OF_URLS 2
/* leading and ending slashes only for demo purposes, get cropped automatically when setting the Uri-Path */
char* service_urls[NUMBER_OF_URLS] = {".well-known/core", "/rd"};

extern resource_t res_light;
extern resource_t res_toggle;
extern resource_t res_leds;
uip_ipaddr_t server_ipaddr;
static void res_post_put_handler(void *request, void *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);

/*A simple actuator example, depending on the color query parameter and post variable mode, corresponding led is activated or deactivated*/
RESOURCE(res_leds,
         "title=\"LED\";rt=\"Control\"",
         NULL,
         res_post_put_handler,
         res_post_put_handler,
         NULL);

static void
res_post_put_handler(void *request, void *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  printf("put\n");
}

static int error = 0;

void
client_chunk_handler(void *response) {
  const uint8_t *chunk;
  coap_packet_t * pkt = (coap_packet_t *) response;
  int len = coap_get_payload(response, &chunk);
  static uint8_t more_;
  static uint8_t code;
  uint32_t seq_num;

  if (len > 0) {
    PRINTF("|%.*s\n", len, (char *) chunk);
  }
  if (pkt->type == COAP_TYPE_ACK && IS_OPTION(pkt, COAP_OPTION_BLOCK1)){
    coap_get_header_block1(response, &seq_num, &more_, NULL, NULL);
    PRINTF("ACK for block %u\n", seq_num);
    error = 0;
    return;
  }
  if (pkt->type == COAP_TYPE_ACK){
    PRINTF("ACK for message\n");
    error = 0;
    return;
  }  
  if ((pkt->code == REST.status.OK)){
    PRINTF("Response OK\n");
    error = 0;
    return; 
  }
  if (pkt->code == REST.status.CREATED){
    PRINTF("Resource Created\n");
    error = 0;
    return;
  }
  PRINTF("Response code %d\n", pkt->code);
  error = 1;
}


PROCESS(rest_server_example, "Er");
AUTOSTART_PROCESSES(&rest_server_example);

PROCESS_THREAD(rest_server_example, ev, data)
{
  PROCESS_BEGIN();
  srand(node_id);

  /* Initialize the REST engine. */
  rest_init_engine();
  SERVER_NODE(&server_ipaddr);
  /* Activate the application-specific resources. */

  rest_activate_resource(&res_leds, "actuators/leds");  
  printf("Resources activated\n");

  static coap_packet_t request[1]; /* This way the packet can be treated as pointer as usual. */
  
  static long unsigned int time=0;
  static char content[12];
  //static uip_ipaddr_t server_ipaddr;
  static uint8_t *token_ptr;
  static uint8_t token_len;
  //int wait_time = getRandUint(MAX_WAITING);
  //static int base_wait = BASE_WAITING;

  int wait_time = 2;
  int base_wait = 4;

  etimer_set(&et, (wait_time + base_wait) * CLOCK_SECOND);

  while(1) {
    PROCESS_YIELD();
    if (etimer_expired(&et)) break;
    }
  etimer_reset(&et);
  etimer_set(&et, 5 * CLOCK_SECOND);
  size_t len;
  while(1) {
    PROCESS_YIELD();
    if (etimer_expired(&et)) {
      PRINTF("Sending msg to rd...");

      coap_init_message(request, COAP_TYPE_NON, COAP_POST, 0 );
      coap_set_header_uri_path(request, "/rd");
      coap_set_payload(request, content, snprintf(content, sizeof(content), "%lu", time++));
      request->mid = coap_get_mid();
      len = coap_serialize_message(request, uip_appdata);
      coap_send_message(&server_ipaddr, REMOTE_PORT, uip_appdata, len);      
      PRINTF("Done\n");
      etimer_reset(&et);
     }
  } /* while (1) */

  PROCESS_END();
}
