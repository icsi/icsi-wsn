/*==================================================================*\
|                EXIP - Embeddable EXI Processor in C                |
|--------------------------------------------------------------------|
|          This work is licensed under BSD 3-Clause License          |
|  The full license terms and conditions are located in LICENSE.txt  |
\===================================================================*/

/**
 * @file ioUtil.c
 * @brief Implements common utilities for StreamIO module
 *
 * @date Oct 26, 2010
 * @author Rumen Kyusakov
 * @version 0.5
 * @par[Revision] $Id: ioUtil.c 328 2013-10-30 16:00:10Z kjussakov $
 */

#include "ioUtil.h"
#include "stdio.h"
#include "energest.h"

uint8_t block_time_written[10];
uint32_t block_start[10];
uint32_t last_block;
        
void print_block_times() {
    printf("Serialization time for each block:\n");
    uint32_t diff;
    if (block_time_written[1]) {
        diff = (uint32_t) ((block_start[1] - block_start[0]) / 0.32768);
        printf("Block 0: tens_of_microseconds %lu\n", diff);
    }
    if (block_time_written[2]) {
        diff = (uint32_t) ((block_start[2] - block_start[1]) / 0.32768);
        printf("Block 1: tens_of_microseconds %lu\n", diff);
    }
    if (block_time_written[3]) {
        diff = (uint32_t) ((block_start[3] - block_start[2]) / 0.32768);
        printf("Block 2: tens_of_microseconds %lu\n", diff);
    }
    if (block_time_written[4]) {
        diff = (uint32_t) ((block_start[4] - block_start[3]) / 0.32768);
        printf("Block 3: tens_of_microseconds %lu\n", diff);
    }
    if (block_time_written[5]) {
        diff = (uint32_t) ((block_start[5] - block_start[4]) / 0.32768);
        printf("Block 4: tens_of_microseconds %lu\n", diff);
    }
    if (block_time_written[6]) {
        diff = (uint32_t) ((block_start[6] - block_start[5]) / 0.32768);
        printf("Block 5: tens_of_microseconds %lu\n", diff);
    }
    if (block_time_written[7]) {
        diff = (uint32_t) ((block_start[7] - block_start[7]) / 0.32768);
        printf("Block 6: tens_of_microseconds %lu\n", diff);
    }
    return;
}

#define TAKE_BLOCK_TIMES 0

void moveBitPointer(EXIStream* strm, unsigned int bitPositions) {
    int nbits;
    uint8_t increment=0;
    if (strm->context.bufferIndx == 0) {
        energest_flush();
        block_start[0] = energest_type_time(ENERGEST_TYPE_CPU);
        block_time_written[1] = 0;
        block_time_written[0] = 1;
    }
    //else
        //last_block = energest_type_time(ENERGEST_TYPE_CPU);
    strm->context.bufferIndx += bitPositions / 8;

    nbits = bitPositions % 8;
	if(nbits < 8 - strm->context.bitPointer) // The remaining (0-7) bit positions can be moved within the current byte
	{
		strm->context.bitPointer += nbits;
	}
	else
	{
                increment=1;
		strm->context.bufferIndx += 1;
		strm->context.bitPointer = nbits - (8 - strm->context.bitPointer);
	}
#if TAKE_BLOCK_TIMES    
    if ((increment==1)||((bitPositions / 8)!=0))
    switch (strm->context.bufferIndx) {
            case 63:
                if (!block_time_written[1]) {
                    block_start[1] = energest_type_time(ENERGEST_TYPE_CPU);
                    block_time_written[1] = 1;
                    block_time_written[2] = 0;
                }
                break;
            case 127:
                if (!block_time_written[2]) {
                    block_start[2] = energest_type_time(ENERGEST_TYPE_CPU);
                    block_time_written[2] = 1;
                    block_time_written[3] = 0;
                }
                break;
            case 191:
                if (!block_time_written[3]) {
                    block_start[3] = energest_type_time(ENERGEST_TYPE_CPU);
                    block_time_written[3] = 1;
                    block_time_written[4] = 0;
                }
                break;
            case 255:
                if (!block_time_written[4]) {
                    block_start[4] = energest_type_time(ENERGEST_TYPE_CPU);
                    block_time_written[4] = 1;
                    block_time_written[5] = 0;
                }
                break;
            case 319:
                if (!block_time_written[5]) {
                    block_start[5] = energest_type_time(ENERGEST_TYPE_CPU);
                    block_time_written[5] = 1;
                    block_time_written[6] = 0;
                }
                break;
            case 383:
                if (!block_time_written[6]) {
                    block_start[6] = energest_type_time(ENERGEST_TYPE_CPU);
                    block_time_written[6] = 1;
                    block_time_written[7] = 0;
                }
                break;
            case 447:
                if (!block_time_written[7]) {
                    block_start[7] = energest_type_time(ENERGEST_TYPE_CPU);
                    block_time_written[7] = 1;
                    block_time_written[8] = 0;
                }
                break;
            case 511:
                if (!block_time_written[8]) {
                    block_start[8] = energest_type_time(ENERGEST_TYPE_CPU);
                    block_time_written[8] = 1;
                    block_time_written[9] = 0;
                }
                break;
            default:
                break;
        }
#endif
}

unsigned char getBitsNumber(uint64_t val)
{
	switch(val)
	{
		case 0:
			return 0;
		case 1:
			return 1;
		case 2:
			return 2;
		case 3:
			return 2;
		case 4:
			return 3;
		case 5:
			return 3;
		case 6:
			return 3;
		case 7:
			return 3;
		case 8:
			return 4;
		case 9:
			return 4;
		case 10:
			return 4;
		case 11:
			return 4;
		case 12:
			return 4;
		case 13:
			return 4;
		case 14:
			return 4;
		case 15:
			return 4;
		default:
		{
			if(val < 32)
				return 5;
			else
				return log2INT(val) + 1;
		}
	}
}

unsigned int log2INT(uint64_t val)
{
	const uint64_t b[] = {0x2, 0xC, 0xF0, 0xFF00, 0xFFFF0000, 0xFFFFFFFF00000000};
	const unsigned int S[] = {1, 2, 4, 8, 16, 32};
	int i;

	unsigned int r = 0; // result of log2(v) will go here
	for (i = 5; i >= 0; i--) // unroll for speed...
	{
	  if (val & b[i])
	  {
		val >>= S[i];
	    r |= S[i];
	  }
	}
	return r;
}
