#SUMMARY and HOW-TO:

###SUMMARY: 

I developed this sample code using Contiki OS protothreads (m2m-erbium) to create a communication module that uses EXI and CoAP for exchanging ETSI M2M messages.
 
I complemented the open-source EXI processor EXIP (open-source, available at http://exip.sourceforge.net) with a messaging library specific for M2M messages.

###HOW-TO:

In order to execute the code, m2m-erbium and exip folders should be copied in the examples and apps folders of Contiki 2.7 respectively (http://sourceforge.net/projects/contiki/files/Contiki/Contiki%202.7/contiki-2.7.zip/download) as per Contiki Makefile. Instant Contiki can be alternatively used (http://sourceforge.net/projects/contiki/files/Instant%20Contiki/). In both cases the MSP430 compiler should be installed.

Two configuration files can be used:

- test-configuration.h in the m2m-erbium folder
- config.h in the folder exip/ICSI/encoding

They are used in the following way:

- In the test-configuration.h file you set one of the #defines of the type TD_M2M_COAP_0X to ON, while all the other are set to OFF. Each of the tests is one of the ETSI M2M COAP plugtests, so that you can choose which one to run.

- In the file config.h the type of message that is sent is selected (application, subscription or contentInstance) by setting one among _APPLICATION, _SUBSCRIPTION,_CONTENT_INSTANCE to ON and the others to OFF in the following way: if for example you are running the test TD_M2M_COAP_04, which is _SUBSCRIPTION (create) (see line 32 of test-configuration.h) you need to set _SUBSCRIPTION to ON in the file config.h

Other useful configurations are:

- In the file test-configuration.h you can switch ON one among EXI and JSON (lines 18-19) depending on the serialization format you want to use in the specific test.
- In the file config.h you can switch ON or OFF the macro variable WITH_SCHEMA (line 30) depending on whether you want to user EXI Schema Less or Schema Enabled.
