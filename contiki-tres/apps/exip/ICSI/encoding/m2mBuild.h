/*
 * File:   m2mBuild.h
 * Author: francesca
 *
 * Created on February 25, 2014, 7:52 PM
 */
/* #include "contiki.h" */
#include "grammarGenerator.h"
#include "ICSIutils.h"
#include "m2mEncode.h"
#include "parsingUtils.h"
#include "exiDecode.h"
#include <stdio.h> /* For printf() */

#ifndef M2MBUILD_H
#define  M2MBUILD_H

errorCode decodeMessage(CharType *output_buf);

errorCode applicationBuild(CharType *output_buf, errorCode tmp_err_code);

errorCode subscriptionBuild(CharType *output_buf, errorCode tmp_err_code);

errorCode notifyBuild(CharType *output_buf, errorCode tmp_err_code);

errorCode containerBuild(CharType *output_buf, errorCode tmp_err_code);

errorCode contentInstanceBuild(CharType *output_buf, errorCode tmp_err_code, SingleString content);

#endif /* M2MBUILD_H */

