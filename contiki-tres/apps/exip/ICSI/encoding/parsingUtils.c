#include "contiki.h"
#include "grammarGenerator.h"
#include <stdio.h>
#include "parsingUtils.h"

void
parseSchema(char *xsdList, EXIPSchema *schema)
{
  errorCode tmp_err_code = EXIP_UNEXPECTED_ERROR;
  char *schemaFile;
  BinaryBuffer buffer[MAX_XSD_FILES_COUNT]; /* up to 10 XSD files */
  char schemaFileName[500];
  unsigned int schemaFilesCount = 0;
  unsigned int i;
  char *token;

  for(token = strtok(xsdList, "=,"), i = 0; token != NULL; token = strtok(NULL, "=,"), i++) {
    schemaFilesCount++;
    if(schemaFilesCount > MAX_XSD_FILES_COUNT) {
      printf("Too many xsd files given as an input: %d", schemaFilesCount);
      exit(1);
    }

    strcpy(schemaFileName, token);
    /* schemaFile = fopen(schemaFileName, "rb" ); */
    if(!schemaFile) {
      printf("Unable to open file %s", schemaFileName);
      exit(1);
    } else {
      /* Get file length */
      /* fseek(schemaFile, 0, SEEK_END); */
      /* buffer[i].bufLen = ftell(schemaFile) + 1; */
      /* fseek(schemaFile, 0, SEEK_SET); */

      /* Allocate memory */
      buffer[i].buf = (char *)malloc(buffer[i].bufLen);
      if(!buffer[i].buf) {
        printf("Memory allocation error!");
        /* fclose(schemaFile); */
        exit(1);
      }

      /* Read file contents into buffer */
      /* fread(buffer[i].buf, buffer[i].bufLen, 1, schemaFile); */
      /* fclose(schemaFile); */

      buffer[i].bufContent = buffer[i].bufLen;
      buffer[i].ioStrm.readWriteToStream = NULL;
      buffer[i].ioStrm.stream = NULL;
    }
  }

  /* Generate the EXI grammars based on the schema information */
  tmp_err_code = generateSchemaInformedGrammars(buffer, schemaFilesCount, SCHEMA_FORMAT_XSD_EXI, NULL, schema, NULL);

  for(i = 0; i < schemaFilesCount; i++) {
    free(buffer[i].buf);
  }

  if(tmp_err_code != EXIP_OK) {
    printf("\nGrammar generation error occurred: %d", tmp_err_code);
    exit(1);
  }
}
size_t
writeFileOutputStream(void *buf, size_t readSize, void *stream)
{
  /* printf("%s",buf); */
  /* char *buffer = (char*) stream; */
  /* strcpy(buffer, buf); */
  return sizeof(char);
  /* return fwrite(buf, 1, readSize, buffer); */
}
