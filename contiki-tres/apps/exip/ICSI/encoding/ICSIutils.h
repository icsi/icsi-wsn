/*
 * File:   ICSIutils.h
 * Author: francesca
 *
 * Created on January 23, 2014, 10:39 AM
 */

#ifndef ICSIUTILS_H
#define  ICSIUTILS_H
#define XML_DATE_AND_TIME_SIZE 19

#include "procTypes.h"
#include "IcsiTypes.h"

#define TRY_CATCH_PARAMETRIC(func, cblock, err) do { err = func; \
                                                     if(err != EXIP_OK) { \
                                                       DEBUG_MSG(ERROR, EXIP_DEBUG, ("\n>Error %s:%d at %s, line %d", GET_ERR_STRING(err), err, __FILE__, __LINE__)); \
                                                       cblock; \
                                                       return err; } } while(0)
#define TRY_FUN(func) { tmp_err_code = func; \
                        if(tmp_err_code != EXIP_OK) { \
                          DEBUG_MSG(ERROR, EXIP_DEBUG, ("\n>Error %s:%d at %s, line %d", GET_ERR_STRING(tmp_err_code), tmp_err_code, __FILE__, __LINE__)); \
                          return tmp_err_code; } }

errorCode setDateAndTime(EXIPDateTime *dateAndTime, uint8_t presenceMask, int year, int month, int day, int hour, int minutes, int seconds, int fsec_val, int fsec_offset);

errorCode initialize_Choice_MTBN_delayTolerance(Choice_MTBN_delayTolerance *choice, boolean MTBN, long *MTBN_value, uint8_t presenceMask, int year, int month, int day, int hour, int minutes, int seconds, int fsec_val, int fsec_offset);

#endif /* ICSIUTILS_H */

