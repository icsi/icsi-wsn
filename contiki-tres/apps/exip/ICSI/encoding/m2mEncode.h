/*==================================================================*\
 |                EXIP - Embeddable EXI Processor in C                |
 ||--------------------------------------------------------------------|
 |          This work is licensed under BSD 3-Clause License          |
 |  The full license terms and conditions are located in LICENSE.txt  |
 \===================================================================*/

/**
 * @file em2mEncode.h
 * @brief Interface for a function encoding a sample EXI message
 *
 * @date ..
 * @author ..
 * @version 0.5
 * @par[Revision] $Id: encodeTestEXI.h 328 2013-10-30 16:00:10Z kjussakov $
 */

#ifndef ENCODEICSIEXI_H_
#define ENCODEICSIEXI_H_

#include "config.h"

errorCode encodeSingleString(QName *qname, EXIStream *pointTestStrm, SingleString s);

errorCode application(EXIPSchema *schemaP, char *output, SingleString appId, EXIPDateTime *expirationTime, AccessRightID accessRightID, SearchStrings *searchstrings, EXIPDateTime *creationTime, AnnounceTo *announceTo, SingleString aPoC, aPoCPaths *apocpaths, SingleString containersreference, SingleString groupsreference, SingleString accessrightsreference, SingleString subscriptionsreference, SingleString notificationchannelsreference);

errorCode subscription(EXIPSchema *schemaP, char *output, SingleString id, EXIPDateTime *expirationTime, Choice_MTBN_delayTolerance *choice, EXIPDateTime *creationTime, EXIPDateTime *lastModifiedTime, FilterCriteria *filtercriteria, SingleString subscriptiontype, AnyURI contact);

errorCode notify(EXIPSchema *schemaP, char *output, SingleString code, SingleString rep, AnyURI subscriptionURI);

errorCode container(EXIPSchema *schemaPtr, char *output, SingleString id, EXIPDateTime *expirationTime, AccessRightID accessrightid, SearchStrings *searchstrings, EXIPDateTime *creationTime, EXIPDateTime *lastmodifiedtime, AnnounceTo *announceto, Integer *maxnrofinstances, Integer *maxbytesize, Integer *maxinstanceage, SingleString contentinstancereference, SingleString subscriptionsreference);

errorCode contentInstance(EXIPSchema *schemaPtr, char *output, SingleString id, SingleString href, EXIPDateTime *creationtime, EXIPDateTime *lastmodifiedtime, EXIPDateTime *delaytolerance, ContentTypes *contenttypes, SingleString contentsize, SingleString content, AccessRightID accessrightid);

void powerlog_print();

#endif /* ENCODEICSIEXI_H_ */
