#include "ICSIutils.h"
#include <stdio.h>
#include <string.h>
#include "IcsiTypes.h"

errorCode
setDateAndTime(EXIPDateTime *dateAndTime, uint8_t presenceMask, int year, int month, int day, int hour, int minutes, int seconds, int fsec_val, int fsec_offset)
{
  if(dateAndTime == NULL) {
    return EXIP_UNEXPECTED_ERROR;
  }
  dateAndTime->presenceMask = presenceMask;
  dateAndTime->dateTime.tm_year = year;   /* 2012 */
  dateAndTime->dateTime.tm_mon = month;   /* July */
  dateAndTime->dateTime.tm_mday = day;
  dateAndTime->dateTime.tm_hour = hour;
  dateAndTime->dateTime.tm_min = minutes;
  dateAndTime->dateTime.tm_sec = seconds;
  dateAndTime->fSecs.value = fsec_val;
  dateAndTime->fSecs.offset = fsec_offset;
  return EXIP_OK;
}
errorCode
initialize_Choice_MTBN_delayTolerance(Choice_MTBN_delayTolerance *choice, boolean MTBN, long *MTBN_value, uint8_t presenceMask, int year, int month, int day, int hour, int minutes, int seconds, int fsec_val, int fsec_offset)
{
  if(choice == NULL) {
    return EXIP_UNEXPECTED_ERROR;
  }
  if(MTBN) {
    if(MTBN_value == NULL) {
      return EXIP_UNEXPECTED_ERROR;
    }
    choice->MTBN = TRUE;
    choice->MTBN_value = (SingleString)MTBN_value;
  } else {
    choice->MTBN = FALSE;
    setDateAndTime(&(choice->delayTolerance), FRACT_PRESENCE, year, month, day, hour, minutes, seconds, fsec_val, fsec_offset);
  }
  return EXIP_OK;
}
/*
   errorCode getXMLdateAndTime(DateAndTime* expirationTime, CharType** str){
    //printf("\n\nStart getXMLdateAndTime\n\n");
    if (expirationTime==NULL) return EXIP_UNEXPECTED_ERROR;
    if (expirationTime->hour<0||expirationTime->hour>=24)
        return EXIP_UNEXPECTED_ERROR;
    if (expirationTime->minutes>=60||expirationTime->minutes<0)
        return EXIP_UNEXPECTED_ERROR;
    if (expirationTime->sec>=60||expirationTime->sec<0)
        return EXIP_UNEXPECTED_ERROR;
    if (expirationTime->month<=0||expirationTime->month>12)
        return EXIP_UNEXPECTED_ERROR;
    if (expirationTime->day<=0||expirationTime->day>31)
        return EXIP_UNEXPECTED_ERROR;
    if (expirationTime->year<0||expirationTime->year>9999) return EXIP_UNEXPECTED_ERROR;

    DEBUG_MSG(INFO, ENCODE_PRINTOUT, ("ICSIutils.c startElement\n"));
    sprintf(*str,"%d",expirationTime->year);

    strcat(*str,"-");
    if (expirationTime->month<10)
        sprintf(*str+5,"0%d",expirationTime->month);
    else
        sprintf(*str+5,"%d",expirationTime->month);

    strcat(*str,"-");
    if (expirationTime->day<10)
        sprintf(*str+8,"0%d",expirationTime->day);
    else sprintf(*str+8,"%d",expirationTime->day);

    strcat(*str,"T");

    if (expirationTime->hour<10)
        sprintf(*str+11,"0%d",expirationTime->hour);
    else sprintf(*str+11,"%d",expirationTime->hour);

    strcat(*str,":");

    if (expirationTime->minutes<10)
        sprintf(*str+14,"0%d",expirationTime->minutes);
    else sprintf(*str+14,"%d",expirationTime->minutes);

    strcat(*str,":");

    if (expirationTime->sec<10)
        sprintf(*str+17,"0%d",expirationTime->sec);
    else sprintf(*str+17,"%d",expirationTime->sec);
    return EXIP_OK;
   }*/

const char *
StatusCodeToString(statusCode c)
{
  switch(c) {
  case STATUS_OK: return "STATUS_OK";
  case STATUS_ACCEPTED: return "STATUS_ACCEPTED";
  case STATUS_BAD_REQUEST: return "STATUS_BAD_REQUEST";
  default: return "[UNKNOWN]";
  }
}