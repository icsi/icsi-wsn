/*==================================================================*\
 |                EXIP - Embeddable EXI Processor in C                |
 ||--------------------------------------------------------------------|
 |          This work is licensed under BSD 3-Clause License          |
 |  The full license terms and conditions are located in LICENSE.txt  |
 \===================================================================*/

/**
 * @file m2mEncode.c
 * @brief Testing the EXI encoder
 *
 * @date Nov 4, 2010
 * @author
 * @version 0.5
 * @par[Revision] $Id: encodeTestEXI.c 328 2013-10-30 16:00:10Z kjussakov $
 */

/*
   locrequestor which is "anyType" has been taken as string. If one needs a different type, needs to extend the libraries

 */

#include "m2mEncode.h"
#include "EXISerializer.h"
#include "stringManipulate.h"
#include <stdio.h>
#include <string.h>
#include "m2mDefinitions.h"
#include "ICSIutils.h"
#include "stdlib.h"

#define ENERGEST_CONF ON

#define MAX_XSD_FILES_COUNT 10
#define EXIP_MMAN ON

/* static char SOME_BINARY_DATA[] = {0x02, 0x6d, 0x2f, 0xa5, 0x20, 0xf2, 0x61, 0x9c, 0xee, 0x0f}; */

/* static String SOME_BINARY_DATA_BASE64 = {"i3sd7fatzxad", 12}; */

#define TRY_CATCH_ENCODE(func) TRY_CATCH(func, serialize.closeEXIStream(&testStrm))

#define TRY_CATCH_ENCODE_PARAMETRIC(func, err, testStream) TRY_CATCH_PARAMETRIC(func, serialize.closeEXIStream(&testStream), err)

errorCode
encodeDateTimeAsString(QName *qnameP, EXIStream *pointTestStrm, EXIPDateTime *date)
{

  errorCode tmp_err_code = EXIP_OK;
  /* EXITypeClass valueType; */
  /* String chVal; */
  int i;
  if(date != NULL) {
    char fsecBuf[30];
    if(IS_PRESENT(date->presenceMask, FRACT_PRESENCE)) {
      unsigned int tmpfValue = date->fSecs.value;
      int digitNum = 0;

      fsecBuf[0] = '.';

      while(tmpfValue) {
        digitNum++;
        tmpfValue = tmpfValue / 10;
      }
      for(i = 0; i < date->fSecs.offset + 1 - digitNum; i++) {
        fsecBuf[1 + i] = '0';
      }

      sprintf(fsecBuf + 1 + i, "%d", date->fSecs.value);
    } else {
      fsecBuf[0] = '\0';
    }
    SingleString s = EXIP_MALLOC(50 * sizeof(CharType));
    sprintf(s, "%04d-%02d-%02dT%02d:%02d:%02d%s", date->dateTime.tm_year + 1900,
            date->dateTime.tm_mon + 1, date->dateTime.tm_mday,
            date->dateTime.tm_hour, date->dateTime.tm_min,
            date->dateTime.tm_sec, fsecBuf);
    TRY_CATCH_ENCODE_PARAMETRIC(encodeSingleString(qnameP, pointTestStrm, s), tmp_err_code, pointTestStrm);
    /* printf("string date time: %d\n",s); */
    EXIP_MFREE(s);
  }
  return tmp_err_code;
}
errorCode
encodeDateTime(QName *qnameP, EXIStream *pointTestStrm, EXIPDateTime *date)
{
  errorCode tmp_err_code = EXIP_OK;
  EXITypeClass valueType;
  if(date != NULL) {
    TRY_CATCH_ENCODE_PARAMETRIC(serialize.startElement(pointTestStrm, *qnameP, &valueType), tmp_err_code, pointTestStrm);
    TRY_CATCH_ENCODE_PARAMETRIC(serialize.dateTimeData(pointTestStrm, *date), tmp_err_code, pointTestStrm);
    TRY_CATCH_ENCODE_PARAMETRIC(serialize.endElement(pointTestStrm), tmp_err_code, pointTestStrm);
  }
  return tmp_err_code;
}
errorCode
encodeIntegerAsString(QName *qnameP, EXIStream *pointTestStrm, Integer *i)
{
  errorCode tmp_err_code = EXIP_OK;
  if(i != NULL) {
    SingleString s = EXIP_MALLOC(10 * sizeof(CharType));
    sprintf(s, "%d", (int)*i);
    encodeSingleString(qnameP, pointTestStrm, s);
    EXIP_MFREE(s);
  }
  return tmp_err_code;
}
errorCode
encodeAttribute(QName *qnameP, EXIStream *pointTestStrm, SingleString s)
{
  errorCode tmp_err_code = EXIP_OK;
  String chVal;
  EXITypeClass valueType;

  if(s != NULL) {
    TRY_CATCH_ENCODE_PARAMETRIC(serialize.attribute(pointTestStrm, *qnameP, TRUE, &valueType), tmp_err_code, pointTestStrm);     /* attribute appId" */
    TRY_CATCH_ENCODE_PARAMETRIC(asciiToString(s, &chVal, &(pointTestStrm->memList), FALSE), tmp_err_code, pointTestStrm);
    TRY_CATCH_ENCODE_PARAMETRIC(serialize.stringData(pointTestStrm, chVal), tmp_err_code, pointTestStrm);
  }
  return tmp_err_code;
}
errorCode
encodeInteger(QName *qnameP, EXIStream *pointTestStrm, Integer *s)
{
  errorCode tmp_err_code = EXIP_OK;
  EXITypeClass valueType;
  if(s != NULL) {
    TRY_CATCH_ENCODE_PARAMETRIC(serialize.startElement(pointTestStrm, *qnameP, &valueType), tmp_err_code, pointTestStrm);
    TRY_CATCH_ENCODE_PARAMETRIC(serialize.intData(pointTestStrm, *s), tmp_err_code, pointTestStrm);
    TRY_CATCH_ENCODE_PARAMETRIC(serialize.endElement(pointTestStrm), tmp_err_code, pointTestStrm);
  }
  return tmp_err_code;
}
errorCode
encodeAnnounceTo(EXIStream *pointTestStrm, AnnounceTo *announceTo)
{
  errorCode tmp_err_code = EXIP_OK;
  QName qname;
  String chVal;
  /* int count; */
  EXITypeClass valueType;
  if(announceTo != NULL) {

    qname.uri = &NS_ETSI_M2M;
    qname.localName = &ANNOUNCE_TO;
    TRY_CATCH_ENCODE_PARAMETRIC(serialize.startElement(pointTestStrm, qname, &valueType), tmp_err_code, pointTestStrm);     /* <searchStrings> */

    if(announceTo->activated != NULL) {
      qname.uri = &NS_ETSI_M2M;
      qname.localName = &ACTIVATED;
      TRY_CATCH_ENCODE_PARAMETRIC(serialize.startElement(pointTestStrm, qname, &valueType), tmp_err_code, pointTestStrm);       /* <activated> */
#if WITH_SCHEMA
      if(*(announceTo->activated)) {
        TRY_CATCH_ENCODE_PARAMETRIC(serialize.booleanData(pointTestStrm, TRUE), tmp_err_code, pointTestStrm);
      } else { TRY_CATCH_ENCODE_PARAMETRIC(serialize.booleanData(pointTestStrm, FALSE), tmp_err_code, pointTestStrm);
      }

#else
      if(*(announceTo->activated)) {
        TRY_CATCH_ENCODE_PARAMETRIC(asciiToString("true", &chVal, &(pointTestStrm->memList), FALSE), tmp_err_code, pointTestStrm);
      } else { TRY_CATCH_ENCODE_PARAMETRIC(asciiToString("false", &chVal, &(pointTestStrm->memList), FALSE), tmp_err_code, pointTestStrm);
      }
      TRY_CATCH_ENCODE_PARAMETRIC(serialize.stringData(pointTestStrm, chVal), tmp_err_code, pointTestStrm);
#endif
      TRY_CATCH_ENCODE_PARAMETRIC(serialize.endElement(pointTestStrm), tmp_err_code, pointTestStrm);       /* </activated> */
    }

    qname.localName = &SCL_LIST;
    TRY_CATCH_ENCODE_PARAMETRIC(serialize.startElement(pointTestStrm, qname, &valueType), tmp_err_code, pointTestStrm);     /* <sclList> */

    if(announceTo->sclList.length > 0) {
#if WITH_SCHEMA
      qname.localName = &REFERENCE;
      for(count = 0; count < announceTo->sclList.length; count++) {
        TRY_CATCH_ENCODE_PARAMETRIC(serialize.startElement(pointTestStrm, qname, &valueType), tmp_err_code, pointTestStrm);         /* <searchString> */
        TRY_CATCH_ENCODE_PARAMETRIC(asciiToString(announceTo->sclList.strings[count], &chVal, &(pointTestStrm->memList), FALSE), tmp_err_code, pointTestStrm);
        TRY_CATCH_ENCODE_PARAMETRIC(serialize.stringData(pointTestStrm, chVal), tmp_err_code, pointTestStrm);
        TRY_CATCH_ENCODE_PARAMETRIC(serialize.endElement(pointTestStrm), tmp_err_code, pointTestStrm);         /* </searchString> */
      }
#else
      /*
         for(count=0; count < announceTo->sclList.length; count ++)
         {
              TRY_CATCH_ENCODE_PARAMETRIC(serialize.startElement(pointTestStrm, qname, &valueType), tmp_err_code, pointTestStrm); // <searchString>
              TRY_CATCH_ENCODE_PARAMETRIC(asciiToString(announceTo->sclList.strings[count], &chVal, &(pointTestStrm->memList), FALSE), tmp_err_code, pointTestStrm);
              TRY_CATCH_ENCODE_PARAMETRIC(serialize.stringData(pointTestStrm, chVal), tmp_err_code, pointTestStrm);
              TRY_CATCH_ENCODE_PARAMETRIC(serialize.endElement(pointTestStrm), tmp_err_code, pointTestStrm); //</searchString>
         }
       */
#endif
    }
    TRY_CATCH_ENCODE_PARAMETRIC(serialize.endElement(pointTestStrm), tmp_err_code, pointTestStrm);

    if(announceTo->global != NULL) {
      qname.uri = &NS_ETSI_M2M;
      qname.localName = &GLOBAL;
      TRY_CATCH_ENCODE_PARAMETRIC(serialize.startElement(pointTestStrm, qname, &valueType), tmp_err_code, pointTestStrm);       /* <activated> */
#if WITH_SCHEMA
      if(*(announceTo->global)) {
        TRY_CATCH_ENCODE_PARAMETRIC(serialize.booleanData(pointTestStrm, TRUE), tmp_err_code, pointTestStrm);
      } else { TRY_CATCH_ENCODE_PARAMETRIC(serialize.booleanData(pointTestStrm, FALSE), tmp_err_code, pointTestStrm);
      }
#else
      if(*(announceTo->global)) {
        TRY_CATCH_ENCODE_PARAMETRIC(asciiToString("true", &chVal, &(pointTestStrm->memList), FALSE), tmp_err_code, pointTestStrm);
      } else { TRY_CATCH_ENCODE_PARAMETRIC(asciiToString("false", &chVal, &(pointTestStrm->memList), FALSE), tmp_err_code, pointTestStrm);
      }
      TRY_CATCH_ENCODE_PARAMETRIC(serialize.stringData(pointTestStrm, chVal), tmp_err_code, pointTestStrm);
#endif
      TRY_CATCH_ENCODE_PARAMETRIC(serialize.endElement(pointTestStrm), tmp_err_code, pointTestStrm);       /* </activated> */
    }
    TRY_CATCH_ENCODE_PARAMETRIC(serialize.endElement(pointTestStrm), tmp_err_code, pointTestStrm);     /* </searchStrings> */
  }
  return tmp_err_code;
}
errorCode
encodeStringsVector(const char *ch1, const char *ch2, EXIStream *pointTestStrm, SearchStrings *s)
{
  QName qname;
  errorCode tmp_err_code = EXIP_OK;
  String chVal;
  EXITypeClass valueType;
  if(s != NULL) {
    if(s->length > 0) {
      qname.uri = &NS_ETSI_M2M;
      qname.localName = ch1;
      TRY_CATCH_ENCODE_PARAMETRIC(serialize.startElement(pointTestStrm, qname, &valueType), tmp_err_code, pointTestStrm);       /* <searchStrings> */

      qname.localName = ch2;
      int count = 0;
      do {
        TRY_CATCH_ENCODE_PARAMETRIC(serialize.startElement(pointTestStrm, qname, &valueType), tmp_err_code, pointTestStrm);         /* <searchString> */
        TRY_CATCH_ENCODE_PARAMETRIC(asciiToString(s->strings[count], &chVal, &(pointTestStrm->memList), FALSE), tmp_err_code, pointTestStrm);
        TRY_CATCH_ENCODE_PARAMETRIC(serialize.stringData(pointTestStrm, chVal), tmp_err_code, pointTestStrm);
        TRY_CATCH_ENCODE_PARAMETRIC(serialize.endElement(pointTestStrm), tmp_err_code, pointTestStrm);         /* </searchString> */
        count++;
      } while(count < s->length);

      TRY_CATCH_ENCODE_PARAMETRIC(serialize.endElement(pointTestStrm), tmp_err_code, pointTestStrm);       /* </searchStrings> */
    }
  }
  return tmp_err_code;
}
errorCode
application(EXIPSchema *schemaP, char *output, SingleString appId, EXIPDateTime *expirationTime, AccessRightID accessRightID, SearchStrings *searchstrings, EXIPDateTime *creationTime, AnnounceTo *announceTo, SingleString aPoC, aPoCPaths *apocpaths, SingleString containersreference, SingleString groupsreference, SingleString accessrightsreference, SingleString subscriptionsreference, SingleString notificationchannelsreference)
{
  errorCode tmp_err_code = EXIP_UNEXPECTED_ERROR;
  d_malloc_init();
  EXIStream testStrm;
  String uri;
  String ln;
  QName qname = { &uri, &ln, NULL };
  /* String chVal; */
  BinaryBuffer buffer;
  EXITypeClass valueType;
  /* int apocpath_i, j; */

  buffer.buf = output;
  buffer.bufLen = EXI_ENCODED_STREAM_BUFFER_SIZE;
  buffer.bufContent = 0;
  serialize.initHeader(&testStrm);
  testStrm.header.has_cookie = TRUE;
  testStrm.header.has_options = TRUE;
  SET_STRICT(testStrm.header.opts.enumOpt);
  buffer.ioStrm.readWriteToStream = NULL;
  buffer.ioStrm.stream = NULL;
  DEBUG_MSG(INFO, ENCODE_PRINTOUT, ("I\n"));
  TRY_CATCH_ENCODE(serialize.initStream(&testStrm, buffer, schemaP));
  TRY_CATCH_ENCODE(serialize.exiHeader(&testStrm));
  TRY_CATCH_ENCODE(serialize.startDocument(&testStrm));
  qname.uri = &NS_ETSI_M2M;
  qname.localName = &APPLICATION;
  TRY_CATCH_ENCODE(serialize.startElement(&testStrm, qname, &valueType));   /* <application> */

#if WITH_SCHEMA
  /* qname.localName = &APP_ID; */
  /* TRY_CATCH_ENCODE(encodeAttribute(&qname, &testStrm, appId)); */
  /* qname.localName = &EXPIRATION_TIME; */
  /* TRY_CATCH_ENCODE(encodeDateTime(&qname, &testStrm, expirationTime)); */
#else
  qname.localName = &APP_ID;
  TRY_CATCH_ENCODE(encodeAttribute(&qname, &testStrm, appId));
  qname.localName = &EXPIRATION_TIME;
  TRY_CATCH_ENCODE(encodeDateTimeAsString(&qname, &testStrm, expirationTime));
#endif
  DEBUG_MSG(INFO, ENCODE_PRINTOUT, ("IVbis\n"));
  qname.localName = &ACCESS_RIGHT_ID;
  TRY_CATCH_ENCODE(encodeSingleString(&qname, &testStrm, accessRightID));
  DEBUG_MSG(INFO, ENCODE_PRINTOUT, ("V\n"));
  encodeStringsVector(&SEARCH_STRINGS, &SEARCH_STRING, &testStrm, searchstrings);
  DEBUG_MSG(INFO, ENCODE_PRINTOUT, ("VI\n"));
#if WITH_SCHEMA
  /* qname.localName = &CREATION_TIME; */
  /* TRY_CATCH_ENCODE(encodeDateTime(&qname, &testStrm, creationTime)); */
#else
  qname.localName = &CREATION_TIME;
  TRY_CATCH_ENCODE(encodeDateTimeAsString(&qname, &testStrm, creationTime));
#endif
  TRY_CATCH_ENCODE(encodeAnnounceTo(&testStrm, announceTo));
  DEBUG_MSG(INFO, ENCODE_PRINTOUT, ("VII\n"));
  if(aPoC != NULL) {
    qname.uri = &NS_ETSI_M2M;
    qname.localName = &APOC;
    TRY_CATCH_ENCODE(encodeSingleString(&qname, &testStrm, aPoC));
  }
  DEBUG_MSG(INFO, ENCODE_PRINTOUT, ("VIII\n"));
  if(apocpaths != NULL) {
    qname.uri = &NS_ETSI_M2M;
    qname.localName = &APOCPATHS;
    TRY_CATCH_ENCODE(serialize.startElement(&testStrm, qname, &valueType));     /* <aPoCPaths> */
    if(apocpaths->length != 0) {
      int apocpath_index;
      for(apocpath_index = 0; apocpath_index < apocpaths->length; apocpath_index++) {
        if(apocpaths->apocpaths[apocpath_index].path != NULL) {
          qname.localName = &APOCPATH;
          TRY_CATCH_ENCODE(serialize.startElement(&testStrm, qname, &valueType));           /* <aPoCPath> */

          qname.localName = &PATH;
          TRY_CATCH_ENCODE(encodeSingleString(&qname, &testStrm, apocpaths->apocpaths[apocpath_index].path));

          if(apocpaths->apocpaths[apocpath_index].accessRightID != NULL) {
#if WITH_SCHEMA
            qname.localName = &ACCESS_RIGHT_ID;
            TRY_CATCH_ENCODE(encodeSingleString(&qname, &testStrm, apocpaths->apocpaths[apocpath_index].accessRightID));
#endif
          }
#if WITH_SCHEMA
          if(apocpaths->apocpaths[apocpath_index].searchStrings != NULL) {
            if(apocpaths->apocpaths[apocpath_index].searchStrings->length != 0) {
              qname.localName = &SEARCH_STRINGS;

              TRY_CATCH_ENCODE(serialize.startElement(&testStrm, qname, &valueType));               /* <searchStrings> */

              qname.localName = &SEARCH_STRING;
              for(j = 0; j < apocpaths->apocpaths[apocpath_index].searchStrings->length; j++) {
                /* printf("Item %d out of %d: %s\n",j,apocpaths->apocpaths[apocpath_index].searchStrings->length,apocpaths->apocpaths[apocpath_i].searchStrings->strings[j]); */
                /* TRY_CATCH_ENCODE(encodeSingleString(&qname, &testStrm, apocpaths->apocpaths[apocpath_i].searchStrings->strings[j])); */
              }

              TRY_CATCH_ENCODE(serialize.endElement(&testStrm));               /* </searchStrings> */
            }
          }
#endif
          TRY_CATCH_ENCODE(serialize.endElement(&testStrm));           /* </aPoCPath> */
        }
      }
    }

    TRY_CATCH_ENCODE(serialize.endElement(&testStrm));     /* </aPoCPaths> */
  }
  DEBUG_MSG(INFO, ENCODE_PRINTOUT, ("IX\n"));
#if WITH_SCHEMA
#else
  qname.localName = &CONTAINERS_REFERENCE;
  encodeSingleString(&qname, &testStrm, containersreference);
#endif
  qname.localName = &GROUPS_REFERENCE;
  encodeSingleString(&qname, &testStrm, groupsreference);

  qname.localName = &ACCESS_RIGHTS_REFERENCE;
  encodeSingleString(&qname, &testStrm, accessrightsreference);

  qname.localName = &SUBSCRIPTIONS_REFERENCE;
  encodeSingleString(&qname, &testStrm, subscriptionsreference);

  qname.localName = &NOTIFICATION_CHANNELS_REFERENCE;
  encodeSingleString(&qname, &testStrm, notificationchannelsreference);

  TRY_CATCH_ENCODE(serialize.endElement(&testStrm));   /* </application> */
  TRY_CATCH_ENCODE(serialize.endDocument(&testStrm));
  DEBUG_MSG(INFO, ENCODE_PRINTOUT, ("X\n"));
  /* VI: Free the memory allocated by the EXI stream object */
  TRY_CATCH_ENCODE(serialize.closeEXIStream(&testStrm));
  DEBUG_MSG(INFO, ENCODE_PRINTOUT, ("END m2mEncode\n"));
  return tmp_err_code;
}
errorCode
encodeSingleString(QName *qname, EXIStream *pointTestStrm, SingleString s)
{
  errorCode tmp_err_code = EXIP_OK;
  String chVal;
  EXITypeClass valueType;
  if(s != NULL) {
    TRY_CATCH_ENCODE_PARAMETRIC(serialize.startElement(pointTestStrm, *qname, &valueType), tmp_err_code, pointTestStrm);     /* <aPoC> */
    TRY_CATCH_ENCODE_PARAMETRIC(asciiToString(s, &chVal, &(pointTestStrm->memList), FALSE), tmp_err_code, pointTestStrm);
    TRY_CATCH_ENCODE_PARAMETRIC(serialize.stringData(pointTestStrm, chVal), tmp_err_code, pointTestStrm);
    TRY_CATCH_ENCODE_PARAMETRIC(serialize.endElement(pointTestStrm), tmp_err_code, pointTestStrm);     /* </aPoC> */
  }
  return tmp_err_code;
}
errorCode
subscription(EXIPSchema *schemaP, char *output, SingleString id, EXIPDateTime *expirationTime, Choice_MTBN_delayTolerance *choice, EXIPDateTime *creationTime, EXIPDateTime *lastModifiedTime, FilterCriteria *filtercriteria, SingleString subscriptiontype, AnyURI contact)
{
  errorCode tmp_err_code = EXIP_UNEXPECTED_ERROR;
  /* #if EXIP_MMAN */
  d_malloc_init();
  /* endif */
  int i;
  EXIStream testStrm;
  String uri;
  String ln;
  QName qname = { &uri, &ln, NULL };
  /* String chVal; */
  BinaryBuffer buffer;
  EXITypeClass valueType;
  buffer.buf = output;
  buffer.bufLen = EXI_ENCODED_STREAM_BUFFER_SIZE;
  buffer.bufContent = 0;
  serialize.initHeader(&testStrm);
  testStrm.header.has_cookie = TRUE;
  testStrm.header.has_options = TRUE;
  SET_STRICT(testStrm.header.opts.enumOpt);
  buffer.ioStrm.readWriteToStream = NULL;
  buffer.ioStrm.stream = NULL;
  TRY_CATCH_ENCODE(serialize.initStream(&testStrm, buffer, schemaP));
  TRY_CATCH_ENCODE(serialize.exiHeader(&testStrm));
  TRY_CATCH_ENCODE(serialize.startDocument(&testStrm));
  qname.uri = &NS_ETSI_M2M;
  qname.localName = &SUBSCRIPTION;
  TRY_CATCH_ENCODE(serialize.startElement(&testStrm, qname, &valueType));   /* </subscription> */

#if WITH_SCHEMA
  /* qname.localName = &ID; */
  /* TRY_CATCH_ENCODE(encodeAttribute(&qname, &testStrm, id)); */
  qname.localName = &EXPIRATION_TIME;
  TRY_CATCH_ENCODE(encodeDateTime(&qname, &testStrm, expirationTime));
  if(choice != NULL) {
    if(choice->MTBN) {
      qname.localName = &MINIMAL_TIME_BETWEEN_NOTIFICATIONS;
      TRY_CATCH_ENCODE(encodeInteger(&qname, &testStrm, choice->MTBN_value));
    } else {
      qname.localName = &DELAY_TOLERANCE;
      TRY_CATCH_ENCODE(encodeDateTime(&qname, &testStrm, choice->delayTolerance));
    }
  }
  qname.localName = &CREATION_TIME;
  TRY_CATCH_ENCODE(encodeDateTime(&qname, &testStrm, creationTime));
  qname.localName = &LAST_MODIFIED_TIME;
  TRY_CATCH_ENCODE(encodeDateTime(&qname, &testStrm, lastModifiedTime));
#else
  qname.localName = &ID;
  TRY_CATCH_ENCODE(encodeAttribute(&qname, &testStrm, id));
  qname.localName = &EXPIRATION_TIME;
  TRY_CATCH_ENCODE(encodeDateTimeAsString(&qname, &testStrm, expirationTime));

  if(choice != NULL) {
    if(choice->MTBN) {
      qname.localName = &MINIMAL_TIME_BETWEEN_NOTIFICATIONS;
      TRY_CATCH_ENCODE(encodeDateTimeAsString(&qname, &testStrm, choice->MTBN_value));
    } else {
      qname.localName = &DELAY_TOLERANCE;
      TRY_CATCH_ENCODE(encodeDateTimeAsString(&qname, &testStrm, choice->delayTolerance));
    }
  }
  qname.localName = &CREATION_TIME;
  TRY_CATCH_ENCODE(encodeDateTimeAsString(&qname, &testStrm, creationTime));
  qname.localName = &LAST_MODIFIED_TIME;
  TRY_CATCH_ENCODE(encodeDateTimeAsString(&qname, &testStrm, lastModifiedTime));
#endif

  if((filtercriteria != NULL) && ((filtercriteria->ifModifiedSince != NULL) || (filtercriteria->ifUnmodifiedSince != NULL) || (filtercriteria->ifMatch.length > 0) || (filtercriteria->ifNoneMatch.length > 0))) {
    qname.localName = &FILTER_CRITERIA;
    TRY_CATCH_ENCODE(serialize.startElement(&testStrm, qname, &valueType));     /* </filterCriteria> */

    if(filtercriteria->ifModifiedSince != NULL) {
      qname.localName = &IF_MODIFIED_SINCE;
#if WITH_SCHEMA
      /* TRY_CATCH_ENCODE(encodeDateTime(&qname, &testStrm, filtercriteria->ifModifiedSince)); */
#else
      TRY_CATCH_ENCODE(encodeDateTimeAsString(&qname, &testStrm, filtercriteria->ifModifiedSince));
#endif
    }
    if(filtercriteria->ifUnmodifiedSince != NULL) {
      qname.localName = &IF_UNMODIFIED_SINCE;
#if WITH_SCHEMA
      /* TRY_CATCH_ENCODE(encodeDateTime(&qname, &testStrm, filtercriteria->ifUnmodifiedSince)); */
#else
      TRY_CATCH_ENCODE(encodeDateTimeAsString(&qname, &testStrm, filtercriteria->ifUnmodifiedSince));
#endif
    }

    if(filtercriteria->ifMatch.length > 0) {
      qname.localName = &IF_MATCH;
      for(i = 0; i < filtercriteria->ifMatch.length; i++) {
#if WITH_SCHEMA
#else
        TRY_CATCH_ENCODE(encodeSingleString(&qname, &testStrm, filtercriteria->ifMatch.strings[i]));
#endif
      }
    }
    if(filtercriteria->ifNoneMatch.length > 0) {
      qname.localName = &IF_NONE_MATCH;
      for(i = 0; i < filtercriteria->ifNoneMatch.length; i++) {
#if WITH_SCHEMA
#else
        TRY_CATCH_ENCODE(encodeSingleString(&qname, &testStrm, filtercriteria->ifNoneMatch.strings[i]));
#endif
      }
    }
    TRY_CATCH_ENCODE(serialize.endElement(&testStrm));     /* </filterCriteria> */
  }
  qname.localName = &SUBSCRIPTION_TYPE;
  encodeSingleString(&qname, &testStrm, subscriptiontype);
  qname.localName = &CONTACT;
  encodeSingleString(&qname, &testStrm, contact);
  TRY_CATCH_ENCODE(serialize.endElement(&testStrm));   /* </subscription> */
  TRY_CATCH_ENCODE(serialize.endDocument(&testStrm));
  TRY_CATCH_ENCODE(serialize.closeEXIStream(&testStrm));
  return tmp_err_code;
}
errorCode
notify(EXIPSchema *schemaP, char *output, SingleString code, SingleString rep, AnyURI subscriptionURI)
{
  errorCode tmp_err_code = EXIP_UNEXPECTED_ERROR;
#if EXIP_MMAN
  d_malloc_init();
#endif
  EXIStream testStrm;
  String uri;
  String ln;
  /* int count; */
  QName qname = { &uri, &ln, NULL };
  BinaryBuffer buffer;
  EXITypeClass valueType;

  if((code == NULL) || (subscriptionURI == NULL)) {
    return EXIP_UNEXPECTED_ERROR;
  }

  buffer.buf = output;
  buffer.bufLen = EXI_ENCODED_STREAM_BUFFER_SIZE;
  buffer.bufContent = 0;
  serialize.initHeader(&testStrm);
  testStrm.header.has_cookie = TRUE;
  testStrm.header.has_options = TRUE;
  buffer.ioStrm.readWriteToStream = NULL;
  buffer.ioStrm.stream = NULL;
  TRY_CATCH_ENCODE(serialize.initStream(&testStrm, buffer, schemaP));
  TRY_CATCH_ENCODE(serialize.exiHeader(&testStrm));
  TRY_CATCH_ENCODE(serialize.startDocument(&testStrm));

  qname.uri = &NS_ETSI_M2M;
  qname.localName = &NOTIFY;
  TRY_CATCH_ENCODE(serialize.startElement(&testStrm, qname, &valueType));   /* </subscription> */

#if WITH_SCHEMA
  qname.localName = &STATUS_CODE;
  TRY_CATCH_ENCODE(encodeSingleString(&qname, &testStrm, code));

  qname.localName = &SUBSCRIPTION_REFERENCE;

  /* errorCode qnameData(EXIStream* strm, subscriptionURI); */

  TRY_CATCH_ENCODE(encodeSingleString(&qname, &testStrm, subscriptionURI));
#else
  qname.localName = &STATUS_CODE;
  TRY_CATCH_ENCODE(encodeSingleString(&qname, &testStrm, code));

  qname.localName = &REPRESENTATION;
  TRY_CATCH_ENCODE(encodeSingleString(&qname, &testStrm, rep));

  qname.localName = &SUBSCRIPTION_REFERENCE;
  TRY_CATCH_ENCODE(encodeSingleString(&qname, &testStrm, subscriptionURI));
#endif
  TRY_CATCH_ENCODE(serialize.endElement(&testStrm));   /* </subscription> */
  TRY_CATCH_ENCODE(serialize.endDocument(&testStrm));
  TRY_CATCH_ENCODE(serialize.closeEXIStream(&testStrm));
  return tmp_err_code;
}
errorCode
container(EXIPSchema *schemaPtr, char *output, SingleString id, EXIPDateTime *expirationTime, AccessRightID accessrightid, SearchStrings *searchstrings, EXIPDateTime *creationTime, EXIPDateTime *lastmodifiedtime, AnnounceTo *announceto, Integer *maxnrofinstances, Integer *maxbytesize, Integer *maxinstanceage, SingleString contentinstancereference, SingleString subscriptionsreference)
{

  errorCode tmp_err_code = EXIP_OK;
#if EXIP_MMAN
  d_malloc_init();
#endif
  EXIStream testStrm;
  String uri;
  String ln;
  QName qname = { &uri, &ln, NULL };
  /* String chVal; */
  BinaryBuffer buffer;
  EXITypeClass valueType;

  buffer.buf = output;
  buffer.bufLen = EXI_ENCODED_STREAM_BUFFER_SIZE;
  buffer.bufContent = 0;

  serialize.initHeader(&testStrm);

  testStrm.header.has_cookie = TRUE;
  testStrm.header.has_options = TRUE;

  /* makeCustomOpts(&(testStrm.header.opts.enumOpt)); */

  buffer.ioStrm.readWriteToStream = NULL;
  buffer.ioStrm.stream = NULL;

  TRY_CATCH_ENCODE(serialize.initStream(&testStrm, buffer, schemaPtr));
  TRY_CATCH_ENCODE(serialize.exiHeader(&testStrm));
  TRY_CATCH_ENCODE(serialize.startDocument(&testStrm));

  qname.uri = &NS_ETSI_M2M;
  qname.localName = &CONTAINER;
  TRY_CATCH_ENCODE(serialize.startElement(&testStrm, qname, &valueType));   /* </container> */

#if WITH_SCHEMA
  qname.localName = &ID;
  /* TRY_CATCH_ENCODE(encodeAttribute(&qname, &testStrm, id)); */
  /* TRY_CATCH_ENCODE(serialize.attribute(&testStrm, qname, TRUE, &valueType)); // testByte=" */
  /* TRY_CATCH_ENCODE(asciiToString("sciao", &chVal, &testStrm.memList, FALSE)); */
  /* TRY_CATCH_ENCODE(serialize.stringData(&testStrm, chVal)); */
  /* qname.localName = &EXPIRATION_TIME; */
  /* TRY_CATCH_ENCODE(encodeDateTime(&qname, &testStrm, expirationTime)); */
  /* qname.localName = &ACCESS_RIGHT_ID; */
  /* TRY_CATCH_ENCODE(encodeSingleString(&qname, &testStrm, accessrightid)); */
#else
  qname.localName = &ID;
  encodeAttribute(&qname, &testStrm, id);
  qname.localName = &EXPIRATION_TIME;
  TRY_CATCH_ENCODE(encodeDateTimeAsString(&qname, &testStrm, expirationTime));
  qname.localName = &ACCESS_RIGHT_ID;
  TRY_CATCH_ENCODE(encodeSingleString(&qname, &testStrm, accessrightid));
#endif

  encodeStringsVector(&SEARCH_STRINGS, &SEARCH_STRING, &testStrm, searchstrings);

#if WITH_SCHEMA
  /* qname.localName = &CREATION_TIME; */
  /* TRY_CATCH_ENCODE(encodeDateTime(&qname, &testStrm, creationTime)); */
  /* qname.localName = &LAST_MODIFIED_TIME; */
  /* TRY_CATCH_ENCODE(encodeDateTime(&qname, &testStrm, lastmodifiedtime)); */

#else
  qname.localName = &CREATION_TIME;
  TRY_CATCH_ENCODE(encodeDateTimeAsString(&qname, &testStrm, creationTime));
  qname.localName = &LAST_MODIFIED_TIME;
  TRY_CATCH_ENCODE(encodeDateTimeAsString(&qname, &testStrm, lastmodifiedtime));
#endif
  TRY_CATCH_ENCODE(encodeAnnounceTo(&testStrm, announceto));

#if WITH_SCHEMA
  qname.localName = &MAX_NR_OF_INSTANCES;
  TRY_CATCH_ENCODE(encodeInteger(&qname, &testStrm, maxnrofinstances));

  qname.localName = &MAX_BYTE_SIZE;
  TRY_CATCH_ENCODE(encodeInteger(&qname, &testStrm, maxbytesize));

  qname.localName = &MAX_INSTANCE_AGE;
  TRY_CATCH_ENCODE(encodeInteger(&qname, &testStrm, maxinstanceage));
#else
  qname.localName = &MAX_NR_OF_INSTANCES;
  TRY_CATCH_ENCODE(encodeIntegerAsString(&qname, &testStrm, maxnrofinstances));

  qname.localName = &MAX_BYTE_SIZE;
  TRY_CATCH_ENCODE(encodeIntegerAsString(&qname, &testStrm, maxbytesize));

  qname.localName = &MAX_INSTANCE_AGE;
  TRY_CATCH_ENCODE(encodeIntegerAsString(&qname, &testStrm, maxinstanceage));
#endif
  qname.localName = &CONTENT_INSTANCES_REFERENCE;
  TRY_CATCH_ENCODE(encodeSingleString(&qname, &testStrm, contentinstancereference));

  qname.localName = &SUBSCRIPTIONS_REFERENCE;
  TRY_CATCH_ENCODE(encodeSingleString(&qname, &testStrm, subscriptionsreference));

  TRY_CATCH_ENCODE(serialize.endElement(&testStrm));   /* </container> */
  TRY_CATCH_ENCODE(serialize.endDocument(&testStrm));
  TRY_CATCH_ENCODE(serialize.closeEXIStream(&testStrm));
  return tmp_err_code;
}


errorCode
contentInstance(EXIPSchema *schemaPtr, char *output, SingleString id, SingleString href, EXIPDateTime *creationtime, EXIPDateTime *lastmodifiedtime, EXIPDateTime *delaytolerance, ContentTypes *contenttypes, SingleString contentsize, SingleString content, AccessRightID accessrightid)
{
  errorCode tmp_err_code = EXIP_UNEXPECTED_ERROR;
#if EXIP_MMAN
  d_malloc_init();
#endif
  EXIStream testStrm;
  String uri;
  String ln;
  QName qname = { &uri, &ln, NULL };
  /* String chVal; */
  BinaryBuffer buffer;
  EXITypeClass valueType;
  buffer.buf = output;
  buffer.bufLen = EXI_ENCODED_STREAM_BUFFER_SIZE;
  buffer.bufContent = 0;
  serialize.initHeader(&testStrm);
  testStrm.header.has_cookie = TRUE;
  testStrm.header.has_options = TRUE;
  /* SET_STRICT(testStrm.header.opts.enumOpt); */
  buffer.ioStrm.readWriteToStream = NULL;
  buffer.ioStrm.stream = NULL;
  TRY_CATCH_ENCODE(serialize.initStream(&testStrm, buffer, schemaPtr));
  TRY_CATCH_ENCODE(serialize.exiHeader(&testStrm));
  TRY_CATCH_ENCODE(serialize.startDocument(&testStrm));
  qname.uri = &NS_ETSI_M2M;
  qname.localName = &CONTENT_INSTANCE;
  TRY_CATCH_ENCODE(serialize.startElement(&testStrm, qname, &valueType));   /* </contentInstance> */

#if WITH_SCHEMA
  qname.localName = &CREATION_TIME;
  TRY_CATCH_ENCODE(encodeDateTime(&qname, &testStrm, creationtime));
  qname.localName = &LAST_MODIFIED_TIME;
  TRY_CATCH_ENCODE(encodeDateTime(&qname, &testStrm, lastmodifiedtime));
  qname.localName = &DELAY_TOLERANCE;
  TRY_CATCH_ENCODE(encodeDateTime(&qname, &testStrm, delaytolerance));
  qname.localName = &CONTENT;
  encodeSingleString(&qname, &testStrm, content);
#else
  qname.uri = &NS_ETSI_M2M;
  qname.localName = &ID;   /* che fastidio!! Invece con APP_ID funziona!! */
  /* encodeAttribute(&qname, &testStrm, id); */

  //qname.localName = &ACCESS_RIGHT_ID;
  //TRY_CATCH_ENCODE(encodeSingleString(&qname, &testStrm, accessrightid));

  qname.localName = &H_REF;
  encodeAttribute(&qname, &testStrm, href);

  //qname.localName = &CREATION_TIME;
  //TRY_CATCH_ENCODE(encodeDateTimeAsString(&qname, &testStrm, creationtime));

  //qname.localName = &LAST_MODIFIED_TIME;
  //TRY_CATCH_ENCODE(encodeDateTimeAsString(&qname, &testStrm, lastmodifiedtime));

  //qname.localName = &DELAY_TOLERANCE;
  //RY_CATCH_ENCODE(encodeDateTimeAsString(&qname, &testStrm, delaytolerance));

  encodeStringsVector(&CONTENT_TYPES, &CONTENT_TYPE, &testStrm, contenttypes);

  qname.localName = &CONTENT_SIZE;
  encodeSingleString(&qname, &testStrm, contentsize);

  qname.localName = &CONTENT;
  encodeSingleString(&qname, &testStrm, content);
#endif
  TRY_CATCH_ENCODE(serialize.endElement(&testStrm));   /* </contentInstance> */
  TRY_CATCH_ENCODE(serialize.endDocument(&testStrm));
  TRY_CATCH_ENCODE(serialize.closeEXIStream(&testStrm));

  return tmp_err_code;
}
void
powerlog_print()
{
  uint32_t cpu, lpm, tx, lst;
  /* energest_init(); */
  energest_flush();

  cpu = energest_type_time(ENERGEST_TYPE_CPU);
  lpm = energest_type_time(ENERGEST_TYPE_LPM);
  tx = energest_type_time(ENERGEST_TYPE_TRANSMIT);
  lst = energest_type_time(ENERGEST_TYPE_LISTEN);
  printf("P: %lu, %lu, %lu, %lu\n", cpu, lpm, tx, lst);
}
