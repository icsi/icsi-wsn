#include "m2mBuild.h"
#include <stdio.h>

#if _APPLICATION && WITH_SCHEMA
#include "application.h"
#elif _SUBSCRIPTION && WITH_SCHEMA
#include "subscription.h"
#elif _NOTIFY && WITH_SCHEMA
#include "notify.h"
#elif _CONTAINER && WITH_SCHEMA
#include "container.h"
#elif _CONTENT_INSTANCE && WITH_SCHEMA
#include "contentInstance.h"
#endif

errorCode
decodeMessage(CharType *output_buf)
{

  errorCode tmp_err_code = EXIP_OK;
  uint32_t en_start_decode = 0;
  uint32_t en_end_decode = 0;
  _m2m_object *m2m;
  int i;

  char *xmloutput;
  xmloutput = NULL;
  init_xml_output();

  energest_flush();
  en_start_decode = energest_type_time(ENERGEST_TYPE_CPU);
  tmp_err_code = decodeExiBuffer(SCHEMA_OPT, output_buf, &xmloutput, &m2m);
  en_end_decode = energest_type_time(ENERGEST_TYPE_CPU);

  if(m2m != NULL) {
#if SKIP_XML
    printf("DECODING: start=%lu; end=%lu; diff=%lu value in ms=%d\n", en_start_decode, en_end_decode, en_end_decode - en_start_decode, (int)((en_end_decode, en_end_decode - en_start_decode) / 32.768));
    printf("\nBinary output:\n\n");
#endif
    if(m2m->application != NULL) {
#if SKIP_XML
      printf("application\n");
#endif
      if(m2m->application->accessRightID != NULL) {
#if SKIP_XML
        if(m2m->application->accessRightID->length > 0) {
          printf("     accessRightID: %.*s\n", m2m->application->accessRightID->length, m2m->application->accessRightID->str);
        } else { printf("     accessRightID:\n");
        }
#endif
        EXIP_MFREE(m2m->application->accessRightID);
      }
#if SKIP_XML
      else { printf("accessRightId null\n");
      }
#endif
      if(m2m->application->searchStrings != NULL) {
#if SKIP_XML
        printf("     searchStrings:\n");
#endif
        if(m2m->application->searchStrings->length > 0) {
          for(i = 0; i < m2m->application->searchStrings->length; i++) {
#if SKIP_XML
            printf("          searchString: %.*s\n", m2m->application->searchStrings->strings[i]->length, m2m->application->searchStrings->strings[i]->str);
#endif
            EXIP_MFREE(m2m->application->searchStrings->strings[i]);
          }
        }
      }
      if(m2m->application->aPoC != NULL) {
#if SKIP_XML
        if(m2m->application->aPoC->length > 0) {
          printf("     aPoC: %.*s\n", m2m->application->aPoC->length, m2m->application->aPoC->str);
        } else { printf("     aPoC:\n");
        }
#endif
        EXIP_MFREE(m2m->application->aPoC);
      }
      if(m2m->application->groupsReference != NULL) {
#if SKIP_XML
        if(m2m->application->aPoC->length > 0) {
          printf("     groupsReference: %.*s\n", m2m->application->groupsReference->length, m2m->application->groupsReference->str);
        } else { printf("     groupsReference:\n");
        }
#endif
        EXIP_MFREE(m2m->application->groupsReference);
      }
      if(m2m->application->accessRightsReference != NULL) {
#if SKIP_XML
        if(m2m->application->accessRightsReference->length > 0) {
          printf("     accessRightsReference: %.*s\n", m2m->application->accessRightsReference->length, m2m->application->accessRightsReference->str);
        } else { printf("     accessRightsReference:\n");
        }
#endif
        EXIP_MFREE(m2m->application->accessRightsReference);
      }
      if(m2m->application->subscriptionsReference != NULL) {
#if SKIP_XML
        if(m2m->application->subscriptionsReference->length > 0) {
          printf("     subscriptionsReference: %.*s\n", m2m->application->subscriptionsReference->length, m2m->application->subscriptionsReference->str);
        } else { printf("     subscriptionsReference:\n");
        }
#endif
        EXIP_MFREE(m2m->application->subscriptionsReference);
      }
      if(m2m->application->notificationChannelsReference != NULL) {
#if SKIP_XML
        if(m2m->application->notificationChannelsReference->length > 0) {
          printf("     notificationsChannelsReference: %.*s\n", m2m->application->notificationChannelsReference->length, m2m->application->notificationChannelsReference->str);
        } else { printf("     notificationsChannelsReference:\n");
        }
#endif
        EXIP_MFREE(m2m->application->notificationChannelsReference);
      }
    }
#if SKIP_XML
    else if(m2m->subscription != NULL) {
      printf("subscription\n");
    }
#endif
  }

#if !SKIP_XML
  if(xmloutput == NULL) {
    printf("\n^^^^null output\n");
  } else {
    if(strlen(xmloutput) == 0) {
      printf("^^^^non null output but null length\n");
    }
  }
  if((tmp_err_code == EXIP_OK) && (xmloutput != NULL) && (strlen(xmloutput) != 0)) {
    /* printf("DECODING: start=%lu; end=%lu; diff=%lu value in ms=%d\n",en_start_decode,en_end_decode,en_end_decode-en_start_decode,(int)((en_end_decode,en_end_decode-en_start_decode)/ 32.768 )); */
    /* printf("xml:%s size=%d\n",xmloutput,strlen(xmloutput)); */
    /* printf("Size of encoded EXI: %d\n",strlen(output_buf)); */
    printf("xml:%s", xmloutput);
  }
  destroy_xml_output();
#endif
  return tmp_err_code;
}

#define GSCL_BASE    "/m2m"
#define APPLICATIONS    "/applications"


errorCode
applicationBuild(CharType *output_buf, errorCode tmp_err_code)
{
  char *xmloutput = NULL;

  EXIPDateTime expirationTime;
  EXIPDateTime creationTime;

  SingleString appId = "application1";
  SingleString accessRightID[1];
  accessRightID[0] = "/accessRights/Locadmin_AR";

  SingleString searchstring[3];
  searchstring[0] = "searchString";
  searchstring[1] = "search_string_1";
  searchstring[2] = "search_string_2";

  SearchStrings searchstrings;
  searchstrings.strings = searchstring;
  searchstrings.length = 1;

  AnnounceTo announceTo;
  SingleString apoc = "apoc_";
  SingleString containersreference = GSCL_BASE APPLICATIONS "/app/containers";
  SingleString groupsreference = GSCL_BASE APPLICATIONS "/app/groups";
  SingleString accessrightsreference = GSCL_BASE "/accessRights/Locadmin_AR";
  SingleString subscriptionsreference = GSCL_BASE APPLICATIONS "/app/subscriptions";
  SingleString notificationchannelsreference = GSCL_BASE APPLICATIONS "/app/notifications";

  aPoCPaths apocpaths;
  aPoCPath apocpath[2];
  apocpath[0].path = "pathnumber_0";
  apocpath[0].accessRightID = "pathnumber_0_access_right_ID";
  apocpath[0].searchStrings = &searchstrings;
  apocpath[1].path = "pathnumber_1";
  apocpath[1].accessRightID = "pathnumber_1_access_right_ID";
  apocpath[1].searchStrings = searchstring;
  apocpaths.length = 1;
  apocpaths.apocpaths = &apocpath;

  TRY_FUN(setDateAndTime(&creationTime, FRACT_PRESENCE, 114, 2, 5, 11, 10, 55, 840, 5));
  TRY_FUN(setDateAndTime(&expirationTime, FRACT_PRESENCE, 114, 3, 24, 12, 00, 55, 840, 5));

  boolean activated = TRUE;
  boolean global = FALSE;
  SingleString refs[1];
  refs[0] = "minnie";
  announceTo.sclList.strings = refs;
  announceTo.sclList.length = 1;
  announceTo.activated = &activated;
  announceTo.global = &global;


#if PROFILE_4
  //tmp_err_code = application(SCHEMA_OPT, output_buf, appId, NULL, accessRightID[0], &searchstrings, NULL, NULL, apoc, NULL, NULL, groupsreference, accessrightsreference, subscriptionsreference, notificationchannelsreference);
  tmp_err_code = application(SCHEMA_OPT, output_buf, appId, NULL, NULL, &searchstrings, NULL, NULL, apoc, NULL, NULL, NULL, NULL, NULL, NULL);
#elif PROFILE_1
  tmp_err_code = application(SCHEMA_OPT, output_buf, appId, NULL, NULL, &searchstrings, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
#elif PROFILE_2
  tmp_err_code = application(SCHEMA_OPT, output_buf, appId, NULL, accessRightID[0], &searchstrings, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
#elif PROFILE_3
  /* tmp_err_code = application(SCHEMA_OPT, output_buf, NULL, NULL, accessRightID[0], NULL, NULL, NULL, NULL, NULL, NULL, groupsreference, NULL, NULL, NULL); */
  tmp_err_code = application(SCHEMA_OPT, output_buf, NULL, NULL, accessRightID[0], &searchstrings, NULL, NULL, apoc, NULL, NULL, groupsreference, accessrightsreference, NULL, NULL);
#endif

#if WRITE_OUTPUT_TO_FILE
  FILE *fp;
  fp = fopen("output.txt", "w");
  fwrite(output_buf, 1, strlen(output_buf) + 1, fp);
  fclose(fp);
#endif


  PRINTF("---------------------------\nexi:\n\n%s\n\nsize=%d\n---------------------------\n", output_buf, strlen(output_buf));

  /* print_block_times(); */

  return EXIP_OK;
}


errorCode
subscriptionBuild(CharType *output_buf, errorCode tmp_err_code)
{

  uint32_t en_start_encode = 0;
  uint32_t en_end_encode = 0;

  uint32_t en_start_decode = 0;
  uint32_t en_end_decode = 0;

  EXIPDateTime expirationTime;
  EXIPDateTime creationTime;
  EXIPDateTime lastModifiedTime;
  EXIPDateTime ifModifiedSince;
  EXIPDateTime ifUnmodifiedSince;

  Choice_MTBN_delayTolerance choice;
  AnyURI contact = "coap://DA_IP_Addr:Port/da_notif";

  SingleString subscription_id = "subscription_id";

  SingleString vect[2];
  vect[0] = "if_match_1";
  vect[1] = "if_match_2";

  FilterCriteria filter;
  filter.ifModifiedSince = &ifModifiedSince;
  filter.ifUnmodifiedSince = &ifUnmodifiedSince;
  filter.ifMatch.length = 2;
  filter.ifMatch.strings = vect;
  filter.ifNoneMatch.length = 2;
  filter.ifNoneMatch.strings = vect;

  SingleString subscriptionType = "ASYNCHRONOUS";

  long MTBN = 101;

  choice.MTBN = FALSE;
  choice.MTBN_value = &MTBN;
  choice.delayTolerance = &creationTime;

  TRY_FUN(setDateAndTime(&(expirationTime), FRACT_PRESENCE, 112, 8, 31, 13, 33, 55, 839, 5));
  TRY_FUN(setDateAndTime(&(creationTime), FRACT_PRESENCE, 112, 6, 31, 13, 33, 55, 839, 5));
  TRY_FUN(setDateAndTime(&(lastModifiedTime), FRACT_PRESENCE, 112, 6, 31, 13, 33, 55, 839, 5));
  TRY_FUN(setDateAndTime(&(ifModifiedSince), FRACT_PRESENCE, 112, 6, 31, 13, 33, 55, 839, 5));
  TRY_FUN(setDateAndTime(&(ifUnmodifiedSince), FRACT_PRESENCE, 112, 6, 31, 13, 33, 55, 839, 5));

  en_start_encode = energest_type_time(ENERGEST_TYPE_CPU);

#if PROFILE_4
  tmp_err_code = subscription(SCHEMA_OPT, output_buf, NULL, &expirationTime, &choice, NULL, NULL, NULL, NULL, contact);
#elif PROFILE_1
  tmp_err_code = subscription(SCHEMA_OPT, output_buf, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
#elif PROFILE_2
  tmp_err_code = subscription(SCHEMA_OPT, output_buf, NULL, &expirationTime, NULL, NULL, NULL, NULL, NULL, NULL);
#elif PROFILE_3
  tmp_err_code = subscription(SCHEMA_OPT, output_buf, NULL, &expirationTime, &choice, NULL, NULL, NULL, NULL, NULL);
#endif
  /* tmp_err_code = subscription(SCHEMA_OPT, output_buf, subscription_id, &expirationTime, &choice, &creationTime, &lastModifiedTime, &filter, subscriptionType, contact); */

  en_end_encode = energest_type_time(ENERGEST_TYPE_CPU);

  /* printf("ENCODING: start=%lu; end=%lu; diff=%lu value in ms=%d\n",en_start_encode,en_end_encode,en_end_encode-en_start_encode,(int)((en_end_encode,en_end_encode-en_start_encode)/ 32.768 )); */

  /* printf("---------------------------\nexi:\n\n%s\n\nsize=%d\n---------------------------\n",output_buf,strlen(output_buf)); */

#if WRITE_OUTPUT_TO_FILE
  FILE *fp;
  fp = fopen("output.txt", "w");
  fwrite(output_buf, 1, strlen(output_buf) + 1, fp);
  fclose(fp);
#endif

  print_block_times();

  return EXIP_OK;
}


errorCode
notifyBuild(CharType *output_buf, errorCode tmp_err_code)
{

  uint32_t en_start_encode = 0;
  uint32_t en_end_encode = 0;
  char *xmloutput = NULL;
  statusCode statuscode = STATUS_OK;
  AnyURI anyuri = "coap;//GW-IP_Addr:Port/gw01/applications/app/subscriptions/sub";

  SingleString code = StatusCodeToString(statuscode);
  SingleString rep = "representation_in_base64";

  en_start_encode = energest_type_time(ENERGEST_TYPE_CPU);

#if PROFILE_4
  tmp_err_code = notify(SCHEMA_OPT, output_buf, code, rep, anyuri);
#elif PROFILE_1
  tmp_err_code = notify(SCHEMA_OPT, output_buf, code, NULL, anyuri);
#else
  tmp_err_code = notify(SCHEMA_OPT, output_buf, code, rep, anyuri);
#endif

  en_end_encode = energest_type_time(ENERGEST_TYPE_CPU);

  printf("start=%lu; end=%lu; diff=%lu\n", en_start_encode, en_end_encode, en_end_encode - en_start_encode);

  printf("---------------------------\nexi:\n\n%s\n\nsize=%d\n---------------------------\n", output_buf, strlen(output_buf));

  return EXIP_OK;
}


errorCode
containerBuild(CharType *output_buf, errorCode tmp_err_code)
{

  char *xmloutput = NULL;
  uint32_t en_start_encode = 0;
  uint32_t en_end_encode = 0;
  EXIPDateTime creationTime;
  EXIPDateTime expirationTime;
  EXIPDateTime lastModifiedTime;

  TRY_FUN(setDateAndTime(&creationTime, FRACT_PRESENCE, 112, 6, 31, 13, 33, 55, 839, 5));
  TRY_FUN(setDateAndTime(&expirationTime, FRACT_PRESENCE, 112, 6, 19, 13, 31, 55, 839, 5));
  TRY_FUN(setDateAndTime(&lastModifiedTime, FRACT_PRESENCE, 112, 6, 17, 13, 0, 55, 839, 5));

  SingleString id = "container_id";

  AccessRightID accessright = "access_right";
  SingleString searchstring[3];
  searchstring[0] = "topolino";
  searchstring[1] = "paperino";
  searchstring[2] = "pippo";

  SearchStrings searchstrings;
  searchstrings.strings = searchstring;
  searchstrings.length = 1;

  AnnounceTo announceTo;
  boolean activated = TRUE;
  boolean global = FALSE;
  SingleString refs[1];
  refs[0] = "minnie";
  announceTo.sclList.strings = refs;
  announceTo.sclList.length = 1;
  announceTo.activated = &activated;
  announceTo.global = &global;

  Integer maxnrofinstances = 1200;
  Integer maxnbytesize = 400;
  Integer maxinstanceage = 450;
  SingleString contentinstancereference = "content_instance_reference";
  SingleString subscriptionsreference = "subscriptions_reference";

  en_start_encode = energest_type_time(ENERGEST_TYPE_CPU);

  tmp_err_code = container(SCHEMA_OPT, output_buf, id, &expirationTime, accessright, &searchstrings, &creationTime, &lastModifiedTime, &announceTo, &maxnrofinstances, &maxnbytesize, &maxinstanceage, contentinstancereference, subscriptionsreference);

  en_end_encode = energest_type_time(ENERGEST_TYPE_CPU);

  printf("start=%lu; end=%lu; diff=%lu\n", en_start_encode, en_end_encode, en_end_encode - en_start_encode);

  printf("---------------------------\nexi:\n\n%s\n\nsize=%d\n---------------------------\n", output_buf, strlen(output_buf));

  /*

          {
                  init_xml_output();
                  tmp_err_code = decodeExiBuffer(SCHEMA_OPT, output_buf,&xmloutput);//&prfx_schema
                  if (xmloutput==NULL) printf("\n^^^^null output\n");
                  else {
                  if (strlen(xmloutput)==0) printf("^^^^non null output but null length\n");
                  }
                  if ((tmp_err_code==EXIP_OK)&&(xmloutput!=NULL)&&(strlen(xmloutput)!=0)) printf("---------------------------\nxml:\n\n%s\n\nsize=%d\n---------------------------",xmloutput,strlen(xmloutput));
                  destroy_xml_output();
          }
   */

  return EXIP_OK;
}


errorCode
contentInstanceBuild(CharType *output_buf, errorCode tmp_err_code, SingleString content)
{
  uint32_t en_start_encode = 0;
  uint32_t en_end_encode = 0;
  char *xmloutput = NULL;
  statusCode statuscode = STATUS_OK;
  //AnyURI anyuri = "a_uri_of_some_kind";
  SingleString code = StatusCodeToString(statuscode);

  SingleString accessRightID = "/accessRights/Locadmin_AR";

  AnnounceTo announceTo;
  boolean activated = TRUE;
  boolean global = FALSE;
  SingleString searchstring[3];
  searchstring[0] = "searchstring_0";
  searchstring[1] = "searchstring_1";
  searchstring[2] = "searchstring_2";

  SingleString id = "content_instance_id";
  SingleString href = "h_ref";
  SingleString contentSize = "450";
  /* SingleString content = "content_base_64"; */
  ContentTypes contenttypes;
  contenttypes.strings = searchstring;
  contenttypes.length = 1;

  EXIPDateTime creationTime;
  EXIPDateTime lastModifiedTime;
  EXIPDateTime delayTolerance;
  TRY_FUN(setDateAndTime(&(creationTime), FRACT_PRESENCE, 112, 6, 31, 13, 33, 55, 839, 5));
  TRY_FUN(setDateAndTime(&(lastModifiedTime), FRACT_PRESENCE, 112, 6, 31, 13, 33, 55, 839, 5));
  TRY_FUN(setDateAndTime(&(delayTolerance), FRACT_PRESENCE, 112, 6, 31, 13, 33, 55, 839, 5));

  //en_start_encode = energest_type_time(ENERGEST_TYPE_CPU);

  //tmp_err_code = contentInstance(SCHEMA_OPT, output_buf, NULL, NULL, NULL, &lastModifiedTime, NULL, NULL, NULL, content);
  tmp_err_code = contentInstance(SCHEMA_OPT, output_buf, NULL, NULL, NULL, NULL, NULL, NULL, NULL, content, accessRightID); //we don't have time synch

  //en_end_encode = energest_type_time(ENERGEST_TYPE_CPU);

  /* printf("start=%lu; end=%lu; diff=%lu\n", en_start_encode, en_end_encode, en_end_encode - en_start_encode); */

  /* printf("---------------------------\nexi:\n\n%s\n\nsize=%d\n---------------------------\n", output_buf, strlen(output_buf)); */

  /* print_block_times(); */

  return EXIP_OK;
}

