/*
 * File:   config.h
 * Author: francesca
 *
 * Created on March 11, 2014, 6:00 PM
 */

#ifndef CONFIG_H
#define CONFIG_H

#define EXI_ENCODED_STREAM_BUFFER_SIZE 500
#define DECODE_INPUT_BUFFER_SIZE EXI_ENCODED_STREAM_BUFFER_SIZE
#define DECODE_OUTPUT_BUFFER_SIZE 800

#include "procTypes.h"
#include "IcsiTypes.h"
#include "energest.h"

#define SKIP_XML OFF

#define XML_OUTPUT_COMPUTE ON

#define DECODE_ONLY OFF

#define _APPLICATION OFF
#define _SUBSCRIPTION OFF
#define _NOTIFY OFF
#define _CONTAINER OFF
#define _CONTENT_INSTANCE ON

#define WRITE_OUTPUT_TO_FILE OFF

#define WITH_SCHEMA OFF

#define PROFILE_1 OFF
#define PROFILE_2 OFF
#define PROFILE_3 OFF
#define PROFILE_4 ON

#if WITH_SCHEMA
#define SCHEMA_OPT &prfx_schema
#else
#define SCHEMA_OPT NULL
#endif

#define SKIP_XML_DECODE_DEBUG OFF

#define FRANCESCA_DEBUG   OFF
#define M2MENCODE_PRINTOUT OFF
#define GRAMMARS OFF

#define DECODE_PRINTOUT OFF
#define ENCODE_PRINTOUT OFF
#define MEM_MANAGEMENT OFF

#define DECODE_XML_PRINTOUT OFF

#define PRINT_ENCODED OFF

#if PRINT_ENCODED
#define PRINTF(...) printf(__VA_ARGS__)
#define PRINT6ADDR(addr) PRINTF("[%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x]", ((uint8_t *)addr)[0], ((uint8_t *)addr)[1], ((uint8_t *)addr)[2], ((uint8_t *)addr)[3], ((uint8_t *)addr)[4], ((uint8_t *)addr)[5], ((uint8_t *)addr)[6], ((uint8_t *)addr)[7], ((uint8_t *)addr)[8], ((uint8_t *)addr)[9], ((uint8_t *)addr)[10], ((uint8_t *)addr)[11], ((uint8_t *)addr)[12], ((uint8_t *)addr)[13], ((uint8_t *)addr)[14], ((uint8_t *)addr)[15])
#define PRINTLLADDR(lladdr) PRINTF("[%02x:%02x:%02x:%02x:%02x:%02x]", (lladdr)->addr[0], (lladdr)->addr[1], (lladdr)->addr[2], (lladdr)->addr[3], (lladdr)->addr[4], (lladdr)->addr[5])
#else
#define PRINTF(...)
#define PRINT6ADDR(addr)
#define PRINTLLADDR(addr)
#endif

#endif /* CONFIG_H */

