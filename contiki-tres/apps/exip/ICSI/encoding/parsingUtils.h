#include "grammarGenerator.h"
#include <stdio.h>

#ifndef MAX_XSD
#define MAX_XSD
#define MAX_XSD_FILES_COUNT 10
#endif

void parseSchema(char *xsdList, EXIPSchema *schema);

size_t writeFileOutputStream(void *buf, size_t readSize, void *stream);
