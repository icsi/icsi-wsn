/*
 * File:   m2mDefinitions.h
 * Author: francesca
 *
 * Created on January 23, 2014, 12:41 PM
 */

#ifndef M2MDEFINITIONS_H
#define M2MDEFINITIONS_H

#include <stdio.h>

const String NS_ETSI_M2M = { "http://uri.etsi.org/m2m", 23 };
const String NS_SCHEMA = { "http://www.w3.org/2001/XMLSchema-instance", 41 };
const String NS_EMPTY_STR = { NULL, 0 };
const String EXPIRATION_TIME = { "expirationTime", 14 };
const String CREATION_TIME = { "creationTime", 12 };
const String CONTAINERS_REFERENCE = { "containersReference", 20 };
const String GROUPS_REFERENCE = { "groupsReference", 15 };
const String ACCESS_RIGHTS_REFERENCE = { "accessRightsReference", 21 };
const String SUBSCRIPTIONS_REFERENCE = { "subscriptionsReference", 22 };
const String NOTIFICATION_CHANNELS_REFERENCE = { "notificationChannelsReference", 29 };

const String SUBSCRIPTION = { "subscription", 12 };
const String SUBSCRIPTION_TYPE = { "subscriptionType", 16 };
const String SUBSCRIPTION_REFERENCE = { "subscriptionReference", 21 };
const String DELAY_TOLERANCE = { "delayTolerance", 14 };
const String LAST_MODIFIED_TIME = { "lastModifiedTime", 16 };
const String MINIMAL_TIME_BETWEEN_NOTIFICATIONS = { "minimalTimeBetweenNotifications", 31 };
const String FILTER_CRITERIA = { "filterCriteria", 14 };
const String IF_MODIFIED_SINCE = { "ifModifiedSince", 15 };
const String IF_UNMODIFIED_SINCE = { "ifUnmodifiedSince", 17 };
const String IF_MATCH = { "ifMatch", 7 };
const String IF_NONE_MATCH = { "ifNoneMatch", 11 };
const String CONTACT = { "contact", 7 };
const String ID = { "id", 2 };

const String APPLICATION = { "application", 11 };
const String APP_ID = { "appId", 5 };
const String ACCESS_RIGHT_ID = { "accessRightID", 13 };
const String SEARCH_STRINGS = { "searchStrings", 13 };
const String SEARCH_STRING = { "searchString", 12 };
const String ANNOUNCE_TO = { "announceTo", 10 };
const String SCL_LIST = { "sclList", 7 };
const String REFERENCE = { "reference", 9 };
const String APOC = { "aPoC", 4 };
const String APOCPATHS = { "aPoCPaths", 9 };
const String APOCPATH = { "aPoCPath", 8 };
const String PATH = { "path", 4 };
const String LOCREQUESTOR = { "locRequestor", 12 };
const String ACTIVATED = { "activated", 9 };
const String GLOBAL = { "global", 6 };

const String NOTIFY = { "notify", 6 };
const String STATUS_CODE = { "statusCode", 10 };
const String REPRESENTATION = { "representation", 14 };
const String SUBSCIPTION_REFERENCE = { "subscriptionReference", 21 };

const String CONTAINER = { "container", 9 };
const String MAX_NR_OF_INSTANCES = { "maxNrOfInstances", 16 };
const String MAX_BYTE_SIZE = { "maxByteSize", 11 };
const String MAX_INSTANCE_AGE = { "maxInstanceAge", 14 };
const String CONTENT_INSTANCES_REFERENCE = { "contentInstancesReference", 25 };

const String CONTENT_INSTANCE = { "contentInstance", 15 };
const String CONTENT_SIZE = { "contentSize", 11 };
const String CONTENT_TYPES = { "contentTypes", 12 };
const String CONTENT_TYPE = { "contentType", 11 };
const String CONTENT = { "content", 7 };
const String H_REF = { "href", 4 };

const String SCHEMA = { "schema", 6 };
const String NAME = { "name", 4 };
const String XMLNS = { "xmlns", 5 };
const String TYPE = { "type", 4 };
const String ELEMENT = { "element", 7 };
const String TARGET_NAMESPACE = { "targetNamespace", 15 };

#endif /* M2MDEFINITIONS_H */

