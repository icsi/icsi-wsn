#include "exiDecode.h"
#include "grammarGenerator.h"
#include "EXIParser.h"
#include "stringManipulate.h"
#include "m2m_object.h"
#include <stdio.h>
#include <string.h>
#include "decodeM2MUtils.h"

#define MAX_PREFIXES 10

struct appData {
  unsigned char outputFormat;
  unsigned char expectAttributeData;
  char nameBuf[200];   /* needed for the OUT_XML Output Format */
  struct element *stack;   /* needed for the OUT_XML Output Format */
  unsigned char unclosedElement;   /* needed for the OUT_XML Output Format */
  char prefixes[MAX_PREFIXES][200];   /* needed for the OUT_XML Output Format */
  unsigned char prefixesCount;   /* needed for the OUT_XML Output Format */
};

struct element {
  struct element *next;
  char *name;
};

static void push(struct element **stack, struct element *el);
static struct element *pop(struct element **stack);
static struct element *createElement(char *name);
static void destroyElement(struct element *el);

/* returns != 0 if error */
static char lookupPrefix(struct appData *aData, String ns, unsigned char *prxHit, unsigned char *prefixIndex);

/* ****************************************** */

/* Content Handler API */
static errorCode sample_fatalError(const errorCode code, const char *msg, void *app_data);
static errorCode sample_startDocument(void *app_data);
static errorCode sample_endDocument(void *app_data);
static errorCode sample_startElement(QName qname, void *app_data);
static errorCode sample_endElement(void *app_data);
static errorCode sample_attribute(QName qname, void *app_data);
static errorCode sample_stringData(const String value, void *app_data);
static errorCode sample_decimalData(Decimal value, void *app_data);
static errorCode sample_intData(Integer int_val, void *app_data);
static errorCode sample_floatData(Float fl_val, void *app_data);
static errorCode sample_booleanData(boolean bool_val, void *app_data);
static errorCode sample_dateTimeData(EXIPDateTime dt_val, void *app_data);
static errorCode sample_binaryData(const char *binary_val, Index nbytes, void *app_data);
static errorCode sample_qnameData(const QName qname, void *app_data);

static errorCode m2m_startElement(QName qname, void *app_data);
static errorCode m2m_stringData(const String value, void *app_data);

errorCode
decodeExiBuffer(EXIPSchema *schemaPtr, char *inputBuffer, char **xmloutput, _m2m_object **m2m)
{

  d_malloc_init();
  getMemUsage();
  unsigned char outFlag = OUT_XML;
  boolean outOfBandOpts = FALSE;
  Parser testParser;
  BinaryBuffer buffer;
  errorCode tmp_err_code = EXIP_UNEXPECTED_ERROR;
  struct appData parsingData;
  EXIOptions options;
  makeDefaultOpts(&options);
  EXIOptions *opts = &options;
  buffer.buf = inputBuffer;
  buffer.bufLen = DECODE_INPUT_BUFFER_SIZE;
  buffer.bufContent = strlen(inputBuffer) + 1;
  /* Parsing steps: */

  /* I: First, define an external stream for the input to the parser if any, otherwise set to NULL */
  buffer.ioStrm.readWriteToStream = NULL;   /* inputStream; */
  buffer.ioStrm.stream = NULL;   /* infile; */

  /* II: Second, initialize the parser object */
  TRY(initParser(&testParser, buffer, &parsingData));

  /* III: Initialize the parsing data and hook the callback handlers to the parser object. */
  /*      If out-of-band options are defined use testParser.strm.header.opts to set them */
  parsingData.expectAttributeData = 0;
  parsingData.stack = NULL;
  parsingData.unclosedElement = 0;
  parsingData.prefixesCount = 0;
  parsingData.outputFormat = outFlag;
  if(outOfBandOpts && opts != NULL) {
    testParser.strm.header.opts = *opts;
  }

  testParser.handler.fatalError = sample_fatalError;
  testParser.handler.error = sample_fatalError;
#if SKIP_XML
  testParser.handler.startElement = m2m_startElement;
  testParser.handler.endElement = NULL;
  testParser.handler.stringData = m2m_stringData;
  testParser.handler.startDocument = NULL;
  testParser.handler.endDocument = NULL;
#else
  testParser.handler.startElement = sample_startElement;
  testParser.handler.endElement = sample_endElement;
  testParser.handler.stringData = sample_stringData;
  testParser.handler.startDocument = sample_startDocument;
  testParser.handler.endDocument = sample_endDocument;
#endif
  testParser.handler.dateTimeData = sample_dateTimeData;
  testParser.handler.attribute = sample_attribute;
  testParser.handler.decimalData = NULL;   /* sample_decimalData; */
  testParser.handler.intData = sample_intData;
  testParser.handler.floatData = NULL;   /* sample_floatData; */
  testParser.handler.booleanData = sample_booleanData;
  testParser.handler.binaryData = NULL;   /* sample_binaryData; */
  testParser.handler.qnameData = sample_qnameData;

  /* IV: Parse the header of the stream */

  TRY(parseHeader(&testParser, outOfBandOpts));

  /* IV.1: Set the schema to be used for parsing. */
  /* The schemaID mode and schemaID field can be read at */
  /* parser.strm.header.opts.schemaIDMode and */
  /* parser.strm.header.opts.schemaID respectively */
  /* If schemaless mode, use setSchema(&parser, NULL); */

  TRY(setSchema(&testParser, schemaPtr));

  /* V: Parse the body of the EXI stream */

  /* tmp_err_code = parse_m2m_object(&testParser); */

  current_production = &m2m_object;
  currentStringData = NULL;
  currentDateTime = NULL;

  while(tmp_err_code == EXIP_OK) {
    tmp_err_code = parseNext(&testParser);
  }
  /* VI: Free the memory allocated by the parser */
  /* printf("DONE decoding\n"); */
  destroyParser(&testParser);

  if(tmp_err_code == EXIP_PARSING_COMPLETE) {
    tmp_err_code = EXIP_OK;
  } else {
    tmp_err_code = tmp_err_code;
  }

  if(tmp_err_code != EXIP_OK) {
    DEBUG_MSG(INFO, DECODE_PRINTOUT, ("\nError (code: %d) during parsing of the EXI stream\n", tmp_err_code));
  } else {
    DEBUG_MSG(INFO, DECODE_PRINTOUT, ("\nSuccessful parsing of the EXI stream\n"));
#if SKIP_XML
    *xmloutput = NULL;
    *m2m = &__m2m_object;
#else
    *xmloutput = xml_output;
    *m2m = NULL;
#endif
  }
  return tmp_err_code;
}
/*static errorCode parse_m2m_object(Parser* testParser){

    errorCode tmp_err_code;
    tmp_err_code = parseNext(&testParser);
    if ()
   }*/

static errorCode
sample_fatalError(const errorCode code, const char *msg, void *app_data)
{
  printf("\n%d : FATAL ERROR: %s\n", code, msg);
  return EXIP_HANDLER_STOP;
}
static errorCode
sample_startDocument(void *app_data)
{
  struct appData *appD = (struct appData *)app_data;
  if(appD->outputFormat == OUT_EXI) {
    printf("SD\n");
  } else if(appD->outputFormat == OUT_XML) {
    printToXMLoutput("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
    DEBUG_MSG(INFO, DECODE_PRINTOUT, ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>++++++\n"));
  }

  return EXIP_OK;
}
static errorCode
sample_endDocument(void *app_data)
{
  struct appData *appD = (struct appData *)app_data;
  if(appD->outputFormat == OUT_EXI) {
    printf("ED\n");
  } else if(appD->outputFormat == OUT_XML) {
    DEBUG_MSG(INFO, DECODE_PRINTOUT, ("\n"));
  }

  return EXIP_OK;
}
static errorCode
m2m_startElement(QName qname, void *app_data)
{
  struct appData *appD = (struct appData *)app_data;
  errorCode tmp_err_code = EXIP_OK;
  char error = 0;
  unsigned char prefixIndex = 0;
  unsigned char prxHit = 1;
  int t;

  if(!isStringEmpty(qname.uri)) {
    error = lookupPrefix(appD, *qname.uri, &prxHit, &prefixIndex);
    if(error != 0) {
      return EXIP_HANDLER_STOP;
    }

    sprintf(appD->nameBuf, "p%d:", prefixIndex);
    t = strlen(appD->nameBuf);
    memcpy(appD->nameBuf + t, qname.localName->str, qname.localName->length);
    appD->nameBuf[t + qname.localName->length] = '\0';
  } else {
    memcpy(appD->nameBuf, qname.localName->str, qname.localName->length);
    appD->nameBuf[qname.localName->length] = '\0';
  }
  push(&(appD->stack), createElement(appD->nameBuf));

  DEBUG_MSG(INFO, SKIP_XML_DECODE_DEBUG, ("~~~current element: %.*s\n", qname.localName->length, qname.localName->str));

  if((!strncmp(qname.localName->str, "application", qname.localName->length)) && (current_production->label == START)) {
    tmp_err_code = generateApplication();
  } else {
    if(!strncmp(qname.localName->str, "expirationTime", qname.localName->length) && (current_production->label == APPLICATION)) {
      tmp_err_code = generateExpirationTime();
    } else {
      if(!strncmp(qname.localName->str, "accessRightID", qname.localName->length) && (current_production->label == APPLICATION)) {
        tmp_err_code = generateAccessRightID();
      } else {
        if(!strncmp(qname.localName->str, "searchString", qname.localName->length)) {
          tmp_err_code = generateSearchString();
        } else {
          if(!strncmp(qname.localName->str, "searchStrings", qname.localName->length)) {
            tmp_err_code = generateSearchStrings();
          } else {
            if(!strncmp(qname.localName->str, "creationTime", qname.localName->length) && (current_production->label == APPLICATION)) {
              tmp_err_code = generateCreationTime();
            } else {
              if(!strncmp(qname.localName->str, "lastModifiedTime", qname.localName->length) && (current_production->label == APPLICATION)) {
                tmp_err_code = generateLastModifiedTime();
              } else {
                if(!strncmp(qname.localName->str, "aPoC", qname.localName->length)) {
                  tmp_err_code = generateAPoC();
                } else {
                  if(!strncmp(qname.localName->str, "groupsReference", qname.localName->length)) {
                    tmp_err_code = generateGroupsReference();
                  } else {
                    if(!strncmp(qname.localName->str, "accessRightsReference", qname.localName->length)) {
                      tmp_err_code = generateAccessRightsReference();
                    } else {
                      if(!strncmp(qname.localName->str, "subscriptionsReference", qname.localName->length)) {
                        tmp_err_code = generateSubscriptionsReference();
                      } else {
                        if(!strncmp(qname.localName->str, "notificationChannelsReference", qname.localName->length)) {
                          tmp_err_code = generateNotificationChannelsReference();
                        } else {
                          tmp_err_code = EXIP_UNEXPECTED_ERROR;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  appD->unclosedElement = 1;
  return tmp_err_code;
}
static errorCode
sample_startElement(QName qname, void *app_data)
{
  struct appData *appD = (struct appData *)app_data;
  if(appD->outputFormat == OUT_EXI) {
    printf("SE ");
    /* printString(qname.uri); */
    printf(" ");
    /* printString(qname.localName); */
    printf("\n");
  } else if(appD->outputFormat == OUT_XML) {
    char error = 0;
    unsigned char prefixIndex = 0;
    unsigned char prxHit = 1;
    int t;

    if(!isStringEmpty(qname.uri)) {
      error = lookupPrefix(appD, *qname.uri, &prxHit, &prefixIndex);
      if(error != 0) {
        return EXIP_HANDLER_STOP;
      }

      sprintf(appD->nameBuf, "p%d:", prefixIndex);
      t = strlen(appD->nameBuf);
      memcpy(appD->nameBuf + t, qname.localName->str, qname.localName->length);
      appD->nameBuf[t + qname.localName->length] = '\0';
    } else {
      memcpy(appD->nameBuf, qname.localName->str, qname.localName->length);
      appD->nameBuf[qname.localName->length] = '\0';
    }
    push(&(appD->stack), createElement(appD->nameBuf));
    if(appD->unclosedElement) {
      DEBUG_MSG(INFO, DECODE_PRINTOUT, (">\n"));
      printToXMLoutput(">\n");
    }
    DEBUG_MSG(INFO, DECODE_PRINTOUT, ("<%s", appD->nameBuf));
    printToXMLoutput("<");
    printToXMLoutput(appD->nameBuf);
    if(prxHit == 0) {
      sprintf(appD->nameBuf, " xmlns:p%d=\"", prefixIndex);
      DEBUG_MSG(INFO, DECODE_PRINTOUT, ("%s", appD->nameBuf));
      /* printString(qname.uri); */
      DEBUG_MSG(INFO, DECODE_PRINTOUT, ("\""));
      printToXMLoutput(appD->nameBuf);
      copyToXMLoutput(qname.uri->str, qname.uri->length);
      printToXMLoutput("\"");
    }

    appD->unclosedElement = 1;
  }

  return EXIP_OK;
}
static errorCode
sample_endElement(void *app_data)
{
  struct appData *appD = (struct appData *)app_data;
  if(appD->outputFormat == OUT_EXI) {
    printf("EE\n");
  } else if(appD->outputFormat == OUT_XML) {
    struct element *el;

    if(appD->unclosedElement) {
      DEBUG_MSG(INFO, DECODE_PRINTOUT, (">\n"));
      printToXMLoutput(">\n");
    }
    appD->unclosedElement = 0;
    el = pop(&(appD->stack));
    DEBUG_MSG(INFO, DECODE_PRINTOUT, ("</%s>\n", el->name));
    printToXMLoutput("</");
    printToXMLoutput(el->name);
    printToXMLoutput(">\n");
    destroyElement(el);
  }

  return EXIP_OK;
}
static errorCode
sample_attribute(QName qname, void *app_data)
{
  struct appData *appD = (struct appData *)app_data;
  if(appD->outputFormat == OUT_EXI) {
    printf("AT ");
    /* printString(qname.uri); */
    printf(" ");
    /* printString(qname.localName); */
    printf("=\"");
  } else if(appD->outputFormat == OUT_XML) {
    char error = 0;
    unsigned char prefixIndex = 0;
    unsigned char prxHit = 1;
    int t;

    printToXMLoutput(" ");
    if(!isStringEmpty(qname.uri)) {
      error = lookupPrefix(appD, *qname.uri, &prxHit, &prefixIndex);
      if(error != 0) {
        return EXIP_HANDLER_STOP;
      }

      sprintf(appD->nameBuf, "p%d:", prefixIndex);
      t = strlen(appD->nameBuf);
      memcpy(appD->nameBuf + t, qname.localName->str, qname.localName->length);
      appD->nameBuf[t + qname.localName->length] = '\0';
    } else {
      memcpy(appD->nameBuf, qname.localName->str, qname.localName->length);
      appD->nameBuf[qname.localName->length] = '\0';
    }
    /*if(!isStringEmpty(qname.uri))
       {
            printString(qname.uri);
            printf(":");
            printToXMLoutput(qname.uri->str);
            printToXMLoutput(":");
       }*/
    /* printString(qname.localName); */
    printToXMLoutput(appD->nameBuf);

    /* printToXMLoutput(qname.localName->str); */
    printToXMLoutput("=\"");
  }
  appD->expectAttributeData = 1;

  return EXIP_OK;
}
static errorCode
m2m_stringData(const String value, void *app_data)
{

  errorCode tmp_err_code = EXIP_OK;
  struct appData *appD = (struct appData *)app_data;
  if(appD->expectAttributeData) {
    /* copyToXMLoutput(value.str,value.length); */
    /* printToXMLoutput("\""); */
    appD->expectAttributeData = 0;
  } else {
    if(appD->unclosedElement) {
      /* printToXMLoutput(">"); */
      DEBUG_MSG(INFO, DECODE_PRINTOUT, (">"));
    }
    appD->unclosedElement = 0;
    copyToXMLoutput(value.str, value.length);

    if(currentStringData != NULL) {
      (*currentStringData)->str = value.str;
      (*currentStringData)->length = value.length;
      currentStringData = NULL;
    } else {
      if(currentDateTime != NULL) {
        SingleString tmp[10];
        strncpy(tmp, value.str, 4);
        (*currentDateTime)->dateTime.tm_year = atoi(tmp);
        if(value.str[6] == '-') {
          strncpy(tmp, value.str + 5, 6);
          (*currentDateTime)->dateTime.tm_mon = atoi(tmp);
          if(value.str[8] == 'T') {
            strncpy(tmp, value.str + 7, 8);
          } else {
            strncpy(tmp, value.str + 7, 9);
          }
          (*currentDateTime)->dateTime.tm_mday = atoi(tmp);
        } else {
          strncpy(tmp, value.str + 5, 7);
          (*currentDateTime)->dateTime.tm_mon = atoi(tmp);
          if(value.str[9] == 'T') {
            strncpy(tmp, value.str + 8, 9);
          } else {
            strncpy(tmp, value.str + 8, 10);
          }
          (*currentDateTime)->dateTime.tm_mday = atoi(tmp);
        }
        currentDateTime = NULL;
      } else {
        printf("WARNING: should write content\n");         /* tmp_err_code = EXIP_UNEXPECTED_ERROR; */
        tmp_err_code = EXIP_UNEXPECTED_ERROR;
      }
    }
  }
  return tmp_err_code;
}
static errorCode
sample_stringData(const String value, void *app_data)
{
  struct appData *appD = (struct appData *)app_data;
  if(appD->outputFormat == OUT_EXI) {
    if(appD->expectAttributeData) {
      /* printString(&value); */
      printf("\"\n");
      printToXMLoutput(value.str);
      printToXMLoutput("\"\n");
      appD->expectAttributeData = 0;
    } else {
      printf("CH ");
      /* printString(&value); */
      printf("\n");
    }
  } else if(appD->outputFormat == OUT_XML) {
    if(appD->expectAttributeData) {
      /* printString(&value); */
      copyToXMLoutput(value.str, value.length);
      printToXMLoutput("\"");
      appD->expectAttributeData = 0;
    } else {
      if(appD->unclosedElement) {
        printToXMLoutput(">");
        DEBUG_MSG(INFO, DECODE_PRINTOUT, (">"));
      }
      appD->unclosedElement = 0;
      /* printString(&value); */
      copyToXMLoutput(value.str, value.length);
      /* printf("About to write: %s length %d or %d\n",value.str,strlen(value.str),value.length); */
    }
  }

  return EXIP_OK;
}
static errorCode
sample_decimalData(Decimal value, void *app_data)
{
  return sample_floatData(value, app_data);
}
static errorCode
sample_intData(Integer int_val, void *app_data)
{
  struct appData *appD = (struct appData *)app_data;
  char tmp_buf[30];
  if(appD->outputFormat == OUT_EXI) {
    if(appD->expectAttributeData) {
      sprintf(tmp_buf, "%lld", (long long int)int_val);
      printf("%s", tmp_buf);
      printf("\"\n");
      appD->expectAttributeData = 0;
    } else {
      printf("CH ");
      sprintf(tmp_buf, "%lld", (long long int)int_val);
      printf("%s", tmp_buf);
      printf("\n");
    }
  } else if(appD->outputFormat == OUT_XML) {
    if(appD->expectAttributeData) {
      sprintf(tmp_buf, "%lld", (long long int)int_val);
      DEBUG_MSG(INFO, DECODE_PRINTOUT, ("%s", tmp_buf));
      printToXMLoutput(tmp_buf);
      DEBUG_MSG(INFO, DECODE_PRINTOUT, ("\""));
      printToXMLoutput("\"");
      appD->expectAttributeData = 0;
    } else {
      if(appD->unclosedElement) {
        DEBUG_MSG(INFO, DECODE_PRINTOUT, (">"));
        printToXMLoutput(">");
      }
      appD->unclosedElement = 0;
      sprintf(tmp_buf, "%lld", (long long int)int_val);
      DEBUG_MSG(INFO, DECODE_PRINTOUT, ("%s", tmp_buf));
      printToXMLoutput(tmp_buf);
    }
  }

  return EXIP_OK;
}
static errorCode
sample_booleanData(boolean bool_val, void *app_data)
{
  struct appData *appD = (struct appData *)app_data;

  if(appD->outputFormat == OUT_EXI) {
    if(appD->expectAttributeData) {
      if(bool_val) {
        printf("true\"\n");
      } else {
        printf("false\"\n");
      }

      appD->expectAttributeData = 0;
    } else {
      printf("CH ");
      if(bool_val) {
        printf("true\n");
      } else {
        printf("false\n");
      }
    }
  } else if(appD->outputFormat == OUT_XML) {
    if(appD->expectAttributeData) {
      if(bool_val) {
        printToXMLoutput("true\"");
        DEBUG_MSG(INFO, DECODE_PRINTOUT, ("true\""));
      } else {
        DEBUG_MSG(INFO, DECODE_PRINTOUT, ("false\""));
        printToXMLoutput("false\"\n");
      }
      appD->expectAttributeData = 0;
    } else {
      if(appD->unclosedElement) {
        printToXMLoutput(">");
        DEBUG_MSG(INFO, DECODE_PRINTOUT, (">"));
      }
      appD->unclosedElement = 0;

      if(bool_val) {
        DEBUG_MSG(INFO, DECODE_PRINTOUT, ("true"));
        printToXMLoutput("true");
      } else {
        DEBUG_MSG(INFO, DECODE_PRINTOUT, ("false"));
        printToXMLoutput("false");
      }
    }
  }

  return EXIP_OK;
}
static errorCode
sample_floatData(Float fl_val, void *app_data)
{
  struct appData *appD = (struct appData *)app_data;
  char tmp_buf[30];
  if(appD->outputFormat == OUT_EXI) {
    if(appD->expectAttributeData) {
      sprintf(tmp_buf, "%lldE%d", (long long int)fl_val.mantissa, fl_val.exponent);
      printf("%s", tmp_buf);
      printf("\"\n");
      appD->expectAttributeData = 0;
    } else {
      printf("CH ");
      sprintf(tmp_buf, "%lldE%d", (long long int)fl_val.mantissa, fl_val.exponent);
      printf("%s", tmp_buf);
      printf("\n");
    }
  } else if(appD->outputFormat == OUT_XML) {
    if(appD->expectAttributeData) {
      sprintf(tmp_buf, "%lldE%d", (long long int)fl_val.mantissa, fl_val.exponent);
      DEBUG_MSG(INFO, DECODE_PRINTOUT, ("%s", tmp_buf));
      DEBUG_MSG(INFO, DECODE_PRINTOUT, ("\""));
      appD->expectAttributeData = 0;
    } else {
      if(appD->unclosedElement) {
        DEBUG_MSG(INFO, DECODE_PRINTOUT, (">"));
      }
      appD->unclosedElement = 0;
      sprintf(tmp_buf, "%lldE%d", (long long int)fl_val.mantissa, fl_val.exponent);
      DEBUG_MSG(INFO, DECODE_PRINTOUT, ("%s", tmp_buf));
    }
  }

  return EXIP_OK;
}
static errorCode
sample_dateTimeData(EXIPDateTime dt_val, void *app_data)
{
  struct appData *appD = (struct appData *)app_data;
  char fsecBuf[30];
  int i;

  if(IS_PRESENT(dt_val.presenceMask, FRACT_PRESENCE)) {
    unsigned int tmpfValue = dt_val.fSecs.value;
    int digitNum = 0;

    fsecBuf[0] = '.';

    while(tmpfValue) {
      digitNum++;
      tmpfValue = tmpfValue / 10;
    }
    for(i = 0; i < dt_val.fSecs.offset + 1 - digitNum; i++) {
      fsecBuf[1 + i] = '0';
    }

    sprintf(fsecBuf + 1 + i, "%d", dt_val.fSecs.value);
  } else {
    fsecBuf[0] = '\0';
  }

  if(appD->outputFormat == OUT_EXI) {
    if(appD->expectAttributeData) {
      printf("%04d-%02d-%02dT%02d:%02d:%02d%s", dt_val.dateTime.tm_year + 1900,
             dt_val.dateTime.tm_mon + 1, dt_val.dateTime.tm_mday,
             dt_val.dateTime.tm_hour, dt_val.dateTime.tm_min,
             dt_val.dateTime.tm_sec, fsecBuf);
      printf("\"\n");
      appD->expectAttributeData = 0;
    } else {
      printf("CH ");
      printf("sample_dateTimeData1 %04d-%02d-%02dT%02d:%02d:%02d%s", dt_val.dateTime.tm_year + 1900,
             dt_val.dateTime.tm_mon + 1, dt_val.dateTime.tm_mday,
             dt_val.dateTime.tm_hour, dt_val.dateTime.tm_min,
             dt_val.dateTime.tm_sec, fsecBuf);
      printf("\n");
    }
  } else if(appD->outputFormat == OUT_XML) {
    if(appD->expectAttributeData) {
      CharType *s = EXIP_MALLOC(50 * sizeof(CharType));
      DEBUG_MSG(INFO, DECODE_PRINTOUT, ("%04d-%02d-%02dT%02d:%02d:%02d%s", dt_val.dateTime.tm_year + 1900,
                                        dt_val.dateTime.tm_mon + 1, dt_val.dateTime.tm_mday,
                                        dt_val.dateTime.tm_hour, dt_val.dateTime.tm_min,
                                        dt_val.dateTime.tm_sec, fsecBuf));
      DEBUG_MSG(INFO, DECODE_PRINTOUT, ("%d; %04d\n", dt_val.dateTime.tm_year + 1900, dt_val.dateTime.tm_year + 1900));
      sprintf(s, "%04d-%02d-%02dT%02d:%02d:%02d%s", dt_val.dateTime.tm_year + 1900,
              dt_val.dateTime.tm_mon + 1, dt_val.dateTime.tm_mday,
              dt_val.dateTime.tm_hour, dt_val.dateTime.tm_min,
              dt_val.dateTime.tm_sec, fsecBuf);
      printToXMLoutput(s);
      DEBUG_MSG(INFO, DECODE_PRINTOUT, ("\""));
      printToXMLoutput("\"");
      EXIP_MFREE(s);
      appD->expectAttributeData = 0;
    } else {
      if(appD->unclosedElement) {
        DEBUG_MSG(INFO, DECODE_PRINTOUT, (">"));
        printToXMLoutput(">");
      }
#if SKIP_XML
      (*currentDateTime)->TimeZone = dt_val.TimeZone;
      (*currentDateTime)->dateTime = dt_val.dateTime;
      (*currentDateTime)->fSecs = dt_val.fSecs;
      (*currentDateTime)->presenceMask = dt_val.presenceMask;
      currentDateTime = NULL;
#endif
      CharType *s = EXIP_MALLOC(50 * sizeof(CharType));
      appD->unclosedElement = 0;
      DEBUG_MSG(INFO, DECODE_PRINTOUT, ("%04d-%02d-%02dT%02d:%02d:%02d%s", dt_val.dateTime.tm_year + 1900,
                                        dt_val.dateTime.tm_mon + 1, dt_val.dateTime.tm_mday,
                                        dt_val.dateTime.tm_hour, dt_val.dateTime.tm_min,
                                        dt_val.dateTime.tm_sec, fsecBuf));
      DEBUG_MSG(INFO, DECODE_PRINTOUT, ("sample_dateTimeData2 %d; %04d\n", dt_val.dateTime.tm_year + 1900, dt_val.dateTime.tm_year + 1900));
      sprintf(s, "%04d-%02d-%02dT%02d:%02d:%02d%s", dt_val.dateTime.tm_year + 1900,
              dt_val.dateTime.tm_mon + 1, dt_val.dateTime.tm_mday,
              dt_val.dateTime.tm_hour, dt_val.dateTime.tm_min,
              dt_val.dateTime.tm_sec, fsecBuf);
      printToXMLoutput(s);
      EXIP_MFREE(s);
    }
  }

  return EXIP_OK;
}
static errorCode
sample_binaryData(const char *binary_val, Index nbytes, void *app_data)
{
  struct appData *appD = (struct appData *)app_data;

  if(appD->outputFormat == OUT_EXI) {
    if(appD->expectAttributeData) {
      printf("[binary: %d bytes]", (int)nbytes);
      printf("\"\n");
      appD->expectAttributeData = 0;
    } else {
      printf("CH ");
      printf("[binary: %d bytes]", (int)nbytes);
      printf("\n");
    }
  } else if(appD->outputFormat == OUT_XML) {
    if(appD->expectAttributeData) {
      DEBUG_MSG(INFO, DECODE_PRINTOUT, ("[binary: %d bytes]", (int)nbytes));
      DEBUG_MSG(INFO, DECODE_PRINTOUT, ("\""));
      appD->expectAttributeData = 0;
    } else {
      if(appD->unclosedElement) {
        DEBUG_MSG(INFO, DECODE_PRINTOUT, (">"));
      }
      appD->unclosedElement = 0;
      DEBUG_MSG(INFO, DECODE_PRINTOUT, ("[binary: %d bytes]", (int)nbytes));
    }
  }

  return EXIP_OK;
}
static errorCode
sample_qnameData(const QName qname, void *app_data)
{
  struct appData *appD = (struct appData *)app_data;
  if(appD->outputFormat == OUT_EXI) {
    if(appD->expectAttributeData) {
      /* printString(qname.uri); */
      printf(":");
      /* printString(qname.localName); */
      printf("\"\n");
      appD->expectAttributeData = 0;
    } else {
      printf("QNAME ");
      /* printString(qname.uri); */
      printf(":");
      /* printString(qname.localName); */
      printf("\n");
    }
  } else if(appD->outputFormat == OUT_XML) {
    if(appD->expectAttributeData) {
      /* printString(qname.uri); */
      DEBUG_MSG(INFO, DECODE_PRINTOUT, (":"));
      /* printString(qname.localName); */
      DEBUG_MSG(INFO, DECODE_PRINTOUT, ("\""));
      appD->expectAttributeData = 0;
    } else {
      if(appD->unclosedElement) {
        DEBUG_MSG(INFO, DECODE_PRINTOUT, (">"));
      }
      appD->unclosedElement = 0;
      /* printString(qname.uri); */
      DEBUG_MSG(INFO, DECODE_PRINTOUT, (":"));
      /* printString(qname.localName); */
    }
  }

  return EXIP_OK;
}
/* Stuff needed for the OUT_XML Output Format */
/* ****************************************** */

static void
push(struct element **stack, struct element *el)
{
  if(*stack == NULL) {
    *stack = el;
  } else {
    el->next = *stack;
    *stack = el;
  }
}
static struct element *
pop(struct element **stack)
{
  if(*stack == NULL) {
    return NULL;
  } else {
    struct element *result;
    result = *stack;
    *stack = (*stack)->next;
    return result;
  }
}
static struct element *
createElement(char *name)
{
  struct element *el;
  el = EXIP_MALLOC(sizeof(struct element));
  if(el == NULL) {
    exit(1);
  }
  el->next = NULL;
  el->name = EXIP_MALLOC(strlen(name) + 1);
  if(el->name == NULL) {
    exit(1);
  }
  strcpy(el->name, name);
  return el;
}
static void
destroyElement(struct element *el)
{
  EXIP_MFREE(el->name);
  EXIP_MFREE(el);
}
/* ****************************************** */

static char
lookupPrefix(struct appData *aData, String ns, unsigned char *prxHit, unsigned char *prefixIndex)
{
  int i;
  for(i = 0; i < aData->prefixesCount; i++) {
    if(stringEqualToAscii(ns, aData->prefixes[i])) {
      *prefixIndex = i;
      *prxHit = 1;
      return 0;
    }
  }

  if(aData->prefixesCount == MAX_PREFIXES) {
    return 1;
  } else {
    memcpy(aData->prefixes[aData->prefixesCount], ns.str, ns.length);
    aData->prefixes[aData->prefixesCount][ns.length] = '\0';
    *prefixIndex = aData->prefixesCount;
    aData->prefixesCount += 1;
    *prxHit = 0;
    return 0;
  }
}
