/*
 * File:   exiDecode.h
 * Author: francesca
 *
 * Created on February 11, 2014, 11:37 AM
 */

#ifndef EXIDECODE_H
#define EXIDECODE_H

#include "procTypes.h"
#include "config.h"

#define OUT_EXI 0
#define OUT_XML 1
#define OUT_BINARY 2

void init_xml_output();

void destroy_xml_output();

errorCode decodeExiBuffer(EXIPSchema *schemaPtr, char *inputBuffer, char **xmloutput, _m2m_object **m2m);

#endif /* EXIDECODE_H */

