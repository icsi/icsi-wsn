/*
 * File:   m2m_object.h
 * Author: francesca
 *
 * Created on April 10, 2014, 7:06 PM
 */

#ifndef M2M_OBJECT_H
#define M2M_OBJECT_H

#include <stdio.h>

typedef enum label {
  START, END, SUBSCRIPTIONS_REFERENCE, NOTIFICATION_CHANNELS_REFERENCE, GROUPS_REFERENCE, ACCESS_RIGHTS_REFERENCE, CONTAINERS_REFERENCE, LOC_REQUESTOR, APOC_PATH, APOC_PATHS, APOC, ANNOUNCE_TO,
  LAST_MODIFIED_TIME, CREATION_TIME, SEARCH_STRINGS, SEARCH_STRING, ACCESS_RIGHT_ID, EXPIRATION_TIME, APPLICATION
} label;

typedef enum production_type {
  RECURSIVE, NON_RECURSIVE
} production_type;

struct state {
  label label;
  struct state *next;
};
typedef struct state state;

struct production {
  label label;
  production_type type;
  struct production **prod;
};
typedef struct production production;

static const production end = { END, NON_RECURSIVE, NULL };

static const production *P1[1] = { &end };

static const production notification_channels_reference = { NOTIFICATION_CHANNELS_REFERENCE, NON_RECURSIVE, P1 };

static const production *P2[2] = { &notification_channels_reference, &end };

static const production subscriptions_reference = { SUBSCRIPTIONS_REFERENCE, NON_RECURSIVE, P2 };

static const production *P3[3] = { &subscriptions_reference, &notification_channels_reference, &subscriptions_reference, &end };

static const production access_rights_reference = { ACCESS_RIGHTS_REFERENCE, NON_RECURSIVE, P3 };

static const production *P4[4] = { &access_rights_reference, &subscriptions_reference, &notification_channels_reference, &end };

static const production groups_reference = { GROUPS_REFERENCE, NON_RECURSIVE, P4 };

static const production *P5[5] = { &groups_reference, &access_rights_reference, &subscriptions_reference, &notification_channels_reference, &end };

static const production containers_reference = { CONTAINERS_REFERENCE, NON_RECURSIVE, P5 };

static const production *P6[6] = { &containers_reference, &groups_reference, &access_rights_reference, &subscriptions_reference, &notification_channels_reference, &end };

static const production loc_requestor = { LOC_REQUESTOR, NON_RECURSIVE, P6 };

static const production apoc_path = { APOC_PATH, RECURSIVE, };

static const production *P7[7] = { &apoc_path, &loc_requestor, &containers_reference, &groups_reference, &access_rights_reference, &subscriptions_reference, &notification_channels_reference, &end };

static const production apoc_paths = { APOC_PATHS, NON_RECURSIVE, P7 };

static const production *P8[8] = { &apoc_paths, &loc_requestor, &containers_reference, &groups_reference, &access_rights_reference, &subscriptions_reference, &notification_channels_reference, &end };

static const production apoc = { APOC, NON_RECURSIVE, P8 };

static const production *P9[9] = { &apoc, &apoc_paths, &loc_requestor, &containers_reference, &groups_reference, &access_rights_reference, &subscriptions_reference, &notification_channels_reference, &end };

static const production announce_to = { ANNOUNCE_TO, NON_RECURSIVE, P9 };

static const production *P10[10] = { &announce_to, &apoc, &apoc_paths, &loc_requestor, &containers_reference, &groups_reference, &access_rights_reference, &subscriptions_reference, &notification_channels_reference, &end };

static const production last_modified_time = { LAST_MODIFIED_TIME, NON_RECURSIVE, P10 };

static const production *P11[11] = { &last_modified_time, &announce_to, &apoc, &apoc_paths, &loc_requestor, &containers_reference, &groups_reference, &access_rights_reference, &subscriptions_reference, &notification_channels_reference, &end };

static const production creation_time = { CREATION_TIME, NON_RECURSIVE, P11 };

static const production *P12_1[12] = { &creation_time, &last_modified_time, &announce_to, &apoc, &apoc_paths, &loc_requestor, &containers_reference, &groups_reference, &access_rights_reference, &subscriptions_reference, &notification_channels_reference, &end };

static const production search_string = { SEARCH_STRING, RECURSIVE, P12_1 };

static const production *P12[13] = { &search_string, &creation_time, &last_modified_time, &announce_to, &apoc, &apoc_paths, &loc_requestor, &containers_reference, &groups_reference, &access_rights_reference, &subscriptions_reference, &notification_channels_reference, &end };

static const production search_strings = { SEARCH_STRINGS, NON_RECURSIVE, P12 };

static const production *P13[13] = { &search_strings, &creation_time, &last_modified_time, &announce_to, &apoc, &apoc_paths, &loc_requestor, &containers_reference, &groups_reference, &access_rights_reference, &subscriptions_reference, &notification_channels_reference, &end };

static const production access_right_id = { ACCESS_RIGHT_ID, NON_RECURSIVE, P13 };

static const production *P14[14] = { &access_right_id, &search_strings, &creation_time, &last_modified_time, &announce_to, &apoc, &apoc_paths, &loc_requestor, &containers_reference, &groups_reference, &access_rights_reference, &subscriptions_reference, &notification_channels_reference, &end };

static const production expiration_time = { EXPIRATION_TIME, NON_RECURSIVE, P14 };

static const production *PA[15] = { &expiration_time, &access_right_id, &search_strings, &creation_time, &last_modified_time, &announce_to, &apoc, &apoc_paths, &loc_requestor, &containers_reference, &groups_reference, &access_rights_reference, &subscriptions_reference, &notification_channels_reference, &end };

static const production m2m_application = { APPLICATION, NON_RECURSIVE, PA };

static const production *PS[2] = { &m2m_application, &end };

static const production m2m_object = { START, NON_RECURSIVE, PS };

#endif /* M2M_OBJECT_H */

