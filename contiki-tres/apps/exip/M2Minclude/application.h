/** AUTO-GENERATED: Mon Mar  3 17:57:40 2014
 * Copyright (c) 2010 - 2011, Rumen Kyusakov, EISLAB, LTU
 * $Id$ */

#include "procTypes.h"

#define CONST

#define CONST_STRING_DEF const

#define CONST_PROD const

/** START_STRINGS_DEFINITONS */

CONST_STRING_DEF CharType prfx_LN_0_0[] = { 0x61, 0x70, 0x70, 0x49, 0x64 }; /* appId */
CONST_STRING_DEF CharType prfx_LN_0_1[] = { 0x69, 0x64 }; /* id */
CONST_STRING_DEF CharType prfx_LN_0_2[] = { 0x69, 0x66, 0x4d, 0x61, 0x74, 0x63, 0x68 }; /* ifMatch */
CONST_STRING_DEF CharType prfx_LN_0_3[] = { 0x69, 0x66, 0x4d, 0x6f, 0x64, 0x69, 0x66, 0x69, 0x65, 0x64, 0x53, 0x69, 0x6e, 0x63, 0x65 }; /* ifModifiedSince */
CONST_STRING_DEF CharType prfx_LN_0_4[] = { 0x69, 0x66, 0x4e, 0x6f, 0x6e, 0x65, 0x4d, 0x61, 0x74, 0x63, 0x68 }; /* ifNoneMatch */
CONST_STRING_DEF CharType prfx_LN_0_5[] = { 0x69, 0x66, 0x55, 0x6e, 0x6d, 0x6f, 0x64, 0x69, 0x66, 0x69, 0x65, 0x64, 0x53, 0x69, 0x6e, 0x63, 0x65 }; /* ifUnmodifiedSince */
CONST_STRING_DEF CharType prfx_LN_0_6[] = { 0x72, 0x65, 0x66, 0x65, 0x72, 0x65, 0x6e, 0x63, 0x65 }; /* reference */
CONST_STRING_DEF CharType prfx_LN_0_7[] = { 0x73, 0x63, 0x68, 0x65, 0x64, 0x75, 0x6c, 0x65, 0x53, 0x74, 0x72, 0x69, 0x6e, 0x67 }; /* scheduleString */
CONST_STRING_DEF CharType prfx_LN_0_8[] = { 0x74, 0x6f, 0x6c, 0x65, 0x72, 0x61, 0x62, 0x6c, 0x65, 0x44, 0x65, 0x6c, 0x61, 0x79 }; /* tolerableDelay */
CONST_STRING_DEF CharType prfx_LN_0_9[] = { 0x74, 0x6f, 0x6c, 0x65, 0x72, 0x61, 0x62, 0x6c, 0x65, 0x54, 0x69, 0x6d, 0x65 }; /* tolerableTime */
CONST_STRING_DEF CharType prfx_URI_1[] = { 0x68, 0x74, 0x74, 0x70, 0x3a, 0x2f, 0x2f, 0x77, 0x77, 0x77, 0x2e, 0x77, 0x33, 0x2e, 0x6f, 0x72, 0x67, 0x2f, 0x58, 0x4d, 0x4c, 0x2f, 0x31, 0x39, 0x39, 0x38, 0x2f, 0x6e, 0x61, 0x6d, 0x65, 0x73, 0x70, 0x61, 0x63, 0x65 }; /* http://www.w3.org/XML/1998/namespace */
CONST_STRING_DEF CharType prfx_PFX_1_0[] = { 0x78, 0x6d, 0x6c }; /* xml */
CONST_STRING_DEF CharType prfx_LN_1_0[] = { 0x62, 0x61, 0x73, 0x65 }; /* base */
CONST_STRING_DEF CharType prfx_LN_1_1[] = { 0x69, 0x64 }; /* id */
CONST_STRING_DEF CharType prfx_LN_1_2[] = { 0x6c, 0x61, 0x6e, 0x67 }; /* lang */
CONST_STRING_DEF CharType prfx_LN_1_3[] = { 0x73, 0x70, 0x61, 0x63, 0x65 }; /* space */
CONST_STRING_DEF CharType prfx_URI_2[] = { 0x68, 0x74, 0x74, 0x70, 0x3a, 0x2f, 0x2f, 0x77, 0x77, 0x77, 0x2e, 0x77, 0x33, 0x2e, 0x6f, 0x72, 0x67, 0x2f, 0x32, 0x30, 0x30, 0x31, 0x2f, 0x58, 0x4d, 0x4c, 0x53, 0x63, 0x68, 0x65, 0x6d, 0x61, 0x2d, 0x69, 0x6e, 0x73, 0x74, 0x61, 0x6e, 0x63, 0x65 }; /* http://www.w3.org/2001/XMLSchema-instance */
CONST_STRING_DEF CharType prfx_PFX_2_0[] = { 0x78, 0x73, 0x69 }; /* xsi */
CONST_STRING_DEF CharType prfx_LN_2_0[] = { 0x6e, 0x69, 0x6c }; /* nil */
CONST_STRING_DEF CharType prfx_LN_2_1[] = { 0x74, 0x79, 0x70, 0x65 }; /* type */
CONST_STRING_DEF CharType prfx_URI_3[] = { 0x68, 0x74, 0x74, 0x70, 0x3a, 0x2f, 0x2f, 0x77, 0x77, 0x77, 0x2e, 0x77, 0x33, 0x2e, 0x6f, 0x72, 0x67, 0x2f, 0x32, 0x30, 0x30, 0x31, 0x2f, 0x58, 0x4d, 0x4c, 0x53, 0x63, 0x68, 0x65, 0x6d, 0x61 }; /* http://www.w3.org/2001/XMLSchema */
CONST_STRING_DEF CharType prfx_LN_3_0[] = { 0x45, 0x4e, 0x54, 0x49, 0x54, 0x49, 0x45, 0x53 }; /* ENTITIES */
CONST_STRING_DEF CharType prfx_LN_3_1[] = { 0x45, 0x4e, 0x54, 0x49, 0x54, 0x59 }; /* ENTITY */
CONST_STRING_DEF CharType prfx_LN_3_2[] = { 0x49, 0x44 }; /* ID */
CONST_STRING_DEF CharType prfx_LN_3_3[] = { 0x49, 0x44, 0x52, 0x45, 0x46 }; /* IDREF */
CONST_STRING_DEF CharType prfx_LN_3_4[] = { 0x49, 0x44, 0x52, 0x45, 0x46, 0x53 }; /* IDREFS */
CONST_STRING_DEF CharType prfx_LN_3_5[] = { 0x4e, 0x43, 0x4e, 0x61, 0x6d, 0x65 }; /* NCName */
CONST_STRING_DEF CharType prfx_LN_3_6[] = { 0x4e, 0x4d, 0x54, 0x4f, 0x4b, 0x45, 0x4e }; /* NMTOKEN */
CONST_STRING_DEF CharType prfx_LN_3_7[] = { 0x4e, 0x4d, 0x54, 0x4f, 0x4b, 0x45, 0x4e, 0x53 }; /* NMTOKENS */
CONST_STRING_DEF CharType prfx_LN_3_8[] = { 0x4e, 0x4f, 0x54, 0x41, 0x54, 0x49, 0x4f, 0x4e }; /* NOTATION */
CONST_STRING_DEF CharType prfx_LN_3_9[] = { 0x4e, 0x61, 0x6d, 0x65 }; /* Name */
CONST_STRING_DEF CharType prfx_LN_3_10[] = { 0x51, 0x4e, 0x61, 0x6d, 0x65 }; /* QName */
CONST_STRING_DEF CharType prfx_LN_3_11[] = { 0x61, 0x6e, 0x79, 0x53, 0x69, 0x6d, 0x70, 0x6c, 0x65, 0x54, 0x79, 0x70, 0x65 }; /* anySimpleType */
CONST_STRING_DEF CharType prfx_LN_3_12[] = { 0x61, 0x6e, 0x79, 0x54, 0x79, 0x70, 0x65 }; /* anyType */
CONST_STRING_DEF CharType prfx_LN_3_13[] = { 0x61, 0x6e, 0x79, 0x55, 0x52, 0x49 }; /* anyURI */
CONST_STRING_DEF CharType prfx_LN_3_14[] = { 0x62, 0x61, 0x73, 0x65, 0x36, 0x34, 0x42, 0x69, 0x6e, 0x61, 0x72, 0x79 }; /* base64Binary */
CONST_STRING_DEF CharType prfx_LN_3_15[] = { 0x62, 0x6f, 0x6f, 0x6c, 0x65, 0x61, 0x6e }; /* boolean */
CONST_STRING_DEF CharType prfx_LN_3_16[] = { 0x62, 0x79, 0x74, 0x65 }; /* byte */
CONST_STRING_DEF CharType prfx_LN_3_17[] = { 0x64, 0x61, 0x74, 0x65 }; /* date */
CONST_STRING_DEF CharType prfx_LN_3_18[] = { 0x64, 0x61, 0x74, 0x65, 0x54, 0x69, 0x6d, 0x65 }; /* dateTime */
CONST_STRING_DEF CharType prfx_LN_3_19[] = { 0x64, 0x65, 0x63, 0x69, 0x6d, 0x61, 0x6c }; /* decimal */
CONST_STRING_DEF CharType prfx_LN_3_20[] = { 0x64, 0x6f, 0x75, 0x62, 0x6c, 0x65 }; /* double */
CONST_STRING_DEF CharType prfx_LN_3_21[] = { 0x64, 0x75, 0x72, 0x61, 0x74, 0x69, 0x6f, 0x6e }; /* duration */
CONST_STRING_DEF CharType prfx_LN_3_22[] = { 0x66, 0x6c, 0x6f, 0x61, 0x74 }; /* float */
CONST_STRING_DEF CharType prfx_LN_3_23[] = { 0x67, 0x44, 0x61, 0x79 }; /* gDay */
CONST_STRING_DEF CharType prfx_LN_3_24[] = { 0x67, 0x4d, 0x6f, 0x6e, 0x74, 0x68 }; /* gMonth */
CONST_STRING_DEF CharType prfx_LN_3_25[] = { 0x67, 0x4d, 0x6f, 0x6e, 0x74, 0x68, 0x44, 0x61, 0x79 }; /* gMonthDay */
CONST_STRING_DEF CharType prfx_LN_3_26[] = { 0x67, 0x59, 0x65, 0x61, 0x72 }; /* gYear */
CONST_STRING_DEF CharType prfx_LN_3_27[] = { 0x67, 0x59, 0x65, 0x61, 0x72, 0x4d, 0x6f, 0x6e, 0x74, 0x68 }; /* gYearMonth */
CONST_STRING_DEF CharType prfx_LN_3_28[] = { 0x68, 0x65, 0x78, 0x42, 0x69, 0x6e, 0x61, 0x72, 0x79 }; /* hexBinary */
CONST_STRING_DEF CharType prfx_LN_3_29[] = { 0x69, 0x6e, 0x74 }; /* int */
CONST_STRING_DEF CharType prfx_LN_3_30[] = { 0x69, 0x6e, 0x74, 0x65, 0x67, 0x65, 0x72 }; /* integer */
CONST_STRING_DEF CharType prfx_LN_3_31[] = { 0x6c, 0x61, 0x6e, 0x67, 0x75, 0x61, 0x67, 0x65 }; /* language */
CONST_STRING_DEF CharType prfx_LN_3_32[] = { 0x6c, 0x6f, 0x6e, 0x67 }; /* long */
CONST_STRING_DEF CharType prfx_LN_3_33[] = { 0x6e, 0x65, 0x67, 0x61, 0x74, 0x69, 0x76, 0x65, 0x49, 0x6e, 0x74, 0x65, 0x67, 0x65, 0x72 }; /* negativeInteger */
CONST_STRING_DEF CharType prfx_LN_3_34[] = { 0x6e, 0x6f, 0x6e, 0x4e, 0x65, 0x67, 0x61, 0x74, 0x69, 0x76, 0x65, 0x49, 0x6e, 0x74, 0x65, 0x67, 0x65, 0x72 }; /* nonNegativeInteger */
CONST_STRING_DEF CharType prfx_LN_3_35[] = { 0x6e, 0x6f, 0x6e, 0x50, 0x6f, 0x73, 0x69, 0x74, 0x69, 0x76, 0x65, 0x49, 0x6e, 0x74, 0x65, 0x67, 0x65, 0x72 }; /* nonPositiveInteger */
CONST_STRING_DEF CharType prfx_LN_3_36[] = { 0x6e, 0x6f, 0x72, 0x6d, 0x61, 0x6c, 0x69, 0x7a, 0x65, 0x64, 0x53, 0x74, 0x72, 0x69, 0x6e, 0x67 }; /* normalizedString */
CONST_STRING_DEF CharType prfx_LN_3_37[] = { 0x70, 0x6f, 0x73, 0x69, 0x74, 0x69, 0x76, 0x65, 0x49, 0x6e, 0x74, 0x65, 0x67, 0x65, 0x72 }; /* positiveInteger */
CONST_STRING_DEF CharType prfx_LN_3_38[] = { 0x73, 0x68, 0x6f, 0x72, 0x74 }; /* short */
CONST_STRING_DEF CharType prfx_LN_3_39[] = { 0x73, 0x74, 0x72, 0x69, 0x6e, 0x67 }; /* string */
CONST_STRING_DEF CharType prfx_LN_3_40[] = { 0x74, 0x69, 0x6d, 0x65 }; /* time */
CONST_STRING_DEF CharType prfx_LN_3_41[] = { 0x74, 0x6f, 0x6b, 0x65, 0x6e }; /* token */
CONST_STRING_DEF CharType prfx_LN_3_42[] = { 0x75, 0x6e, 0x73, 0x69, 0x67, 0x6e, 0x65, 0x64, 0x42, 0x79, 0x74, 0x65 }; /* unsignedByte */
CONST_STRING_DEF CharType prfx_LN_3_43[] = { 0x75, 0x6e, 0x73, 0x69, 0x67, 0x6e, 0x65, 0x64, 0x49, 0x6e, 0x74 }; /* unsignedInt */
CONST_STRING_DEF CharType prfx_LN_3_44[] = { 0x75, 0x6e, 0x73, 0x69, 0x67, 0x6e, 0x65, 0x64, 0x4c, 0x6f, 0x6e, 0x67 }; /* unsignedLong */
CONST_STRING_DEF CharType prfx_LN_3_45[] = { 0x75, 0x6e, 0x73, 0x69, 0x67, 0x6e, 0x65, 0x64, 0x53, 0x68, 0x6f, 0x72, 0x74 }; /* unsignedShort */
CONST_STRING_DEF CharType prfx_URI_4[] = { 0x68, 0x74, 0x74, 0x70, 0x3a, 0x2f, 0x2f, 0x75, 0x72, 0x69, 0x2e, 0x65, 0x74, 0x73, 0x69, 0x2e, 0x6f, 0x72, 0x67, 0x2f, 0x6d, 0x32, 0x6d }; /* http://uri.etsi.org/m2m */
CONST_STRING_DEF CharType prfx_LN_4_0[] = { 0x41, 0x50, 0x6f, 0x43, 0x50, 0x61, 0x74, 0x68 }; /* APoCPath */
CONST_STRING_DEF CharType prfx_LN_4_1[] = { 0x41, 0x50, 0x6f, 0x43, 0x50, 0x61, 0x74, 0x68, 0x73 }; /* APoCPaths */
CONST_STRING_DEF CharType prfx_LN_4_2[] = { 0x41, 0x50, 0x6f, 0x63, 0x48, 0x61, 0x6e, 0x64, 0x6c, 0x69, 0x6e, 0x67 }; /* APocHandling */
CONST_STRING_DEF CharType prfx_LN_4_3[] = { 0x41, 0x6e, 0x6e, 0x6f, 0x75, 0x6e, 0x63, 0x65, 0x54, 0x6f }; /* AnnounceTo */
CONST_STRING_DEF CharType prfx_LN_4_4[] = { 0x41, 0x6e, 0x79, 0x55, 0x52, 0x49, 0x4c, 0x69, 0x73, 0x74 }; /* AnyURIList */
CONST_STRING_DEF CharType prfx_LN_4_5[] = { 0x41, 0x70, 0x70, 0x6c, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e }; /* Application */
CONST_STRING_DEF CharType prfx_LN_4_6[] = { 0x41, 0x70, 0x70, 0x6c, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73 }; /* ApplicationStatus */
CONST_STRING_DEF CharType prfx_LN_4_7[] = { 0x46, 0x69, 0x6c, 0x74, 0x65, 0x72, 0x43, 0x72, 0x69, 0x74, 0x65, 0x72, 0x69, 0x61, 0x54, 0x79, 0x70, 0x65 }; /* FilterCriteriaType */
CONST_STRING_DEF CharType prfx_LN_4_8[] = { 0x4e, 0x61, 0x6d, 0x65, 0x64, 0x52, 0x65, 0x66, 0x65, 0x72, 0x65, 0x6e, 0x63, 0x65, 0x43, 0x6f, 0x6c, 0x6c, 0x65, 0x63, 0x74, 0x69, 0x6f, 0x6e }; /* NamedReferenceCollection */
CONST_STRING_DEF CharType prfx_LN_4_9[] = { 0x4f, 0x6e, 0x6c, 0x69, 0x6e, 0x65, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73 }; /* OnlineStatus */
CONST_STRING_DEF CharType prfx_LN_4_10[] = { 0x52, 0x63, 0x61, 0x74, 0x54, 0x79, 0x70, 0x65 }; /* RcatType */
CONST_STRING_DEF CharType prfx_LN_4_11[] = { 0x52, 0x65, 0x66, 0x65, 0x72, 0x65, 0x6e, 0x63, 0x65, 0x54, 0x6f, 0x4e, 0x61, 0x6d, 0x65, 0x64, 0x52, 0x65, 0x73, 0x6f, 0x75, 0x72, 0x63, 0x65 }; /* ReferenceToNamedResource */
CONST_STRING_DEF CharType prfx_LN_4_12[] = { 0x53, 0x63, 0x68, 0x65, 0x64, 0x75, 0x6c, 0x65 }; /* Schedule */
CONST_STRING_DEF CharType prfx_LN_4_13[] = { 0x53, 0x65, 0x61, 0x72, 0x63, 0x68, 0x53, 0x74, 0x72, 0x69, 0x6e, 0x67, 0x73 }; /* SearchStrings */
CONST_STRING_DEF CharType prfx_LN_4_14[] = { 0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x43, 0x6f, 0x64, 0x65 }; /* StatusCode */
CONST_STRING_DEF CharType prfx_LN_4_15[] = { 0x54, 0x72, 0x70, 0x64, 0x74, 0x54, 0x79, 0x70, 0x65 }; /* TrpdtType */
CONST_STRING_DEF CharType prfx_LN_4_16[] = { 0x61, 0x50, 0x6f, 0x43 }; /* aPoC */
CONST_STRING_DEF CharType prfx_LN_4_17[] = { 0x61, 0x50, 0x6f, 0x43, 0x50, 0x61, 0x74, 0x68 }; /* aPoCPath */
CONST_STRING_DEF CharType prfx_LN_4_18[] = { 0x61, 0x50, 0x6f, 0x43, 0x50, 0x61, 0x74, 0x68, 0x73 }; /* aPoCPaths */
CONST_STRING_DEF CharType prfx_LN_4_19[] = { 0x61, 0x50, 0x6f, 0x63, 0x48, 0x61, 0x6e, 0x64, 0x6c, 0x69, 0x6e, 0x67 }; /* aPocHandling */
CONST_STRING_DEF CharType prfx_LN_4_20[] = { 0x61, 0x63, 0x63, 0x65, 0x73, 0x73, 0x52, 0x69, 0x67, 0x68, 0x74, 0x49, 0x44 }; /* accessRightID */
CONST_STRING_DEF CharType prfx_LN_4_21[] = { 0x61, 0x63, 0x63, 0x65, 0x73, 0x73, 0x52, 0x69, 0x67, 0x68, 0x74, 0x73, 0x52, 0x65, 0x66, 0x65, 0x72, 0x65, 0x6e, 0x63, 0x65 }; /* accessRightsReference */
CONST_STRING_DEF CharType prfx_LN_4_22[] = { 0x61, 0x63, 0x74, 0x69, 0x76, 0x61, 0x74, 0x65, 0x64 }; /* activated */
CONST_STRING_DEF CharType prfx_LN_4_23[] = { 0x61, 0x6e, 0x6e, 0x6f, 0x75, 0x6e, 0x63, 0x65, 0x54, 0x6f }; /* announceTo */
CONST_STRING_DEF CharType prfx_LN_4_24[] = { 0x61, 0x70, 0x70, 0x49, 0x64 }; /* appId */
CONST_STRING_DEF CharType prfx_LN_4_25[] = { 0x61, 0x70, 0x70, 0x6c, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e }; /* application */
CONST_STRING_DEF CharType prfx_LN_4_26[] = { 0x61, 0x70, 0x70, 0x6c, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73 }; /* applicationStatus */
CONST_STRING_DEF CharType prfx_LN_4_27[] = { 0x61, 0x70, 0x70, 0x6c, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x52, 0x65, 0x66, 0x65, 0x72, 0x65, 0x6e, 0x63, 0x65 }; /* applicationsReference */
CONST_STRING_DEF CharType prfx_LN_4_28[] = { 0x63, 0x6f, 0x6e, 0x74, 0x61, 0x63, 0x74, 0x55, 0x52, 0x49 }; /* contactURI */
CONST_STRING_DEF CharType prfx_LN_4_29[] = { 0x63, 0x6f, 0x6e, 0x74, 0x61, 0x69, 0x6e, 0x65, 0x72, 0x73, 0x52, 0x65, 0x66, 0x65, 0x72, 0x65, 0x6e, 0x63, 0x65 }; /* containersReference */
CONST_STRING_DEF CharType prfx_LN_4_30[] = { 0x63, 0x6f, 0x6e, 0x74, 0x65, 0x6e, 0x74, 0x49, 0x6e, 0x73, 0x74, 0x61, 0x6e, 0x63, 0x65, 0x73, 0x52, 0x65, 0x66, 0x65, 0x72, 0x65, 0x6e, 0x63, 0x65 }; /* contentInstancesReference */
CONST_STRING_DEF CharType prfx_LN_4_31[] = { 0x63, 0x6f, 0x6e, 0x74, 0x65, 0x6e, 0x74, 0x54, 0x79, 0x70, 0x65 }; /* contentType */
CONST_STRING_DEF CharType prfx_LN_4_32[] = { 0x63, 0x72, 0x65, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x54, 0x69, 0x6d, 0x65 }; /* creationTime */
CONST_STRING_DEF CharType prfx_LN_4_33[] = { 0x64, 0x65, 0x6c, 0x61, 0x79, 0x54, 0x6f, 0x6c, 0x65, 0x72, 0x61, 0x6e, 0x63, 0x65 }; /* delayTolerance */
CONST_STRING_DEF CharType prfx_LN_4_34[] = { 0x65, 0x78, 0x70, 0x69, 0x72, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x54, 0x69, 0x6d, 0x65 }; /* expirationTime */
CONST_STRING_DEF CharType prfx_LN_4_35[] = { 0x66, 0x69, 0x6c, 0x74, 0x65, 0x72, 0x43, 0x72, 0x69, 0x74, 0x65, 0x72, 0x69, 0x61 }; /* filterCriteria */
CONST_STRING_DEF CharType prfx_LN_4_36[] = { 0x67, 0x6c, 0x6f, 0x62, 0x61, 0x6c }; /* global */
CONST_STRING_DEF CharType prfx_LN_4_37[] = { 0x67, 0x72, 0x6f, 0x75, 0x70, 0x73, 0x52, 0x65, 0x66, 0x65, 0x72, 0x65, 0x6e, 0x63, 0x65 }; /* groupsReference */
CONST_STRING_DEF CharType prfx_LN_4_38[] = { 0x69, 0x64 }; /* id */
CONST_STRING_DEF CharType prfx_LN_4_39[] = { 0x6c, 0x61, 0x73, 0x74, 0x4d, 0x6f, 0x64, 0x69, 0x66, 0x69, 0x65, 0x64, 0x54, 0x69, 0x6d, 0x65 }; /* lastModifiedTime */
CONST_STRING_DEF CharType prfx_LN_4_40[] = { 0x6c, 0x69, 0x6e, 0x6b }; /* link */
CONST_STRING_DEF CharType prfx_LN_4_41[] = { 0x6c, 0x6f, 0x63, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x6f, 0x72 }; /* locRequestor */
CONST_STRING_DEF CharType prfx_LN_4_42[] = { 0x6d, 0x67, 0x6d, 0x74, 0x4f, 0x62, 0x6a, 0x73, 0x52, 0x65, 0x66, 0x65, 0x72, 0x65, 0x6e, 0x63, 0x65 }; /* mgmtObjsReference */
CONST_STRING_DEF CharType prfx_LN_4_43[] = { 0x6e, 0x61, 0x6d, 0x65 }; /* name */
CONST_STRING_DEF CharType prfx_LN_4_44[] = { 0x6e, 0x61, 0x6d, 0x65, 0x64, 0x52, 0x65, 0x66, 0x65, 0x72, 0x65, 0x6e, 0x63, 0x65 }; /* namedReference */
CONST_STRING_DEF CharType prfx_LN_4_45[] = { 0x6e, 0x6f, 0x74, 0x69, 0x66, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x43, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x73, 0x52, 0x65, 0x66, 0x65, 0x72, 0x65, 0x6e, 0x63, 0x65 }; /* notificationChannelsReference */
CONST_STRING_DEF CharType prfx_LN_4_46[] = { 0x6f, 0x6e, 0x6c, 0x69, 0x6e, 0x65, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73 }; /* onlineStatus */
CONST_STRING_DEF CharType prfx_LN_4_47[] = { 0x70, 0x61, 0x74, 0x68 }; /* path */
CONST_STRING_DEF CharType prfx_LN_4_48[] = { 0x73, 0x63, 0x6c, 0x49, 0x64 }; /* sclId */
CONST_STRING_DEF CharType prfx_LN_4_49[] = { 0x73, 0x63, 0x6c, 0x4c, 0x69, 0x73, 0x74 }; /* sclList */
CONST_STRING_DEF CharType prfx_LN_4_50[] = { 0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x53, 0x74, 0x72, 0x69, 0x6e, 0x67 }; /* searchString */
CONST_STRING_DEF CharType prfx_LN_4_51[] = { 0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x53, 0x74, 0x72, 0x69, 0x6e, 0x67, 0x73 }; /* searchStrings */
CONST_STRING_DEF CharType prfx_LN_4_52[] = { 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x43, 0x6f, 0x64, 0x65 }; /* statusCode */
CONST_STRING_DEF CharType prfx_LN_4_53[] = { 0x73, 0x75, 0x62, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x52, 0x65, 0x66, 0x65, 0x72, 0x65, 0x6e, 0x63, 0x65 }; /* subscriptionsReference */

/** END_STRINGS_DEFINITONS */

static CONST_PROD Production prfx_prod_0_0[1] =
{
  {
    838860801, 0,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_0_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_0[2] =
{
  { prfx_prod_0_0, 1, 0 },
  { prfx_prod_0_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_1_0[1] =
{
  {
    838860801, 1,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_1_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_1[2] =
{
  { prfx_prod_1_0, 1, 0 },
  { prfx_prod_1_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_2_0[1] =
{
  {
    838860801, 2,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_2_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_2[2] =
{
  { prfx_prod_2_0, 1, 0 },
  { prfx_prod_2_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_3_0[1] =
{
  {
    838860801, 3,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_3_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_3[2] =
{
  { prfx_prod_3_0, 1, 0 },
  { prfx_prod_3_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_4_0[1] =
{
  {
    838860801, 4,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_4_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_4[2] =
{
  { prfx_prod_4_0, 1, 0 },
  { prfx_prod_4_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_5_0[1] =
{
  {
    838860801, 5,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_5_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_5[2] =
{
  { prfx_prod_5_0, 1, 0 },
  { prfx_prod_5_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_6_0[1] =
{
  {
    838860801, 6,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_6_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_6[2] =
{
  { prfx_prod_6_0, 1, 0 },
  { prfx_prod_6_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_7_0[1] =
{
  {
    838860801, 7,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_7_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_7[2] =
{
  { prfx_prod_7_0, 1, 0 },
  { prfx_prod_7_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_8_0[1] =
{
  {
    838860801, 8,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_8_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_8[2] =
{
  { prfx_prod_8_0, 1, 0 },
  { prfx_prod_8_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_9_0[1] =
{
  {
    838860801, 9,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_9_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_9[2] =
{
  { prfx_prod_9_0, 1, 0 },
  { prfx_prod_9_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_10_0[1] =
{
  {
    838860801, 10,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_10_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_10[2] =
{
  { prfx_prod_10_0, 1, 0 },
  { prfx_prod_10_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_11_0[1] =
{
  {
    838860801, 11,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_11_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_11[2] =
{
  { prfx_prod_11_0, 1, 0 },
  { prfx_prod_11_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_12_0[4] =
{
  {
    838860801, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    536870913, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    369098752, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_12_1[3] =
{
  {
    838860801, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    536870913, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_12[2] =
{
  { prfx_prod_12_0, 4, 1 },
  { prfx_prod_12_1, 3, 1 }
};

static CONST_PROD Production prfx_prod_13_0[1] =
{
  {
    838860801, 13,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_13_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_13[2] =
{
  { prfx_prod_13_0, 1, 0 },
  { prfx_prod_13_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_14_0[1] =
{
  {
    838860801, 14,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_14_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_14[2] =
{
  { prfx_prod_14_0, 1, 0 },
  { prfx_prod_14_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_15_0[1] =
{
  {
    838860801, 15,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_15_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_15[2] =
{
  { prfx_prod_15_0, 1, 0 },
  { prfx_prod_15_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_16_0[1] =
{
  {
    838860801, 16,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_16_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_16[2] =
{
  { prfx_prod_16_0, 1, 0 },
  { prfx_prod_16_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_17_0[1] =
{
  {
    838860801, 17,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_17_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_17[2] =
{
  { prfx_prod_17_0, 1, 0 },
  { prfx_prod_17_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_18_0[1] =
{
  {
    838860801, 18,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_18_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_18[2] =
{
  { prfx_prod_18_0, 1, 0 },
  { prfx_prod_18_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_19_0[1] =
{
  {
    838860801, 19,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_19_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_19[2] =
{
  { prfx_prod_19_0, 1, 0 },
  { prfx_prod_19_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_20_0[1] =
{
  {
    838860801, 20,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_20_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_20[2] =
{
  { prfx_prod_20_0, 1, 0 },
  { prfx_prod_20_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_21_0[1] =
{
  {
    838860801, 21,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_21_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_21[2] =
{
  { prfx_prod_21_0, 1, 0 },
  { prfx_prod_21_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_22_0[1] =
{
  {
    838860801, 22,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_22_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_22[2] =
{
  { prfx_prod_22_0, 1, 0 },
  { prfx_prod_22_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_23_0[1] =
{
  {
    838860801, 23,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_23_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_23[2] =
{
  { prfx_prod_23_0, 1, 0 },
  { prfx_prod_23_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_24_0[1] =
{
  {
    838860801, 24,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_24_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_24[2] =
{
  { prfx_prod_24_0, 1, 0 },
  { prfx_prod_24_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_25_0[1] =
{
  {
    838860801, 25,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_25_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_25[2] =
{
  { prfx_prod_25_0, 1, 0 },
  { prfx_prod_25_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_26_0[1] =
{
  {
    838860801, 26,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_26_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_26[2] =
{
  { prfx_prod_26_0, 1, 0 },
  { prfx_prod_26_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_27_0[1] =
{
  {
    838860801, 27,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_27_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_27[2] =
{
  { prfx_prod_27_0, 1, 0 },
  { prfx_prod_27_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_28_0[1] =
{
  {
    838860801, 28,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_28_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_28[2] =
{
  { prfx_prod_28_0, 1, 0 },
  { prfx_prod_28_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_29_0[1] =
{
  {
    838860801, 29,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_29_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_29[2] =
{
  { prfx_prod_29_0, 1, 0 },
  { prfx_prod_29_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_30_0[1] =
{
  {
    838860801, 30,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_30_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_30[2] =
{
  { prfx_prod_30_0, 1, 0 },
  { prfx_prod_30_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_31_0[1] =
{
  {
    838860801, 31,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_31_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_31[2] =
{
  { prfx_prod_31_0, 1, 0 },
  { prfx_prod_31_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_32_0[1] =
{
  {
    838860801, 32,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_32_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_32[2] =
{
  { prfx_prod_32_0, 1, 0 },
  { prfx_prod_32_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_33_0[1] =
{
  {
    838860801, 33,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_33_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_33[2] =
{
  { prfx_prod_33_0, 1, 0 },
  { prfx_prod_33_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_34_0[1] =
{
  {
    838860801, 34,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_34_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_34[2] =
{
  { prfx_prod_34_0, 1, 0 },
  { prfx_prod_34_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_35_0[1] =
{
  {
    838860801, 35,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_35_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_35[2] =
{
  { prfx_prod_35_0, 1, 0 },
  { prfx_prod_35_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_36_0[1] =
{
  {
    838860801, 36,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_36_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_36[2] =
{
  { prfx_prod_36_0, 1, 0 },
  { prfx_prod_36_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_37_0[1] =
{
  {
    838860801, 37,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_37_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_37[2] =
{
  { prfx_prod_37_0, 1, 0 },
  { prfx_prod_37_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_38_0[1] =
{
  {
    838860801, 38,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_38_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_38[2] =
{
  { prfx_prod_38_0, 1, 0 },
  { prfx_prod_38_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_39_0[1] =
{
  {
    838860801, 39,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_39_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_39[2] =
{
  { prfx_prod_39_0, 1, 0 },
  { prfx_prod_39_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_40_0[1] =
{
  {
    838860801, 40,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_40_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_40[2] =
{
  { prfx_prod_40_0, 1, 0 },
  { prfx_prod_40_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_41_0[1] =
{
  {
    838860801, 41,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_41_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_41[2] =
{
  { prfx_prod_41_0, 1, 0 },
  { prfx_prod_41_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_42_0[1] =
{
  {
    838860801, 42,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_42_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_42[2] =
{
  { prfx_prod_42_0, 1, 0 },
  { prfx_prod_42_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_43_0[1] =
{
  {
    838860801, 43,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_43_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_43[2] =
{
  { prfx_prod_43_0, 1, 0 },
  { prfx_prod_43_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_44_0[1] =
{
  {
    838860801, 44,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_44_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_44[2] =
{
  { prfx_prod_44_0, 1, 0 },
  { prfx_prod_44_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_45_0[1] =
{
  {
    838860801, 45,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_45_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_45[2] =
{
  { prfx_prod_45_0, 1, 0 },
  { prfx_prod_45_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_46_0[2] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316481, 39,
    { 4, 50 }
  }
};

static CONST_PROD Production prfx_prod_46_1[2] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316481, 39,
    { 4, 50 }
  }
};

static CONST_PROD GrammarRule prfx_rule_46[2] =
{
  { prfx_prod_46_0, 2, 1 },
  { prfx_prod_46_1, 2, 1 }
};

static CONST_PROD Production prfx_prod_47_0[2] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316481, 13,
    { 0, 6 }
  }
};

static CONST_PROD Production prfx_prod_47_1[2] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316481, 13,
    { 0, 6 }
  }
};

static CONST_PROD GrammarRule prfx_rule_47[2] =
{
  { prfx_prod_47_0, 2, 1 },
  { prfx_prod_47_1, 2, 1 }
};

static CONST_PROD Production prfx_prod_48_0[2] =
{
  {
    503316482, 47,
    { 4, 49 }
  },
  {
    503316481, 15,
    { 4, 22 }
  }
};

static CONST_PROD Production prfx_prod_48_1[1] =
{
  {
    503316482, 47,
    { 4, 49 }
  }
};

static CONST_PROD Production prfx_prod_48_2[2] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316483, 15,
    { 4, 36 }
  }
};

static CONST_PROD Production prfx_prod_48_3[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_48[4] =
{
  { prfx_prod_48_0, 2, 0 },
  { prfx_prod_48_1, 1, 0 },
  { prfx_prod_48_2, 2, 1 },
  { prfx_prod_48_3, 1, 1 }
};

static CONST_PROD Production prfx_prod_49_0[1] =
{
  {
    503316481, 13,
    { 4, 47 }
  }
};

static CONST_PROD Production prfx_prod_49_1[3] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316483, 46,
    { 4, 51 }
  },
  {
    503316482, 13,
    { 4, 20 }
  }
};

static CONST_PROD Production prfx_prod_49_2[2] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316483, 46,
    { 4, 51 }
  }
};

static CONST_PROD Production prfx_prod_49_3[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_49[4] =
{
  { prfx_prod_49_0, 1, 0 },
  { prfx_prod_49_1, 3, 1 },
  { prfx_prod_49_2, 2, 1 },
  { prfx_prod_49_3, 1, 1 }
};

static CONST_PROD Production prfx_prod_50_0[2] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316481, 49,
    { 4, 17 }
  }
};

static CONST_PROD Production prfx_prod_50_1[2] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316481, 49,
    { 4, 17 }
  }
};

static CONST_PROD GrammarRule prfx_rule_50[2] =
{
  { prfx_prod_50_0, 2, 1 },
  { prfx_prod_50_1, 2, 1 }
};

static CONST_PROD Production prfx_prod_51_0[16] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316495, 13,
    { 4, 45 }
  },
  {
    503316494, 13,
    { 4, 53 }
  },
  {
    503316493, 13,
    { 4, 21 }
  },
  {
    503316492, 13,
    { 4, 37 }
  },
  {
    503316491, 13,
    { 4, 29 }
  },
  {
    503316490, 12,
    { 4, 41 }
  },
  {
    503316489, 50,
    { 4, 18 }
  },
  {
    503316488, 13,
    { 4, 16 }
  },
  {
    503316487, 48,
    { 4, 23 }
  },
  {
    503316486, 18,
    { 4, 39 }
  },
  {
    503316485, 18,
    { 4, 32 }
  },
  {
    503316484, 46,
    { 4, 51 }
  },
  {
    503316483, 13,
    { 4, 20 }
  },
  {
    503316482, 18,
    { 4, 34 }
  },
  {
    335544321, 13,
    { 0, 0 }
  }
};

static CONST_PROD Production prfx_prod_51_1[15] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316495, 13,
    { 4, 45 }
  },
  {
    503316494, 13,
    { 4, 53 }
  },
  {
    503316493, 13,
    { 4, 21 }
  },
  {
    503316492, 13,
    { 4, 37 }
  },
  {
    503316491, 13,
    { 4, 29 }
  },
  {
    503316490, 12,
    { 4, 41 }
  },
  {
    503316489, 50,
    { 4, 18 }
  },
  {
    503316488, 13,
    { 4, 16 }
  },
  {
    503316487, 48,
    { 4, 23 }
  },
  {
    503316486, 18,
    { 4, 39 }
  },
  {
    503316485, 18,
    { 4, 32 }
  },
  {
    503316484, 46,
    { 4, 51 }
  },
  {
    503316483, 13,
    { 4, 20 }
  },
  {
    503316482, 18,
    { 4, 34 }
  }
};

static CONST_PROD Production prfx_prod_51_2[14] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316495, 13,
    { 4, 45 }
  },
  {
    503316494, 13,
    { 4, 53 }
  },
  {
    503316493, 13,
    { 4, 21 }
  },
  {
    503316492, 13,
    { 4, 37 }
  },
  {
    503316491, 13,
    { 4, 29 }
  },
  {
    503316490, 12,
    { 4, 41 }
  },
  {
    503316489, 50,
    { 4, 18 }
  },
  {
    503316488, 13,
    { 4, 16 }
  },
  {
    503316487, 48,
    { 4, 23 }
  },
  {
    503316486, 18,
    { 4, 39 }
  },
  {
    503316485, 18,
    { 4, 32 }
  },
  {
    503316484, 46,
    { 4, 51 }
  },
  {
    503316483, 13,
    { 4, 20 }
  }
};

static CONST_PROD Production prfx_prod_51_3[13] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316495, 13,
    { 4, 45 }
  },
  {
    503316494, 13,
    { 4, 53 }
  },
  {
    503316493, 13,
    { 4, 21 }
  },
  {
    503316492, 13,
    { 4, 37 }
  },
  {
    503316491, 13,
    { 4, 29 }
  },
  {
    503316490, 12,
    { 4, 41 }
  },
  {
    503316489, 50,
    { 4, 18 }
  },
  {
    503316488, 13,
    { 4, 16 }
  },
  {
    503316487, 48,
    { 4, 23 }
  },
  {
    503316486, 18,
    { 4, 39 }
  },
  {
    503316485, 18,
    { 4, 32 }
  },
  {
    503316484, 46,
    { 4, 51 }
  }
};

static CONST_PROD Production prfx_prod_51_4[12] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316495, 13,
    { 4, 45 }
  },
  {
    503316494, 13,
    { 4, 53 }
  },
  {
    503316493, 13,
    { 4, 21 }
  },
  {
    503316492, 13,
    { 4, 37 }
  },
  {
    503316491, 13,
    { 4, 29 }
  },
  {
    503316490, 12,
    { 4, 41 }
  },
  {
    503316489, 50,
    { 4, 18 }
  },
  {
    503316488, 13,
    { 4, 16 }
  },
  {
    503316487, 48,
    { 4, 23 }
  },
  {
    503316486, 18,
    { 4, 39 }
  },
  {
    503316485, 18,
    { 4, 32 }
  }
};

static CONST_PROD Production prfx_prod_51_5[11] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316495, 13,
    { 4, 45 }
  },
  {
    503316494, 13,
    { 4, 53 }
  },
  {
    503316493, 13,
    { 4, 21 }
  },
  {
    503316492, 13,
    { 4, 37 }
  },
  {
    503316491, 13,
    { 4, 29 }
  },
  {
    503316490, 12,
    { 4, 41 }
  },
  {
    503316489, 50,
    { 4, 18 }
  },
  {
    503316488, 13,
    { 4, 16 }
  },
  {
    503316487, 48,
    { 4, 23 }
  },
  {
    503316486, 18,
    { 4, 39 }
  }
};

static CONST_PROD Production prfx_prod_51_6[10] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316495, 13,
    { 4, 45 }
  },
  {
    503316494, 13,
    { 4, 53 }
  },
  {
    503316493, 13,
    { 4, 21 }
  },
  {
    503316492, 13,
    { 4, 37 }
  },
  {
    503316491, 13,
    { 4, 29 }
  },
  {
    503316490, 12,
    { 4, 41 }
  },
  {
    503316489, 50,
    { 4, 18 }
  },
  {
    503316488, 13,
    { 4, 16 }
  },
  {
    503316487, 48,
    { 4, 23 }
  }
};

static CONST_PROD Production prfx_prod_51_7[9] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316495, 13,
    { 4, 45 }
  },
  {
    503316494, 13,
    { 4, 53 }
  },
  {
    503316493, 13,
    { 4, 21 }
  },
  {
    503316492, 13,
    { 4, 37 }
  },
  {
    503316491, 13,
    { 4, 29 }
  },
  {
    503316490, 12,
    { 4, 41 }
  },
  {
    503316489, 50,
    { 4, 18 }
  },
  {
    503316488, 13,
    { 4, 16 }
  }
};

static CONST_PROD Production prfx_prod_51_8[8] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316495, 13,
    { 4, 45 }
  },
  {
    503316494, 13,
    { 4, 53 }
  },
  {
    503316493, 13,
    { 4, 21 }
  },
  {
    503316492, 13,
    { 4, 37 }
  },
  {
    503316491, 13,
    { 4, 29 }
  },
  {
    503316490, 12,
    { 4, 41 }
  },
  {
    503316489, 50,
    { 4, 18 }
  }
};

static CONST_PROD Production prfx_prod_51_9[7] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316495, 13,
    { 4, 45 }
  },
  {
    503316494, 13,
    { 4, 53 }
  },
  {
    503316493, 13,
    { 4, 21 }
  },
  {
    503316492, 13,
    { 4, 37 }
  },
  {
    503316491, 13,
    { 4, 29 }
  },
  {
    503316490, 12,
    { 4, 41 }
  }
};

static CONST_PROD Production prfx_prod_51_10[6] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316495, 13,
    { 4, 45 }
  },
  {
    503316494, 13,
    { 4, 53 }
  },
  {
    503316493, 13,
    { 4, 21 }
  },
  {
    503316492, 13,
    { 4, 37 }
  },
  {
    503316491, 13,
    { 4, 29 }
  }
};

static CONST_PROD Production prfx_prod_51_11[5] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316495, 13,
    { 4, 45 }
  },
  {
    503316494, 13,
    { 4, 53 }
  },
  {
    503316493, 13,
    { 4, 21 }
  },
  {
    503316492, 13,
    { 4, 37 }
  }
};

static CONST_PROD Production prfx_prod_51_12[4] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316495, 13,
    { 4, 45 }
  },
  {
    503316494, 13,
    { 4, 53 }
  },
  {
    503316493, 13,
    { 4, 21 }
  }
};

static CONST_PROD Production prfx_prod_51_13[3] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316495, 13,
    { 4, 45 }
  },
  {
    503316494, 13,
    { 4, 53 }
  }
};

static CONST_PROD Production prfx_prod_51_14[2] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316495, 13,
    { 4, 45 }
  }
};

static CONST_PROD Production prfx_prod_51_15[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_51[16] =
{
  { prfx_prod_51_0, 16, 3 },
  { prfx_prod_51_1, 15, 1 },
  { prfx_prod_51_2, 14, 1 },
  { prfx_prod_51_3, 13, 1 },
  { prfx_prod_51_4, 12, 1 },
  { prfx_prod_51_5, 11, 1 },
  { prfx_prod_51_6, 10, 1 },
  { prfx_prod_51_7, 9, 1 },
  { prfx_prod_51_8, 8, 1 },
  { prfx_prod_51_9, 7, 1 },
  { prfx_prod_51_10, 6, 1 },
  { prfx_prod_51_11, 5, 1 },
  { prfx_prod_51_12, 4, 1 },
  { prfx_prod_51_13, 3, 1 },
  { prfx_prod_51_14, 2, 1 },
  { prfx_prod_51_15, 1, 1 }
};

static CONST_PROD Production prfx_prod_52_0[1] =
{
  {
    838860801, 46,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_52_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_52[2] =
{
  { prfx_prod_52_0, 1, 0 },
  { prfx_prod_52_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_53_0[5] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316484, 39,
    { 0, 4 }
  },
  {
    503316483, 39,
    { 0, 2 }
  },
  {
    503316482, 18,
    { 0, 5 }
  },
  {
    503316481, 18,
    { 0, 3 }
  }
};

static CONST_PROD Production prfx_prod_53_1[4] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316484, 39,
    { 0, 4 }
  },
  {
    503316483, 39,
    { 0, 2 }
  },
  {
    503316482, 18,
    { 0, 5 }
  }
};

static CONST_PROD Production prfx_prod_53_2[3] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316484, 39,
    { 0, 4 }
  },
  {
    503316483, 39,
    { 0, 2 }
  }
};

static CONST_PROD Production prfx_prod_53_3[3] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316484, 39,
    { 0, 4 }
  },
  {
    503316483, 39,
    { 0, 2 }
  }
};

static CONST_PROD Production prfx_prod_53_4[2] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316484, 39,
    { 0, 4 }
  }
};

static CONST_PROD GrammarRule prfx_rule_53[5] =
{
  { prfx_prod_53_0, 5, 1 },
  { prfx_prod_53_1, 4, 1 },
  { prfx_prod_53_2, 3, 1 },
  { prfx_prod_53_3, 3, 1 },
  { prfx_prod_53_4, 2, 1 }
};

static CONST_PROD Production prfx_prod_54_0[2] =
{
  {
    838860802, 13,
    { URI_MAX, LN_MAX }
  },
  {
    335544321, 11,
    { 0, 1 }
  }
};

static CONST_PROD Production prfx_prod_54_1[1] =
{
  {
    838860802, 13,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_54_2[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_54[3] =
{
  { prfx_prod_54_0, 2, 2 },
  { prfx_prod_54_1, 1, 0 },
  { prfx_prod_54_2, 1, 1 }
};

static CONST_PROD Production prfx_prod_55_0[2] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316481, 54,
    { 4, 44 }
  }
};

static CONST_PROD Production prfx_prod_55_1[2] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316481, 54,
    { 4, 44 }
  }
};

static CONST_PROD GrammarRule prfx_rule_55[2] =
{
  { prfx_prod_55_0, 2, 1 },
  { prfx_prod_55_1, 2, 1 }
};

static CONST_PROD Production prfx_prod_56_0[2] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316481, 39,
    { 0, 7 }
  }
};

static CONST_PROD Production prfx_prod_56_1[2] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316481, 39,
    { 0, 7 }
  }
};

static CONST_PROD GrammarRule prfx_rule_56[2] =
{
  { prfx_prod_56_0, 2, 1 },
  { prfx_prod_56_1, 2, 1 }
};

static CONST_PROD Production prfx_prod_57_0[1] =
{
  {
    838860801, 47,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_57_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_57[2] =
{
  { prfx_prod_57_0, 1, 0 },
  { prfx_prod_57_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_58_0[1] =
{
  {
    838860801, 48,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_58_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_58[2] =
{
  { prfx_prod_58_0, 1, 0 },
  { prfx_prod_58_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_59_0[3] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316482, 40,
    { 0, 9 }
  },
  {
    503316481, 21,
    { 0, 8 }
  }
};

static CONST_PROD Production prfx_prod_59_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_59_2[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_59[3] =
{
  { prfx_prod_59_0, 3, 1 },
  { prfx_prod_59_1, 1, 1 },
  { prfx_prod_59_2, 1, 1 }
};

static CONST_PROD Production prfx_prod_60_0[1] =
{
  {
    838860801, 49,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_60_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_60[2] =
{
  { prfx_prod_60_0, 1, 0 },
  { prfx_prod_60_1, 1, 1 }
};

static CONST_PROD Production prfx_prod_61_0[1] =
{
  {
    838860801, 50,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD Production prfx_prod_61_1[1] =
{
  {
    687865855, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_rule_61[2] =
{
  { prfx_prod_61_0, 1, 0 },
  { prfx_prod_61_1, 1, 1 }
};

static CONST_PROD EXIGrammar prfx_grammarTable[62] =
{
  { prfx_rule_0, 33554432, 2 },
  { prfx_rule_1, 570425344, 2 },
  { prfx_rule_2, 33554432, 2 },
  { prfx_rule_3, 570425344, 2 },
  { prfx_rule_4, 33554432, 2 },
  { prfx_rule_5, 570425344, 2 },
  { prfx_rule_6, 570425344, 2 },
  { prfx_rule_7, 33554432, 2 },
  { prfx_rule_8, 33554432, 2 },
  { prfx_rule_9, 570425344, 2 },
  { prfx_rule_10, 33554432, 2 },
  { prfx_rule_11, 570425344, 2 },
  { prfx_rule_12, 570425345, 2 },
  { prfx_rule_13, 33554432, 2 },
  { prfx_rule_14, 33554432, 2 },
  { prfx_rule_15, 33554432, 2 },
  { prfx_rule_16, 33554432, 2 },
  { prfx_rule_17, 33554432, 2 },
  { prfx_rule_18, 33554432, 2 },
  { prfx_rule_19, 570425344, 2 },
  { prfx_rule_20, 33554432, 2 },
  { prfx_rule_21, 33554432, 2 },
  { prfx_rule_22, 33554432, 2 },
  { prfx_rule_23, 33554432, 2 },
  { prfx_rule_24, 33554432, 2 },
  { prfx_rule_25, 33554432, 2 },
  { prfx_rule_26, 33554432, 2 },
  { prfx_rule_27, 33554432, 2 },
  { prfx_rule_28, 33554432, 2 },
  { prfx_rule_29, 570425344, 2 },
  { prfx_rule_30, 570425344, 2 },
  { prfx_rule_31, 33554432, 2 },
  { prfx_rule_32, 570425344, 2 },
  { prfx_rule_33, 33554432, 2 },
  { prfx_rule_34, 570425344, 2 },
  { prfx_rule_35, 570425344, 2 },
  { prfx_rule_36, 570425344, 2 },
  { prfx_rule_37, 33554432, 2 },
  { prfx_rule_38, 570425344, 2 },
  { prfx_rule_39, 570425344, 2 },
  { prfx_rule_40, 33554432, 2 },
  { prfx_rule_41, 570425344, 2 },
  { prfx_rule_42, 33554432, 2 },
  { prfx_rule_43, 570425344, 2 },
  { prfx_rule_44, 570425344, 2 },
  { prfx_rule_45, 570425344, 2 },
  { prfx_rule_46, 1107296256, 2 },
  { prfx_rule_47, 1107296256, 2 },
  { prfx_rule_48, 1107296256, 4 },
  { prfx_rule_49, 1107296256, 4 },
  { prfx_rule_50, 1107296256, 2 },
  { prfx_rule_51, 1107296257, 16 },
  { prfx_rule_52, 1107296256, 2 },
  { prfx_rule_53, 1107296256, 5 },
  { prfx_rule_54, 1107296257, 3 },
  { prfx_rule_55, 1107296256, 2 },
  { prfx_rule_56, 1107296256, 2 },
  { prfx_rule_57, 1107296256, 2 },
  { prfx_rule_58, 1107296256, 2 },
  { prfx_rule_59, 1107296256, 3 },
  { prfx_rule_60, 1107296256, 2 },
  { prfx_rule_61, 1107296256, 2 },
};

static CONST_PROD PfxTable prfx_pfxTable_0 =
{
  1,
  {
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 }
  }
};

static CONST_PROD LnEntry prfx_LnEntry_0[10] =
{
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_0_0, 5 },
    INDEX_MAX, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_0_1, 2 },
    INDEX_MAX, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_0_2, 7 },
    INDEX_MAX, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_0_3, 15 },
    INDEX_MAX, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_0_4, 11 },
    INDEX_MAX, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_0_5, 17 },
    INDEX_MAX, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_0_6, 9 },
    INDEX_MAX, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_0_7, 14 },
    INDEX_MAX, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_0_8, 14 },
    INDEX_MAX, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_0_9, 13 },
    INDEX_MAX, INDEX_MAX
  }
};

static CONST_PROD PfxTable prfx_pfxTable_1 =
{
  1,
  {
    { prfx_PFX_1_0, 3 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 }
  }
};

static CONST_PROD LnEntry prfx_LnEntry_1[4] =
{
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_1_0, 4 },
    INDEX_MAX, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_1_1, 2 },
    INDEX_MAX, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_1_2, 4 },
    INDEX_MAX, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_1_3, 5 },
    INDEX_MAX, INDEX_MAX
  }
};

static CONST_PROD PfxTable prfx_pfxTable_2 =
{
  1,
  {
    { prfx_PFX_2_0, 3 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 }
  }
};

static CONST_PROD LnEntry prfx_LnEntry_2[2] =
{
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_2_0, 3 },
    INDEX_MAX, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_2_1, 4 },
    INDEX_MAX, INDEX_MAX
  }
};

static CONST_PROD LnEntry prfx_LnEntry_3[46] =
{
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_0, 8 },
    INDEX_MAX, 0
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_1, 6 },
    INDEX_MAX, 1
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_2, 2 },
    INDEX_MAX, 2
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_3, 5 },
    INDEX_MAX, 3
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_4, 6 },
    INDEX_MAX, 4
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_5, 6 },
    INDEX_MAX, 5
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_6, 7 },
    INDEX_MAX, 6
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_7, 8 },
    INDEX_MAX, 7
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_8, 8 },
    INDEX_MAX, 8
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_9, 4 },
    INDEX_MAX, 9
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_10, 5 },
    INDEX_MAX, 10
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_11, 13 },
    INDEX_MAX, 11
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_12, 7 },
    INDEX_MAX, 12
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_13, 6 },
    INDEX_MAX, 13
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_14, 12 },
    INDEX_MAX, 14
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_15, 7 },
    INDEX_MAX, 15
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_16, 4 },
    INDEX_MAX, 16
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_17, 4 },
    INDEX_MAX, 17
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_18, 8 },
    INDEX_MAX, 18
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_19, 7 },
    INDEX_MAX, 19
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_20, 6 },
    INDEX_MAX, 20
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_21, 8 },
    INDEX_MAX, 21
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_22, 5 },
    INDEX_MAX, 22
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_23, 4 },
    INDEX_MAX, 23
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_24, 6 },
    INDEX_MAX, 24
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_25, 9 },
    INDEX_MAX, 25
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_26, 5 },
    INDEX_MAX, 26
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_27, 10 },
    INDEX_MAX, 27
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_28, 9 },
    INDEX_MAX, 28
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_29, 3 },
    INDEX_MAX, 29
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_30, 7 },
    INDEX_MAX, 30
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_31, 8 },
    INDEX_MAX, 31
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_32, 4 },
    INDEX_MAX, 32
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_33, 15 },
    INDEX_MAX, 33
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_34, 18 },
    INDEX_MAX, 34
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_35, 18 },
    INDEX_MAX, 35
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_36, 16 },
    INDEX_MAX, 36
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_37, 15 },
    INDEX_MAX, 37
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_38, 5 },
    INDEX_MAX, 38
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_39, 6 },
    INDEX_MAX, 39
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_40, 4 },
    INDEX_MAX, 40
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_41, 5 },
    INDEX_MAX, 41
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_42, 12 },
    INDEX_MAX, 42
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_43, 11 },
    INDEX_MAX, 43
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_44, 12 },
    INDEX_MAX, 44
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_3_45, 13 },
    INDEX_MAX, 45
  }
};

static CONST LnEntry prfx_LnEntry_4[54] =
{
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_0, 8 },
    INDEX_MAX, 49
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_1, 9 },
    INDEX_MAX, 50
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_2, 12 },
    INDEX_MAX, 61
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_3, 10 },
    INDEX_MAX, 48
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_4, 10 },
    INDEX_MAX, 47
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_5, 11 },
    INDEX_MAX, 51
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_6, 17 },
    INDEX_MAX, 52
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_7, 18 },
    INDEX_MAX, 53
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_8, 24 },
    INDEX_MAX, 55
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_9, 12 },
    INDEX_MAX, 57
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_10, 8 },
    INDEX_MAX, 58
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_11, 24 },
    INDEX_MAX, 54
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_12, 8 },
    INDEX_MAX, 56
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_13, 13 },
    INDEX_MAX, 46
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_14, 10 },
    INDEX_MAX, 60
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_15, 9 },
    INDEX_MAX, 59
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_16, 4 },
    13, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_17, 8 },
    49, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_18, 9 },
    50, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_19, 12 },
    61, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_20, 13 },
    13, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_21, 21 },
    13, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_22, 9 },
    15, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_23, 10 },
    48, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_24, 5 },
    13, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_25, 11 },
    51, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_26, 17 },
    52, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_27, 21 },
    13, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_28, 10 },
    13, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_29, 19 },
    13, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_30, 25 },
    13, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_31, 11 },
    39, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_32, 12 },
    18, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_33, 14 },
    18, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_34, 14 },
    18, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_35, 14 },
    53, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_36, 6 },
    15, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_37, 15 },
    13, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_38, 2 },
    INDEX_MAX, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_39, 16 },
    18, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_40, 4 },
    13, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_41, 12 },
    12, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_42, 17 },
    13, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_43, 4 },
    INDEX_MAX, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_44, 14 },
    54, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_45, 29 },
    13, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_46, 12 },
    57, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_47, 4 },
    13, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_48, 5 },
    13, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_49, 7 },
    47, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_50, 12 },
    39, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_51, 13 },
    46, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_52, 10 },
    60, INDEX_MAX
  },
  {
#if VALUE_CROSSTABLE_USE
    NULL,
#endif
    { prfx_LN_4_53, 22 },
    13, INDEX_MAX
  }
};

static CONST_PROD UriEntry prfx_uriEntry[5] =
{
  {
    { { sizeof(LnEntry), 10, 10 }, prfx_LnEntry_0, 10 },
    &prfx_pfxTable_0,
    { NULL, 0 }
  },
  {
    { { sizeof(LnEntry), 4, 4 }, prfx_LnEntry_1, 4 },
    &prfx_pfxTable_1,
    { prfx_URI_1, 36 }
  },
  {
    { { sizeof(LnEntry), 2, 2 }, prfx_LnEntry_2, 2 },
    &prfx_pfxTable_2,
    { prfx_URI_2, 41 }
  },
  {
    { { sizeof(LnEntry), 46, 46 }, prfx_LnEntry_3, 46 },
    NULL,
    { prfx_URI_3, 32 }
  },
  {
    { { sizeof(LnEntry), 54, 54 }, prfx_LnEntry_4, 54 },
    NULL,
    { prfx_URI_4, 23 }
  }
};

static CONST_PROD Production prfx_prod_doc_content[37] =
{
  {
    536870913, INDEX_MAX,
    { URI_MAX, LN_MAX }
  },
  {
    503316481, 13,
    { 4, 53 }
  },
  {
    503316481, 60,
    { 4, 52 }
  },
  {
    503316481, 46,
    { 4, 51 }
  },
  {
    503316481, 39,
    { 4, 50 }
  },
  {
    503316481, 47,
    { 4, 49 }
  },
  {
    503316481, 13,
    { 4, 48 }
  },
  {
    503316481, 13,
    { 4, 47 }
  },
  {
    503316481, 57,
    { 4, 46 }
  },
  {
    503316481, 13,
    { 4, 45 }
  },
  {
    503316481, 54,
    { 4, 44 }
  },
  {
    503316481, 13,
    { 4, 42 }
  },
  {
    503316481, 12,
    { 4, 41 }
  },
  {
    503316481, 13,
    { 4, 40 }
  },
  {
    503316481, 18,
    { 4, 39 }
  },
  {
    503316481, 13,
    { 4, 37 }
  },
  {
    503316481, 15,
    { 4, 36 }
  },
  {
    503316481, 53,
    { 4, 35 }
  },
  {
    503316481, 18,
    { 4, 34 }
  },
  {
    503316481, 18,
    { 4, 33 }
  },
  {
    503316481, 18,
    { 4, 32 }
  },
  {
    503316481, 39,
    { 4, 31 }
  },
  {
    503316481, 13,
    { 4, 30 }
  },
  {
    503316481, 13,
    { 4, 29 }
  },
  {
    503316481, 13,
    { 4, 28 }
  },
  {
    503316481, 13,
    { 4, 27 }
  },
  {
    503316481, 52,
    { 4, 26 }
  },
  {
    503316481, 51,
    { 4, 25 }
  },
  {
    503316481, 13,
    { 4, 24 }
  },
  {
    503316481, 48,
    { 4, 23 }
  },
  {
    503316481, 15,
    { 4, 22 }
  },
  {
    503316481, 13,
    { 4, 21 }
  },
  {
    503316481, 13,
    { 4, 20 }
  },
  {
    503316481, 61,
    { 4, 19 }
  },
  {
    503316481, 50,
    { 4, 18 }
  },
  {
    503316481, 49,
    { 4, 17 }
  },
  {
    503316481, 13,
    { 4, 16 }
  }
};

static CONST_PROD Production prfx_prod_doc_end[1] =
{
  {
    0xAFFFFFF, INDEX_MAX,
    { URI_MAX, LN_MAX }
  }
};

static CONST_PROD GrammarRule prfx_docGrammarRule[2] =
{
  { prfx_prod_doc_content, 37, 0 },
  { prfx_prod_doc_end, 1, 0 }
};

static CONST_PROD SimpleType prfx_simpleTypes[51] =
{
  { 1174405120, 1, 0x0000000000000000, 0x0000000000000000 },
  { 167776256, 0, 0x0000000000000000, 0x0000000000000000 },
  { 167772160, 0, 0x0000000000000000, 0x0000000000000000 },
  { 167776256, 0, 0x0000000000000000, 0x0000000000000000 },
  { 1174405120, 3, 0x0000000000000000, 0x0000000000000000 },
  { 167776256, 0, 0x0000000000000000, 0x0000000000000000 },
  { 167776256, 0, 0x0000000000000000, 0x0000000000000000 },
  { 1174405120, 6, 0x0000000000000000, 0x0000000000000000 },
  { 167772160, 0, 0x0000000000000000, 0x0000000000000000 },
  { 167776256, 0, 0x0000000000000000, 0x0000000000000000 },
  { 167772160, 0, 0x0000000000000000, 0x0000000000000000 },
  { 167776256, 0, 0x0000000000000000, 0x0000000000000000 },
  { 4096, 0, 0x0000000000000000, 0x0000000000000000 },
  { 167772160, 0, 0x0000000000000000, 0x0000000000000000 },
  { 1006632960, 0, 0x0000000000000000, 0x0000000000000000 },
  { 838860800, 0, 0x0000000000000000, 0x0000000000000000 },
  { 1526727232, 0, 0x000000000000007F, 0x00000000FFFFFF80 },
  { 704643072, 0, 0x0000000000000000, 0x0000000000000000 },
  { 671088640, 0, 0x0000000000000000, 0x0000000000000000 },
  { 503320576, 0, 0x0000000000000000, 0x0000000000000000 },
  { 335544320, 0, 0x0000000000000000, 0x0000000000000000 },
  { 167772160, 0, 0x0000000000000000, 0x0000000000000000 },
  { 335544320, 0, 0x0000000000000000, 0x0000000000000000 },
  { 721420288, 0, 0x0000000000000000, 0x0000000000000000 },
  { 721420288, 0, 0x0000000000000000, 0x0000000000000000 },
  { 721420288, 0, 0x0000000000000000, 0x0000000000000000 },
  { 687865856, 0, 0x0000000000000000, 0x0000000000000000 },
  { 704643072, 0, 0x0000000000000000, 0x0000000000000000 },
  { 1006632960, 0, 0x0000000000000000, 0x0000000000000000 },
  { 1509953536, 0, 0x0000000000000000, 0x0000000000000000 },
  { 1509953536, 0, 0x0000000000000000, 0x0000000000000000 },
  { 167772160, 0, 0x0000000000000000, 0x0000000000000000 },
  { 1509953536, 0, 0x0000000000000000, 0x0000000000000000 },
  { 1509949504, 0, 0x00000000FFFFFFFF, 0x0000000000000000 },
  { 1543508480, 0, 0x0000000000000000, 0x0000000000000000 },
  { 1509953600, 0, 0x0000000000000000, 0x0000000000000000 },
  { 167776256, 0, 0x0000000000000000, 0x0000000000000000 },
  { 1543504384, 0, 0x0000000000000000, 0x0000000000000001 },
  { 1509954112, 0, 0x0000000000007FFF, 0x00000000FFFF8000 },
  { 167776256, 0, 0x0000000000000000, 0x0000000000000000 },
  { 738197504, 0, 0x0000000000000000, 0x0000000000000000 },
  { 167776256, 0, 0x0000000000000000, 0x0000000000000000 },
  { 1526727232, 0, 0x00000000000000FF, 0x0000000000000000 },
  { 1543508480, 0, 0x0000000000000000, 0x0000000000000000 },
  { 1543508480, 0, 0x0000000000000000, 0x0000000000000000 },
  { 1543508544, 0, 0x000000000000FFFF, 0x0000000000000000 },
  { 167772176, 0, 0x0000000000000000, 0x0000000000000000 },
  { 167772176, 0, 0x0000000000000000, 0x0000000000000000 },
  { 167772176, 0, 0x0000000000000000, 0x0000000000000000 },
  { 167772176, 0, 0x0000000000000000, 0x0000000000000000 },
  { 167772176, 0, 0x0000000000000000, 0x0000000000000000 }
};

CONST_PROD CharType prfx_ENUM_0_0[] = { 0x4f, 0x4e, 0x4c, 0x49, 0x4e, 0x45 }; /* ONLINE */
CONST_PROD CharType prfx_ENUM_0_1[] = { 0x4f, 0x46, 0x46, 0x4c, 0x49, 0x4e, 0x45 }; /* OFFLINE */
CONST_PROD CharType prfx_ENUM_0_2[] = { 0x4e, 0x4f, 0x54, 0x5f, 0x52, 0x45, 0x41, 0x43, 0x48, 0x41, 0x42, 0x4c, 0x45 }; /* NOT_REACHABLE */

static CONST_PROD String prfx_enumValues_0[3] = {
  { prfx_ENUM_0_0, 6 },
  { prfx_ENUM_0_1, 7 },
  { prfx_ENUM_0_2, 13 }
};

CONST_PROD CharType prfx_ENUM_1_0[] = { 0x4f, 0x4e, 0x4c, 0x49, 0x4e, 0x45 }; /* ONLINE */
CONST_PROD CharType prfx_ENUM_1_1[] = { 0x4f, 0x46, 0x46, 0x4c, 0x49, 0x4e, 0x45 }; /* OFFLINE */
CONST_PROD CharType prfx_ENUM_1_2[] = { 0x4e, 0x4f, 0x54, 0x5f, 0x52, 0x45, 0x41, 0x43, 0x48, 0x41, 0x42, 0x4c, 0x45 }; /* NOT_REACHABLE */

static CONST_PROD String prfx_enumValues_1[3] = {
  { prfx_ENUM_1_0, 6 },
  { prfx_ENUM_1_1, 7 },
  { prfx_ENUM_1_2, 13 }
};

CONST_PROD CharType prfx_ENUM_2_0[] = { 0x52, 0x43, 0x41, 0x54, 0x5f, 0x30 }; /* RCAT_0 */
CONST_PROD CharType prfx_ENUM_2_1[] = { 0x52, 0x43, 0x41, 0x54, 0x5f, 0x31 }; /* RCAT_1 */
CONST_PROD CharType prfx_ENUM_2_2[] = { 0x52, 0x43, 0x41, 0x54, 0x5f, 0x32 }; /* RCAT_2 */
CONST_PROD CharType prfx_ENUM_2_3[] = { 0x52, 0x43, 0x41, 0x54, 0x5f, 0x33 }; /* RCAT_3 */
CONST_PROD CharType prfx_ENUM_2_4[] = { 0x52, 0x43, 0x41, 0x54, 0x5f, 0x34 }; /* RCAT_4 */
CONST_PROD CharType prfx_ENUM_2_5[] = { 0x52, 0x43, 0x41, 0x54, 0x5f, 0x35 }; /* RCAT_5 */
CONST_PROD CharType prfx_ENUM_2_6[] = { 0x52, 0x43, 0x41, 0x54, 0x5f, 0x36 }; /* RCAT_6 */
CONST_PROD CharType prfx_ENUM_2_7[] = { 0x52, 0x43, 0x41, 0x54, 0x5f, 0x37 }; /* RCAT_7 */

static CONST_PROD String prfx_enumValues_2[8] = {
  { prfx_ENUM_2_0, 6 },
  { prfx_ENUM_2_1, 6 },
  { prfx_ENUM_2_2, 6 },
  { prfx_ENUM_2_3, 6 },
  { prfx_ENUM_2_4, 6 },
  { prfx_ENUM_2_5, 6 },
  { prfx_ENUM_2_6, 6 },
  { prfx_ENUM_2_7, 6 }
};

CONST_PROD CharType prfx_ENUM_3_0[] = { 0x53, 0x54, 0x41, 0x54, 0x55, 0x53, 0x5f, 0x4f, 0x4b }; /* STATUS_OK */
CONST_PROD CharType prfx_ENUM_3_1[] = { 0x53, 0x54, 0x41, 0x54, 0x55, 0x53, 0x5f, 0x41, 0x43, 0x43, 0x45, 0x50, 0x54, 0x45, 0x44 }; /* STATUS_ACCEPTED */
CONST_PROD CharType prfx_ENUM_3_2[] = { 0x53, 0x54, 0x41, 0x54, 0x55, 0x53, 0x5f, 0x42, 0x41, 0x44, 0x5f, 0x52, 0x45, 0x51, 0x55, 0x45, 0x53, 0x54 }; /* STATUS_BAD_REQUEST */
CONST_PROD CharType prfx_ENUM_3_3[] = { 0x53, 0x54, 0x41, 0x54, 0x55, 0x53, 0x5f, 0x50, 0x45, 0x52, 0x4d, 0x49, 0x53, 0x53, 0x49, 0x4f, 0x4e, 0x5f, 0x44, 0x45, 0x4e, 0x49, 0x45, 0x44 }; /* STATUS_PERMISSION_DENIED */
CONST_PROD CharType prfx_ENUM_3_4[] = { 0x53, 0x54, 0x41, 0x54, 0x55, 0x53, 0x5f, 0x46, 0x4f, 0x52, 0x42, 0x49, 0x44, 0x44, 0x45, 0x4e }; /* STATUS_FORBIDDEN */
CONST_PROD CharType prfx_ENUM_3_5[] = { 0x53, 0x54, 0x41, 0x54, 0x55, 0x53, 0x5f, 0x4e, 0x4f, 0x54, 0x5f, 0x46, 0x4f, 0x55, 0x4e, 0x44 }; /* STATUS_NOT_FOUND */
CONST_PROD CharType prfx_ENUM_3_6[] = { 0x53, 0x54, 0x41, 0x54, 0x55, 0x53, 0x5f, 0x4d, 0x45, 0x54, 0x48, 0x4f, 0x44, 0x5f, 0x4e, 0x4f, 0x54, 0x5f, 0x41, 0x4c, 0x4c, 0x4f, 0x57, 0x45, 0x44 }; /* STATUS_METHOD_NOT_ALLOWED */
CONST_PROD CharType prfx_ENUM_3_7[] = { 0x53, 0x54, 0x41, 0x54, 0x55, 0x53, 0x5f, 0x4e, 0x4f, 0x54, 0x5f, 0x41, 0x43, 0x43, 0x45, 0x50, 0x54, 0x41, 0x42, 0x4c, 0x45 }; /* STATUS_NOT_ACCEPTABLE */
CONST_PROD CharType prfx_ENUM_3_8[] = { 0x53, 0x54, 0x41, 0x54, 0x55, 0x53, 0x5f, 0x52, 0x45, 0x51, 0x55, 0x45, 0x53, 0x54, 0x5f, 0x54, 0x49, 0x4d, 0x45, 0x4f, 0x55, 0x54 }; /* STATUS_REQUEST_TIMEOUT */
CONST_PROD CharType prfx_ENUM_3_9[] = { 0x53, 0x54, 0x41, 0x54, 0x55, 0x53, 0x5f, 0x43, 0x4f, 0x4e, 0x46, 0x4c, 0x49, 0x43, 0x54 }; /* STATUS_CONFLICT */
CONST_PROD CharType prfx_ENUM_3_10[] = { 0x53, 0x54, 0x41, 0x54, 0x55, 0x53, 0x5f, 0x55, 0x4e, 0x53, 0x55, 0x50, 0x50, 0x4f, 0x52, 0x54, 0x45, 0x44, 0x5f, 0x4d, 0x45, 0x44, 0x49, 0x41, 0x5f, 0x54, 0x59, 0x50, 0x45 }; /* STATUS_UNSUPPORTED_MEDIA_TYPE */
CONST_PROD CharType prfx_ENUM_3_11[] = { 0x53, 0x54, 0x41, 0x54, 0x55, 0x53, 0x5f, 0x49, 0x4e, 0x54, 0x45, 0x52, 0x4e, 0x41, 0x4c, 0x5f, 0x53, 0x45, 0x52, 0x56, 0x45, 0x52, 0x5f, 0x45, 0x52, 0x52, 0x4f, 0x52 }; /* STATUS_INTERNAL_SERVER_ERROR */
CONST_PROD CharType prfx_ENUM_3_12[] = { 0x53, 0x54, 0x41, 0x54, 0x55, 0x53, 0x5f, 0x4e, 0x4f, 0x54, 0x5f, 0x49, 0x4d, 0x50, 0x4c, 0x45, 0x4d, 0x45, 0x4e, 0x54, 0x45, 0x44 }; /* STATUS_NOT_IMPLEMENTED */
CONST_PROD CharType prfx_ENUM_3_13[] = { 0x53, 0x54, 0x41, 0x54, 0x55, 0x53, 0x5f, 0x42, 0x41, 0x44, 0x5f, 0x47, 0x41, 0x54, 0x45, 0x57, 0x41, 0x59 }; /* STATUS_BAD_GATEWAY */
CONST_PROD CharType prfx_ENUM_3_14[] = { 0x53, 0x54, 0x41, 0x54, 0x55, 0x53, 0x5f, 0x53, 0x45, 0x52, 0x56, 0x49, 0x43, 0x45, 0x5f, 0x55, 0x4e, 0x41, 0x56, 0x41, 0x49, 0x4c, 0x41, 0x42, 0x4c, 0x45 }; /* STATUS_SERVICE_UNAVAILABLE */
CONST_PROD CharType prfx_ENUM_3_15[] = { 0x53, 0x54, 0x41, 0x54, 0x55, 0x53, 0x5f, 0x47, 0x41, 0x54, 0x45, 0x57, 0x41, 0x59, 0x5f, 0x54, 0x49, 0x4d, 0x45, 0x4f, 0x55, 0x54 }; /* STATUS_GATEWAY_TIMEOUT */
CONST_PROD CharType prfx_ENUM_3_16[] = { 0x53, 0x54, 0x41, 0x54, 0x55, 0x53, 0x5f, 0x44, 0x45, 0x4c, 0x45, 0x54, 0x45, 0x44 }; /* STATUS_DELETED */
CONST_PROD CharType prfx_ENUM_3_17[] = { 0x53, 0x54, 0x41, 0x54, 0x55, 0x53, 0x5f, 0x45, 0x58, 0x50, 0x49, 0x52, 0x45, 0x44 }; /* STATUS_EXPIRED */

static CONST_PROD String prfx_enumValues_3[18] = {
  { prfx_ENUM_3_0, 9 },
  { prfx_ENUM_3_1, 15 },
  { prfx_ENUM_3_2, 18 },
  { prfx_ENUM_3_3, 24 },
  { prfx_ENUM_3_4, 16 },
  { prfx_ENUM_3_5, 16 },
  { prfx_ENUM_3_6, 25 },
  { prfx_ENUM_3_7, 21 },
  { prfx_ENUM_3_8, 22 },
  { prfx_ENUM_3_9, 15 },
  { prfx_ENUM_3_10, 29 },
  { prfx_ENUM_3_11, 28 },
  { prfx_ENUM_3_12, 22 },
  { prfx_ENUM_3_13, 18 },
  { prfx_ENUM_3_14, 26 },
  { prfx_ENUM_3_15, 22 },
  { prfx_ENUM_3_16, 14 },
  { prfx_ENUM_3_17, 14 }
};

CONST_PROD CharType prfx_ENUM_4_0[] = { 0x53, 0x48, 0x41, 0x4c, 0x4c, 0x4f, 0x57 }; /* SHALLOW */
CONST_PROD CharType prfx_ENUM_4_1[] = { 0x44, 0x45, 0x45, 0x50 }; /* DEEP */

static CONST_PROD String prfx_enumValues_4[2] = {
  { prfx_ENUM_4_0, 7 },
  { prfx_ENUM_4_1, 4 }
};

static CONST_PROD EnumDefinition prfx_enumTable[5] = {
  { 46, prfx_enumValues_0, 3 },
  { 47, prfx_enumValues_1, 3 },
  { 48, prfx_enumValues_2, 8 },
  { 49, prfx_enumValues_3, 18 },
  { 50, prfx_enumValues_4, 2 }
};

extern CONST EXIPSchema prfx_schema =
{
  { NULL, NULL },
  { { sizeof(UriEntry), 5, 5 }, prfx_uriEntry, 5 },
  { prfx_docGrammarRule, 100663296, 2 },
  { { sizeof(SimpleType), 51, 51 }, prfx_simpleTypes, 51 },
  { { sizeof(EXIGrammar), 62, 62 }, prfx_grammarTable, 62 },
  62,
  { { sizeof(EnumDefinition), 5, 5 }, prfx_enumTable, 5 }
};

