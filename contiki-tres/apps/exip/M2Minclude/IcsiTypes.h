/*==================================================================*\
 |                EXIP - Embeddable EXI Processor in C                |
 ||--------------------------------------------------------------------|
 |          This work is licensed under BSD 3-Clause License          |
 |  The full license terms and conditions are located in LICENSE.txt  |
 \===================================================================*/

/**
 * @file procTypes.h
 * @brief Common structure types used throughout the project
 *
 * @date --
 * @author --
 * @version --
 * @par[Revision] $Id: procTypes.h 328 2013-10-30 16:00:10Z kjussakov $
 */

#ifndef ICSITYPES_H_
#define ICSITYPES_H_

typedef CharType *SingleString;

typedef SingleString AnyURI;

typedef SingleString AccessRightID;

struct stringsVector {
  SingleString *strings;
  int length;
};
typedef struct stringsVector StringsVector;
typedef StringsVector SearchStrings;
typedef StringsVector References;
typedef StringsVector ContentTypes;

struct announceTo {
  References sclList;
  boolean *activated;
  boolean *global;
};
typedef struct announceTo AnnounceTo;

struct APoCPath {
  SingleString path;
  SingleString accessRightID;
  SearchStrings *searchStrings;
};
typedef struct APoCPath aPoCPath;

struct APoCPaths {
  aPoCPath *apocpaths;
  int length;
};
typedef struct APoCPaths aPoCPaths;

struct filterCriteria {
  EXIPDateTime *ifModifiedSince;
  EXIPDateTime *ifUnmodifiedSince;
  StringsVector ifMatch;
  StringsVector ifNoneMatch;
};
typedef struct filterCriteria FilterCriteria;

struct choice_MTBN_delayTolerance {

  boolean MTBN;
  long *MTBN_value;
  EXIPDateTime *delayTolerance;
};
typedef struct choice_MTBN_delayTolerance Choice_MTBN_delayTolerance;

struct stringsVector_ {
  String **strings;
  int length;
};
typedef struct stringsVector_ SearchStrings_;

struct _Application {

  EXIPDateTime *expirationTime;
  String *accessRightID;
  SearchStrings_ *searchStrings;
  EXIPDateTime *creationTime;
  EXIPDateTime *lastModifiedTime;
  AnnounceTo *announceTo;
  String *aPoC;
  aPoCPaths *aPoCPaths;
  String *containersReference;
  String *groupsReference;
  String *accessRightsReference;
  String *subscriptionsReference;
  String *notificationChannelsReference;
  String *namespace;
};
typedef struct _Application _application;

struct _Subscription {

  EXIPDateTime *expirationTime;
  SingleString accessRightID;
  SearchStrings *searchStrings;
  EXIPDateTime *creationTime;
  EXIPDateTime *lastModifiedTime;
  AnnounceTo *announceTo;
  SingleString aPoC;
  aPoCPaths aPoCPaths;
  SingleString containersReference;
  SingleString accessRightsReference;
  SingleString subscriptionsReference;
  SingleString notificationChannelsReference;
};
typedef struct _Subscription _subscription;

struct m2m_object {

  _application *application;
  _subscription *subscription;
};
typedef struct m2m_object _m2m_object;

typedef enum {
  STATUS_OK,
  STATUS_ACCEPTED,
  STATUS_BAD_REQUEST,
  STATUS_PERMISSION_DENIED,
  STATUS_FORBIDDEN,
  STATUS_NOT_FOUND,
  STATUS_METHOD_NOT_ALLOWED,
  STATUS_NOT_ACCEPTABLE,
  STATUS_REQUEST_TIMEOUT,
  STATUS_CONFLICT,
  STATUS_UNSUPPORTED_MEDIA_TYPE,
  STATUS_INTERNAL_SERVER_ERROR,
  STATUS_NOT_IMPLEMENTED,
  STATUS_BAD_GATEWAY,
  STATUS_SERVICE_UNAVAILABLE,
  STATUS_GATEWAY_TIMEOUT,
  STATUS_DELETED,
  STATUS_EXPIRED
} statusCode;

#endif

