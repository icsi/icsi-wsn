/*
 * File:   decodeM2MUtils.h
 * Author: francesca
 *
 * Created on April 15, 2014, 4:07 PM
 */

#ifndef DECODEM2MUTILS_H
#define DECODEM2MUTILS_H
#include "IcsiTypes.h"

char xml_output[DECODE_OUTPUT_BUFFER_SIZE];

Index xml_output_index = DECODE_OUTPUT_BUFFER_SIZE;

_m2m_object __m2m_object;

production *current_production;

uint32_t current_structure;

String **currentStringData;

EXIPDateTime **currentDateTime;

void
init_xml_output()
{
  xml_output_index = 0;
  return;
}
void
destroy_xml_output()
{
  xml_output_index = DECODE_OUTPUT_BUFFER_SIZE;
  return;
}
errorCode
copyToXMLoutput(CharType *buf, int length)
{
  /* printf("length=%d string %s\n",length,buf); */
  if(length + xml_output_index >= DECODE_OUTPUT_BUFFER_SIZE) {
    DEBUG_MSG(INFO, FRANCESCA_DEBUG, ("____FRA_debug, copyToXMLoutput: FAIL\n"));
    return EXIP_UNEXPECTED_ERROR;
  }
  strcpy(xml_output + xml_output_index, buf);
  xml_output_index += length;
  xml_output[xml_output_index] = '\0';
  return EXIP_OK;
}
errorCode
printToXMLoutput(CharType *buf)
{
  return copyToXMLoutput(buf, strlen(buf));
}
errorCode
generateApplication()
{
  errorCode tmp_err_code = EXIP_OK;
  DEBUG_MSG(INFO, SKIP_XML_DECODE_DEBUG, ("Application element\n"));
  if(current_production->label == START) {
    current_production = current_production->prod[0];
    _application *__application = EXIP_MALLOC(sizeof(_application));
    current_structure = __application;
    __m2m_object.application = __application;
    __application->aPoC = NULL;
    __application->accessRightID = NULL;
    __application->accessRightsReference = NULL;
    __application->announceTo = NULL;
    __application->containersReference = NULL;
    __application->creationTime = NULL;
    __application->expirationTime = NULL;
    __application->lastModifiedTime = NULL;
    __application->notificationChannelsReference = NULL;
    __application->searchStrings = NULL;
    __application->subscriptionsReference = NULL;
    currentStringData = NULL;
  } else { tmp_err_code = EXIP_UNEXPECTED_ERROR;
  }
  return tmp_err_code;
}
errorCode
generateExpirationTime()
{
  errorCode tmp_err_code = EXIP_OK;
  if(current_production->label == APPLICATION) {
    current_production = current_production->prod[0];
    currentDateTime = &(__m2m_object.application->expirationTime);
    *currentDateTime = EXIP_MALLOC(sizeof(EXIPDateTime));
  } else {
    tmp_err_code = EXIP_UNEXPECTED_ERROR;
  }
  return tmp_err_code;
}
errorCode
generateCreationTime()
{
  errorCode tmp_err_code = EXIP_OK;
  boolean found = TRUE;
  if(current_production->label == APPLICATION) {
    current_production = current_production->prod[3];
  } else {
    if(current_production->label == EXPIRATION_TIME) {
      current_production = current_production->prod[2];
    } else {
      if(current_production->label == ACCESS_RIGHT_ID) {
        current_production = current_production->prod[1];
      } else {
        if(current_production->label == SEARCH_STRINGS) {
          current_production = current_production->prod[0];
        } else {
          found = FALSE;
          tmp_err_code = EXIP_UNEXPECTED_ERROR;
        }
      }
    }
  }
  if(found) {
    currentDateTime = &(__m2m_object.application->creationTime);
    *currentDateTime = EXIP_MALLOC(sizeof(EXIPDateTime));
  }
  return tmp_err_code;
}
errorCode
generateLastModifiedTime()
{
  errorCode tmp_err_code = EXIP_OK;
  boolean found = TRUE;
  if(current_production->label == APPLICATION) {
    current_production = current_production->prod[4];
  } else {
    if(current_production->label == EXPIRATION_TIME) {
      current_production = current_production->prod[3];
    } else {
      if(current_production->label == ACCESS_RIGHT_ID) {
        current_production = current_production->prod[2];
      } else {
        if(current_production->label == SEARCH_STRINGS) {
          current_production = current_production->prod[1];
        } else {
          if(current_production->label == CREATION_TIME) {
            current_production = current_production->prod[0];
          } else {
            found = FALSE;
            tmp_err_code = EXIP_UNEXPECTED_ERROR;
          }
        }
      }
    }
  }
  if(found) {
    currentDateTime = &(__m2m_object.application->expirationTime);
    *currentDateTime = EXIP_MALLOC(sizeof(EXIPDateTime));
  }
  return tmp_err_code;
}
errorCode
generateAccessRightID()
{
  errorCode tmp_err_code = EXIP_OK;
  boolean found = TRUE;
  DEBUG_MSG(INFO, SKIP_XML_DECODE_DEBUG, ("accessRightID in APPLICATION\n"));
  if(current_production->label == APPLICATION) {
    current_production = current_production->prod[1];
  } else {
    if(current_production->label == EXPIRATION_TIME) {
      current_production = current_production->prod[0];
    } else {
      found = FALSE;
      tmp_err_code = EXIP_UNEXPECTED_ERROR;
    }
  }
  if(found) {
    _application *currentApplication = (_application *)current_structure;
    currentStringData = &(currentApplication->accessRightID);     /* &(__m2m_object.application->accessRightID); */
    *currentStringData = EXIP_MALLOC(sizeof(String));
  }
  return tmp_err_code;
}
errorCode
generateSearchString()
{
  int length;
  boolean found = TRUE;
  errorCode tmp_err_code = EXIP_OK;
  DEBUG_MSG(INFO, SKIP_XML_DECODE_DEBUG, ("searchString element\n"));
  if(current_production->label == SEARCH_STRINGS) {
    DEBUG_MSG(INFO, SKIP_XML_DECODE_DEBUG, ("searchString element in SEARCH_STRINGS\n"));
    current_production = current_production->prod[0];
    length = __m2m_object.application->searchStrings->length;
    if(length == 0) {
      __m2m_object.application->searchStrings->strings = EXIP_MALLOC(sizeof(String));
      __m2m_object.application->searchStrings->length = 1;
    } else {
      /* realloc puts the new element at the end of the queue */
      __m2m_object.application->searchStrings->strings = EXIP_REALLOC(__m2m_object.application->searchStrings->strings, (length + 1) * sizeof(String));
      __m2m_object.application->searchStrings->length++;
    }
  } else {
    if(current_production->label == SEARCH_STRING) {
      DEBUG_MSG(INFO, SKIP_XML_DECODE_DEBUG, ("searchString element in SEARCH_STRINGS\n"));
      __m2m_object.application->searchStrings->strings = EXIP_REALLOC(__m2m_object.application->searchStrings->strings, (__m2m_object.application->searchStrings->length + 1) * sizeof(String));
      __m2m_object.application->searchStrings->length++;

      /* current_production = current_production; */
    } else {
      tmp_err_code = EXIP_UNEXPECTED_ERROR;
      found = FALSE;
    }
  }
  if(found) {
    currentStringData = &(__m2m_object.application->searchStrings->strings[length]);     /* &(search_strings->strings[0]); */
    *currentStringData = EXIP_MALLOC(sizeof(String));
  }
  return tmp_err_code;
}
errorCode
generateSearchStrings()
{
  errorCode tmp_err_code = EXIP_OK;
  currentStringData = NULL;
  _application *currentApplication = (_application *)current_structure;
  current_structure = currentApplication->searchStrings;
  if(current_production->label == APPLICATION) {
    current_production = current_production->prod[2];
  } else {
    if(current_production->label == EXPIRATION_TIME) {
      current_production = current_production->prod[1];
    } else {
      if(current_production->label == ACCESS_RIGHT_ID) {
        current_production = current_production->prod[0];
      } else { tmp_err_code = EXIP_UNEXPECTED_ERROR;
      }
    }
  }
  SearchStrings *s = EXIP_MALLOC(sizeof(SearchStrings));
  s->length = 0;
  s->strings = NULL;
  __m2m_object.application->searchStrings = s;
  return tmp_err_code;
}
errorCode
generateAPoC()
{
  errorCode tmp_err_code = EXIP_OK;
  if(current_production->label == APPLICATION) {
    current_production = current_production->prod[6];
  } else {
    if(current_production->label == EXPIRATION_TIME) {
      current_production = current_production->prod[5];
    } else {
      if(current_production->label == ACCESS_RIGHT_ID) {
        current_production = current_production->prod[4];
      } else {
        if(current_production->label == SEARCH_STRINGS) {
          current_production = current_production->prod[4];
        } else {
          if(current_production->label == SEARCH_STRING) {
            current_production = current_production->prod[3];
          } else {
            if(current_production->label == CREATION_TIME) {
              current_production = current_production->prod[2];
            } else {
              if(current_production->label == LAST_MODIFIED_TIME) {
                current_production = current_production->prod[1];
              } else {
                if(current_production->label == ANNOUNCE_TO) {
                  current_production = current_production->prod[0];
                } else { tmp_err_code = EXIP_UNEXPECTED_ERROR;
                }
              }
            }
          }
        }
      }
    }
  }
  currentStringData = &(__m2m_object.application->aPoC);
  *currentStringData = EXIP_MALLOC(sizeof(String));
  return tmp_err_code;
}
errorCode
generateAPoCPath()
{
  int length;
  errorCode tmp_err_code = EXIP_OK;
  if(current_production->label == APOC_PATHS) {
    current_production = current_production->prod[0];
    length = __m2m_object.application->aPoCPaths->length;
    if(length == 0) {
      __m2m_object.application->aPoCPaths->apocpaths = EXIP_MALLOC(sizeof(aPoCPath));
      __m2m_object.application->aPoCPaths->length = 1;
    } else {
      /* realloc puts the new element at the end of the queue */
      __m2m_object.application->aPoCPaths->apocpaths = EXIP_REALLOC(__m2m_object.application->aPoCPaths->apocpaths, (length + 1) * sizeof(aPoCPath));
      __m2m_object.application->aPoCPaths->length++;
    }
    currentStringData = &(__m2m_object.application->aPoCPaths->apocpaths[length]);     /* &(search_strings->strings[0]); */
    *currentStringData = EXIP_MALLOC(sizeof(aPoCPath));
  } else {
    if(current_production->label == APOC_PATH) {
      /* current_production = current_production; */
    } else {
      tmp_err_code = EXIP_UNEXPECTED_ERROR;
    }
  }
  return tmp_err_code;
}
errorCode
generateAPoCPaths()
{
  errorCode tmp_err_code = EXIP_OK;
  boolean found = TRUE;
  currentStringData = NULL;
  SearchStrings *s = NULL;
  if(current_production->label == APPLICATION) {
    current_production = current_production->prod[7];
  } else {
    if(current_production->label == EXPIRATION_TIME) {
      current_production = current_production->prod[6];
    } else {
      if(current_production->label == ACCESS_RIGHT_ID) {
        current_production = current_production->prod[5];
      } else {
        if(current_production->label == SEARCH_STRINGS) {
          current_production = current_production->prod[5];
        } else {
          if(current_production->label == SEARCH_STRING) {
            current_production = current_production->prod[4];
          } else {
            if(current_production->label == CREATION_TIME) {
              current_production = current_production->prod[3];
            } else {
              if(current_production->label == LAST_MODIFIED_TIME) {
                current_production = current_production->prod[2];
              } else {
                if(current_production->label == ANNOUNCE_TO) {
                  current_production = current_production->prod[1];
                } else {
                  if(current_production->label == APOC) {
                    current_production = current_production->prod[0];
                  } else {
                    found = FALSE;
                    tmp_err_code = EXIP_UNEXPECTED_ERROR;
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  if(found) {
    s = EXIP_MALLOC(sizeof(SearchStrings));
    s->length = 0;
    s->strings = NULL;
    __m2m_object.application->searchStrings = s;
  }
  return tmp_err_code;
}
errorCode
generateContainersReference()
{
  errorCode tmp_err_code = EXIP_OK;
  boolean found = TRUE;
  if(current_production->label == ANNOUNCE_TO) {
    current_production = current_production->prod[2];
  } else {
    if(current_production->label == LAST_MODIFIED_TIME) {
      current_production = current_production->prod[3];
    } else {
      if(current_production->label == CREATION_TIME) {
        current_production = current_production->prod[4];
      } else {
        if(current_production->label == SEARCH_STRINGS) {
          current_production = current_production->prod[6];
        } else {
          if(current_production->label == SEARCH_STRING) {
            current_production = current_production->prod[5];
          } else {
            if(current_production->label == ACCESS_RIGHT_ID) {
              current_production = current_production->prod[6];
            } else {
              if(current_production->label == EXPIRATION_TIME) {
                current_production = current_production->prod[7];
              } else {
                if(current_production->label == APOC) {
                  current_production = current_production->prod[2];
                } else {
                  if(current_production->label == APOC_PATHS) {                   /*  */
                    current_production = current_production->prod[2];
                  } else {
                    if(current_production->label == LOC_REQUESTOR) {
                      current_production = current_production->prod[0];
                    } else {
                      found = FALSE;
                      tmp_err_code = EXIP_UNEXPECTED_ERROR;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  if(found) {
    currentStringData = &(__m2m_object.application->groupsReference);
    *currentStringData = EXIP_MALLOC(sizeof(String));
  }
  return tmp_err_code;
}
errorCode
generateGroupsReference()
{
  errorCode tmp_err_code = EXIP_OK;
  boolean found = TRUE;
  if(current_production->label == ANNOUNCE_TO) {
    current_production = current_production->prod[3];
  } else {
    if(current_production->label == LAST_MODIFIED_TIME) {
      current_production = current_production->prod[4];
    } else {
      if(current_production->label == CREATION_TIME) {
        current_production = current_production->prod[5];
      } else {
        if(current_production->label == SEARCH_STRINGS) {
          current_production = current_production->prod[7];
        } else {
          if(current_production->label == SEARCH_STRING) {
            current_production = current_production->prod[6];
          } else {
            if(current_production->label == ACCESS_RIGHT_ID) {
              current_production = current_production->prod[7];
            } else {
              if(current_production->label == EXPIRATION_TIME) {
                current_production = current_production->prod[8];
              } else {
                if(current_production->label == APOC) {
                  DEBUG_MSG(INFO, SKIP_XML_DECODE_DEBUG, ("groupsReference element in APOC\n"));
                  current_production = current_production->prod[3];
                } else {
                  if(current_production->label == APOC_PATHS) {                   /*  */
                    current_production = current_production->prod[3];
                  } else {
                    if(current_production->label == LOC_REQUESTOR) {
                      current_production = current_production->prod[1];
                    } else {
                      if(current_production->label == CONTAINERS_REFERENCE) {
                        current_production = current_production->prod[0];
                      } else {
                        tmp_err_code = EXIP_UNEXPECTED_ERROR;
                        found = FALSE;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  if(found) {
    currentStringData = &(__m2m_object.application->groupsReference);
    *currentStringData = EXIP_MALLOC(sizeof(String));
  }
  return tmp_err_code;
}
errorCode
generateAccessRightsReference()
{
  errorCode tmp_err_code = EXIP_OK;
  boolean found = TRUE;
  if(current_production->label == ANNOUNCE_TO) {
    current_production = current_production->prod[5];
  } else {
    if(current_production->label == LAST_MODIFIED_TIME) {
      current_production = current_production->prod[6];
    } else {
      if(current_production->label == CREATION_TIME) {
        current_production = current_production->prod[7];
      } else {
        if(current_production->label == SEARCH_STRINGS) {
          current_production = current_production->prod[9];
        } else {
          if(current_production->label == SEARCH_STRING) {
            current_production = current_production->prod[8];
          } else {
            if(current_production->label == ACCESS_RIGHT_ID) {
              current_production = current_production->prod[9];
            } else {
              if(current_production->label == EXPIRATION_TIME) {
                current_production = current_production->prod[10];
              } else {
                if(current_production->label == APOC) {
                  current_production = current_production->prod[4];
                } else {
                  if(current_production->label == APOC_PATHS) {                   /*  */
                    current_production = current_production->prod[4];
                  } else {
                    if(current_production->label == LOC_REQUESTOR) {
                      current_production = current_production->prod[2];
                    } else {
                      if(current_production->label == CONTAINERS_REFERENCE) {
                        current_production = current_production->prod[1];
                      } else {
                        if(current_production->label == GROUPS_REFERENCE) {
                          DEBUG_MSG(INFO, SKIP_XML_DECODE_DEBUG, ("AccessRightsReference element in GROUPS_REFERENCE\n"));
                          current_production = current_production->prod[0];
                        } else {
                          found = FALSE;
                          tmp_err_code = EXIP_UNEXPECTED_ERROR;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  if(found) {
    currentStringData = &(__m2m_object.application->accessRightsReference);
    *currentStringData = EXIP_MALLOC(sizeof(String));
  }
  return tmp_err_code;
}
errorCode
generateSubscriptionsReference()
{
  errorCode tmp_err_code = EXIP_OK;
  boolean found = TRUE;
  if(current_production->label == ANNOUNCE_TO) {
    current_production = current_production->prod[6];
  } else {
    if(current_production->label == LAST_MODIFIED_TIME) {
      current_production = current_production->prod[7];
    } else {
      if(current_production->label == CREATION_TIME) {
        current_production = current_production->prod[8];
      } else {
        if(current_production->label == SEARCH_STRINGS) {
          current_production = current_production->prod[10];
        } else {
          if(current_production->label == SEARCH_STRING) {
            current_production = current_production->prod[9];
          } else {
            if(current_production->label == ACCESS_RIGHT_ID) {
              current_production = current_production->prod[7];
            } else {
              if(current_production->label == EXPIRATION_TIME) {
                current_production = current_production->prod[6];
              } else {
                if(current_production->label == APOC) {
                  current_production = current_production->prod[5];
                } else {
                  if(current_production->label == APOC_PATHS) {                   /*  */
                    current_production = current_production->prod[5];
                  } else {
                    if(current_production->label == LOC_REQUESTOR) {
                      current_production = current_production->prod[3];
                    } else {
                      if(current_production->label == CONTAINERS_REFERENCE) {
                        current_production = current_production->prod[2];
                      } else {
                        if(current_production->label == GROUPS_REFERENCE) {
                          current_production = current_production->prod[1];
                        } else {
                          if(current_production->label == ACCESS_RIGHTS_REFERENCE) {
                            current_production = current_production->prod[0];
                          } else {
                            found = FALSE;
                            tmp_err_code = EXIP_UNEXPECTED_ERROR;
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  if(found) {
    currentStringData = &(__m2m_object.application->subscriptionsReference);
    *currentStringData = EXIP_MALLOC(sizeof(String));
  }
  return tmp_err_code;
}
errorCode
generateNotificationChannelsReference()
{
  errorCode tmp_err_code = EXIP_OK;
  boolean found = TRUE;
  if(current_production->label == ANNOUNCE_TO) {
    current_production = current_production->prod[7];
  } else {
    if(current_production->label == LAST_MODIFIED_TIME) {
      current_production = current_production->prod[8];
    } else {
      if(current_production->label == CREATION_TIME) {
        current_production = current_production->prod[9];
      } else {
        if(current_production->label == SEARCH_STRINGS) {
          current_production = current_production->prod[11];
        } else {
          if(current_production->label == SEARCH_STRING) {
            current_production = current_production->prod[10];
          } else {
            if(current_production->label == ACCESS_RIGHT_ID) {
              current_production = current_production->prod[8];
            } else {
              if(current_production->label == EXPIRATION_TIME) {
                current_production = current_production->prod[7];
              } else {
                if(current_production->label == APOC) {
                  current_production = current_production->prod[6];
                } else {
                  if(current_production->label == APOC_PATHS) {                   /*  */
                    current_production = current_production->prod[6];
                  } else {
                    if(current_production->label == LOC_REQUESTOR) {
                      current_production = current_production->prod[4];
                    } else {
                      if(current_production->label == CONTAINERS_REFERENCE) {
                        current_production = current_production->prod[3];
                      } else {
                        if(current_production->label == GROUPS_REFERENCE) {
                          current_production = current_production->prod[2];
                        } else {
                          if(current_production->label == ACCESS_RIGHTS_REFERENCE) {
                            current_production = current_production->prod[1];
                          } else {
                            if(current_production->label == SUBSCRIPTIONS_REFERENCE) {
                              current_production = current_production->prod[0];
                            } else {
                              found = FALSE;
                              tmp_err_code = EXIP_UNEXPECTED_ERROR;
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  if(found) {
    currentStringData = &(__m2m_object.application->notificationChannelsReference);
    *currentStringData = EXIP_MALLOC(sizeof(String));
  }
  return tmp_err_code;
}
#endif /* DECODEM2MUTILS_H */

