/*
 * "Copyright (c) 2008, 2009 The Regents of the University  of California.
 * All rights reserved."
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without written agreement is
 * hereby granted, provided that the above copyright notice, the following
 * two paragraphs and the author appear in all copies of this software.
 *
 * IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY FOR
 * DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT
 * OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF THE UNIVERSITY OF
 * CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATION TO
 * PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS."
 *
 */

#ifndef D_MEM_H_
#define D_MEM_H_

#include <stdint.h>
#define D_MEM_HEAP_SIZE 8500
//8500
//4095
//5119
//6143 -> first to work with only application without header
//9215
//7167
//8191 -> first to work with only application with header
//9215
//10239
//11263
//12288
//13311 
//14335
//15349
//16383
// align on this number of byte boundarie#s
#define D_MEM_ALIGN 2
#define D_MEM_LEN 0x3fff
//0x0fff <-- 4095
//0x1fff <-- 8191
//
//0x3fff <-- 16383
#define D_MEM_INUSE   0x8000

extern uint8_t heap[D_MEM_HEAP_SIZE];
typedef uint16_t bndrt_t;

void d_malloc_init();
void* d_malloc(uint16_t sz);
void d_free(void *ptr);
uint16_t d_malloc_freespace();

void* d_realloc(void* ptr, uint16_t size);

uint16_t getMemUsage();

#endif // D_MEM_H_
