/*
 * Copyright (c) 2015, Andrea Azzara'
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.

 */

/**
 * \file
 *
 * \author
 *      Andrea Azzara'  <a.azzara@sssup.it>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/* contiki */
#include "contiki.h"
#include "dev/leds.h"
#include "exiDecode.h"
#include "m2mBuild.h"
#define DEBUG 1

#undef PRINTF
#undef PRINT6ADDR
#undef PRINTLLADDR
#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#define PRINTFLN(format, ...) printf(format "\n", ##__VA_ARGS__)
#else
#define PRINTF(...)
#define PRINTFLN(...)
#endif

#define EXI_BUFFER_SIZE 1500//550

PROCESS(m2m_test, "M2M test");
AUTOSTART_PROCESSES(&m2m_test);

static char outbuffer[EXI_BUFFER_SIZE];

PROCESS_THREAD(m2m_test, ev, data) {
  PROCESS_BEGIN();
  PRINTFLN("M2M Ttest");
  static errorCode tmp_err_code = EXIP_OK;
  contentInstanceBuild(outbuffer, tmp_err_code, (SingleString)"Test Content");
  
  //decodeMessage(outbuffer);

  _m2m_object *m2m;
  int i;

  char *xmloutput;
  xmloutput = NULL;
  init_xml_output();

  tmp_err_code = decodeExiBuffer(SCHEMA_OPT, outbuffer, &xmloutput, &m2m);
  
  if(xmloutput == NULL) {
    printf("\nnull output\n");
  } else {
    if(strlen(xmloutput) == 0) {
      printf("non null output but null length\n");
    }
  }  
  printf("Encoded\n");
  if((tmp_err_code == EXIP_OK) && (xmloutput != NULL) && (strlen(xmloutput) != 0)) {
      unsigned int j;
      unsigned int outlen = strlen(xmloutput);
      for (j=0; j< outlen; j++){
        //printf("%02x\n", xmloutput[j]);
        if (xmloutput[j] == '\n' || xmloutput[j] == '\r'){
          xmloutput[j] = ' ';
        }
    }        
    printf("\n\nXML %s\n", xmloutput); 
  }
  PROCESS_END();
}

