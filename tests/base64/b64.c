/*
 * Copyright (c) 2015, Andrea Azzara'
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.

 */

/**
 * \file
 *
 * \author
 *      Andrea Azzara'  <a.azzara@sssup.it>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "base64.h"
/* contiki */
#include "contiki.h"
#include "dev/leds.h"

#define DEBUG 1

#undef PRINTF
#undef PRINT6ADDR
#undef PRINTLLADDR
#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#define PRINTFLN(format, ...) printf(format "\n", ##__VA_ARGS__)
#else
#define PRINTF(...)
#define PRINTFLN(...)
#endif


PROCESS(b64, "Base64 test");
AUTOSTART_PROCESSES(&b64);

int getRandUint(unsigned int mod){
  return (unsigned int)(rand() % mod);
}

#define CONTENT_BUFFER_SIZE    1000
#define B64_SIZE (4 * (CONTENT_BUFFER_SIZE / 3) + (CONTENT_BUFFER_SIZE % 3 != 0 ? 4 : 0))
unsigned char content_instance[CONTENT_BUFFER_SIZE];
unsigned char base64[B64_SIZE];


const char * test_string = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nec mauris eget massa iaculis efficitur. Donec dignissim non dui et cursus. Integer rutrum nisi iaculis, consectetur augue eu, varius augue. Nam nibh mauris, egestas vitae ante sit amet, tincidunt viverra est. Nulla facilisi. Praesent gravida elit ac sem blandit fermentum. Duis molestie blandit dapibus. Quisque interdum sapien justo, sed ultrices lacus malesuada at.";

PROCESS_THREAD(b64, ev, data) {
  PROCESS_BEGIN();
  
  size_t output_length = B64_SIZE;  
  int ret = base64_encode(base64, &output_length, (const unsigned char *)test_string, strlen(test_string));
  PRINTFLN("ret val = %d", ret);
  if (ret != 0){
    PRINTF("Warning: base64 output len = 0\n");
  }
  printf("Base64 string: %u %s\n", strlen((const char *)base64), base64);
  PROCESS_END();
}

