/*
 * Copyright (c) 2015, Andrea Azzara'
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.

 */

/**
 * \file
 *
 * \author
 *      Andrea Azzara'  <a.azzara@sssup.it>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* contiki */
#include "contiki.h"
#include "contiki-net.h"
#include "er-coap.h"
#include "dev/leds.h"
#include "er-coap-engine.h"

#define DEBUG 1

#undef PRINTF
#undef PRINT6ADDR
#undef PRINTLLADDR
#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#define PRINTFLN(format, ...) printf(format "\n", ##__VA_ARGS__)
#define PRINT6ADDR(addr) PRINTF("[%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x]", ((uint8_t *)addr)[0], ((uint8_t *)addr)[1], ((uint8_t *)addr)[2], ((uint8_t *)addr)[3], ((uint8_t *)addr)[4], ((uint8_t *)addr)[5], ((uint8_t *)addr)[6], ((uint8_t *)addr)[7], ((uint8_t *)addr)[8], ((uint8_t *)addr)[9], ((uint8_t *)addr)[10], ((uint8_t *)addr)[11], ((uint8_t *)addr)[12], ((uint8_t *)addr)[13], ((uint8_t *)addr)[14], ((uint8_t *)addr)[15])
#define PRINTLLADDR(lladdr) PRINTF("[%02x:%02x:%02x:%02x:%02x:%02x]",(lladdr)->addr[0], (lladdr)->addr[1], (lladdr)->addr[2], (lladdr)->addr[3],(lladdr)->addr[4], (lladdr)->addr[5])
#else
#define PRINTF(...)
#define PRINT6ADDR(addr)
#define PRINTLLADDR(addr)
#define PRINTFLN(...)
#endif


PROCESS(coap_test_main, "CoAP Test main");
AUTOSTART_PROCESSES(&coap_test_main);


static void output_get_handler(void *request, void *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
static void output_event_handler();

EVENT_RESOURCE(output,
               "title=\"Algo output\";obs",
               output_get_handler,
               NULL,
               NULL,
               NULL,
               output_event_handler);

/* Output Resource */
static void
output_get_handler(void *request, void *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
	//PRINTF("Output get handler\n");
  REST.set_response_payload(response, buffer, snprintf((char *)buffer, preferred_size, "10 50 20")); //slot id, status, confidence
}

static void
output_event_handler()
{
  /* Notify the registered observers which will trigger the res_get_handler to create the response. */
  REST.notify_subscribers(&output);
}


PROCESS_THREAD(coap_test_main, ev, data) {
  PROCESS_BEGIN();
  static struct etimer et;  
  /* Initialize the REST engine. */
  rest_init_engine();    
  rest_activate_resource(&output, "output");
  etimer_reset(&et);
  etimer_set(&et, CLOCK_SECOND);  
  while(1) {
    PROCESS_YIELD();
    if (etimer_expired(&et)) {
      etimer_set(&et, TRES_TEST_PERIOD * CLOCK_SECOND);
      output.trigger();
      PRINTF("Triggering event\n");
    }
  }
  PROCESS_END();
}

