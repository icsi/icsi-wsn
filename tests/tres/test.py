from tres_pymite import *
i = getInputList()
print "list:", len(i)

idpos = 0
respos = 1
weightpos = 2

if len(i) == 1:
  slotId = i[0][idpos]
  res = i[0][respos]  # slotid, result, weight
  setOutput([res, slotId])
else:
  if i[0][idpos] == i[1][idpos]:  # same slot id
    slotId = i[1][idpos]
    r0 = i[0][respos]
    r1 = i[1][respos]
    w0 = i[0][weightpos]
    w1 = i[1][weightpos]
    res = (r0 * w0 + r1 * w1) / (w0 + w1)
    setOutput([res, slotId])
    print 'TRES OK', res
