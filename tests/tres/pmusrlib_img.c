/**
 * PyMite library image file.
 *
 * Automatically created from:
 * 	test.py
 *	img-list-terminator
 * by pmImgCreator.py on
 * Tue Jun  9 17:10:06 2015.
 * 
 * Byte count: 508
 * 
 * Selected memspace type: RAM
 * 
 * DO NOT EDIT THIS FILE.
 * ANY CHANGES WILL BE LOST.
 */

/* Place the image into RAM */
#ifdef __cplusplus
extern
#endif
unsigned char const
usrlib_img[] =
{


/* test.py */
    0x0A, 0xFB, 0x01, 0x00, 0x40, 0x04, 0x00, 0x01, 
    0x00, 0x04, 0x0F, 0x03, 0x0B, 0x00, 0x74, 0x72, 
    0x65, 0x73, 0x5F, 0x70, 0x79, 0x6D, 0x69, 0x74, 
    0x65, 0x03, 0x0C, 0x00, 0x67, 0x65, 0x74, 0x49, 
    0x6E, 0x70, 0x75, 0x74, 0x4C, 0x69, 0x73, 0x74, 
    0x03, 0x01, 0x00, 0x69, 0x03, 0x03, 0x00, 0x6C, 
    0x65, 0x6E, 0x03, 0x05, 0x00, 0x69, 0x64, 0x70, 
    0x6F, 0x73, 0x03, 0x06, 0x00, 0x72, 0x65, 0x73, 
    0x70, 0x6F, 0x73, 0x03, 0x09, 0x00, 0x77, 0x65, 
    0x69, 0x67, 0x68, 0x74, 0x70, 0x6F, 0x73, 0x03, 
    0x06, 0x00, 0x73, 0x6C, 0x6F, 0x74, 0x49, 0x64, 
    0x03, 0x03, 0x00, 0x72, 0x65, 0x73, 0x03, 0x09, 
    0x00, 0x73, 0x65, 0x74, 0x4F, 0x75, 0x74, 0x70, 
    0x75, 0x74, 0x03, 0x02, 0x00, 0x72, 0x30, 0x03, 
    0x02, 0x00, 0x72, 0x31, 0x03, 0x02, 0x00, 0x77, 
    0x30, 0x03, 0x02, 0x00, 0x77, 0x31, 0x03, 0x04, 
    0x00, 0x74, 0x65, 0x73, 0x74, 0x03, 0x24, 0x00, 
    0x0A, 0x01, 0x09, 0x01, 0x0F, 0x02, 0x06, 0x01, 
    0x06, 0x01, 0x06, 0x02, 0x13, 0x01, 0x0E, 0x01, 
    0x0E, 0x01, 0x14, 0x02, 0x1D, 0x01, 0x0E, 0x01, 
    0x0E, 0x01, 0x0E, 0x01, 0x0E, 0x01, 0x0E, 0x01, 
    0x1A, 0x01, 0x10, 0x01, 0x03, 0x08, 0x00, 0x74, 
    0x65, 0x73, 0x74, 0x2E, 0x70, 0x79, 0x00, 0x04, 
    0x08, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0x04, 0x01, 
    0x03, 0x01, 0x00, 0x2A, 0x03, 0x05, 0x00, 0x6C, 
    0x69, 0x73, 0x74, 0x3A, 0x01, 0x00, 0x00, 0x00, 
    0x00, 0x01, 0x01, 0x00, 0x00, 0x00, 0x01, 0x02, 
    0x00, 0x00, 0x00, 0x03, 0x07, 0x00, 0x54, 0x52, 
    0x45, 0x53, 0x20, 0x4F, 0x4B, 0x00, 0x64, 0x00, 
    0x00, 0x64, 0x01, 0x00, 0x6B, 0x00, 0x00, 0x54, 
    0x65, 0x01, 0x00, 0x83, 0x00, 0x00, 0x5A, 0x02, 
    0x00, 0x64, 0x02, 0x00, 0x47, 0x65, 0x03, 0x00, 
    0x65, 0x02, 0x00, 0x83, 0x01, 0x00, 0x47, 0x48, 
    0x64, 0x03, 0x00, 0x5A, 0x04, 0x00, 0x64, 0x04, 
    0x00, 0x5A, 0x05, 0x00, 0x64, 0x05, 0x00, 0x5A, 
    0x06, 0x00, 0x65, 0x03, 0x00, 0x65, 0x02, 0x00, 
    0x83, 0x01, 0x00, 0x64, 0x04, 0x00, 0x6A, 0x02, 
    0x00, 0x6F, 0x30, 0x00, 0x01, 0x65, 0x02, 0x00, 
    0x64, 0x03, 0x00, 0x19, 0x65, 0x04, 0x00, 0x19, 
    0x5A, 0x07, 0x00, 0x65, 0x02, 0x00, 0x64, 0x03, 
    0x00, 0x19, 0x65, 0x05, 0x00, 0x19, 0x5A, 0x08, 
    0x00, 0x65, 0x09, 0x00, 0x65, 0x08, 0x00, 0x65, 
    0x07, 0x00, 0x67, 0x02, 0x00, 0x83, 0x01, 0x00, 
    0x01, 0x6E, 0x9B, 0x00, 0x01, 0x65, 0x02, 0x00, 
    0x64, 0x03, 0x00, 0x19, 0x65, 0x04, 0x00, 0x19, 
    0x65, 0x02, 0x00, 0x64, 0x04, 0x00, 0x19, 0x65, 
    0x04, 0x00, 0x19, 0x6A, 0x02, 0x00, 0x6F, 0x7D, 
    0x00, 0x01, 0x65, 0x02, 0x00, 0x64, 0x04, 0x00, 
    0x19, 0x65, 0x04, 0x00, 0x19, 0x5A, 0x07, 0x00, 
    0x65, 0x02, 0x00, 0x64, 0x03, 0x00, 0x19, 0x65, 
    0x05, 0x00, 0x19, 0x5A, 0x0A, 0x00, 0x65, 0x02, 
    0x00, 0x64, 0x04, 0x00, 0x19, 0x65, 0x05, 0x00, 
    0x19, 0x5A, 0x0B, 0x00, 0x65, 0x02, 0x00, 0x64, 
    0x03, 0x00, 0x19, 0x65, 0x06, 0x00, 0x19, 0x5A, 
    0x0C, 0x00, 0x65, 0x02, 0x00, 0x64, 0x04, 0x00, 
    0x19, 0x65, 0x06, 0x00, 0x19, 0x5A, 0x0D, 0x00, 
    0x65, 0x0A, 0x00, 0x65, 0x0C, 0x00, 0x14, 0x65, 
    0x0B, 0x00, 0x65, 0x0D, 0x00, 0x14, 0x17, 0x65, 
    0x0C, 0x00, 0x65, 0x0D, 0x00, 0x17, 0x15, 0x5A, 
    0x08, 0x00, 0x65, 0x09, 0x00, 0x65, 0x08, 0x00, 
    0x65, 0x07, 0x00, 0x67, 0x02, 0x00, 0x83, 0x01, 
    0x00, 0x01, 0x64, 0x06, 0x00, 0x47, 0x65, 0x08, 
    0x00, 0x47, 0x48, 0x6E, 0x01, 0x00, 0x01, 0x64, 
    0x07, 0x00, 0x53, 

/* img-list-terminator */
    0xFF, 
};
